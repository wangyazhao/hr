package org.hr.service;

import java.util.List;
import java.util.Map;

import org.hr.pojo.Role;

public interface RoleService {

	/**
	 * 获取所有的角色
	 * @return
	 */
	public List<Role> getAll();
	
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer page,Integer rows,Role condition);
	
	/**
	 * 添加
	 */
	public Map<String,Object> addrole(Role role);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Role view(Integer id);
	
	/**
	 * 修改
	 * @param role
	 * @return
	 */
	public Map<String,Object> update(Role role);
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	public Map<String,Object> delById(Integer id); 
	
	
}
