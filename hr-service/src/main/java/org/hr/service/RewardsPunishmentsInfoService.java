package org.hr.service;

import java.util.Map;

import org.hr.pojo.RewardsPunishmentsInfo;

public interface RewardsPunishmentsInfoService {
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer page, Integer rows,String sort, String order,RewardsPunishmentsInfo condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param rewardsPunishmentsInfo
	 * @return
	 */
	public Map<String, Object> add(RewardsPunishmentsInfo rewardsPunishmentsInfo);
	
	/**
	 * 编辑
	 * @param rewardsPunishmentsInfo
	 * @return
	 */
	public Map<String, Object> edit(RewardsPunishmentsInfo rewardsPunishmentsInfo);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public RewardsPunishmentsInfo getById(Integer id);
}
