package org.hr.service;

import java.util.Map;

import org.hr.pojo.Departure;

public interface DepartureService {
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer page, Integer rows,String sort, String order,Departure condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param departure
	 * @return
	 */
	public Map<String, Object> add(Departure departure);
	
	/**
	 * 编辑
	 * @param departure
	 * @return
	 */
	public Map<String, Object> edit(Departure departure);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Departure getById(Integer id);
	
	
	public Integer getcontractinfoid(Integer id);
}
