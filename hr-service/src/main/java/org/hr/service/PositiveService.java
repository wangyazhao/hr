package org.hr.service;

import java.util.Map;

import org.hr.pojo.Positive;

public interface PositiveService {
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer byid, Integer page, Integer rows,String sort, String order,Positive condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param positive
	 * @return
	 */
	public Map<String, Object> add(Positive positive);
	
	/**
	 * 编辑
	 * @param positive
	 * @return
	 */
	public Map<String, Object> edit(Positive positive);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Positive getById(Integer id);
}
