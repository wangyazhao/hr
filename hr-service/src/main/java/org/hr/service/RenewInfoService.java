package org.hr.service;

import java.util.Map;

import org.hr.pojo.RenewInfo;

public interface RenewInfoService {
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer byid,Integer page, Integer rows,String sort, String order,RenewInfo condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param renewInfo
	 * @return
	 */
	public Map<String, Object> add(RenewInfo renewInfo);
	
	/**
	 * 编辑
	 * @param renewInfo
	 * @return
	 */
	public Map<String, Object> edit(RenewInfo renewInfo);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public RenewInfo getById(Integer id);
}
