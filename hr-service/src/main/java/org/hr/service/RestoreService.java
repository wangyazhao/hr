package org.hr.service;

import java.util.Map;

import org.hr.pojo.Restore;

public interface RestoreService {
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer byid,Integer page, Integer rows,String sort, String order,Restore condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param restore
	 * @return
	 */
	public Map<String, Object> add(Restore restore);
	
	/**
	 * 编辑
	 * @param restore
	 * @return
	 */
	public Map<String, Object> edit(Restore restore);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Restore getById(Integer id);
}
