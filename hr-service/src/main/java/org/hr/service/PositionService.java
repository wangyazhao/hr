package org.hr.service;

import java.util.List;
import java.util.Map;

import org.hr.pojo.Position;

public interface PositionService {

	/**
	 * 获取全部
	 */
	public List<Position> getAll();	
	
	/**
	 * 获取全部数据
	 */
	public Map<String, Object> listMap(Integer page, Integer rows, Position condition);
 
	/**
	 * 添加
	 */
	public Map<String,Object> addposition(Position position);
	
	/**
	 * 修改
	 */
	public Position view(Integer id);
	public Map<String,Object> update(Position position);
	 
	
	/**
	 * 删除
	 */
	public Map<String,Object> delById(Integer id);



 
	
}
