package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.PositionDao;
import org.hr.pojo.Position;
import org.hr.service.PositionService;
import org.springframework.stereotype.Service;

@Service("positionService")
public class PositionServiceImpl implements PositionService{
	
	@Resource
	private PositionDao positiondao;

	public void setPositiondao(PositionDao positiondao) {
		this.positiondao = positiondao;
	}

	@Override
	public List<Position> getAll() { 
		return positiondao.getAll();
	}

	@Override
	public Map<String, Object> addposition(Position position) {
		Map<String, Object> map = new HashMap<>();
		positiondao.add(position);  
		map.put("result", true);
		return map; 
	}

	@Override
	public Position view(Integer id) { 
		return positiondao.getById(id);
	}

	@Override
	public Map<String, Object> update(Position position) { 
		Map<String, Object> map = new HashMap<>(); 
		positiondao.update(position);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> delById(Integer id) {
		Map<String, Object> map = new HashMap<>(); 
		positiondao.deleteById(id);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> listMap(Integer page, Integer rows, Position condition) { 
		Map<String, Object> map = new HashMap<>(); 
		int start = (page - 1) * rows;
		List<Position> list = positiondao.getByname(start, rows, condition);
		int total = positiondao.getCountByCondition(condition); 
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	
	
	
	
	

}
