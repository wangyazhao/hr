package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.SectionDao;
import org.hr.pojo.Section;
import org.hr.service.SectionService;
import org.springframework.stereotype.Service;

@Service("sectionService")
public class SectionServiceImpl implements SectionService {

	@Resource
	private SectionDao sectionDao;
	
	
	public void setSectionDao(SectionDao sectionDao) {
		this.sectionDao = sectionDao;
	}



	@Override
	public List<Section> getAll() {
		return sectionDao.getAll();
	}



	@Override
	public Section getById(Integer id) {
		return sectionDao.getById(id);
	}


	@Override
	public List<Section> listData() {
		List<Section> list = sectionDao.getAll();
		Section section = new Section();
		section.setId(0);
		section.setText("顶级部门");
		list.add(0, section);
		return list;
	}


	@Override
	public Map<String, Object> add(Section section) {
		Map<String, Object> map = new HashMap<>();
		sectionDao.add(section);
		map.put("result", true);
		return map;
	}
/*
 		Map<String, Object> map = new HashMap<>();
		map.put("result", true);
		return map;
  */


	@Override
	public Map<String, Object> del(Integer id) {
		Map<String, Object> map = new HashMap<>();
		sectionDao.delSectionByParentId(id);
		sectionDao.deleteById(id);
		map.put("result", true);
		return map;
	}



	@Override
	public Map<String, Object> update(Section section) {
		Map<String, Object> map = new HashMap<>();
		sectionDao.update(section);
		map.put("result", true);
		return map;
	}

}
