package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.ContractInfoDao;
import org.hr.dao.RemoveInfoDao;
import org.hr.pojo.ContractInfo;
import org.hr.pojo.DataDictionary;
import org.hr.pojo.RemoveInfo;
import org.hr.service.RemoveInfoService;
import org.springframework.stereotype.Service;
@Service("removeInfoService")
public class RemoveInfoServiceImpl implements RemoveInfoService {

	@Resource
	private RemoveInfoDao removeInfoDao;

	@Resource
	private ContractInfoDao contractInfoDao;
	
	public void setContractInfoDao(ContractInfoDao contractInfoDao) {
		this.contractInfoDao = contractInfoDao;
	}
	public void setRemoveInfoDao(RemoveInfoDao removeInfoDao) {
		this.removeInfoDao = removeInfoDao;
	}
	@Override
	public Map<String, Object> listMap(Integer byid,Integer page, Integer rows, String sort, String order,
			RemoveInfo condition) {
		Map<String, Object> map = new HashMap<>();
		
		ContractInfo contractInfo = new ContractInfo();
		contractInfo.setId(byid);
		condition.setContractInfo(contractInfo);
		
		int start = (page - 1) * rows;
		List<RemoveInfo> list = removeInfoDao.getListByCondition(start, rows, condition, sort, order);
		int total = removeInfoDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		removeInfoDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(RemoveInfo removeInfo) {
		Map<String, Object> map = new HashMap<>();
		
		//更改合同的状态解除
		ContractInfo contractInfo = new ContractInfo();
		contractInfo.setId(removeInfo.getContractInfo().getId());
		DataDictionary contractState = new DataDictionary();
		contractState.setId(71);
		contractInfo.setContractstate(contractState);
		contractInfoDao.update(contractInfo);
		
		removeInfoDao.add(removeInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> edit(RemoveInfo removeInfo) {
		Map<String, Object> map = new HashMap<>();
		
		
		removeInfoDao.update(removeInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public RemoveInfo getById(Integer id) {
		return removeInfoDao.getById(id);
	}
}
