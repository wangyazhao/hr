package org.hr.service.impl;

import javax.annotation.Resource;

import org.hr.dao.EmployedInfoDao;
import org.hr.service.EmployedInfoService;
import org.springframework.stereotype.Service;

@Service("employedInfoService")
public class EmployedInfoServiceImpl implements EmployedInfoService{

	@Resource
	private EmployedInfoDao employedInfoDao;

	public void setEmployedInfoDao(EmployedInfoDao employedInfoDao) {
		this.employedInfoDao = employedInfoDao;
	}
	
	
	
}
