package org.hr.service.impl;

import javax.annotation.Resource;

import org.hr.dao.InterviewDao;
import org.hr.service.InterviewService;
import org.springframework.stereotype.Service;
@Service("interviewService")
public class InterviewServiceImpl implements InterviewService{

	@Resource
	private InterviewDao interviewDao;

	public void setInterviewDao(InterviewDao interviewDao) {
		this.interviewDao = interviewDao;
	}
	
	
}
