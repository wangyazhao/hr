package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.ArchivesDao;
import org.hr.dao.ContractInfoDao;
import org.hr.dao.DepartureDao;
import org.hr.dao.RemoveInfoDao;
import org.hr.pojo.Archives;
import org.hr.pojo.ContractInfo;
import org.hr.pojo.DataDictionary;
import org.hr.pojo.Departure;
import org.hr.pojo.RemoveInfo;
import org.hr.service.DepartureService;
import org.springframework.stereotype.Service;

@Service("departureService")
public class DepartureImpl implements DepartureService {

	@Resource
	private ArchivesDao archivesDao;
	
	public void setArchivesDao(ArchivesDao archivesDao) {
		this.archivesDao = archivesDao;
	}
	@Resource
	private DepartureDao departureDao;

	public void setDepartureDao(DepartureDao departureDao) {
		this.departureDao = departureDao;
	}
	
	@Resource
	private ContractInfoDao contractInfoDao;
	
	
	public void setContractInfoDao(ContractInfoDao contractInfoDao) {
		this.contractInfoDao = contractInfoDao;
	}
	@Resource
	private  RemoveInfoDao removeInfoDao;
	
	
	public void setRemoveInfoDao(RemoveInfoDao removeInfoDao) {
		this.removeInfoDao = removeInfoDao;
	}

	@Override
	public Map<String, Object> listMap(Integer page, Integer rows, String sort, String order,
			Departure condition) {
		Map<String, Object> map = new HashMap<>();
		int start = (page - 1) * rows;
		List<Departure> list = departureDao.getListByCondition(start, rows, condition, sort, order);
		int total = departureDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		departureDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(Departure departure) {
		Map<String, Object> map = new HashMap<>();
		
		up(departure);
		
		Archives archives = new Archives();
		archives.setId(departure.getArchives().getId());
		DataDictionary status = new DataDictionary();
		status.setId(107);
		archives.setStatus(status);
		archivesDao.update(archives);
		departureDao.add(departure);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> edit(Departure departure) {
		Map<String, Object> map = new HashMap<>();
		up(departure);
		
		departureDao.update(departure);
		map.put("result", true);
		return map;
	}

	private void up(Departure departure) {
		if (departure.getWhether().getId() == 1) {
			RemoveInfo removeInfo = new RemoveInfo();
			ContractInfo contractInfo = new ContractInfo();
			contractInfo.setId(departureDao.getcontractinfoid(departure.getArchives().getId()));
			removeInfo.setContractInfo(contractInfo);
			removeInfo.setTime(departure.getTime());
			removeInfo.setText("离职解除合同");
			DataDictionary type = new DataDictionary();
			type.setId(departure.getJctype());
			removeInfo.setType(type);
			
			removeInfoDao.add(removeInfo);
			
			//更改合同的状态解除
			DataDictionary contractState = new DataDictionary();
			contractState.setId(71);
			contractInfo.setContractstate(contractState);
			contractInfoDao.update(contractInfo);
			
		}
	}

	@Override
	public Departure getById(Integer id) {
		return departureDao.getById(id);
	}

	@Override
	public Integer getcontractinfoid(Integer id) {
		return departureDao.getcontractinfoid(id);
	}

}
