package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.ArchivesDao;
import org.hr.pojo.Archives;
import org.hr.pojo.Hr;
import org.hr.service.ArchivesService;
import org.springframework.stereotype.Service;

@Service("archivesService")
public class ArchivesServiceImpl implements ArchivesService {

	@Resource
	private ArchivesDao archivesDao;
	
	public void setArchivesDao(ArchivesDao archivesDao) {
		this.archivesDao = archivesDao;
	}
	


	@Override
	public List<Archives> getAll() {
		return archivesDao.getAll();
	}

	@Override
	public Archives getByID(Integer id) {
		return archivesDao.getById(id);
	}
	@Override
	public Map<String, Object> listMap(Integer page, Integer rows, String sort, String order, Archives condition) {
		Map<String, Object> map = new HashMap<>();
		int start = (page - 1) * rows;
		List<Archives> list = archivesDao.getListByCondition(start, rows, condition, sort, order);
		int total = archivesDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		
		archivesDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(Archives archives) {
		Map<String, Object> map = new HashMap<>();
		archivesDao.add(archives);;
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> edit(Archives archives) {
		Map<String, Object> map = new HashMap<>();
		archivesDao.update(archives);;
		map.put("result", true);
		return map;
	}



	@Override
	public List<Hr> groupBysex(Integer sid,String type) {
		return archivesDao.groupBysex(sid,type);
	}



	@Override
	public List<Hr> groupByposition(Integer id) {
		return archivesDao.groupByposition(id);
	}


}
