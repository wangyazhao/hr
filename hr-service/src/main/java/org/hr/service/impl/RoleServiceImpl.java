package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.RoleDao;
import org.hr.pojo.Role;
import org.hr.service.RoleService;
import org.springframework.stereotype.Service;
@Service("roleService")
public class RoleServiceImpl implements RoleService {

	@Resource
	 private RoleDao roledao;
 
	public void setRoledao(RoleDao roledao) {
		this.roledao = roledao;
	}

	@Override
	public List<Role> getAll() {
		return roledao.getAll();
	}

	@Override
	public Map<String,Object> listMap(Integer page, Integer rows, Role condition) {
		Map<String, Object> map = new HashMap<>(); 
		int start = (page - 1) * rows; 
		List<Role> list = roledao.getListByName(start, condition,rows); 
		int total = roledao.getCountByCondition(condition);  
		map.put("rows",list);
		map.put("total",total); 
		return map; 
	}

	@Override
	public Map<String, Object> addrole(Role role) {
		Map<String, Object> map = new HashMap<>();
		roledao.add(role); 
		
		map.put("result", true);
		return map;
	}

	@Override
	public Role view(Integer id) { 
		return roledao.getById(id);
	}

	@Override
	public Map<String, Object> update(Role role) {
		Map<String, Object> map = new HashMap<>(); 
		roledao.update(role);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> delById(Integer id) {
		Map<String, Object> map = new HashMap<>(); 
		roledao.deleteById(id);
		map.put("result", true);
		return map;
	}

}


