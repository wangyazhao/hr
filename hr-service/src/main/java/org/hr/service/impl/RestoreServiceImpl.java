package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.ContractInfoDao;
import org.hr.dao.RestoreDao;
import org.hr.pojo.ContractInfo;
import org.hr.pojo.DataDictionary;
import org.hr.pojo.Restore;
import org.hr.service.RestoreService;
import org.springframework.stereotype.Service;

@Service("restoreService")
public class RestoreServiceImpl implements RestoreService{
	@Resource
	private RestoreDao restoreDao;

	public void setRestoreDao(RestoreDao restoreDao) {
		this.restoreDao = restoreDao;
	}
	@Resource
	private ContractInfoDao contractInfoDao;
	
	public void setContractInfoDao(ContractInfoDao contractInfoDao) {
		this.contractInfoDao = contractInfoDao;
	}
	@Override
	public Map<String, Object> listMap(Integer byid,Integer page, Integer rows, String sort, String order,
			Restore condition) {
		Map<String, Object> map = new HashMap<>();
		
		ContractInfo contractInfo = new ContractInfo();
		contractInfo.setId(byid);
		condition.setContractInfo(contractInfo);
		
		int start = (page - 1) * rows;
		List<Restore> list = restoreDao.getListByCondition(start, rows, condition, sort, order);
		int total = restoreDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		restoreDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(Restore restore) {
		Map<String, Object> map = new HashMap<>();
		
		//更改合同的状态解除
		ContractInfo contractInfo = new ContractInfo();
		contractInfo.setId(restore.getContractInfo().getId());
		DataDictionary contractState = new DataDictionary();
		contractState.setId(69);
		contractInfo.setContractstate(contractState);
		contractInfoDao.update(contractInfo);
		
		
		restoreDao.add(restore);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> edit(Restore restore) {
		Map<String, Object> map = new HashMap<>();
		restoreDao.update(restore);
		map.put("result", true);
		return map;
	}

	@Override
	public Restore getById(Integer id) {
		return restoreDao.getById(id);
	}
}
