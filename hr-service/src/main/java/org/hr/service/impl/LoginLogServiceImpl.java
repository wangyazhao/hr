package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.LoginLogDao;
import org.hr.pojo.LoginLog;
import org.hr.service.LoginLogService;
import org.springframework.stereotype.Service;
@Service("loginService")
public class LoginLogServiceImpl implements LoginLogService{

	@Resource
	private LoginLogDao logindao;

	public void setLogindao(LoginLogDao logindao) {
		this.logindao = logindao;
	}
	@Override
	public Map<String, Object> listMap(Integer page, Integer rows, String sort, String order,
			LoginLog condition) {
		Map<String, Object> map = new HashMap<>();
		int start = (page - 1) * rows;
		List<LoginLog> list = logindao.getListByCondition(start, rows, condition, sort, order);
		int total = logindao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		logindao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(LoginLog login) {
		Map<String, Object> map = new HashMap<>();
		logindao.add(login);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> edit(LoginLog login) {
		Map<String, Object> map = new HashMap<>();
		logindao.update(login);
		map.put("result", true);
		return map;
	}

	@Override
	public LoginLog getById(Integer id) {
		return logindao.getById(id);
	}
}
