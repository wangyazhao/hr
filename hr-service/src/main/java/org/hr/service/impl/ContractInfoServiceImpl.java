package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.ContractInfoDao;
import org.hr.dao.RenewInfoDao;
import org.hr.pojo.ContractInfo;
import org.hr.pojo.RenewInfo;
import org.hr.service.ContractInfoService;
import org.springframework.stereotype.Service;

@Service("contractInfoService")
public class ContractInfoServiceImpl implements ContractInfoService{

	@Resource
	private ContractInfoDao contractInfoDao;

	public void setContractInfoDao(ContractInfoDao contractInfoDao) {
		this.contractInfoDao = contractInfoDao;
	}
	
	@Resource
	private RenewInfoDao renewInfoDao;
	
	public void setRenewInfoDao(RenewInfoDao renewInfoDao) {
		this.renewInfoDao = renewInfoDao;
	}

	@Override
	public Map<String, Object> listMap(Integer page, Integer rows, String sort, String order,
			ContractInfo condition) {
		Map<String, Object> map = new HashMap<>();
		int start = (page - 1) * rows;
		List<ContractInfo> list = contractInfoDao.getListByCondition(start, rows, condition, sort, order);
		int total = contractInfoDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		contractInfoDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(ContractInfo contractInfo) {
		Map<String, Object> map = new HashMap<>();
		contractInfoDao.add(contractInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> edit(ContractInfo contractInfo) {
		Map<String, Object> map = new HashMap<>();
		contractInfoDao.update(contractInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public ContractInfo getById(Integer id) {
		ContractInfo contractInfo = contractInfoDao.getById(id);
		RenewInfo renewInfo = renewInfoDao.getMaxByContractinfoid(id);
		if (renewInfo != null) {
			contractInfo.setPositiveoperant(renewInfo.getOperanttime());
			contractInfo.setPositiveexpire(renewInfo.getExpiretime());
		}
		return contractInfo;
	}

	@Override
	public ContractInfo getbyarchivesId(Integer id) {
		return contractInfoDao.getbyarchivesId(id);
	}
	
}
