package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.RewardsPunishmentsInfoDao;
import org.hr.pojo.RewardsPunishmentsInfo;
import org.hr.service.RewardsPunishmentsInfoService;
import org.springframework.stereotype.Service;
@Service("rewardsPunishmentsInfoService")
public class RewardsPunishmentsInfoServiceImpl implements RewardsPunishmentsInfoService {

	@Resource
	private RewardsPunishmentsInfoDao rewardsPunishmentsInfoDao;

	public void setRewardsPunishmentsInfoDao(RewardsPunishmentsInfoDao rewardsPunishmentsInfoDao) {
		this.rewardsPunishmentsInfoDao = rewardsPunishmentsInfoDao;
	}

	@Override
	public Map<String, Object> listMap(Integer page, Integer rows, String sort, String order,
			RewardsPunishmentsInfo condition) {
		Map<String, Object> map = new HashMap<>();
		int start = (page - 1) * rows;
		List<RewardsPunishmentsInfo> list = rewardsPunishmentsInfoDao.getListByCondition(start, rows, condition, sort, order);
		int total = rewardsPunishmentsInfoDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		rewardsPunishmentsInfoDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(RewardsPunishmentsInfo rewardsPunishmentsInfo) {
		Map<String, Object> map = new HashMap<>();
		rewardsPunishmentsInfoDao.add(rewardsPunishmentsInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> edit(RewardsPunishmentsInfo rewardsPunishmentsInfo) {
		Map<String, Object> map = new HashMap<>();
		rewardsPunishmentsInfoDao.update(rewardsPunishmentsInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public RewardsPunishmentsInfo getById(Integer id) {
		return rewardsPunishmentsInfoDao.getById(id);
	}
	
	
}
