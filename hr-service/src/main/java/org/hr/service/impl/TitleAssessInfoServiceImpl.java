package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.TitleAssessInfoDao;
import org.hr.pojo.TitleAssessInfo;
import org.hr.service.TitleAssessInfoService;
import org.springframework.stereotype.Service;
@Service("titleAssessInfoService") 
public class TitleAssessInfoServiceImpl implements TitleAssessInfoService {

	@Resource
	private TitleAssessInfoDao titleAssessInfoDao;

	public void setTitleAssessInfoDao(TitleAssessInfoDao titleAssessInfoDao) {
		this.titleAssessInfoDao = titleAssessInfoDao;
	}

	@Override
	public Map<String, Object> listMap(Integer page, Integer rows, String sort, String order,
			TitleAssessInfo condition) {
		Map<String, Object> map = new HashMap<>();
		int start = (page - 1) * rows;
		List<TitleAssessInfo> list = titleAssessInfoDao.getListByCondition(start, rows, condition, sort, order);
		int total = titleAssessInfoDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		titleAssessInfoDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(TitleAssessInfo titleAssessInfo) {
		Map<String, Object> map = new HashMap<>();
		titleAssessInfoDao.add(titleAssessInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> edit(TitleAssessInfo titleAssessInfo) {
		Map<String, Object> map = new HashMap<>();
		titleAssessInfoDao.update(titleAssessInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public TitleAssessInfo getById(Integer id) {
		return titleAssessInfoDao.getById(id);
	}
}
