package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.ArchivesDao;
import org.hr.dao.MobilizeDao;
import org.hr.pojo.Archives;
import org.hr.pojo.DataDictionary;
import org.hr.pojo.Mobilize;
import org.hr.pojo.Position;
import org.hr.pojo.Section;
import org.hr.service.MobilizeService;
import org.springframework.stereotype.Service;

@Service("mobilizeService")
public class MobilizeServiceImpl implements MobilizeService {

	@Resource
	private ArchivesDao archivesDao;
	
	public void setArchivesDao(ArchivesDao archivesDao) {
		this.archivesDao = archivesDao;
	}
	
	@Resource
	private MobilizeDao mobilizeDao;

	public void setMobilizeDao(MobilizeDao mobilizeDao) {
		this.mobilizeDao = mobilizeDao;
	}
	@Override
	public Map<String, Object> listMap(Integer page, Integer rows, String sort, String order,
			Mobilize condition) {
		Map<String, Object> map = new HashMap<>();
		int start = (page - 1) * rows;
		List<Mobilize> list = mobilizeDao.getListByCondition(start, rows, condition, sort, order);
		int total = mobilizeDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		mobilizeDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(Mobilize mobilize) {
		Map<String, Object> map = new HashMap<>();
		updateArchives(mobilize);
		mobilizeDao.add(mobilize);
		map.put("result", true);
		return map;
	}

	
	@Override
	public Map<String, Object> edit(Mobilize mobilize) {
		Map<String, Object> map = new HashMap<>();
		updateArchives(mobilize);
		mobilizeDao.update(mobilize);
		map.put("result", true);
		return map;
	}


	@Override
	public Mobilize getById(Integer id) {
		return mobilizeDao.getById(id);
	}
	
	
	private void updateArchives(Mobilize mobilize) {
		Archives oldarchives = archivesDao.getById(mobilize.getArchives().getId());
		//更新部门
		Archives archives = new Archives();
		archives.setId(oldarchives.getId());
		Section section = new Section();
		section.setId(mobilize.getSectionamenew());
		archives.setSection(section);
		
		//更新学历
		DataDictionary education = new DataDictionary();
		education.setId(mobilize.getEducationnew());
		archives.setEducation(education);
		
		DataDictionary jobTitle = new DataDictionary();//职称
		jobTitle.setId(mobilize.getZhichengnew());
		archives.setJobtitle(jobTitle);
		
		DataDictionary employeeswork = new DataDictionary();//工种
		employeeswork.setId(mobilize.getEmployeesworknew());
		archives.setEmployeeswork(employeeswork);
		
		DataDictionary jobs = new DataDictionary();//岗位
		jobs.setId(mobilize.getJobsnew());
		archives.setJobs(jobs);
		
		Position position = new Position();//职务
		position.setId(mobilize.getPositionnew());
		archives.setPosition(position);
		
		archives.setBasicsalary(mobilize.getQiannew());//工资
		
		archivesDao.update(archives);
	}


}
