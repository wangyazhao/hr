package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.AuditDao;
import org.hr.dao.CultivateDao;
import org.hr.pojo.Audit;
import org.hr.pojo.Cultivate;
import org.hr.pojo.DataDictionary;
import org.hr.service.CultivateService;
import org.springframework.stereotype.Service;


@Service("cultivateService")
public class CultivateServiceImpl implements CultivateService{

	@Resource
	private CultivateDao cultivateDao;

	public void setCultivateDao(CultivateDao cultivateDao) {
		this.cultivateDao = cultivateDao;
	}
	@Resource
	private AuditDao auditDao;
	
	public void setAuditDao(AuditDao auditDao) {
		this.auditDao = auditDao;
	}

	@Override
	public Map<String, Object> listMap(Integer page, Integer rows, String sort, String order,
			Cultivate condition) {
		Map<String, Object> map = new HashMap<>();
		int start = (page - 1) * rows;
		List<Cultivate> list = cultivateDao.getListByCondition(start, rows, condition, sort, order);
		int total = cultivateDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		cultivateDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(Cultivate cultivate,Integer [] aid) {
		
		Map<String, Object> map = new HashMap<>();
		DataDictionary statu = new DataDictionary();
		statu.setId(143);
		cultivate.setStatu(statu);
		cultivateDao.add(cultivate);
		cultivateDao.addCultivateArchives(cultivate.getId(), aid);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> edit(Cultivate cultivate,Integer [] aid) {
		Map<String, Object> map = new HashMap<>();
		cultivateDao.delCultivateArchives(cultivate.getId());
		cultivateDao.update(cultivate);
		cultivateDao.addCultivateArchives(cultivate.getId(), aid);
		map.put("result", true);
		return map;
	}

	@Override
	public Cultivate getById(Integer id) {
		return cultivateDao.getById(id);
	}


	@Override
	public List<Cultivate> getArchivesname(Integer id) {
		return cultivateDao.getbuarchivesname(id);
	}

	@Override
	public Map<String, Object> addAudit(Audit audit) {
		Map<String, Object> map = new HashMap<>();
		Cultivate cultivate = new Cultivate();
		DataDictionary statu = new DataDictionary();
		if (audit.getWhether().getId() == 1) {
			statu.setId(145);
		}
		if (audit.getWhether().getId() == 2) {
			statu.setId(144);
		}
		cultivate.setStatu(statu);
		cultivate.setId(audit.getCultivate().getId());
		cultivateDao.update(cultivate);
		auditDao.add(audit);
		map.put("result", true);
		return map;
	}
}
