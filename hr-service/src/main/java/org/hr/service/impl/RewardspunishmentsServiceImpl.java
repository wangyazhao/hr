package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.RewardspunishmentsDao;
import org.hr.pojo.Rewardspunishments;
import org.hr.service.RewardspunishmentsService;
import org.springframework.stereotype.Service;

@Service("rewardspunishmentsService")
public class RewardspunishmentsServiceImpl implements RewardspunishmentsService{
	@Resource
	private RewardspunishmentsDao rewardspunishmentsDao;
	 
	public void setRewardspunishmentsDao(RewardspunishmentsDao rewardspunishmentsDao) {
		this.rewardspunishmentsDao = rewardspunishmentsDao;
	}
	@Override
	public List<Rewardspunishments> list() {
		return rewardspunishmentsDao.getAll();
	}
 
	@Override
	public Rewardspunishments view(Integer id) {
		return rewardspunishmentsDao.getById(id);
	}
	@Override
	public Map<String, Object> update(Rewardspunishments rewardspunishments) {
		Map<String, Object> map = new HashMap<>();
		rewardspunishmentsDao.update(rewardspunishments);;
		map.put("result", true);
		return map;
	}
	@Override
	public Map<String, Object> delByIds(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		rewardspunishmentsDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}
	@Override
	public Map<String, Object> add(Rewardspunishments rewardspunishments) {
		Map<String, Object> map = new HashMap<>();
		rewardspunishmentsDao.add(rewardspunishments); 
		map.put("result", true);
		return map;
	}
 
 

}
