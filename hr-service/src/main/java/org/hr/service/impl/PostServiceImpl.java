package org.hr.service.impl;

import javax.annotation.Resource;

import org.hr.dao.PostDao;
import org.hr.service.PostService;
import org.springframework.stereotype.Service;

@Service("postService")
public class PostServiceImpl implements PostService {

	@Resource
	private PostDao postDao;

	public void setPostDao(PostDao postDao) {
		this.postDao = postDao;
	}
	
}
