package org.hr.service.impl;

 import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.CertificateInfoDao;
import org.hr.pojo.CertificateInfo;
import org.hr.service.CertificateInfoService;
import org.springframework.stereotype.Service;

 @Service("certificateInfoService")
public class CertificateInfoServiceImpl implements CertificateInfoService{

	@Resource
	private CertificateInfoDao certificateInfoDao;

	public void setCertificateInfoDao(CertificateInfoDao certificateInfoDao) {
		this.certificateInfoDao = certificateInfoDao;
	}
	@Override
	public Map<String, Object> listMap(Integer page, Integer rows, String sort, String order,
			CertificateInfo condition) {
		Map<String, Object> map = new HashMap<>();
		int start = (page - 1) * rows;
		List<CertificateInfo> list = certificateInfoDao.getListByCondition(start, rows, condition, sort, order);
		int total = certificateInfoDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		certificateInfoDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(CertificateInfo certificateInfo) {
		Map<String, Object> map = new HashMap<>();
		certificateInfoDao.add(certificateInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> edit(CertificateInfo certificateInfo) {
		Map<String, Object> map = new HashMap<>();
		certificateInfoDao.update(certificateInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public CertificateInfo getById(Integer id) {
		return certificateInfoDao.getById(id);
	}
}
