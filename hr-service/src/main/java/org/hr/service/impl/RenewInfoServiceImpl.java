package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.RenewInfoDao;
import org.hr.pojo.ContractInfo;
import org.hr.pojo.RenewInfo;
import org.hr.service.RenewInfoService;
import org.springframework.stereotype.Service;

@Service("renewInfoService")
public class RenewInfoServiceImpl implements RenewInfoService{

	@Resource
	private RenewInfoDao renewInfoDao;

	public void setRenewInfoDao(RenewInfoDao renewInfoDao) {
		this.renewInfoDao = renewInfoDao;
	}
	@Override
	public Map<String, Object> listMap(Integer byid,Integer page, Integer rows, String sort, String order,
			RenewInfo condition) {
		Map<String, Object> map = new HashMap<>();
		
		ContractInfo contractInfo = new ContractInfo();
		contractInfo.setId(byid);
		condition.setContractInfo(contractInfo);
		
		int start = (page - 1) * rows;
		List<RenewInfo> list = renewInfoDao.getListByCondition(start, rows, condition, sort, order);
		int total = renewInfoDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		renewInfoDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(RenewInfo renewInfo) {
		Map<String, Object> map = new HashMap<>();
		renewInfoDao.add(renewInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> edit(RenewInfo renewInfo) {
		Map<String, Object> map = new HashMap<>();
		renewInfoDao.update(renewInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public RenewInfo getById(Integer id) {
		return renewInfoDao.getById(id);
	}
}
