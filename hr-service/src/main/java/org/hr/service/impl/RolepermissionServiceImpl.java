package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.RolepermissionDao;
import org.hr.pojo.Rolepermission;
import org.hr.service.RolepermissionService;
import org.springframework.stereotype.Service;
 
@Service("rolepermissionService")
public class RolepermissionServiceImpl implements RolepermissionService {

	@Resource
	private RolepermissionDao rolepermissionDao;

	public void setRolepermissionDao(RolepermissionDao rolepermissionDao) {
		this.rolepermissionDao = rolepermissionDao;
	}

	@Override
	public List<Rolepermission> list(){
		return rolepermissionDao.getAll();
	}
	@Override
	public List<Rolepermission> lists(){
		List<Rolepermission> list  = rolepermissionDao.getAll();
		Rolepermission rolepermission = new Rolepermission();
		rolepermission.setId(0);
		rolepermission.setText("顶级权限");
		list.add(0, rolepermission);
		return list;
	}

	@Override
	public Map<String, Object> addrole(Rolepermission rolepermission) {
		Map<String, Object> map = new HashMap<>(); 
		rolepermissionDao.add(rolepermission);  
		map.put("result", true);
		return map;
	}

	@Override
	public Rolepermission view(Integer id) {
		return rolepermissionDao.getById(id);
	}

	@Override
	public Map<String, Object> update(Rolepermission rolepermission) {
		Map<String, Object> map = new HashMap<>(); 
		rolepermissionDao.update(rolepermission);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> delByIds(Integer[] ids) {
		Map<String, Object> map = new HashMap<>(); 
 
		map.put("result", true);
		return map;
	}

	@Override
	public List<Rolepermission> type() {
		return rolepermissionDao.getPermissionType();
	}

	@Override
	public List<Rolepermission> level() {
 		return rolepermissionDao.getPermissionLevel();
	}

	@Override
	public List<Integer> getPermissionIdsByRoleId(Integer roleId) {
		return rolepermissionDao.getPermissionIdsByRoleId(roleId);
	}

	@Override
	public void deletePermissionsByRoleId(Integer roleId) {
		rolepermissionDao.deletePermissionsByRoleId(roleId);
	}

	@Override
	public List<Rolepermission> getPermissionsByUserId(Integer userId, String type) {
		return rolepermissionDao.getPermissionsByUserId(userId, type);
	}

	@Override
	public List<String> getPermissionCodeByUserId(Integer userId) {
		return rolepermissionDao.getPermissionCodeByUserId(userId);
	}

	@Override
	public List<String> getMenuCodeByUserId(Integer userId) {
		return rolepermissionDao.getMenuCodeByUserId(userId);
	}

	@Override
	public List<Rolepermission> getPermissionByUserId(Integer userId) {
		return rolepermissionDao.getPermissionByUserId(userId);
	}

 
	
	
	
}
