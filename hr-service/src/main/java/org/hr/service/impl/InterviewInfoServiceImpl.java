package org.hr.service.impl;

import javax.annotation.Resource;

import org.hr.dao.InterviewInfoDao;
import org.hr.service.InterviewInfoService;
import org.springframework.stereotype.Service;
@Service("interviewInfoService")
public class InterviewInfoServiceImpl implements InterviewInfoService{

	@Resource
	private InterviewInfoDao interviewInfoDao;

	public void setInterviewInfoDao(InterviewInfoDao interviewInfoDao) {
		this.interviewInfoDao = interviewInfoDao;
	}
}
