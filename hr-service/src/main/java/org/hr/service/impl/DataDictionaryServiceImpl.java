package org.hr.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hr.dao.DataDictionaryDao;
import org.hr.pojo.DataDictionary;
import org.hr.service.DataDictionaryService;
import org.springframework.stereotype.Service;

@Service("dateDictionaryService")
public class DataDictionaryServiceImpl implements DataDictionaryService {

	@Resource
	private DataDictionaryDao dateDictionaryDao;

	public void setDateDictionaryDao(DataDictionaryDao dateDictionaryDao) {
		this.dateDictionaryDao = dateDictionaryDao;
	}

	@Override
	public List<DataDictionary> getTypeNameByType(String typeCode) {
		return dateDictionaryDao.getTypeNameByType(typeCode);
	}

	@Override
	public List<DataDictionary> getByTitleAssessInfoName() {
		return dateDictionaryDao.getByTitleAssessinfoName();
	}

  
	
}
