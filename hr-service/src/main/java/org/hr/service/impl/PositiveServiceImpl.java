package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.ContractInfoDao;
import org.hr.dao.PositiveDao;
import org.hr.pojo.ContractInfo;
import org.hr.pojo.DataDictionary;
import org.hr.pojo.Positive;
import org.hr.service.PositiveService;
import org.springframework.stereotype.Service;

@Service("positiveService")
public class PositiveServiceImpl implements PositiveService{
	@Resource
	private PositiveDao positiveDao;
	
	@Resource
	private ContractInfoDao contractInfoDao;

	public void setContractInfoDao(ContractInfoDao contractInfoDao) {
		this.contractInfoDao = contractInfoDao;
	}
	public void setPositiveDao(PositiveDao positiveDao) {
		this.positiveDao = positiveDao;
	}
	@Override
	public Map<String, Object> listMap(Integer byid,Integer page, Integer rows, String sort, String order,
			Positive condition) {
		Map<String, Object> map = new HashMap<>();
		
		ContractInfo contractInfo = new ContractInfo();
		contractInfo.setId(byid);
		condition.setContractInfo(contractInfo);
		
		int start = (page - 1) * rows;
		List<Positive> list = positiveDao.getListByCondition(start, rows, condition, sort, order);
		int total = positiveDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		positiveDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(Positive positive) {
		Map<String, Object> map = new HashMap<>();
		
		ContractInfo contractInfo = new ContractInfo();
		contractInfo.setId(positive.getContractInfo().getId());
		if (positive.getType().getId() == 132) {
			contractInfo.setTrialexpire(positive.getTime());
		}
		
		contractInfo.setPositiveoperant(positive.getTime());
		//修改转正状态
		DataDictionary whether = new DataDictionary();
		whether.setId(1);
		contractInfo.setPositive(whether);
		
		
		contractInfoDao.update(contractInfo);
		
		positiveDao.add(positive);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> edit(Positive positive){
		Map<String, Object> map = new HashMap<>();
		
		ContractInfo contractInfo = new ContractInfo();
		contractInfo.setId(positive.getContractInfo().getId());
		if (positive.getType().getId() == 132) {
			contractInfo.setTrialexpire(positive.getTime());
		}
		contractInfo.setPositiveoperant(positive.getTime());
		contractInfoDao.update(contractInfo);
		
		positiveDao.update(positive);
		map.put("result", true);
		return map;
	}

	@Override
	public Positive getById(Integer id) {
		return positiveDao.getById(id);
	}
	
}
