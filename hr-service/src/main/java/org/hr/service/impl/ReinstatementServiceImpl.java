package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.ArchivesDao;
import org.hr.dao.ContractInfoDao;
import org.hr.dao.ReinstatementDao;
import org.hr.dao.RestoreDao;
import org.hr.pojo.Archives;
import org.hr.pojo.ContractInfo;
import org.hr.pojo.DataDictionary;
import org.hr.pojo.Reinstatement;
import org.hr.pojo.Restore;
import org.hr.service.ReinstatementService;
import org.springframework.stereotype.Service;


@Service("reinstatementService")
public class ReinstatementServiceImpl implements ReinstatementService{


	@Resource
	private ArchivesDao archivesDao;
	
	public void setArchivesDao(ArchivesDao archivesDao) {
		this.archivesDao = archivesDao;
	}
	@Resource
	private ContractInfoDao contractInfoDao;
	
	@Resource
	private RestoreDao restoreDao;
	
	
	public void setRestoreDao(RestoreDao restoreDao) {
		this.restoreDao = restoreDao;
	}
	public void setContractInfoDao(ContractInfoDao contractInfoDao) {
		this.contractInfoDao = contractInfoDao;
	}
	@Resource
	private ReinstatementDao reinstatementDao;

	public void setReinstatementDao(ReinstatementDao reinstatementDao) {
		this.reinstatementDao = reinstatementDao;
	}
	@Override
	public Map<String, Object> listMap(Integer page, Integer rows, String sort, String order,
			Reinstatement condition) {
		Map<String, Object> map = new HashMap<>();
		int start = (page - 1) * rows;
		List<Reinstatement> list = reinstatementDao.getListByCondition(start, rows, condition, sort, order);
		int total = reinstatementDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		reinstatementDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(Reinstatement reinstatement) {
		Map<String, Object> map = new HashMap<>();
		Archives archives = new Archives();
		up(reinstatement);
		archives.setId(reinstatement.getArchives().getId());
		DataDictionary status = new DataDictionary();
		status.setId(106);
		archives.setStatus(status);
		archivesDao.update(archives);
		reinstatementDao.add(reinstatement);
		map.put("result", true);
		return map;
	}
	private void up(Reinstatement reinstatement) {
		if (reinstatement.getWhether().getId() == 1) {
			Restore restore = new Restore();
			ContractInfo contractInfo = new ContractInfo();
			contractInfo.setId(reinstatementDao.getcontractinfoid(reinstatement.getArchives().getId()));
			restore.setContractInfo(contractInfo);
			restore.setTime(reinstatement.getTime());
			restore.setRestoreremark("复职恢复合同");
			DataDictionary type = new DataDictionary();
			type.setId(reinstatement.getHftype());
			restore.setType(type);
			
			restoreDao.add(restore);
			
			//更改合同的状态解除
			DataDictionary contractState = new DataDictionary();
			contractState.setId(69);
			contractInfo.setContractstate(contractState);
			contractInfoDao.update(contractInfo);
			
		}
	}
	

	@Override
	public Map<String, Object> edit(Reinstatement reinstatement) {
		Map<String, Object> map = new HashMap<>();
		reinstatementDao.update(reinstatement);
		
		up(reinstatement);
		map.put("result", true);
		return map;
	}
	@Override
	public Reinstatement getById(Integer id) {
		return reinstatementDao.getById(id);
	}
	@Override
	public Integer getcontractinfoid(Integer id) {
		return reinstatementDao.getcontractinfoid(id);
	}
}
