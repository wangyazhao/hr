package org.hr.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hr.dao.ChangeInfoDao;
import org.hr.pojo.ChangeInfo;
import org.hr.pojo.ContractInfo;
import org.hr.service.ChangeInfoService;
import org.springframework.stereotype.Service;

@Service("changeInfoService")
public class ChangeInfoServiceImpl implements ChangeInfoService{

	@Resource
	private ChangeInfoDao changeInfoDao;

	public void setChangeInfoDao(ChangeInfoDao changeInfoDao) {
		this.changeInfoDao = changeInfoDao;
	}
	@Override
	public Map<String, Object> listMap(Integer byid,Integer page, Integer rows, String sort, String order,
			ChangeInfo condition) {
		Map<String, Object> map = new HashMap<>();
		ContractInfo contractInfo = new ContractInfo();
		contractInfo.setId(byid);
		condition.setContractInfo(contractInfo);
		
		int start = (page - 1) * rows;
		List<ChangeInfo> list = changeInfoDao.getListByCondition(start, rows, condition, sort, order);
		int total = changeInfoDao.getCountByCondition(condition);
		map.put("rows", list);
		map.put("total", total);
		return map;
	}

	@Override
	public Map<String, Object> batchDelete(Integer[] ids) {
		Map<String, Object> map = new HashMap<>();
		changeInfoDao.deleteByIds(ids);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> add(ChangeInfo changeInfo) {
		Map<String, Object> map = new HashMap<>();
		changeInfoDao.add(changeInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public Map<String, Object> edit(ChangeInfo changeInfo) {
		Map<String, Object> map = new HashMap<>();
		changeInfoDao.update(changeInfo);
		map.put("result", true);
		return map;
	}

	@Override
	public ChangeInfo getById(Integer id) {
		return changeInfoDao.getById(id);
	}
}
