package org.hr.service;

import java.util.List;

import org.hr.pojo.DataDictionary;

public interface DataDictionaryService {

	public List<DataDictionary> getTypeNameByType(String typeCode);
	
	public List<DataDictionary> getByTitleAssessInfoName();
}
