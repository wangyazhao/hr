package org.hr.service;

import java.util.Map;

import org.hr.pojo.ChangeInfo;

public interface ChangeInfoService {
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer byid, Integer page, Integer rows,String sort, String order,ChangeInfo condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param changeInfo
	 * @return
	 */
	public Map<String, Object> add(ChangeInfo changeInfo);
	
	/**
	 * 编辑
	 * @param changeInfo
	 * @return
	 */
	public Map<String, Object> edit(ChangeInfo changeInfo);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public ChangeInfo getById(Integer id);
}
