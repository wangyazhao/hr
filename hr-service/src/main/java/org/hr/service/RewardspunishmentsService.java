package org.hr.service;

import java.util.List;
import java.util.Map;

import org.hr.pojo.Rewardspunishments;

public interface RewardspunishmentsService {

	public List<Rewardspunishments> list();
	  
  
	/**
	 * 根据id 查询Rewardspunishments
	 */
	public Rewardspunishments view(Integer id);
	
	/**
	 * 修改
	 */
	public Map<String,Object> update(Rewardspunishments rewardspunishments);
	
	/**
	 * 删除用户
	 */
 
	public Map<String, Object> delByIds(Integer[] ids);

	/**
	 * 添加用户
	 */
	public Map<String, Object> add(Rewardspunishments rewardspunishments);
	
	
	
	
	
	
	
}
