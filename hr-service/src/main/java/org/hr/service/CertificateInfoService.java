package org.hr.service;

import java.util.Map;

import org.hr.pojo.CertificateInfo;

public interface CertificateInfoService {
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer page, Integer rows,String sort, String order,CertificateInfo condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param certificateInfo
	 * @return
	 */
	public Map<String, Object> add(CertificateInfo certificateInfo);
	
	/**
	 * 编辑
	 * @param certificateInfo
	 * @return
	 */
	public Map<String, Object> edit(CertificateInfo certificateInfo);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public CertificateInfo getById(Integer id);
}
