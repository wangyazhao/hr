package org.hr.service;

import java.util.Map;

import org.hr.pojo.TitleAssessInfo;

public interface TitleAssessInfoService {

	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer page, Integer rows,String sort, String order,TitleAssessInfo condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param titleAssessInfo
	 * @return
	 */
	public Map<String, Object> add(TitleAssessInfo titleAssessInfo);
	
	/**
	 * 编辑
	 * @param titleAssessInfo
	 * @return
	 */
	public Map<String, Object> edit(TitleAssessInfo titleAssessInfo);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public TitleAssessInfo getById(Integer id);
}
