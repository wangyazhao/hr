package org.hr.service;

import java.util.Map;

import org.hr.pojo.ContractInfo;

public interface ContractInfoService {
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer page, Integer rows,String sort, String order,ContractInfo condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param contractInfo
	 * @return
	 */
	public Map<String, Object> add(ContractInfo contractInfo);
	
	/**
	 * 编辑
	 * @param contractInfo
	 * @return
	 */
	public Map<String, Object> edit(ContractInfo contractInfo);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public ContractInfo getById(Integer id);
	
	public ContractInfo getbyarchivesId(Integer id);
}
