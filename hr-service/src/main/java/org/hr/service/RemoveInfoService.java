package org.hr.service;

import java.util.Map;

import org.hr.pojo.RemoveInfo;

public interface RemoveInfoService {
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer byid,Integer page, Integer rows,String sort, String order,RemoveInfo condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param removeInfo
	 * @return
	 */
	public Map<String, Object> add(RemoveInfo removeInfo);
	
	/**
	 * 编辑
	 * @param removeInfo
	 * @return
	 */
	public Map<String, Object> edit(RemoveInfo removeInfo);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public RemoveInfo getById(Integer id);
}
