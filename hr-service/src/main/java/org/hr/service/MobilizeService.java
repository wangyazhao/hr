package org.hr.service;

import java.util.Map;

import org.hr.pojo.Mobilize;

public interface MobilizeService {

	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer page, Integer rows,String sort, String order,Mobilize condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param mobilize
	 * @return
	 */
	public Map<String, Object> add(Mobilize mobilize);
	
	/**
	 * 编辑
	 * @param mobilize
	 * @return
	 */
	public Map<String, Object> edit(Mobilize mobilize);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Mobilize getById(Integer id);
}
