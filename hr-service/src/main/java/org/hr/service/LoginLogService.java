package org.hr.service;

import java.util.Map;

import org.hr.pojo.LoginLog;

public interface LoginLogService {
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap( Integer page, Integer rows,String sort, String order,LoginLog condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param coginLog
	 * @return
	 */
	public Map<String, Object> add(LoginLog coginLog);
	
	/**
	 * 编辑
	 * @param coginLog
	 * @return
	 */
	public Map<String, Object> edit(LoginLog coginLog);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public LoginLog getById(Integer id);
}
