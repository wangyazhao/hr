package org.hr.service;

import java.util.List;
import java.util.Map;
 
import org.hr.pojo.Rolepermission;
 
public interface RolepermissionService {

	public List<Rolepermission> list();
	
	public List<Rolepermission> lists();
	
	/**
	 * 添加
	 */
	public Map<String,Object> addrole(Rolepermission rolepermission);
	
	/**
	 * 根据ID查询
	 * @param id
	 * @return
	 */
	public Rolepermission view(Integer id);
	
	/**
	 * 修改
	 * @param role
	 * @return
	 */
	public Map<String,Object> update(Rolepermission rolepermission);
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	public Map<String,Object> delByIds(Integer[] ids); 
	
	public List<Rolepermission> type();
	
	public List<Rolepermission> level();
	
	
	
	public List<Integer> getPermissionIdsByRoleId(Integer roleId);
	
	public void deletePermissionsByRoleId(Integer roleId);
	
	public List<Rolepermission> getPermissionsByUserId( Integer userId,String type);
	
	public List<String> getPermissionCodeByUserId(Integer userId);
	
	public List<String> getMenuCodeByUserId(Integer userId);
	
	public List<Rolepermission> getPermissionByUserId(Integer userId);
	
	
}
