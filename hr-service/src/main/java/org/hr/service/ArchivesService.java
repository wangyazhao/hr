package org.hr.service;

import java.util.List;
import java.util.Map;

import org.hr.pojo.Archives;
import org.hr.pojo.Hr;

public interface ArchivesService {

	/**
	 * 获取所有的员工
	 * @return
	 */
	public List<Archives> getAll();
	
	/**
	 * 根据id获取员工
	 * @param id
	 * @return
	 */
	public Archives getByID(Integer id);
	
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer page, Integer rows,String sort, String order,Archives condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param archives
	 * @return
	 */
	public Map<String, Object> add(Archives archives);
	
	/**
	 * 编辑
	 * @param archives
	 * @return
	 */
	public Map<String, Object> edit(Archives archives);
	
	/**
	 * 根据type分类
	 * @return
	 */
	public List<Hr> groupBysex(Integer sid,String type);
	
	/**
	 * 根据type分类
	 * @return
	 */
	public List<Hr> groupByposition(Integer id);
	
}
