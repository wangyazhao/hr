package org.hr.service;

import java.util.List;
import java.util.Map;

import org.hr.pojo.Audit;
import org.hr.pojo.Cultivate;

public interface CultivateService {
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer page, Integer rows,String sort, String order,Cultivate condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param cultivate
	 * @return
	 */
	public Map<String, Object> add(Cultivate cultivate,Integer [] aid);
	
	/**
	 * 编辑
	 * @param cultivate
	 * @return
	 */
	public Map<String, Object> edit(Cultivate cultivate,Integer [] aid);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Cultivate getById(Integer id);
	
	public List<Cultivate> getArchivesname(Integer id);
	
	public Map<String, Object>  addAudit(Audit audit);
}
