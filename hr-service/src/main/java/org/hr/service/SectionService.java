package org.hr.service;

import java.util.List;
import java.util.Map;

import org.hr.pojo.Section;

public interface SectionService {

	/**
	 * 获取所有部门信息
	 * @return
	 */
	public List<Section> getAll();
	
	/**
	 * 根据id获取Section(部门)
	 * @param id
	 * @return
	 */
	public Section getById(Integer id);
	
	/**
	 * 获取所有部门信息
	 * @return
	 */
	public List<Section> listData();
	
	/**
	 * 添加部门
	 * @param section
	 * @return
	 */
	public Map<String, Object>  add(Section section);
	
	/**
	 * 删除id
	 * @param id
	 * @return
	 */
	public Map<String, Object> del(Integer id);
	
	/**
	 * 修改
	 * @param section
	 * @return
	 */
	public Map<String, Object> update(Section section);
}
