package org.hr.service;

import java.util.Map;

import org.hr.pojo.Reinstatement;

public interface ReinstatementService {
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param sort
	 * @param order
	 * @param condition
	 * @return
	 */
	public Map<String, Object> listMap(Integer page, Integer rows,String sort, String order,Reinstatement condition);


	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	public Map<String, Object> batchDelete(Integer [] ids);
	
	/**
	 * 添加
	 * @param reinstatement
	 * @return
	 */
	public Map<String, Object> add(Reinstatement reinstatement);
	
	/**
	 * 编辑
	 * @param reinstatement
	 * @return
	 */
	public Map<String, Object> edit(Reinstatement reinstatement);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Reinstatement getById(Integer id);
	
	public Integer getcontractinfoid(Integer id);
}
