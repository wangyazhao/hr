<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="rewardsPunishmentsInfoTable"  title="RewardsPunishmentsInfo List"
        data-options="url:'rewardsPunishmentsInfo/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'xm',width:60,formatter:xmRewardsPunishmentsInfoFormatter">员工姓名</th>
            <th data-options="field:'jcxm',width:60,formatter:jcxmRewardsPunishmentsInfoFormatter">奖惩项目</th>
            <th data-options="field:'attribute',width:50">奖惩属性</th>
            <th data-options="field:'time',width:50">奖惩时间</th>
            <th data-options="field:'czRewardsPunishmentsInfo',width:50,formatter:czRewardsPunishmentsInfoFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<form id="rewardsPunishmentsInfoCondition1">
	<div id="rewardsPunishmentsInfotb" style="padding:15px 10px;">
		用户名 : <input id="nameSearch5" class="easyui-combobox" data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'archives/all',panelHeight:'auto',panelMaxHeight:250">
		奖惩项目 : <input id="jiangcheng5" class="easyui-combobox" data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'rewardspunishments/las',panelHeight:'auto',panelMaxHeight:250">
		奖惩属性 ：<input id="shuxing5" class="easyui-textbox" name="attribute" >
		<a id="btn" href="javascript:void(0)" onclick="setRewardsPunishmentsInfoCondition();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
		<a id="btn" href="javascript:void(0)" onclick="resetRewardsPunishmentsInfoCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-undo'">Reset</a>
		
		<shiro:hasPermission name="rewardsPunishmentsInfo:add">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_rewardsPunishmentsInfo();" data-options="iconCls:'icon-add'">添加</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="rewardsPunishmentsInfo:dels">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="dels_rewardsPunishmentsInfo();" data-options="iconCls:'icon-remove'">删除</a>
		</shiro:hasPermission>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#rewardsPunishmentsInfoTable").datagrid({
			pagination : true,
			toolbar : "#rewardsPunishmentsInfotb",
			idField : "id"
		});
	
		
	})
	
	//设置条件
	function setRewardsPunishmentsInfoCondition(){ 
		var postData = { 'archives.id': $("#nameSearch5").val(),
						'jcxiangmu.id': $("#jiangcheng5").val(),
						    attribute : $("#shuxing5").val()};
		$("#rewardsPunishmentsInfoTable").datagrid("reload",postData);
	}

	//清除条件
	function resetRewardsPunishmentsInfoCondition(){
		$("#rewardsPunishmentsInfoCondition1").form("clear");
	}
	
	//姓名
	function xmRewardsPunishmentsInfoFormatter(value,row,index){
		if(row.archives == null){
			return "-";
		}
		 return row.archives.name; 
	}
	//奖惩项目
	function jcxmRewardsPunishmentsInfoFormatter(value,row,index){
		
		if( row.jcxiangmu == null || row.jcxiangmu.name == null){
			return "-";
		}
		return row.jcxiangmu.name;
	}
	
	//格式化操作
	function czRewardsPunishmentsInfoFormatter(value,row,index){
		var str = "<a href='javascript:void(0)' onclick='view_rewardsPunishmentsInfo("+row.id+");' >查看</a>  ";
			<shiro:hasPermission name="rewardsPunishmentsInfo:edit">
			str += "<a href='javascript:void(0)' onclick='edit_rewardsPunishmentsInfo("+row.id+");' >修改</a>";
			</shiro:hasPermission>
		return  str;
	}
	
	<shiro:hasPermission name="rewardsPunishmentsInfo:dels">
	//删除选择的数据行
	function dels_rewardsPunishmentsInfo(){
		//获取要删除的数据黄
		var rewardsPunishmentsInfoids = $("#rewardsPunishmentsInfoTable").datagrid("getSelections");
		if(rewardsPunishmentsInfoids.length == 0){
			$.messager.alert("提示","请选择要删除的数据行！","warning");
			return;
		}
		$.messager.confirm("提示","确定要删除选中的员工及其相关数据吗？",function(d){
			if(d){
				var postData = "";
				$.each(rewardsPunishmentsInfoids,function(i){
					postData += "ids="+this.id;
					if(i < rewardsPunishmentsInfoids.length - 1){
						postData += "&";
					}
				});
				$.post("rewardsPunishmentsInfo/dels",postData,function(data){
					if(data.result == true){
						// 删除成功后，刷新表格 reload
						$("#rewardsPunishmentsInfoTable").datagrid("reload");
					}
				})
			}
		})
	}
	</shiro:hasPermission>
	
	//查看
	function view_rewardsPunishmentsInfo(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:600,
			modal:true,
			href : "rewardsPunishmentsInfo/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("rewardsPunishmentsInfo/view",{id:id},function(data){
					$("#rewardsPunishmentsInfoForm5").form("load",data);
					$("#StaffNumer19").textbox("setValue",data.archives.id);//id
					$("#XingMing19").textbox('setValue',data.archives.name);
					$("#xingbie19").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen19").textbox('setValue',data.archives.idcard);
					$("#BuMen19").textbox('setValue',data.archives.section.text);
					$("#ZhiWu19").textbox('setValue',data.archives.position.name);
					$("#GangWei19").textbox('setValue',data.archives.jobs.typename);
					
					
					$("#jiangcheng19").textbox('setValue',data.jcxiangmu.name);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	<shiro:hasPermission name="rewardsPunishmentsInfo:edit">
	//修改
	function edit_rewardsPunishmentsInfo(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑用户",
			iconCls : "icon-edit",
			width:700,
			height:600,
			modal:true,
			href : "rewardsPunishmentsInfo/form",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("rewardsPunishmentsInfo/view",{id:id},function(data){
					$("#rewardsPunishmentsInfoForm").form("load",data);
					$("#StaffNumer").combogrid("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					
					$("#jiangcheng").combobox('setValue',data.jcxiangmu.id);
				});
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#rewardsPunishmentsInfoForm").form("submit",{
						url : "rewardsPunishmentsInfo/edit",
						success : function(data){
							d.dialog("close");
							$("#rewardsPunishmentsInfoTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	
	<shiro:hasPermission name="rewardsPunishmentsInfo:add">
	//添加
	function add_rewardsPunishmentsInfo(){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "新增",
			iconCls : "icon-add",
			width:700,	
			height:540,
			modal:true,//是否是模态框
			href : "rewardsPunishmentsInfo/form",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#rewardsPunishmentsInfoForm").form("submit",{
						url : "rewardsPunishmentsInfo/add",
						success : function(data){
							d.dialog("close");
							$("#rewardsPunishmentsInfoTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
</script>
</body>
</html>




