<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
	<meta charset="UTF-8">
<title>职位管理</title>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/> 
</head>
<body>
<script type="text/javascript">
	$(function(){
		$("#PostitionTable").datagrid({
			pagination : true,
		    singleSelect: true
		});
	});
	
	function addCondition(){
		var d = $("<div></div>").appendTo("body");		
		d.dialog({
			title:"添加职业",
			iconCls:"icon-add",
			width:500,
			height:300,
			modal:true, 
			href:"position/add_form", 
			onClose:function(){$(this).dialog("destroy"); },			
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",		
				handler:function(){
					$("#positionform").form("submit",{
						url:"position/add",
						success:function(data){
							d.dialog("close");
							$("#PostitionTable").datagrid("reload");
						}
					});
				}	
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}] 
		}); 
	}
	
	
	 
	function userCadeFormatter(value,row,index){
		return "<a href='javascript:void(0)' onclick='edit_role("+row.id+");'  >"+value+"</a>";
	}
		 
	function edit_role(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑职务",
			iconCls : "icon-edit",
			width:500,
			height:300,
			modal:true,
			href : "position/add_form",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				  $.post("position/view",{id:id},function(data){
					$("#positionform").form("load",data);
				});   
				},
 
				buttons:[{
					iconCls:"icon-ok",
					text:"确定",
					handler:function(){
						$("#positionform").form("submit",{
							url : "position/updata",
							success : function(data){
								d.dialog("close");
								$("#PostitionTable").datagrid("reload");
							}
						});
					}
				},{
					iconCls:"icon-remove",
					text:"删除",
					handler:function(){
						$.messager.confirm("提示","确定要删除选中的数据吗？",function(da){
							if(da){
								$.post("position/del",{id:id},function(data){
									if(data.result == true){
										// 删除成功后，刷新表格 reload
										d.dialog("close");
										$("#PostitionTable").datagrid("reload");
									}
								});
							}
						});
					}
				},{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						d.dialog("close");
					}
				}]	
		});
			
	}
	
	

</script> 


	<div class="easyui-panel" style="padding:15px 10px;">
		<a id="btn" href="javascript:void(0)" onclick="addCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-add'">添加</a>
	</div>
	<table id="PostitionTable"  title="Postition List"
	        data-options="url:'position/all',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
	    <thead>
	        <tr> 
				<th data-options="field:'id',width:30,sortable:true,order:'desc'">Id</th>
	            <th data-options="field:'name',width:100,formatter:userCadeFormatter">职位名称</th>
	            <th data-options="field:'remark',width:200">职位描述</th>
	        </tr>
	    </thead>
	</table> 
</body>
</html>