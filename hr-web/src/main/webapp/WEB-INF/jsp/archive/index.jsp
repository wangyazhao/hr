<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%

String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="archivesTable"  title="Archives List"
        data-options="url:'archives/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'worknum',width:50">工号</th>
       		<th data-options="field:'name',width:80">姓名</th>
            <th data-options="field:'bm',width:80,formatter:bmArchivesFormatter">部门</th>
            <th data-options="field:'gw',width:50,formatter:gwArchivesFormatter">岗位</th>
            <th data-options="field:'zw',width:30,formatter:zwArchivesFormatter">职务</th>
            <th data-options="field:'gz',width:50,formatter:gzArchivesFormatter">工种</th>
            <th data-options="field:'czArchives',width:50,formatter:czArchivesFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<form id="archivesCondition1">
	<div id="archivestb" style="padding:15px 10px;">
		<div style="padding:0 5px 10px 0;">
		工号 : <input type="text" id="worknum" class="easyui-textbox" />
		用户名 : <input type="text"  id="archivesname" class="easyui-textbox"/>
		性别:<input class="easyui-combobox" id="xingbie1"  data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/sex',panelHeight:'auto',panelMaxHeight:250,editable:false">
		学历:<input id="Xxueli" class="easyui-combobox" type="text" data-options="valueField:'id',textField:'typename',url:'data/education',panelHeight:'auto',panelMaxHeight:250,editable:false">
		工种:<input id="xgongzhong" class="easyui-combobox" type="text"  data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/employeesWork',panelHeight:'auto',panelMaxHeight:250,editable:false">
		</div>
		岗位 : <input id="xyggw" class="easyui-combobox" type="text" data-options="valueField:'id',textField:'typename',url:'data/gangwei',panelHeight:'auto',panelMaxHeight:250">
		职 务 : <input id="xzhiwu" class="easyui-combobox" type="text" data-options="valueField:'id',textField:'name',url:'position/all',panelHeight:'auto',panelMaxHeight:250">
		部门:<input id="XBmName" class="easyui-combotree" type="text" data-options="animate:true,valueField:'id',textField:'typename',url:'section/list',panelHeight:'auto',panelMaxHeight:250,editable:false,lines:'true'">
		
  		<a id="btn" href="javascript:void(0)" onclick="setArchivesCondition();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
		<a id="btn" href="javascript:void(0)" onclick="resetArchivesCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-undo'">Reset</a>
		<shiro:hasPermission name="archive:add">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_archives();" data-options="iconCls:'icon-add'">添加</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="archive:dels">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="dels_archives();" data-options="iconCls:'icon-remove'">删除</a>
		</shiro:hasPermission>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#archivesTable").datagrid({
			pagination : true,
			toolbar : "#archivestb",
			idField : "id"
		}); 
	})
	
	//设置条件
	function setArchivesCondition(){
		var postData = {worknum : $("#worknum").val(),
						name : $("#archivesname").val(),
						'sex.id':$("#xingbie1").val(),
						'education.id':$("#Xxueli").val(),
						'employeeswork.id':$("#xgongzhong").val(),
						'jobs.id':$("#xyggw").val(),
						'position.id':$("#xzhiwu").val(),
						'section.id' :$("#XBmName").val()
			};
		$("#archivesTable").datagrid("reload",postData);
	}

	//清除条件
	function resetArchivesCondition(){
		$("#archivesCondition1").form("clear");
	}
	
	//格式化部门
	function bmArchivesFormatter(value,row,index){
		if(row.section ==null || row.section.text == null){
			return "-";
		}
			return row.section.text;
	}
	//格式化岗位
	function gwArchivesFormatter(value,row,index){
		if(row.jobs ==null || row.jobs.typename == null){
			return "-";
		}
			return row.jobs.typename;
	}
	//格式化职务
	function zwArchivesFormatter(value,row,index){
		if(row.position == null || row.position.name == null){
			return "-";
		}
			return row.position.name;
	} 
	//格式化工种
	function gzArchivesFormatter(value,row,index){
		if(row.employeeswork == null || row.employeeswork.typename == null){
			return "-";
		}
			return row.employeeswork.typename;
	}
	
	//格式化操作
	function czArchivesFormatter(value,row,index){
			
		
		var str = "<a href='javascript:void(0)' onclick='view_archives("+row.id+");' >查看</a>   ";
			<shiro:hasPermission name="archive:edit">
	    	str += "<a href='javascript:void(0)' onclick='edit_archives("+row.id+");' >修改</a>";
		    </shiro:hasPermission>
		return str;
	}
	
	
	<shiro:hasPermission name="archive:dels">
	//删除选择的数据行
	function dels_archives(){
		//获取要删除的数据黄
		var selRows = $("#archivesTable").datagrid("getSelections");
		if(selRows.length == 0){
			$.messager.alert("提示","请选择要删除的数据行！","warning");
			return;
		}
		$.messager.confirm("提示","确定要删除选中的员工及其相关数据吗？",function(d){
			if(d){
				var postData = "";
				$.each(selRows,function(i){
					postData += "ids="+this.id;
					if(i < selRows.length - 1){
						postData += "&";
					}
				});
				$.post("archives/dels",postData,function(data){
					if(data.result == true){
						//4. 删除成功后，刷新表格 reload
						$("#archivesTable").datagrid("reload");
					}
				})
			}
		})
	}
	</shiro:hasPermission>
	
	
	//查看
	function view_archives(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看用户",
			iconCls : "icon-back",
			width:735,
			height:600,
			modal:true,
			href : "archives/toview",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("archives/view",{id:id},function(data){
					$("#archivesForm").form("load",data);
					
					$("#xingbie").combobox("setValue",data.sex.id);//性别
					$("#guanji").combobox("setValue",data.nativeplace.id);//籍贯
					$("#minzu").combobox("setValue",data.national.id );//民族
					$("#hunyin").combobox("setValue",data.marital.id );//婚姻状况
					$("#ZhengZhi").combobox("setValue",data.politicalface.id );//政治面貌
					
					
					$("#xueli").combobox("setValue",data.education.id );//学历
					$("#rencaiFL").combobox("setValue",data.talenttype.id);//人才分类
					$("#rencaiDJ").combobox("setValue",data.talentlevel.id);//人才等级
					
					
					$("#ruzhiDate").datebox('setValue',data.entrytime);//入职日期
					$("#ygbm").combotree("setValue",data.section.id);//员工部门
					$("#yggw").combobox("setValue",data.jobs.id);//员工岗位
					$("#gongzhong").combobox("setValue",data.employeeswork.id);//员工工种
					$("#zhiwu").combobox("setValue",data.position.id);//员工职位
					$("#zhicheng").combobox("setValue",data.jobtitle.id);//员工职称
					$("#YGztai").combobox("setValue",data.status.id);//员工状态
					$("#bank").combobox("setValue",data.bank.id);//工资卡开户行
					
					
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	<shiro:hasPermission name="archive:edit">
	//修改
	function edit_archives(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑用户",
			iconCls : "icon-edit",
			width:735,
			height:600,
			modal:true,
			href : "archives/form",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("archives/view",{id:id},function(data){
					$("#archivesForm").form("load",data);
					
					$("#xingbie").combobox("setValue",data.sex.id);//性别
					$("#guanji").combobox("setValue",data.nativeplace.id);//籍贯
					$("#minzu").combobox("setValue",data.national.id );//民族
					$("#hunyin").combobox("setValue",data.marital.id );//婚姻状况
					$("#ZhengZhi").combobox("setValue",data.politicalface.id );//政治面貌
														
					$("#xueli").combobox("setValue",data.education.id );//学历
					$("#rencaiFL").combobox("setValue",data.talenttype.id);//人才分类
					$("#rencaiDJ").combobox("setValue",data.talentlevel.id);//人才等级
					
					$("#ygbm").combotree("setValue",data.section.id);//员工部门
					$("#yggw").combobox("setValue",data.jobs.id);//员工岗位
					$("#gongzhong").combobox("setValue",data.employeeswork.id);//员工工种
					$("#zhiwu").combobox("setValue",data.position.id);//员工职位
					$("#zhicheng").combobox("setValue",data.jobtitle.id);//员工职称
					$("#YGztai").combobox("setValue",data.status.id);//员工状态
					$("#bank").combobox("setValue",data.bank.id);//工资卡开户行
				});
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#archivesForm").form("submit",{
						url : "archives/edit",
						success : function(data){
							d.dialog("close");
							$("#archivesTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	<shiro:hasPermission name="archive:add">
	//添加员工
	function add_archives(){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "添加员工",
			iconCls : "icon-add",
			width:735,	
			height:600,
			modal:true,//是否是模态框
			href : "archives/form",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#archivesForm").form("submit",{
						url : "archives/add",
						success : function(data){
							d.dialog("close");
							$("#archivesTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
</script>
</body>
</html>




