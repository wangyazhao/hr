<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<form action="" id="archivesForm" method="post">
	<table  border="0"   cellpadding="5" cellspacing="0" width="700px">
		<tbody>
			<tr>
				<td align="center" colspan="4" style="height: 26px"><b>基本信息</b><hr/>
					<input name="id"  type="hidden"  >
				</td>
			</tr>
			<tr>
				<td class="form-item" style="width: 20%"><font color="red">*</font>工号：</td>
				<td style="width: 30%"><input name="worknum" id="gh" class="easyui-textbox" data-options="required:true" type="text"><img src="easyui/themes/icons/large_chart.png" onclick="fenpei()"  title="分配编号" style="vertical-align: middle;"/></td>
				<td class="form-item" style="width: 20%"><font color="red">*</font>姓名：</td>
				<td style="width: 30%"><input name="name" class="easyui-textbox" data-options="required:true" type="text"id="XingMing"  ></td>
			</tr>
			<tr>
				<td class="form-item">性别：</td>
				<td><input id="xingbie" class="easyui-combobox" name="sex.id" style="width: auto;min-width: 100px;"  data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/sex',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
				<td class="form-item">出生年月：</td>
				<td><input name="birthday"  type= "date"   id="ChuSheng" ></td>
			</tr>
			<tr>
				<td class="form-item">籍贯：</td>
				<td><input id="guanji" class="easyui-combobox" name="nativeplace.id"   data-options="valueField:'id',textField:'typename',panelHeight:'auto',panelMaxHeight:250"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/></td>
				<td class="form-item">民族：</td>
				<td><input id="minzu" class="easyui-combobox" name="national.id"   data-options="valueField:'id',textField:'typename',panelHeight:'auto',panelMaxHeight:250"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/></td>
			</tr>
			<tr>
				<td class="form-item">婚姻状况：</td>
				<td><input id="hunyin" class="easyui-combobox" name="marital.id"   data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/marital',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
				<td class="form-item">政治面貌：</td>
				<td><input id="ZhengZhi" class="easyui-combobox" name="politicalface.id"  data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/politicalFace',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
			</tr>
			<tr>
				<td class="form-item">健康状况：</td>
				<td><input name="health" type="text" class="easyui-textbox" ></td>
				<td class="form-item">身份证号：</td>
				<td><input name="idcard" type="text" class="easyui-textbox" ></td>
			</tr>
			
			
			
			<tr>
				<td colspan="4" style="text-align: center"><strong>个人情况</strong><hr/>
				</td>
			</tr>
			<tr>
				<td class="form-item">参加工作时间：</td>
				<td><input id="cjgzsj3" name="joinworktime"   type="date"  ></td>
				<td class="form-item">工作年限：</td>
				<td><input id="gznx" name="workyear" class="easyui-textbox" type="text" ></td>
			</tr>
			<tr>
				<td class="form-item">学历：</td>
				<td><input id="xueli" class="easyui-combobox" name="education.id"   data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/education',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
				<td class="form-item">毕业院校：</td>
				<td><input  id="BiYe" name="finishschool" class="easyui-textbox"type="text"></td>
			</tr>
			<tr>
				<td class="form-item">所学专业：</td>
				<td><input id="suoxuezhuanye" name="major"class="easyui-textbox" type="text" ></td>
				<td class="form-item">毕业时间：</td>
				<td><input id="biyeshijian"  name="graduatetime"  class="easyui-textbox" type="date"   ></td>
			</tr>
			<tr>
				<td class="form-item">所学外语：</td>
				<td><input id="sxwaiyu" name="courseslanguage"class="easyui-textbox" type="text" ></td>
				<td class="form-item">外语等级：</td>
				<td><input id="waiyuDJ" name="languagedcale"class="easyui-textbox" type="text" ></td>
			</tr>
			<tr>
				<td class="form-item">计算机掌握程度：</td>
				<td><input id="jsjzwcd" name="masterdegree" class="easyui-textbox" type="text"  ></td>
				<td class="form-item">计算机等级：</td>
				<td><input id="jsjDJ" name="computerdcale" class="easyui-textbox" type="text"  ></td>
			</tr>
			<tr>
				<td class="form-item">人才分类：</td>
				<td><input id="rencaiFL" class="easyui-combobox" class="easyui-textbox" name="talenttype.id"   data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/rcFenLei',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
				<td class="form-item">人才等级：</td>
				<td><input id="rencaiDJ" class="easyui-combobox" class="easyui-textbox" name="talentlevel.id"   data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/rcFenJi',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
			</tr>
			<tr>
				<td class="form-item">所获证书：</td>
				<td colspan="3"><input id="shZhengShu" name="certificate" class="easyui-textbox" type="text"></td>
			</tr>
			<tr>
				<td class="form-item">教育简历：</td>
				<td colspan="3"><input id="jxJianLi" name="educationresume" class="easyui-textbox" type="text"></td>
			</tr>
			<tr>
				<td class="form-item">特长：</td>
				<td><input id="techang" name="specialty" class="easyui-textbox" type="text" ></td>
				<td class="form-item">爱好：</td>
				<td><input id="aihao" name="hobbys" class="easyui-textbox" type="text" ></td>
			</tr>
			<tr>
				<td class="form-item">电子邮件：</td>
				<td><input id="dianziyoujian" name="email" class="easyui-textbox" data-options="validType:'email'" type="text"  ></td>
				<td class="form-item">联系地址：</td>
				<td><input id="dizhi" name="address" class="easyui-textbox"  type="text" ></td>
			</tr>
			<tr>
				<td class="form-item">手机号：</td>
				<td><input id="tel1" name="tel" class="easyui-textbox" data-options="required:true" type="text"></td>
				<td class="form-item">邮政编码：</td>
				<td><input id="youBian" name="zipcode" class="easyui-textbox" type="text" ></td>
			</tr>
			<tr>
				<td class="form-item">身高：</td>
				<td><input id="shengao" name="height" class="easyui-textbox" type="text" ></td>
				<td class="form-item">体重：</td>
				<td><input id="tizhong" name="weight" class="easyui-textbox" type="text" ></td>
			</tr>
			<tr>
				<td class="form-item">血型：</td>
				<td><input id="xiexing" name="bloodtype" class="easyui-textbox" type="text" ></td>
				<td class="form-item">对工作要求：</td>
				<td><input id="gzyqiu" name="workrequire" class="easyui-textbox" type="text" ></td>
			</tr>
			
			
			
			
			<tr>
				<td colspan="4" style="text-align: center"><strong>单位信息</strong><hr/>
				</td>
			</tr>
			<tr>
				<td class="form-item"><font color="red">*</font>入职日期：</td>
				<td><input  id="ruzhiDate" name="entrytime"   type="date"></td>
				<td class="form-item"><font color="red">*</font>员工部门：</td>
				<td><input id="ygbm" class="easyui-combotree" name="section.id"   data-options=" url:'section/list',animate:true,valueField:'id',textField:'name',panelHeight:'auto',panelMaxHeight:250,lines:'true'"></td>
			</tr>
			<tr>
				<td class="form-item">员工岗位：</td>
				<td><input id="yggw" class="easyui-combobox" name="jobs.id"   data-options="valueField:'id',textField:'typename',panelHeight:'auto',panelMaxHeight:250"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/></td>
				<td class="form-item">员工工种：</td>
				<td><input id="gongzhong" class="easyui-combobox" name="employeeswork.id"   data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/employeesWork',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
			</tr>
			<tr>
				<td class="form-item">员工职位：</td>
				<td><input id="zhiwu" class="easyui-combobox" name="position.id"   data-options="valueField:'id',textField:'name',panelHeight:'auto',panelMaxHeight:250"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/></td>
				<td class="form-item">员工职称：</td>
				<td><input id="zhicheng" class="easyui-combobox" name="jobtitle.id"   data-options="valueField:'id',textField:'typename',panelHeight:'auto',panelMaxHeight:250"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/></td>
			</tr>
			<tr>
				<td class="form-item"><font color="red">*</font>基本工资：</td>
				<td><input id="JbGongZ" name="basicsalary"  class="easyui-textbox" type="text"></td>
				<td class="form-item">员工状态：</td>
				<td><input id="YGztai" class="easyui-combobox" name="status.id"   data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/ygzt',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
			</tr>
			<tr>
				<td class="form-item">工资卡开户行：</td>
				<td><input id="bank" class="easyui-combobox" name="bank.id"   data-options="valueField:'id',textField:'typename',panelHeight:'auto',panelMaxHeight:250"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/></td>
				<td class="form-item">工资卡帐号：</td>
				<td><input id="ZhangHao" name="account"  class="easyui-textbox" type="text" ></td>
			</tr>
			
			
			
			<tr>
				<td colspan="4" style="text-align: center"><strong>其他信息</strong><hr/>
				</td>
			</tr>
			<tr>
				<td class="form-item">社会关系：</td>
				<td colspan="3"><textarea name="socialrelations" class="easyui-textbox" rows="2" cols="20"id="Shgx" style="height: 66px; width: 96%;"></textarea></td>
			</tr>
			<tr>
				<td class="form-item">工作经历：</td>
				<td colspan="3"><textarea name="workexperience" class="easyui-textbox" rows="2" cols="20"id="Gzjl" style="height: 66px; width: 96%;"></textarea></td>
			</tr>
			<tr>
				<td class="form-item">学习经历：</td>
				<td colspan="3"><textarea name="learningexperience" class="easyui-textbox" rows="2" cols="20"id="Xxjl" style="height: 66px; width: 96%;"></textarea></td>
			</tr>
			<tr>
				<td class="form-item">备注：</td>
				<td colspan="3"><textarea name="remark" rows="2" class="easyui-textbox" cols="20" id="BeiZhu" style="height: 66px; width: 96%;"></textarea></td>
			</tr>
		</tbody>
	</table>
</form>
<script type="text/javascript">
function fenpei(){
	var str = "GH-"
	for(let i = 0 ; i< 8 ; i++){
		str += Math.floor(Math.random()*10);
	}
	$("#gh").textbox('setValue',str);
}
$(function(){
	$('#ChuSheng').datebox({
	});	
	$('#cjgzsj3').datebox({
	});	
	$('#biyeshijian').datebox({
	});
	$('#ruzhiDate').datebox({
	    required:true
	});
	$('#minzu').combobox({  
	    prompt:'',
	    required:false,  
	    url:'data/national',
	    editable:true,  
	    filter: function(q, row){  
	        var opts = $(this).combobox('options');  
	        return row[opts.textField].indexOf(q) == 0;  
	    }  
	}); 
	$('#guanji').combobox({  
	    prompt:'',  
	    required:false,  
	    url:'data/nativePlace',  
	    editable:true,  
	    filter: function(q, row){  
	        var opts = $(this).combobox('options');  
	        return row[opts.textField].indexOf(q) == 0;  
	    }  
	}); 
	$('#yggw').combobox({  
	    prompt:'',  
	    required:false,  
	    url:'data/gangwei',  
	    editable:true,  
	    filter: function(q, row){  
	        var opts = $(this).combobox('options');  
	        return row[opts.textField].indexOf(q) == 0;  
	    }  
	}); 
	$('#zhiwu').combobox({  
	    prompt:'',  
	    required:false,  
	    url:'position/all',  
	    editable:true,  
	    filter: function(q, row){  
	        var opts = $(this).combobox('options');  
	        return row[opts.textField].indexOf(q) == 0;  
	    }  
	}); 
	$('#zhicheng').combobox({  
	    prompt:'',
	    required:false,  
	    url:'data/zhiCheng',  
	    editable:true,  
	    filter: function(q, row){  
	        var opts = $(this).combobox('options');  
	        return row[opts.textField].indexOf(q) == 0;  
	    }  
	}); 
	$('#bank').combobox({  
	    prompt:'',  
	    required:false,  
	    url:'data/bank',  
	    editable:true,  
	    filter: function(q, row){  
	        var opts = $(this).combobox('options');  
	        return row[opts.textField].indexOf(q) == 0;  
	    }  
	}); 
})

</script>
</body>
</html>






