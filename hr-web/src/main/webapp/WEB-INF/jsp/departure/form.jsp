<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="" id="departureForm77" method="post">
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
					<td align="center" colspan="4"><b>员工信息</b><input type="hidden" name="id" ><hr/></td>
				</tr>
				<tr>
					<td class="form-item" style="width: 20%"><font color="red">*</font>员工工号：
					</td>
					<td style="width: 30%"><input id="StaffNumer" name="archives.id" type="text"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/></td>
					<td class="form-item" style="width: 20%">员工姓名：</td>
					<td style="width: 30%"><input type="text"id="XingMing" class="easyui-textbox" readonly="readonly"></td>
				</tr>
				<tr>
					<td class="form-item">性别：</td>
					<td><input id="xingbie" class="easyui-textbox"  ></td>
					<td class="form-item">员工身份证：</td>
					<td><input id="ShenFen" class="easyui-textbox" type="text"  readonly="readonly" ></td>
				</tr>
				<tr>
					<td class="form-item">所在部门名称：</td>
					<td colspan="3"><input class="easyui-textbox" type="text" id="BuMen" readonly="readonly" ></td>
				</tr>
				<tr>
					<td class="form-item">岗位：</td>
					<td><input class="easyui-textbox" type="text" id="GangWei" readonly="readonly" ></td>
					<td class="form-item">职务：</td>
					<td><input class="easyui-textbox" type="text" id="ZhiWu" readonly="readonly" ></td>
				</tr>
				
				
				
				
				<tr>
					<td colspan="4" style="text-align: center"><strong>复职信息</strong><hr/>
					</td>
				</tr>
				<tr>
					<td class="form-item">离职时间：</td>
					<td><input id="lzTime"  name="time" type="date"></td>
					<td class="form-item"><font color="red">*</font>离职类型：</td>
					<td><input id="lztype" name="type.id" type="text" class="easyui-combobox" data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/departure',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
				</tr>
				<tr>
					<td class="form-item">去向：</td>
					<td><input id="where"  name="gowhere" type="text" class="easyui-textbox"></td>
					<td class="form-item">工资截至日期：</td>
					<td><input id="gzjz" name="moneytime" type="date"></td>
				</tr>
				<tr>
					<td colspan="4" style="text-align: center"><strong>详细信息</strong><hr/></td>
				</tr>
				
				<tr id="jcht">
					<td style="text-align: center">是否解除合同</td>
					<td><input id="whether" name="whether.id" value="2" type="text" class="easyui-combobox" data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/whether',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
					<td id="jclxtd">解除类型： </td>
                    <td id="jclxtd1"><input name="jctype" id="HtLeiXing" class="easyui-combobox"   data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/jctype',panelHeight:'auto',panelMaxHeight:250,editable:false"></input></td>
				</tr>
				<tr>
					<td class="form-item">离职说明：</td>
					<td colspan="3"><textarea name="remark" rows="2" cols="20"
							id="fuzhi"  style="height: 100px; width: 96%;"></textarea>
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	<script type="text/javascript">
	
	
	
		$(function() {
			
			$("#whether").combobox({
				 onChange:function(n,o){
					 if(n == 1){
						 $("#jclxtd").show();
							$("#jclxtd1").show();
					 }else{
						 $("#jclxtd").hide();
							$("#jclxtd1").hide();
					 }
					 
				 }
			})
			
			$('#StaffNumer').combogrid({    
			   panelWidth:160,    
			   // fitColumns:true
			   required:true,
			    idField:'id',    
			    textField:'worknum',    
			    url:'archives/all',    
			    columns:[[    
			        {field:'id',title:'id',width:27},    
			        {field:'worknum',title:'编号',width:80},  
			        {field:'name',title:'姓名',width:50}
			    ]] ,
			    onChange:function(n,o){
			    	$.post("departure/contractinfoid",{id:n},function(data){
			    		if(data == null || data == ''){
			    			$("#jcht").hide();
			    		}else{
			    			$("#jcht").show();
			    		}
					});
			    	
				$.post("archives/toarchives",{id:n},function(data){
						$("#XingMing").textbox('setValue',data.name);
						$("#xingbie").textbox('setValue',data.sex.typename);
						$("#ShenFen").textbox('setValue',data.idcard);
						$("#BuMen").textbox('setValue',data.section.text);
						$("#ZhiWu").textbox('setValue',data.position.name);
						$("#GangWei").textbox('setValue',data.jobs.typename);
					});
				}   
			});  

			
			 
			$('#gzjz').datebox({required:true});
			$('#lzTime').datebox({required:true})
			
		})
	</script>
</body>
</html>






