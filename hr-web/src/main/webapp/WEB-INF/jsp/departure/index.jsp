<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
            <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="departureTable"  title="Departure List"
        data-options="url:'departure/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'xm',width:60,formatter:xmDepartureFormatter">员工姓名</th>
            <th data-options="field:'number',width:60,formatter:bmDepartureFormatter">员工部门</th>
            <th data-options="field:'name',width:50,formatter:lxDepartureFormatter">离职类型</th>
            <th data-options="field:'time',width:50">离职日期</th>
            <th data-options="field:'czDeparture',width:50,formatter:czDepartureFormatter">操作</th>
        </tr>
    </thead>
</table>
<form id="departureCondition1">
	<div id="departuretb" style="padding:15px 10px;">
		用户名 : <input id="nameSearch5" class="easyui-combobox" type="text" data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'archives/all',panelHeight:'auto',panelMaxHeight:250">
		离职类型:<input id="lztype5" type="text" class="easyui-combobox" data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/departure',panelHeight:'auto',panelMaxHeight:250,editable:false">
		去向 : <input id="where5" type="text" class="easyui-textbox">
		
		<a id="btn" href="javascript:void(0)" onclick="setDepartureCondition();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
		<a id="btn" href="javascript:void(0)" onclick="resetDepartureCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-undo'">Reset</a>
		
		<shiro:hasPermission name="departure:add">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_departure();" data-options="iconCls:'icon-add'">添加</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="departure:dels">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="dels_departure();" data-options="iconCls:'icon-remove'">删除</a>
		</shiro:hasPermission>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#departureTable").datagrid({
			pagination : true,
			toolbar : "#departuretb",
			idField : "id"
		});
	
		
	})
	
	//设置条件
	function setDepartureCondition(){ 
		var postData = { 'archives.id' : $("#nameSearch5").val(),
				 		 'type.id' : $("#lztype5").val(),
				 		  gowhere  : $("#where5").val()
								};
		$("#departureTable").datagrid("reload",postData);
	}

	//清除条件
	function resetDepartureCondition(){
		$("#departureCondition1").form("clear");
	}
	
	//姓名
	function xmDepartureFormatter(value,row,index){
		if(row.archives == null || row.archives.name == null){
			return "-";
		}
		 return row.archives.name; 
	}
	
	//部门
	function bmDepartureFormatter(value,row,index) {
		if(row.archives == null || row.archives.section == null || row.archives.section.text == null){
			return "-";
		}
		return row.archives.section.text;
	}
	//类型
	function lxDepartureFormatter(value,row,index){
		
		if(row.type == null || row.type.typename == null){
			return "-";
		}
		return row.type.typename;
	}
	
	//格式化操作
	function czDepartureFormatter(value,row,index){
		var str = "<a href='javascript:void(0)' onclick='view_departure("+row.id+");' >查看</a>   ";
			<shiro:hasPermission name="departure:edit">
			str += " <a href='javascript:void(0)' onclick='edit_departure("+row.id+");' >修改</a>";
			</shiro:hasPermission>
		return str;
	}
	
	<shiro:hasPermission name="departure:dels">
	//删除选择的数据行
	function dels_departure(){
		//获取要删除的数据黄
		var departureids = $("#departureTable").datagrid("getSelections");
		if(departureids.length == 0){
			$.messager.alert("提示","请选择要删除的数据行！","warning");
			return;
		}
		$.messager.confirm("提示","确定要删除选中的员工及其相关数据吗？",function(d){
			if(d){
				var postData = "";
				$.each(departureids,function(i){
					postData += "ids="+this.id;
					if(i < departureids.length - 1){
						postData += "&";
					}
				});
				$.post("departure/dels",postData,function(data){
					if(data.result == true){
						// 删除成功后，刷新表格 reload
						$("#departureTable").datagrid("reload");
					}
				})
			}
		})
	}
	</shiro:hasPermission>
	
	//查看
	function view_departure(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:600,
			modal:true,
			href : "departure/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("departure/view",{id:id},function(data){
					$("#departureForm77").form("load",data);
					$("#StaffNumer").textbox("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					
					$("#lztype").textbox('setValue',data.type.typename);
					$("#whether").textbox('setValue',data.whether.typename);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	<shiro:hasPermission name="departure:edit">
	//修改
	function edit_departure(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑用户",
			iconCls : "icon-edit",
			width:700,
			height:600,
			modal:true,
			href : "departure/form",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("departure/view",{id:id},function(data){
					$("#departureForm77").form("load",data);
					$("#StaffNumer").combogrid("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					
					
					$("#lztype").combobox('setValue',data.type.id);
					$("#whether").combobox('setValue',data.whether.id);
				});
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#departureForm77").form("submit",{
						url : "departure/edit",
						success : function(data){
							d.dialog("close");
							$("#departureTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	
	<shiro:hasPermission name="departure:add">
	//添加
	function add_departure(){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "新增",
			iconCls : "icon-add",
			width:700,	
			height:580,
			modal:true,//是否是模态框
			href : "departure/form",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#departureForm77").form("submit",{
						url : "departure/add",
						success : function(data){
							d.dialog("close");
							$("#departureTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
</script>
</body>
</html>




