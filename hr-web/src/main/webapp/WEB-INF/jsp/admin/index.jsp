<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>

</head>
<body>
<table id="adminTable"  title="Admin List"
        data-options="url:'user/adminlist',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'usercode',width:50,formatter:userCadeFormatter">用户编码</th>
            <th data-options="field:'username',width:80,sortable:true">账号</th>
            <th data-options="field:'salt',width:50,sortable:true">Salt</th>
            <th data-options="field:'role',width:80,formatter:roleFormatter">角色</th>
            <th data-options="field:'statu',width:50,sortable:true,formatter:statuFormatter">状态</th>
            <shiro:hasPermission name="user:assignadmin">
            <th data-options="field:'cz',width:70,formatter:czFormatter">操作</th>
            </shiro:hasPermission>
           
        </tr>
    </thead>
</table>
<div id="admintb" style="padding:15px 10px;">



	用户编码 : <input type="text" name="" id="admincode" />
	角色: <input id="role" class="easyui-combobox" name="role.id" style="width: auto;min-width: 100px;"  data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'role/all',panelHeight:'auto',panelMaxHeight:250,editable:false,panelMinWidth:150">
	<a id="btn" href="javascript:void(0)" onclick="setCondition();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
	<a id="btn" href="javascript:void(0)" onclick="resetCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-undo'">Reset</a>
	<shiro:hasPermission name="user:addadmin">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_admin();" data-options="iconCls:'icon-add'">添加</a>
	</shiro:hasPermission>
</div>
<script type="text/javascript">
	$(function(){
		$("#adminTable").datagrid({
			pagination : true,
			toolbar : "#admintb",
			idField : "id"
		});
		
		
		
	})
	//格式化角色
	function roleFormatter(value,row,index){
		if(row.roles == null || row.roles.length == 0){
			return "-";
		}
		var str = "";
		for(var i = 0; i < row.roles.length; i++){
			str += row.roles[i].text;
			if(i < row.roles.length -1){
				str += ", ";
			}
		}
		return str;
		
	}
	
	//发送异步请求界面
	function assignPermission(rowId){
	 	$("#adminTable").datagrid("clearSelections");
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title:"角色权限",
			width:350,
			height:310,
			modal:true,
			href : "user/toassign?rid="+rowId, 				
			onClose:function(){$(this).dialog("destroy");},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler : function(){
					var nodes = $("#assignTree").tree("getChecked","checked");
					var half_nodes = $("#assignTree").tree("getChecked","indeterminate");
					$.merge(nodes,half_nodes);
					
					var postData = "";
					for(var i = 0; i<nodes.length;i++){
						postData += "ids="+nodes[i].id + "&";
					}
					postData += "userId="+rowId;
					//发送异步请求
					$.post("user/assignadmin",postData,function(data){
						$.messager.show({
							title:'提示',
							msg:'授权成功！重新登录后生效！',
							timeout:2000,
							showType:'slide'
						});
						d.dialog("close");
						$("#adminTable").datagrid("reload");
						//弹框提示！
					}); 
				}
					 
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler : function(){
					d.dialog("close");
				} 
			}] 
		})  
	} 
	
	
	
	
	//设置条件
	function setCondition(){
		
		var postData = {usercode : $("#admincode").val()};
		/*var ids = $("#roles").combobox("getValues");
		for(var i = 0; i < ids.length ; i++){
			postData["sysRoles["+i+"].id"] = ids[i];
		}*/
		
		$("#adminTable").datagrid("reload",postData);
	}
	
	//添加用户
	function add_admin(){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "添加管理员",
			iconCls : "icon-add",
			width:720,	
			height:250,
			modal:true,//是否是模态框
			href : "user/adminadd",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#adminForm").form("submit",{
						url : "user/toaddadmin",
						success : function(data){
							d.dialog("close");
							$("#adminTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	<shiro:hasPermission name="user:assignadmin">
	//格式化操作
	function czFormatter(value,row,index) {
		return "<a href='javascript:void(0)' onclick='assignPermission("+row.id+")'>分配角色</a>";
	}
	</shiro:hasPermission>
	//
	function userCadeFormatter(value,row,index) {
		<shiro:hasPermission name="user:deladmin">
		return "<a href='javascript:void(0)' onclick='edit_admin("+row.id+");'  >"+value+"</a>";
		</shiro:hasPermission>
		<shiro:lacksPermission name="user:editadmin">
		return value;
		</shiro:lacksPermission>
	}
	
	//修改
	function edit_admin(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑用户",
			iconCls : "icon-edit",
			width:600,
			height:300,
			modal:true,
			href : "user/adminadd",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("user/view",{id:id},function(data){
					$("#adminForm").form("load",data);
				});
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#adminForm").form("submit",{
						url : "user/updataadmin",
						success : function(data){
							d.dialog("close");
							$("#adminTable").datagrid("reload");
						}
					});
				}
			},
			
			<shiro:hasPermission name="user:deladmin">
			{
				iconCls:"icon-remove",
				text:"删除",
				handler:function(){
					$.messager.confirm("提示","确定要删除选中的数据吗？",function(da){
						if(da){
							$.post("user/deladmin",{id:id},function(data){
								if(data.result == true){
									// 删除成功后，刷新表格 reload
									d.dialog("close");
									$("#adminTable").datagrid("reload");
								}
							})
						}
					})
				}
			},
			</shiro:hasPermission>
			{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	//格式化状态
	function statuFormatter(value,row,index){
		if(value == 1){
			return "正常";
		}
			return "锁定";
	}
	
</script>
</body>
</html>




