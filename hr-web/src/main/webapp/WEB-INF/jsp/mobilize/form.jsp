<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="" id="mobilizeForm" method="post">
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
					<td align="center" colspan="4"><b>员工信息</b><input type="hidden" name="id" ><hr/></td>
				</tr>
				<tr>
					<td class="form-item" style="width: 20%"><font color="red">*</font>员工工号：
					</td>
					<td style="width: 30%"><input id="StaffNumer" name="archives.id" type="text"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/></td>
					<td class="form-item" style="width: 20%">员工姓名：</td>
					<td style="width: 30%"><input type="text"id="XingMing" class="easyui-textbox" readonly="readonly"></td>
				</tr>
				<tr>
					<td class="form-item">性别：</td>
					<td><input id="xingbie" class="easyui-textbox" readonly="readonly"  ></td>
					<td class="form-item">员工身份证：</td>
					<td><input id="ShenFen" class="easyui-textbox" type="text"  readonly="readonly" ></td>
				</tr>
				
				
				
				
				<tr>
                    <td colspan="4" style="text-align: center">
                        <strong>调动信息</strong>
                    </td>
                </tr>
                <tr>
                    <td class="form-item">调动标题：
                    </td>
                    <td colspan="3">
                        <input name="title" id="Biaoti" class="easyui-textbox" type="text" data-options="required:true"    style="width:96%;">
                    </td>
                </tr>
                <tr>
                    <td class="form-item">原部门名称： </td>
                    <td><input id="YBmName" class="easyui-combotree" name="sectionameold" type="text" readonly="readonly"    data-options="hasDownArrow:false,animate:true,valueField:'id',textField:'typename',url:'section/list',panelHeight:'auto',panelMaxHeight:250,editable:false,lines:'true'"></td>
                    <td class="form-item">现部门名称：</td>
                    <td><input id="XBmName" class="easyui-combotree" name="sectionamenew" type="text"  data-options="animate:true,valueField:'id',textField:'typename',url:'section/list',panelHeight:'auto',panelMaxHeight:250,editable:false,lines:'true'"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/></td>
                </tr>
                <tr>
                    <td class="form-item">原学历：
                    </td>
                    <td><input id="Yxueli" class="easyui-combobox" name="educationold" type="text" readonly="readonly"   data-options="hasDownArrow:false,valueField:'id',textField:'typename',url:'data/education',panelHeight:'auto',panelMaxHeight:250,editable:false"> </td>
                    <td class="form-item">现学历：</td>
                    <td> <input id="Xxueli" class="easyui-combobox" name="educationnew" type="text" data-options="valueField:'id',textField:'typename',url:'data/education',panelHeight:'auto',panelMaxHeight:250,editable:false"> </td>
                </tr>
                <tr>
                    <td class="form-item">原职称：</td>
                    <td><input id="yzhicheng" class="easyui-combobox" name="zhichengold" type="text" readonly="readonly"   data-options="hasDownArrow:false,url:'data/zhiCheng',valueField:'id',textField:'typename',panelHeight:'auto',panelMaxHeight:250"></td>
                    <td class="form-item">现职称：</td>
                    <td><input id="xzhicheng" class="easyui-combobox" name="zhichengnew" type="text"  data-options="valueField:'id',textField:'typename',url:'data/zhiCheng',panelHeight:'auto',panelMaxHeight:250"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/></td>
                </tr>
                <tr>
                    <td class="form-item">原工种：</td>
                    <td><input id="ygongzhong" class="easyui-combobox" name="employeesworkold" type="text" readonly="readonly"    data-options="hasDownArrow:false,valueField:'id',textField:'typename',url:'data/employeesWork',panelHeight:'auto',panelMaxHeight:250,editable:false"> </td>
                    <td class="form-item">现工种：</td>
                    <td> <input id="xgongzhong" class="easyui-combobox"  name="employeesworknew" type="text"  data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/employeesWork',panelHeight:'auto',panelMaxHeight:250,editable:false"> </td>
                </tr>
                <tr>
                    <td class="form-item">原岗位：
                    </td>
                    <td><input id="yyggw" class="easyui-combobox" name="jobsold" type="text" readonly="readonly"  data-options="hasDownArrow:false,valueField:'id',textField:'typename',url:'data/gangwei',panelHeight:'auto',panelMaxHeight:250"></td>
                    <td class="form-item">现岗位：
                    </td>
                    <td><input id="xyggw" class="easyui-combobox" name="jobsnew" type="text"  data-options="valueField:'id',textField:'typename',url:'data/gangwei',panelHeight:'auto',panelMaxHeight:250"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/> </td>
                </tr>
                <tr>
                    <td class="form-item">原职务：
                    </td>
                    <td><input id="yzhiwu" class="easyui-combobox" name="positionold" type="text"  readonly="readonly"  data-options="hasDownArrow:false,url:'position/all',valueField:'id',textField:'name',panelHeight:'auto',panelMaxHeight:250"></td>
                    <td class="form-item">现职务：
                    </td>
                    <td><input id="xzhiwu" class="easyui-combobox" name="positionnew" type="text"  data-options="valueField:'id',textField:'name',url:'position/all',panelHeight:'auto',panelMaxHeight:250"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/></td>
                </tr>
                <tr>
                    <td class="form-item">原基本工资： </td>
                    <td><input name="qianold" type="text" id="YJbGongZ" class="easyui-textbox" readonly="readonly"> </td>
                    <td class="form-item"><font color="red">*</font>现基本工资：</td>
                    <td><input name="qiannew" type="text"  class="easyui-textbox" id="XjbGongZ" data-options="required:true"  >  </td>
                </tr>
                <tr>
                    <td class="form-item"><font color="red">*</font>调动类型：
                    </td>
                    <td> <input id="DiaoDongLX" class="easyui-combobox" class="easyui-textbox" name="type.id"   data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/ddtype',panelHeight:'auto',panelMaxHeight:250,editable:false"> </td>
                    <td class="form-item"><font color="red">*</font>调动时间：
                    </td>
                    <td>
                        <input name="time" type="date"  id="DiaoDongSj" >
                    </td>
                </tr>
				
				<tr>
					<td class="form-item">调动备注：</td>
					<td colspan="3"><textarea name="remark" rows="2" cols="20"
							id="Beizhu" class="mytextarea" style="height: 60px; width: 96%;"></textarea>
					</td>
				</tr>
				
			</tbody>
		</table>
	</form>
	<script type="text/javascript">
		$(function() {
			
			$('#StaffNumer').combogrid({    
			   panelWidth:160,    
			   // fitColumns:true
			   required:true,
			    idField:'id',    
			    textField:'worknum',    
			    url:'archives/all',    
			    columns:[[    
			        {field:'id',title:'id',width:27},    
			        {field:'worknum',title:'编号',width:80},  
			        {field:'name',title:'姓名',width:50}
			    ]] ,
			    onChange:function(n,o){
				$.post("archives/toarchives",{id:n},function(data){
						$("#XingMing").textbox('setValue',data.name);
						$("#xingbie").textbox('setValue',data.sex.typename);
						$("#ShenFen").textbox('setValue',data.idcard);
						
						$("#YBmName").combotree("setValue",data.section.id);//员工部门
						$("#XBmName").combotree("setValue",data.section.id);//员工部门
						
						$("#Yxueli").combobox("setValue",data.education.id);//原学历
						$("#Xxueli").combobox("setValue",data.education.id);//现学历
						
						$("#yzhicheng").combobox("setValue",data.jobtitle.id);//原职称
						$("#xzhicheng").combobox("setValue",data.jobtitle.id);//现职称
						
						$("#ygongzhong").combobox("setValue",data.employeeswork.id);//原工种
						$("#xgongzhong").combobox("setValue",data.employeeswork.id);//现工种
						
						$("#yyggw").combobox("setValue",data.jobs.id);//原岗位
						$("#xyggw").combobox("setValue",data.jobs.id);//现岗位
						
						$("#yzhiwu").combobox("setValue",data.position.id);//原职务
						$("#xzhiwu").combobox("setValue",data.position.id);//现职务
						
						$("#YJbGongZ").textbox("setValue",data.basicsalary);//原工资
						
					});
				}   
			});  

			
			 
			$('#DiaoDongSj').datebox({required:true});
			
		})
	</script>
</body>
</html>






