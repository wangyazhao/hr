<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="mobilizeTable"  title="Mobilize List"
        data-options="url:'mobilize/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'xm',width:60,formatter:xmMobilizeFormatter">姓名</th>
            <th data-options="field:'title',width:60">调动标题</th>
            <th data-options="field:'namse',width:50,formatter:lxMobilizeFormatter">调动类型</th>
            <th data-options="field:'time',width:50">调动时间</th>
            <th data-options="field:'czMobilize',width:50,formatter:czMobilizeFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<form id="mobilizeCondition1">
	<div id="mobilizetb" style="padding:15px 10px;">
		<div style="padding:0 5px 10px 0;">
		用户名 : <input id="nameSearch5" class="easyui-combobox" name="archives.id" type="text" data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'archives/all',panelHeight:'auto',panelMaxHeight:250">  
		调动标题: <input name="title5" id="Biaoti5" class="easyui-textbox" type="text" data-options=""> 
		调动类型: <input id="DiaoDongLX5" class="easyui-combobox" class="easyui-textbox" data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/ddtype',panelHeight:'auto',panelMaxHeight:250,editable:false"> 
		部门名称: <input id="XBmName5" class="easyui-combotree" type="text" data-options="animate:true,valueField:'id',textField:'typename',url:'section/list',panelHeight:'auto',panelMaxHeight:250,editable:false,lines:'true'"> 
		</div>	
		职     称  : <input id="xzhicheng5" class="easyui-combobox" type="text" data-options="valueField:'id',textField:'typename',url:'data/zhiCheng',panelHeight:'auto',panelMaxHeight:250">
		工     种  : <input id="xgongzhong5" class="easyui-combobox" type="text" data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/employeesWork',panelHeight:'auto',panelMaxHeight:250,editable:false">
		岗     位  : <input id="xyggw5" class="easyui-combobox" type="text" data-options="valueField:'id',textField:'typename',url:'data/gangwei',panelHeight:'auto',panelMaxHeight:250">
		职     务  : <input id="xzhiwu5" class="easyui-combobox" type="text" data-options="valueField:'id',textField:'name',url:'position/all',panelHeight:'auto',panelMaxHeight:250">
		<a id="btn" href="javascript:void(0)" onclick="setMobilizeCondition();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
		<a id="btn" href="javascript:void(0)" onclick="resetMobilizeCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-undo'">Reset</a>
	 	
	 	<shiro:hasPermission name="mobilize:add">
	 	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_mobilize();" data-options="iconCls:'icon-add'">添加</a>
	 	</shiro:hasPermission>
	 	<shiro:hasPermission name="mobilize:dels">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="dels_mobilize();" data-options="iconCls:'icon-remove'">删除</a>
		</shiro:hasPermission>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#mobilizeTable").datagrid({
			pagination : true,
			toolbar : "#mobilizetb",
			idField : "id"
		});
	
		
	})
	
	//设置条件
	function setMobilizeCondition(){
 		var postData = { 'archives.id': $("#nameSearch5").val(),
 								 title: $("#Biaoti5").val(),
 		 				     'type.id': $("#DiaoDongLX5").val(),
 		 				 sectionamenew: $("#XBmName5").val(),
 						   zhichengnew: $("#xzhicheng5").val(),
 					  employeesworknew: $("#xgongzhong5").val(),
 		 					   jobsnew: $("#xyggw5").val(),
 		 				   positionnew: $("#xzhiwu5").val()};  
		$("#mobilizeTable").datagrid("reload",postData);
	}

	//清除条件
	function resetMobilizeCondition(){
		$("#mobilizeCondition1").form("clear");
	}
	
	//姓名
	function xmMobilizeFormatter(value,row,index){
		if(row.archives == null){
			return "-";
		}
		 return row.archives.name; 
	}
	//调动类型
	function lxMobilizeFormatter(value,row,index){
		
		if(row.type == null || row.type.typename == null){
			return "-";
		}
		return row.type.typename;
	}
	
	//格式化操作
	function czMobilizeFormatter(value,row,index){
		var str ="<a href='javascript:void(0)' onclick='view_mobilize("+row.id+");' >查看</a>   ";
			<shiro:hasPermission name="mobilize:edit">
		  	str += " <a href='javascript:void(0)' onclick='edit_mobilize("+row.id+");' >修改</a>";
		  </shiro:hasPermission>
		return str;
	}
	
	<shiro:hasPermission name="mobilize:dels">
	//删除选择的数据行
	function dels_mobilize(){
		//获取要删除的数据黄
		var mobilizeids = $("#mobilizeTable").datagrid("getSelections");
		if(mobilizeids.length == 0){
			$.messager.alert("提示","请选择要删除的数据行！","warning");
			return;
		}
		$.messager.confirm("提示","确定要删除选中的数据及其相关数据吗？",function(d){
			if(d){
				var postData = "";
				$.each(mobilizeids,function(i){
					postData += "ids="+this.id;
					if(i < mobilizeids.length - 1){
						postData += "&";
					}
				});
				$.post("mobilize/dels",postData,function(data){
					if(data.result == true){
						// 删除成功后，刷新表格 reload
						$("#mobilizeTable").datagrid("reload");
					}
				})
			}
		})
	}
	</shiro:hasPermission>
	
	//查看
	function view_mobilize(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:600,
			modal:true,
			href : "mobilize/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("mobilize/view",{id:id},function(data){
					$("#mobilizeForm").form("load",data);
					$("#StaffNumer").textbox("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					
					$("#DiaoDongLX").combobox('setValue',data.type.id);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	<shiro:hasPermission name="mobilize:edit">
	//修改
	function edit_mobilize(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑用户",
			iconCls : "icon-edit",
			width:700,
			height:600,
			modal:true,
			href : "mobilize/form",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("mobilize/view",{id:id},function(data){
					
					$("#mobilizeForm").form("load",data);
					$("#StaffNumer").combogrid("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					
					$("#DiaoDongLX").combobox('setValue',data.type.id);
				});
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#mobilizeForm").form("submit",{
						url : "mobilize/edit",
						success : function(data){
							d.dialog("close");
							$("#mobilizeTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="mobilize:add">
	//添加
	function add_mobilize(){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "新增",
			iconCls : "icon-add",
			width:700,	
			height:600,
			modal:true,//是否是模态框
			href : "mobilize/form",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#mobilizeForm").form("submit",{
						url : "mobilize/add",
						success : function(data){
							d.dialog("close");
							$("#mobilizeTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
</script>
</body>
</html>




