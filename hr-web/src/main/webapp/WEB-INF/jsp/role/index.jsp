<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
	<meta charset="UTF-8">
<title>角色管理</title>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
</head>
<body>
<script type="text/javascript">
	$(function(){
		$("#roleTable").datagrid({
			pagination : true,
		    singleSelect: true
		});
	});
	
	function setCondition(){
		 $("#roleTable").datagrid("reload",{
			text:$("#text2").val()
		 });
	}
	 
	<shiro:hasPermission name ="role:add">
	//添加角色
  	function addCondition(){
  		var d = $("<div></div>").appendTo("body");
  		d.dialog({
			title : "添加",
			iconCls : "icon-add",
			width:500,
			height:300,
			modal:true,
			href:"role/add_form",
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#roleform").form("submit",{
						url : "role/add",
						success : function(data){
							d.dialog("close");
							$("#roleTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		}) 
	}  
  	</shiro:hasPermission>
	
	function userCadeFormatter(value,row,index){
		<shiro:hasPermission name="role:updata">
		return "<a href='javascript:void(0)' onclick='edit_role("+row.id+");'  >"+value+"</a>";
		</shiro:hasPermission>
		<shiro:lacksPermission name="role:updata">
		return value;
		</shiro:lacksPermission>
	}
	 
	<shiro:hasPermission name="role:updata">
	//维护
	function edit_role(id){
		
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑",
			iconCls : "icon-edit",
			width:500,
			height:300,
			modal:true,
			href : "role/edit_form",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				  $.post("role/view",{id:id},function(data){
					console.log(data);
					$("#roleform").form("load",data);
 		  			$("#text").val(data.text);
 					$("#remark").val(data.remark);
 					$("#available").val(data.available); 
				});   
				},
 
				
				buttons:[{
					iconCls:"icon-ok",
					text:"确定",
					handler:function(){
						$("#roleform").form("submit",{
							url : "role/updata",
							success : function(data){
								d.dialog("close");
								$("#roleTable").datagrid("reload");
							}
						});
					}
				},{
					iconCls:"icon-remove",
					text:"删除",
					handler:function(){
						$.messager.confirm("提示","确定要删除选中的数据吗？",function(da){
							if(da){
								$.post("role/del",{id:id},function(data){
									if(data.result == true){
										// 删除成功后，刷新表格 reload
										d.dialog("close");
										$("#roleTable").datagrid("reload");
									}
								})
							}
						})
					}
				},{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						d.dialog("close");
					}
				}]		 
			});
	}
	</shiro:hasPermission>
	
	
	function ztFormatter(value,row,index) {
		if (value == 1) {
			return "正常";
		}
		return "锁定";
	}
	
	<shiro:hasPermission name="role:assign">
	function opFormatter(value,row,index){
		return "<a href='javascript:void(0)' title='分配权限' onclick='assignPermission5("+row.id+");' class='op'><img src='easyui/themes/icons/large_chart.png' width='16'/></a>";
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="role:assign">
	//授权
	function assignPermission5(roleId){
		$("#roleTable").datagrid("clearSelections");	
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "分配权限",
			width:300,
			height:310,
			href : "role/toAssign?rid="+roleId,
			modal:true,
			onClose:function(){$(this).dialog("destroy");},
			
			
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler : function(){
					//获取选中的节点
					var nodes = $("#assignTree5").tree("getChecked","checked");
					var half_nodes = $("#assignTree5").tree("getChecked","indeterminate");
					$.merge(nodes,half_nodes);
					//获取选中节点的编号
					var postData = "";
					for(var i = 0; i<nodes.length;i++){
						postData += "ids="+nodes[i].id + "&";
					}
					postData += "roleId="+roleId;
					//发送异步请求
					$.post("role/assign",postData,function(data){
						$.messager.show({
							title:'提示',
							msg:'授权成功！重新登录后生效！',
							timeout:2000,
							showType:'slide'
						});
						d.dialog("close");
						//弹框提示！
					}); 
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler : function(){
					d.dialog("close");
				}
			}]
			 
			
		});
	}
	</shiro:hasPermission>
	
</script>  


<div class="easyui-panel" style="padding:15px 10px;">
 	快速检索：<input type="text" name="" id="text2" />
	<a id="btn" href="javascript:void(0)" onclick="setCondition();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查询</a>
	<shiro:hasPermission name="role:add">
	<a  href="javascript:void(0)" onclick="addCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-add'">添加</a>
	</shiro:hasPermission>
</div>
<table id="roleTable"  title="Role List"
        data-options="url:'role/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr> 
			<th data-options="field:'id',width:30,sortable:true,order:'desc'">Id</th>
            <th data-options="field:'text',width:100,sortable:true,formatter:userCadeFormatter">名称</th>
            <th data-options="field:'remark',width:200">备注</th>
            <th data-options="field:'available',width:100,formatter:ztFormatter">是否禁用</th> 
            <shiro:hasPermission name="role:assign">
        	<th data-options="field:'world',width:50,formatter:opFormatter">操作</th> 
        	</shiro:hasPermission>    
        </tr>
    </thead>
</table> 


</body>
</html>


 


