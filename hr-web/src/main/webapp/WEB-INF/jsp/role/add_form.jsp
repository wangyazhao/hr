<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<!DOCTYPE html >
<html>
<head> 
<meta charset="UTF-8">
<title>添加角色信息</title>
</head>
<body>
	<form action="" id="roleform" method="post">
		<table border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
					<td style="width: 30%">角色名称:</td>
					<td><input type="text" name="text" class="easyui-validatebox"  /></td>
				</tr>
				<tr>
					<td>备注:</td>
					<td><textarea rows="3" id="remark" name="remark" cols="60"></textarea></td>
				</tr>
				<tr>
					<td>是否禁用:</td>
					<td><input name="available" value="1" class="easyui-switchbutton" data-options="onText:'Yes',offText:'No'"></td>
				</tr>
			</tbody>
		</table>
	</form>
 
<script type="text/javascript">
</script>
</body>
</html>