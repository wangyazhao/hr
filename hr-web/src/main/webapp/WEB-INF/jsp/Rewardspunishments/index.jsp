<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<base href="<%=basePath%>">
	<meta charset="UTF-8">
	<title>Document</title> 
	<script type="text/javascript" src="easyui/jquery.min.js"></script>
	<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
	<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>	
</head>
<body>
	<script type="text/javascript"> 
	$(function(){
		$("#rewardspunishmentsTable").datagrid({
			pagination : true,
			toolbar : "#rewardspunishmentstb",
			idField : "id"
		});
	}); 
   
	  //格式化角色
	function roleFormatter(value,row,index){
		if(row != null && row.attribute != null){
			return row.attribute.typename;
		}else{ 
			return "-";
		}
		
	}  
 	
	<shiro:hasPermission name="rewardspunishments:dels">
	function delete_rewardspunishments(){
		var selRows = $("#rewardspunishmentsTable").datagrid("getSelections");
		if(selRows.length == 0){
			$.messager.alert("提示","请选择要删除的数据行！","warning");
			return;
		}
		$.messager.confirm("提示","确定要删除选中的数据吗？",function(r){
			if(r){
				var postData = "";
				$.each(selRows,function(i){
					postData += "ids=" +this.id;
					if(i < selRows.length - 1){
						postData += "&";
					}
				});
				$.post("rewardspunishments/batchDelete",postData,function(data){
					if(data.result == true){
						$("#rewardspunishmentsTable").datagrid("reload");
					}
				}); 
			}
		});
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="rewardspunishments:add">
	function add_rewardspunishments(){
		var d = $("<div></div>").appendTo("body");
   		 d.dialog({
 			title : "添加奖惩",
			iconCls : "icon-add",
			width:500,
			height:300,
			modal:true,
			href : "rewardspunishments/toadd",
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",	 
				handler:function(){//点击确定按钮的操作
					$("#rewardspunishmentsForm").form("submit",{
						url : "rewardspunishments/add",
						success : function(data){
							d.dialog("close");
							$("#rewardspunishmentsTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
   		 });
		 
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="rewardspunishments:edit">
	function edit_rewardspunishments(){
			var row = $("#rewardspunishmentsTable").datagrid("getSelected");
			if(row == null){
				return;
			}

			//如果选中了多个，只保留row这个
			$("#rewardspunishmentsTable").datagrid("clearSelections");
			$("#rewardspunishmentsTable").datagrid("selectRecord",row.id);
			
			var d = $("<div></div>").appendTo("body");
			d.dialog({
				title : "编辑",
				iconCls : "icon-edit",
				width:500,
				height:300,
				modal:true,
				href : "rewardspunishments/toadd",
				onClose:function(){$(this).dialog("destroy"); },
				onLoad:function(){
					//发送异步请求，查询数据
					$.post("rewardspunishments/view",{id:row.id},function(data){
						$("#rewardspunishmentsForm").form("load",data);
						$("#rewardspunishments_form").combobox("setValue",109);
					});
				},
				buttons:[{
					iconCls:"icon-ok",
					text:"确定",
					handler:function(){
						$("#rewardspunishmentsForm").form("submit",{
							url : "rewardspunishments/edit",
							success : function(data){
								d.dialog("close");
								$("#rewardspunishmentsTable").datagrid("reload");
							}
						});
					}
				},{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						d.dialog("close");
					}
				}]
			});
	}
	</shiro:hasPermission>
	
	</script>

	  <table id="rewardspunishmentsTable"  title="Rewardspunishments List"
	        data-options="url:'rewardspunishments/las',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
	    <thead>
	        <tr>
				<th data-options="field:'tyu',checkbox:true"></th>
				<th data-options="field:'id',width:30,sortable:true,order:'desc'">Id</th>
	            <th data-options="field:'name',width:100,sortable:true">Name</th>
 	            <th data-options="field:'attribute',width:100,sortable:true,formatter:roleFormatter">Available</th>	
 	        </tr>
	    </thead>
	</table>
	<div id="rewardspunishmentstb">
	<shiro:hasPermission name="rewardspunishments:add">
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_rewardspunishments();" data-options="iconCls:'icon-add',plain:true">添加</a>
	</shiro:hasPermission>
	<shiro:hasPermission name="rewardspunishments:edit">
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="edit_rewardspunishments();" data-options="iconCls:'icon-edit',plain:true">修改</a>
	</shiro:hasPermission>
	<shiro:hasPermission name="rewardspunishments:dels">
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="delete_rewardspunishments();" data-options="iconCls:'icon-remove',plain:true">删除</a>
  	</shiro:hasPermission>
  	</div>
</body>
</html>

