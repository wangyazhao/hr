<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html  lang="en">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<form action="" id="sectionForm" method="post">
		<input type="hidden" name="id" />
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
					<td>部门编码 :</td>
					<td><input type="text" id="bmbh" name="code" class="easyui-textbox" data-options="required:true"  /><img src="easyui/themes/icons/large_chart.png" onclick="fenpei()"  title="分配编号" style="vertical-align: middle;"/></td>
				</tr>
				<tr>
					<td>部门名称 : </td>
					<td><input type="text" name="text" class="easyui-textbox" data-options="required:true" /></td>
				</tr>
				<tr>
					<td>上级部门 :</td>
					<td><input id="parent1" class="easyui-combotree" name="parentids" value="0"   data-options="required:true,hasDownArrow:true,animate:true,valueField:'id',textField:'typename',url:'section/listdata',panelHeight:'auto',panelMaxHeight:250,editable:false,lines:'true'"></td>
				</tr>
				<tr>
					<td>部门负责人:</td>
					<td><input type="text" name="principal" class="easyui-textbox" data-options="required:true" /></td>
				</tr>
				<tr>
					<td>部门电话:</td>
					<td><input type="text" name="tel" class="easyui-textbox"/></td>
				</tr>
				<tr>
					<td>部门传真:</td>
					<td><input type="text" name="fax" class="easyui-textbox" /></td>
				</tr>
				<tr>
					<td>部门类型: </td>
					<td><input id="type1" class="easyui-combobox" name="type.id" style="width: auto;min-width: 100px;"  data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/sectionType',panelHeight:'auto',panelMaxHeight:250,editable:false,panelMinWidth:150"></td>
				</tr>
				<tr>
					<td>备注:</td>
					<td>
						<input class="easyui-textbox" name="remark" id="remark" data-options="multiline:true" style="height:60px" width="100%"></input>
					</td>
				</tr>
			</tbody>
		</table>
		
		</form>
		<script type="text/javascript">
		function fenpei(){
			var str = "BM-"
			for(let i = 0 ; i< 8 ; i++){
				str += Math.floor(Math.random()*10);
			}
			$("#bmbh").textbox('setValue',str);
		}
	</script>
</body>
</html>






