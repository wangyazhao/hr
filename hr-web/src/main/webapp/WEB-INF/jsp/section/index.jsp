<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="/common.jsp" %>
</head>
<body>
<div class="easyui-panel" title="部门管理" style="padding:15px 10px;">
	<div style="height: 500px;width: 260px; float: left;">
		<ul id="sectionTree" data-options="url:'section/list',lines:'true',animate:'true'"></ul>
	</div>
	<div style="height: 500px;width: 65%;float: right;">
		<input type="hidden" name="id" id="hidden" />
		
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
					<td align="center" colspan="3"><b id="title">暂未选中任何部门</b><hr/></td>
				</tr>
				<tr>
					<td style="width: 10%"></td>
					<td style="width: 13%">部门编码 :</td>
					<td><span id="code"></span></td>
				</tr>
				<tr>
					<td></td>
					<td>部门名称 :</td>
					<td><span id="name"></span></td>
				</tr>
				<tr>
					<td></td>
					<td>上级部门 :</td>
					<td><span id="parent"></span></td>
				</tr>
				<tr>
					<td></td>
					<td>部门负责人: </td>
					<td><span id="principal"></span></td>
				</tr>
				<tr>
					<td></td>
					<td>部门电话:</td>
					<td><span id="tel"></span></td>
				</tr>
				<tr>
					<td></td>
					<td>部门传真: </td>
					<td><span id="fax"></span></td>
				</tr>
				<tr>
					<td></td>
					<td>部门类型:</td>
					<td><span id="type"></span></td>
				</tr>
				<tr>
					<td></td>
					<td>备注: </td>
					<td><span id="remark"></span></td>
				</tr>
				<tr>
					<td colspan="3"><hr/></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
						<shiro:hasPermission name="section:add">
						<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_section(0);" data-options="iconCls:'icon-add'">添加</a>
						</shiro:hasPermission>
						<span id="operation" >
							<shiro:hasPermission name="section:updata">
							<a href="javascript:void(0)" class="easyui-linkbutton" onclick="edit_section();" data-options="iconCls:'icon-edit'">修改当前部门</a>
							</shiro:hasPermission>
							<shiro:hasPermission name="section:add">
							<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_section(1);" data-options="iconCls:'icon-add'">在此节点下增加</a>
							</shiro:hasPermission>
							<shiro:hasPermission name="section:del">
							<a href="javascript:void(0)" class="easyui-linkbutton" onclick="del_section();" data-options="iconCls:'icon-remove'">删除此部门</a>
							</shiro:hasPermission>
						</span>
					</td>
				</tr>
			</tbody>
		</table>
		
	
			
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#operation").hide();
		$("#sectionTree").tree({
			onClick:function(node){
				$("#hidden").val(node.id);
				$("#operation").show();
				$("#parent").text("-");
				$("#title").text("部门详细信息");
				reloadData(node.id);
				
			}
		});
		
	})
	
	<shiro:hasPermission name="section:add">
	//添加部门
	function add_section(pid) {
		
	
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "添加部门",
			iconCls : "icon-add",
			width:400,	
			height:450,
			modal:true,//是否是模态框
			href : "section/form",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			onLoad:function(){
				if(pid == 1){
					var $addId = $("#hidden").val();
					$("#parent1").combotree("setValue",$addId);
				}
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#sectionForm").form("submit",{
						url : "section/add",
						success : function(data){
							d.dialog("close");
							$("#sectionTree").tree("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>

	<shiro:hasPermission name="section:del">
	//删除
	function del_section(){
		$.messager.confirm("提示","确定要删除该部门以及子部门吗？",function(d){
			if(d){
				var $id = $("#hidden").val();
				$.post("section/delSection",{id:$id},function(data){
					if(data.result == true){
						// 删除成功后，刷新
						$("#title").text("暂未选中任何部门");
						$("#parent").text("");
						$("#code").text("");
						$("#name").text("");
						$("#principal").text("");
						$("#fax").text("");
						$("#tel").text("");
						$("#type").text("");
						$("#remark").text("");
						$("#operation").hide();
						$("#sectionTree").tree("reload");
					}
				})
			}
		})
	}
	</shiro:hasPermission>
	//
	function reloadData(id) {
		$.post("section/view",{id:id},function(data){
			
			if(data.code == null){
				$("#code").text("-");
			}else{
				$("#code").text(data.code);
			}
			
			if(data.text == null){
				$("#name").text("-");
			}else {
				$("#name").text(data.text);
			}
			
			if(data.principal == null){
				$("#principal").text("-");
			}else{
				$("#principal").text(data.principal);
			}
			
			if(data.fax == null){
				$("#fax").text("-");
			}else{
				$("#fax").text(data.fax);
			}
			
			if(data.tel == null){
				$("#tel").text("-");
			}else{
				$("#tel").text(data.tel);
			}
			
			if(data.type.typename == null){
				$("#type").text("-");
			}else{
				$("#type").text(data.type.typename);
			}
			
			if(data.remark == null){
				$("#remark").text("-");
			}else{
				$("#remark").text(data.remark);	
			}
			
			if(data.parent == null || data.parent.text ==null){
				$("#parent").text("-");
			}else{
				$("#parent").text(data.parent.text);
			}
			
			
			
		})
	}
	
	<shiro:hasPermission name="section:updata">
	//修改
	function edit_section(){
		var $id = $("#hidden").val();
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑权限",
			iconCls : "icon-edit",
			width:400,
			height:400,
			modal:true,
			href : "section/form",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("section/view",{id:$id},function(data){
					$("#sectionForm").form("load",data);
					$("#type1").combobox("setValue",data.type.id);
					
				});
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#sectionForm").form("submit",{
						url : "section/updata",
						success : function(data){
							reloadData($id);
							d.dialog("close");
							$("#sectionTree").tree("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
</script>
</body>
</html>




