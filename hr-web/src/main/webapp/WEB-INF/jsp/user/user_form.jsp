<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html  lang="en">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<style type="text/css">
	#userForm p>lable{
		display:inline-block;
		width:100px;
		margin-left:20px;
	}
	#userForm p>lable>span{
		position:absolute;
		margin-left:-15px;
		color: red;
	    font-size: 22px;
	    font-weight: 900;
	    line-height:100%;
	}
	p{
		margin:15px 0px 0px 15px;
		font-size:15px;
	}
</style>
	<form action="" id="userForm" method="post">
		<input type="hidden" name="id" />
		<p>
			<lable><span>*</span>用户编号 : </lable><input type="text" name="usercode" class="easyui-validatebox"  />
		</p>
		<p>
			<lable><span>*</span>账号 : </lable><input type="text" name="username" class="easyui-validatebox"  />
		</p>
		<p>
			<lable>状态 : </lable><input name="statu" value="1" class="easyui-switchbutton" data-options="onText:'Open',offText:'Lock'">
		</p>
		<p>
			<lable>角色: </lable><input id="role1" class="easyui-combobox" name="role.id" style="width: auto;min-width: 100px;"  data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'role/all',panelHeight:'auto',panelMaxHeight:250,editable:false,panelMinWidth:150">
		</p>
		<p>
			<lable>对应员工: </lable><input id="archives" class="easyui-combobox" name="archives.id" style="width: auto;min-width: 100px;"  data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'archives/all',panelHeight:'auto',panelMaxHeight:250,editable:false,panelMinWidth:150">
		</p>
		<hr/>
		<p>
			<lable>部门: </lable><input type="text" readonly="true" name="" id="bumen1" />
		</p>
		<p>
			<lable>职位: </lable><input type="text" readonly="true" name="" id="zhiwei1" />
		</p>
		
		<p>
			<lable>电子邮件: </lable><input type="text" readonly="true" id="email" name="" />
		</p>
		<p>
			<lable>电话: </lable><input type="text" readonly="true" id="tel" />
		</p>
		<hr/>
		<p>
			<lable>备注: </lable><textarea rows="8" id="remark" name="remark" cols="40"></textarea>
		</p>
		<!--<p>
		Roles : <input id="roles_form" class="easyui-tagbox" name="roleIds"
    		data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'role/all',panelHeight:'auto',panelMaxHeight:250,multiple:true,editable:false,panelMinWidth:150">
		</p> -->
	</form>
	<script type="text/javascript">
		$("#archives").combobox({
			onChange:function(n,o){
				$.post("archives/toarchives",{id:n},function(data){
					$("#bumen1").val(data.section.text);
					$("#zhiwei1").val(data.position.name);
					$("#email").val(data.email);
					$("#tel").val(data.tel);
				});
			}
		});
	</script>
</body>
</html>






