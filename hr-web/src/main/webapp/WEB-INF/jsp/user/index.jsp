<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
<link rel="stylesheet" type="text/css" href="easyui/themes/gray/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>

</head>
<body>
<table id="userTable"  title="User List"
        data-options="url:'user/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'usercode',width:50,formatter:userCadeFormatter">用户编码</th>
       		<th data-options="field:'name',width:80,formatter:nameCadeFormatter">用户名</th>
            <th data-options="field:'username',width:80,sortable:true">账号</th>
            <th data-options="field:'archives',width:30,formatter:sexFormatter">性别</th>
            <th data-options="field:'aadt',width:50,formatter:sectionFormatter">部门</th>
            <th data-options="field:'a7t',width:50,formatter:zhiweiFormatter">职位</th>
            <th data-options="field:'role',width:80,formatter:roleFormatter">角色</th>
            <th data-options="field:'tel',width:100,formatter:telFormatter">电话</th>
            <th data-options="field:'statu',width:50,sortable:true,formatter:statuFormatter">状态</th>
            <th data-options="field:'cz',width:70,formatter:czFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<div id="usertb" style="padding:15px 10px;">



	用户编码 : <input type="text" name="" id="usercode" />
	角色: <input id="role" class="easyui-combobox" name="role.id" style="width: auto;min-width: 100px;"  data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'role/all',panelHeight:'auto',panelMaxHeight:250,editable:false,panelMinWidth:150">
	<a id="btn" href="javascript:void(0)" onclick="setCondition();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
	<a id="btn" href="javascript:void(0)" onclick="resetCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-undo'">Reset</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_user();" data-options="iconCls:'icon-add'">添加</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-undo'">导出</a>
</div>
<script type="text/javascript">
	$(function(){
		$("#userTable").datagrid({
			pagination : true,
			toolbar : "#usertb",
			idField : "id"
		});
		
		
		
	})
	
	//设置条件
	function setCondition(){
		
		var postData = {usercode : $("#usercode").val()};
		/*var ids = $("#roles").combobox("getValues");
		for(var i = 0; i < ids.length ; i++){
			postData["sysRoles["+i+"].id"] = ids[i];
		}*/
		
		$("#userTable").datagrid("reload",postData);
	}
	
	//添加用户
	function add_user(){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "添加",
			iconCls : "icon-add",
			width:700,	
			height:450,
			modal:true,//是否是模态框
			href : "user/addForm",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#userForm").form("submit",{
						url : "user/toadd",
						success : function(data){
							d.dialog("close");
							$("#userTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	//格式化操作
	function czFormatter(value,row,index) {
		return "<a href='javascript:void(0)' onclick='assignPermission("+row.id+")'>附加角色</a>";
	}
	
	//授权
	function assignPermission(rowId){
	 	$("#userTable").datagrid("clearSelections");
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title:"角色权限",
			width:350,
			height:310,
			modal:true,
			href : "user/toassign?rid="+rowId, 				
			onClose:function(){$(this).dialog("destroy");},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler : function(){
					var nodes = $("#assignTree").tree("getChecked","checked");
					var half_nodes = $("#assignTree").tree("getChecked","indeterminate");
					$.merge(nodes,half_nodes);
					
					var postData = "";
					for(var i = 0; i<nodes.length;i++){
						postData += "ids="+nodes[i].id + "&";
					}
					postData += "userId="+rowId;
					//发送异步请求
					$.post("user/assign",postData,function(data){
						$.messager.show({
							title:'提示',
							msg:'授权成功！重新登录后生效！',
							timeout:2000,
							showType:'slide'
						});
						d.dialog("close");
						$("#userTable").datagrid("reload");
						//弹框提示！
					}); 
				}
					 
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler : function(){
					d.dialog("close");
				} 
			}] 
		})  
	} 
	//格式化name
	function nameCadeFormatter(value,row,index){
		if(row.archives == null || row.archives.name == null){
			return "-";
		}
		return row.archives.name;
	}
	//格式化角色
	function roleFormatter(value,row,index){
		if(row.roles == null || row.roles.length == 0){
			return "-";
		}
		var str = "";
		for(var i = 0; i < row.roles.length; i++){
			str += row.roles[i].text;
			if(i < row.roles.length -1){
				str += ", ";
			}
		}
		return str;
		
	}
	//格式化部门
	function sectionFormatter(value,row,index){
		if(row.archives == null || row.archives.section == null || row.archives.section.text == null){
			return "-";
		}
			return row.archives.section.text;
	}
	//格式化职位
	function zhiweiFormatter(value,row,index){
		if(row.archives == null || row.archives.position == null || row.archives.position.name == null){
			return "-";
		}
			return row.archives.position.name;
		
	}
	//格式化电话row.archives.details.tel+
	function telFormatter(value,row,index){
		if(row.archives == null || row.archives.tel == null){
			return '-';
		}
		return row.archives.tel;
			
	}
	
	//修改
	function userCadeFormatter(value,row,index) {
		return "<a href='javascript:void(0)' onclick='edit_user("+row.id+");'  >"+value+"</a>";
	}
	
	//维护
	function edit_user(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑用户",
			iconCls : "icon-edit",
			width:700,
			height:450,
			modal:true,
			href : "user/addForm",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("user/view",{id:id},function(data){
					$("#userForm").form("load",data);
					$("#archives").combobox("setValue",data.archives.id); 
					
					$("#zhiwei1").textbox('setValue',data.archives.position.name);
					$("#email").textbox('setValue',data.archives.email);
					$("#tel").textbox('setValue',data.archives.tel);
					
					
					
				});
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#userForm").form("submit",{
						url : "user/updata",
						success : function(data){
							d.dialog("close");
							$("#userTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-remove",
				text:"删除",
				handler:function(){
					$.messager.confirm("提示","确定要删除选中的数据吗？",function(da){
						if(da){
							$.post("user/del",{id:id},function(data){
								if(data.result == true){
									// 删除成功后，刷新表格 reload
									d.dialog("close");
									$("#userTable").datagrid("reload");
								}
							})
						}
					})
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	//格式化性别
	function sexFormatter(value,row,index){
		if(row.sex == 1){
			return "男";
		}
			return "女";
	}
	//格式化状态
	function statuFormatter(value,row,index){
		if(value == 1){
			return "正常";
		}
			return "锁定";
	}
	
</script>
</body>
</html>




