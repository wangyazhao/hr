<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div  id="contractInfoForm" >
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
					<td align="center" colspan="4"><b>员工信息</b><input type="hidden" name="id" ><hr/></td>
				</tr>
				<tr>
					<td class="form-item" style="width: 20%">员工工号：
					</td>
					<td style="width: 30%"><input id="StaffNumer"  type="text"class="easyui-textbox"  readonly="readonly"></td>
					<td class="form-item" style="width: 20%">员工姓名：</td>
					<td style="width: 30%"><input type="text"id="XingMing"class="easyui-textbox"  readonly="readonly"></td>
				</tr>
				<tr>
					<td class="form-item">性别：</td>
					<td><input id="xingbie"class="easyui-textbox"  readonly="readonly" type="text" ></td>
					<td class="form-item">员工身份证：</td>
					<td><input id="ShenFen"class="easyui-textbox"  readonly="readonly" type="text" ></td>
				</tr>
				<tr>
					<td class="form-item">所在部门名称：</td>
					<td colspan="3"><input id="BuMen" class="easyui-textbox"  readonly="readonly" type="text" ></td>
				</tr>
				<tr>
					<td class="form-item">岗位：</td>
					<td><input  id="GangWei" class="easyui-textbox"  readonly="readonly" type="text" ></td>
					<td class="form-item">职务：</td>
					<td><input  id="ZhiWu" class="easyui-textbox"  readonly="readonly" type="text" ></td>
				</tr>
				
				
				
				<tr>
                    <td colspan="4" style="text-align: center"> <strong>合同信息</strong><hr/></td>
                </tr>
                <tr>
                    <td>合同编号： </td>
                    <td>  <input name="contractno" type="text" class="easyui-textbox"  readonly="readonly" >  </td>
                    <td>合同名称：</td>
                    <td> <input name="name" type="text" id="HtMc"class="easyui-textbox"  readonly="readonly" ></td>
                </tr>
                <tr>
                    <td>合同类型： </td>
                    <td><input name="type.id" id="HtLeiXing" class="easyui-textbox"  readonly="readonly"  ></input></td>
                    <td>有无期限：</td>
                    <td><input name="deadline.id" id="QiXian" class="easyui-textbox"  readonly="readonly" ></td>
                </tr>
                <tr>
                   	<td>是否转正：</td>
                   	<td> <input name="positive.id" id="ZhuanZheng" class="easyui-textbox"  readonly="readonly" ></input></td>
                    <td>签约时间：</td>
                    <td><input name="signing" type="text" id="QinYue"  class="easyui-textbox"  readonly="readonly"></td>
                </tr>
                <tr>
                    <td>合同年份：</td>
                    <td><input name="contractyear" id="Nianfen" type="text"  class="easyui-textbox"  readonly="readonly" > </td>
                    <td>合同状态： </td>
                    <td><input name="contractstate.id" id="HtZt"  class="easyui-textbox"  readonly="readonly" ></td>
                 </tr>
                 
                 
                 
                 <tr>
                     <td colspan="4" style="text-align: center"><strong>试用期信息</strong><hr/></td>
                </tr>
                <tr>
                    <td>试用期限： </td>
                    <td> <input name="trialperiod" type="text" id="ShiYongQx" class="easyui-textbox"  readonly="readonly"></td>
                    <td>试用基本工资：</td>
                    <td><input name="trialwage" type="text" value="0" id="ShiYongGz" class="easyui-textbox"  readonly="readonly"></td>
                </tr>
                <tr>
                    <td>试用生效时间：</td>
                    <td><input name="trialoperant" type="text" id="ShiYongSj"  class="easyui-textbox"  readonly="readonly"> </td>
                    <td>试用到期时间：</td>
                    <td> <input name="trialexpire" type="text" id="ShiYongDq"  class="easyui-textbox"  readonly="readonly" > </td>
                </tr>
                
                
                
                <tr>
                    <td colspan="4" style="text-align: center"> <strong>转正信息</strong><hr/></td>
                </tr>
                <tr>
                    <td>合同期限：</td>
                    <td> <input name="contracperiod" type="text" id="HeTongQx"  class="easyui-textbox"  readonly="readonly" > </td>
                    <td>转正基本工资： </td>
                    <td> <input name="positivewage" type="text" value="0" id="HeTongGz"class="easyui-textbox"  readonly="readonly" > </td>
                </tr>
                <tr>
                    <td>生效时间： </td>
                    <td> <input name="positiveoperant" type="text" id="HeTongSj"  class="easyui-textbox"  readonly="readonly"> </td>
                    <td>到期时间：</td>
                    <td> <input name="positiveexpire" type="text" id="HeTongqxsj"  class="easyui-textbox"  readonly="readonly" > </td>
                </tr>
                
                
                
                <tr>
                    <td colspan="4" style="text-align: center"> <strong>详细信息</strong><hr></td>
                </tr>
                <tr>
                    <td>合同内容：</td>
                    <td colspan="3" >
                    	 <textarea name="textc" rows="2" cols="20" id="text"class="easyui-textbox"  readonly="readonly" style="height:124px;width:96%;"></textarea>
                     </td>
                </tr>
                <tr>
                    <td>合同备注：
                    </td>
                    <td colspan="3">
                        <textarea name="remark" rows="2" cols="20" id="HtBeiZhu"class="easyui-textbox"  readonly="readonly" style="height:124px;width:96%;"></textarea>
                    </td>
                </tr>
			</tbody>
		</table>
	</div>
</body>
</html>






