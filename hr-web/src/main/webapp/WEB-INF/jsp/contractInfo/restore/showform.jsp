<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div  id="restoreForm" >
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
                    <td colspan="4" style="text-align: center"> <strong>合同信息</strong><hr/><input id="htid" name="contractInfo.id" type="hidden"> </td>
                   
                </tr>
                <tr>
                    <td style="width: 20%">合同编号：<input type="hidden" name="id" > </td>
                    <td style="width: 30%">  <input name="contractno" id="htno" type="text" class="easyui-textbox"  readonly="readonly">  </td>
                    <td style="width: 20%">合同名称：</td>
                    <td style="width: 30%"> <input name="name" id="htname" type="text"  class="easyui-textbox" readonly="readonly"></td>
                </tr>
				<tr>
                    <td><font color="red">*</font>签约时间：</td>
                    <td colspan="3"><input name="signing" id="htqysj" type="text"class="easyui-textbox"  readonly="readonly"></td>
                </tr>
				
				
				<tr>
					<td align="center" colspan="4"><b>员工信息</b><input type="hidden" name="id" ><hr/></td>
				</tr>
				<tr>
					<td>员工工号：
					</td>
					<td><input id="StaffNumer"  type="text"class="easyui-textbox"  readonly="readonly"></td>
					<td>员工姓名：</td>
					<td><input type="text"id="XingMing"class="easyui-textbox"  readonly="readonly"></td>
				</tr>
				<tr>
					<td>性别：</td>
					<td><input id="xingbie"class="easyui-textbox"  readonly="readonly" type="text" ></td>
					<td>员工身份证：</td>
					<td><input id="ShenFen"class="easyui-textbox"  readonly="readonly" type="text" ></td>
				</tr>
				<tr>
					<td>所在部门名称：</td>
					<td colspan="3"><input id="BuMen" class="easyui-textbox"  readonly="readonly" type="text" ></td>
				</tr>
				<tr>
					<td>岗位：</td>
					<td><input  id="GangWei" class="easyui-textbox"  readonly="readonly" type="text" ></td>
					<td>职务：</td>
					<td><input  id="ZhiWu" class="easyui-textbox"  readonly="readonly" type="text" ></td>
				</tr>
				
				
				<tr>
					<td align="center" colspan="4"><b>变更信息</b><hr/></td>
				</tr>
				
                <tr>
               	 	<td><font color="red">*</font>变更时间：</td>
                    <td><input name="time" type="text" id="bgsj" class="easyui-textbox"  readonly="readonly"></td>
                    <td>合同类型： </td>
                    <td><input id="HtLeiXing" class="easyui-textbox"  readonly="readonly"></input></td>
                </tr>
                    
                 
                <tr>
                    <td>合同备注：
                    </td>
                    <td colspan="3">
                        <textarea name="restoreremark" rows="2" cols="20" id="HtBeiZhu" class="easyui-textbox" style="height:124px;width:96%;"></textarea>
                    </td>
                </tr>
			</tbody>
		</table>
	</div>
</body>
</html>






