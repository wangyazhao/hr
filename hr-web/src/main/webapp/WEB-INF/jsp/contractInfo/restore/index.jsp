<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="restoreTable"  title="Restore List"
        data-options="url:'restore/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'contractno',width:60,formatter:htbhRestoreFormatter">合同编号</th>
            <th data-options="field:'name',width:60,formatter:htRestoreFormatter">合同名称</th>
            <th data-options="field:'xm',width:40,formatter:xmRestoreFormatter">员工姓名</th>
            <th data-options="field:'time',width:40">变更时间</th>
            <th data-options="field:'zt',width:60,formatter:lbRestoreFormatter">变更类别</th>
            <th data-options="field:'czRestore',width:50,formatter:czRestoreFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<div id="restoretb" style="padding: 10px;">
	<shiro:hasPermission name="restore:add">
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_restore();" data-options="iconCls:'icon-add'">添加</a>
	</shiro:hasPermission>
	<shiro:hasPermission name="restore:dels">
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="dels_restore();" data-options="iconCls:'icon-remove'">删除</a>
	</shiro:hasPermission>
</div>




<script type="text/javascript">
	$(function(){
		$("#restoreTable").datagrid({
			pagination : true,
			toolbar : "#restoretb",
			idField : "id",
			queryParams: {
				byid:${param.byid}
			}

			
		});
	
		
	})
	
	//姓名
	function xmRestoreFormatter(value,row,index){
		if(row.contractInfo == null || row.contractInfo.archives == null || row.contractInfo.archives.name == null){
			return "-";
		}
		 return row.contractInfo.archives.name; 
	}
	
	//合同名称
	function htRestoreFormatter(value,row,index){
		if(row.contractInfo == null || row.contractInfo.name == null ){
			return "-";
		}
		 return row.contractInfo.name; 
	}
	
	//合同编号
	function htbhRestoreFormatter(value,row,index){
		if(row.contractInfo == null || row.contractInfo.contractno == null ){
			return "-";
		}
		 return row.contractInfo.contractno; 
	}
	
	//变更类别
	function lbRestoreFormatter(value,row,index) {
		if(row.type == null || row.type.typename == null){
			return "-";
		}
		return row.type.typename;
	}
	
	//部门
	function bmRestoreFormatter(value,row,index) {
		if(row.contractInfo == null || row.contractInfo.archives == null || row.contractInfo.archives.section == null || row.contractInfo.archives.section.text == null){
			return "-";
		}
		 return row.contractInfo.archives.section.text; 
	}
	
	
	//格式化操作
	function czRestoreFormatter(value,row,index){
		var str="<a href='javascript:void(0)' onclick='view_restore("+row.id+");' >查看</a> ";
		 return str
	}
	
	<shiro:hasPermission name="restore:add">
	//添加
	function add_restore(){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "新增",
			iconCls : "icon-add",
			width:700,	
			height:540,
			modal:true,//是否是模态框
			href : "restore/form",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("contractInfo/view",{id:${param.byid}},function(data){
					$("#restoreForm").form("load",data);
					$("#htid").val(data.id);
					$("#StaffNumer").textbox("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					
				})
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#restoreForm").form("submit",{
						url : "restore/add",
						success : function(data){
							d.dialog("close");
							$("#restoreTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="restore:dels">
	//删除选择的数据行
	function dels_restore(){
		//获取要删除的数据黄
		var restoreids = $("#restoreTable").datagrid("getSelections");
		if(restoreids.length == 0){
			$.messager.alert("提示","请选择要删除的数据行！","warning");
			return;
		}
		$.messager.confirm("提示","确定要删除选中的数据吗？",function(d){
			if(d){
				var postData = "";
				$.each(restoreids,function(i){
					postData += "ids="+this.id;
					if(i < restoreids.length - 1){
						postData += "&";
					}
				});
				$.post("restore/dels",postData,function(data){
					if(data.result == true){
						// 删除成功后，刷新表格 reload
						$("#restoreTable").datagrid("reload");
					}
				})
			}
		})
	}
	</shiro:hasPermission>
	
	//查看
	function view_restore(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:600,
			modal:true,
			href : "restore/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("restore/view",{id:id},function(data){
					$("#restoreForm").form("load",data);
					$("#htid").val(data.contractInfo.id);
					$("#htno").textbox("setValue",data.contractInfo.contractno);
					$("#htname").textbox('setValue',data.contractInfo.name);
					$("#htqysj").textbox('setValue',data.contractInfo.signing);
					
					$("#StaffNumer").textbox("setValue",data.contractInfo.archives.id);//id
					$("#XingMing").textbox('setValue',data.contractInfo.archives.name);
					$("#xingbie").textbox('setValue',data.contractInfo.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.contractInfo.archives.idcard);
					$("#BuMen").textbox('setValue',data.contractInfo.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.contractInfo.archives.position.name);
					$("#GangWei").textbox('setValue',data.contractInfo.archives.jobs.typename);
					
					$("#HtLeiXing").textbox('setValue',data.type.typename);
				})
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	//修改
	function edit_restore(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑用户",
			iconCls : "icon-edit",
			width:700,
			height:600,
			modal:true,
			href : "restore/form",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("restore/view",{id:id},function(data){
					$("#restoreForm").form("load",data);
					$("#htid").val(data.contractInfo.id);
					$("#htno").textbox("setValue",data.contractInfo.contractno);
					$("#htname").textbox('setValue',data.contractInfo.name);
					$("#htqysj").textbox('setValue',data.contractInfo.signing);
					
					$("#StaffNumer").textbox("setValue",data.contractInfo.archives.id);//id
					$("#XingMing").textbox('setValue',data.contractInfo.archives.name);
					$("#xingbie").textbox('setValue',data.contractInfo.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.contractInfo.archives.idcard);
					$("#BuMen").textbox('setValue',data.contractInfo.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.contractInfo.archives.position.name);
					$("#GangWei").textbox('setValue',data.contractInfo.archives.jobs.typename);
					
					$("#HtLeiXing").combobox('setValue',data.type.id);
				})
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#restoreForm").form("submit",{
						url : "restore/edit",
						success : function(data){
							d.dialog("close");
							$("#restoreTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	
	
	
</script>
</body>
</html>




