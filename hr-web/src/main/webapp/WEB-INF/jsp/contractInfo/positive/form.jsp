<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="" id="positiveForm" method="post">
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
                    <td colspan="4" style="text-align: center"> <strong>合同信息</strong><hr/><input id="htid" name="contractInfo.id" type="hidden"> </td>
                   
                </tr>
                <tr>
                    <td style="width: 20%">合同编号：<input type="hidden" name="id" > </td>
                    <td style="width: 30%">  <input name="contractno" id="htno" type="text" class="easyui-textbox"  readonly="readonly">  </td>
                    <td style="width: 20%">合同名称：</td>
                    <td style="width: 30%"> <input name="name" id="htname" type="text"  class="easyui-textbox" readonly="readonly"></td>
                </tr>
				<tr>
                    <td><font color="red">*</font>签约时间：</td>
                    <td colspan="3"><input name="signing" id="htqysj" type="text"class="easyui-textbox"  readonly="readonly"></td>
                </tr>
			
				<tr>
					<td align="center" colspan="4"><b>员工信息</b><hr/></td>
				</tr>
				<tr>
					<td><font color="red">*</font>员工工号：</td>
					<td><input id="StaffNumer" type="text" class="easyui-textbox" readonly="readonly"></td>
					<td>员工姓名：</td>
					<td><input type="text"id="XingMing" class="easyui-textbox" readonly="readonly"></td>
				</tr>
				<tr>
					<td>性别：</td>
					<td><input id="xingbie" class="easyui-textbox"  ></td>
					<td>员工身份证：</td>
					<td><input id="ShenFen" class="easyui-textbox" type="text"  readonly="readonly" ></td>
				</tr>
				<tr>
					<td>所在部门名称：</td>
					<td colspan="3"><input class="easyui-textbox" type="text" id="BuMen" readonly="readonly" ></td>
				</tr>
				<tr>
					<td>岗位：</td>
					<td><input class="easyui-textbox" type="text" id="GangWei" readonly="readonly" ></td>
					<td>职务：</td>
					<td><input class="easyui-textbox" type="text" id="ZhiWu" readonly="readonly" ></td>
				</tr>
				
				<tr>
					<td align="center" colspan="4"><b>合同转正信息</b><hr/></td>
				</tr>
				
                <tr>
               	 	<td><font color="red">*</font>转正时间：</td>
                    <td><input name="time" type="date" id="bgsj"></td>
                    <td>转正类别： </td>
                    <td><input name="type.id" id="HtLeiXing" class="easyui-combobox"   data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/positivetype',panelHeight:'auto',panelMaxHeight:250,editable:false"></input></td>
                </tr>
                    
                 
                <tr>
                    <td>合同内容：
                    </td>
                    <td colspan="3">
                        <textarea name="positiveremark" rows="2" cols="20" id="HtBeiZhu" class="easyui-textbox" style="height:124px;width:96%;"></textarea>
                    </td>
                </tr>
			</tbody>
		</table>
	</form>
	<script type="text/javascript">
	$(function(){
		$('#bgsj').datebox({required:true});
	})
			 
	</script>
</body>
</html>






