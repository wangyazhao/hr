<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="" id="contractInfoForm" method="post">
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
					<td align="center" colspan="4"><b>员工信息</b><input type="hidden" name="id" ><hr/></td>
				</tr>
				<tr>
					<td  style="width: 20%"><font color="red">*</font>员工工号：
					</td>
					<td style="width: 30%"><input id="StaffNumer" name="archives.id" type="text"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/></td>
					<td  style="width: 20%">员工姓名：</td>
					<td style="width: 30%"><input type="text"id="XingMing10" class="easyui-textbox" readonly="readonly"></td>
				</tr>
				<tr>
					<td>性别：</td>
					<td><input id="xingbie10" class="easyui-textbox"  ></td>
					<td>员工身份证：</td>
					<td><input id="ShenFen" class="easyui-textbox" type="text"  readonly="readonly" ></td>
				</tr>
				<tr>
					<td>所在部门名称：</td>
					<td colspan="3"><input class="easyui-textbox" type="text" id="BuMen" readonly="readonly" ></td>
				</tr>
				<tr>
					<td>岗位：</td>
					<td><input class="easyui-textbox" type="text" id="GangWei" readonly="readonly" ></td>
					<td>职务：</td>
					<td><input class="easyui-textbox" type="text" id="ZhiWu" readonly="readonly" ></td>
				</tr>
				
				
				
				
				<tr>
                    <td colspan="4" style="text-align: center"> <strong>合同信息</strong><hr/></td>
                </tr>
                <tr>
                    <td>合同编号： </td>
                    <td>  <input name="contractno" type="text" id="HtBianHao" class="easyui-textbox" data-options="required:true"> <img src="easyui/themes/icons/large_chart.png" onclick="fenpei()"  title="分配编号" style="vertical-align: middle;"/> </td>
                    <td>合同名称：</td>
                    <td> <input name="name" type="text" id="HtMc" class="easyui-textbox" data-options="required:true"></td>
                </tr>
                <tr>
                    <td>合同类型： </td>
                    <td><input name="type.id" id="HtLeiXing" class="easyui-combobox"   data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/contractType',panelHeight:'auto',panelMaxHeight:250,editable:false"></input></td>
                    <td>有无期限：</td>
                    <td><input name="deadline.id" id="QiXian" class="easyui-combobox"   data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/without',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
                </tr>
                <tr>
                   	<td>是否转正：</td>
                   	<td> <input name="positive.id" id="ZhuanZheng" class="easyui-combobox"   data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/whether',panelHeight:'auto',panelMaxHeight:250,editable:false"></input></td>
                    <td><font color="red">*</font>签约时间：</td>
                    <td><input name="signing" type="date" id="QinYue"></td>
                </tr>
                <tr>
                    <td>合同年份：</td>
                    <td><input name="contractyear" id="Nianfen" type="text"> </td>
                    <td>合同状态： </td>
                    <td><input name="contractstate.id" id="HtZt" class="easyui-combobox"   data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/contractState',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
                 </tr>
                 
                 
                 
                 <tr>
                     <td colspan="4" style="text-align: center"><strong>试用期信息</strong><hr/></td>
                </tr>
                <tr>
                    <td>试用期限： </td>
                    <td> <input name="trialperiod" type="text" id="ShiYongQx"  class="easyui-textbox"></td>
                    <td><font color="red">*</font>试用基本工资：</td>
                    <td><input name="trialwage" type="text" value="0" id="ShiYongGz"  class="easyui-textbox"data-options="required:true"></td>
                </tr>
                <tr>
                    <td><font color="red">*</font>试用生效时间：</td>
                    <td><input name="trialoperant" type="date" id="ShiYongSj" > </td>
                    <td><font color="red">*</font>试用到期时间：</td>
                    <td> <input name="trialexpire" type="date" id="ShiYongDq"  > </td>
                </tr>
                
                
                
                <tr>
                    <td colspan="4" style="text-align: center"> <strong>转正信息</strong><hr/></td>
                </tr>
                <tr>
                    <td>合同期限：</td>
                    <td> <input name="contracperiod" type="text" id="HeTongQx"  > </td>
                    <td><font color="red">*</font>转正基本工资： </td>
                    <td> <input name="positivewage" type="text" value="0" id="HeTongGz" class="easyui-textbox" > </td>
                </tr>
                <tr>
                    <td><font color="red">*</font>生效时间： </td>
                    <td> <input name="positiveoperant" type="date" id="HeTongSj" > </td>
                    <td><font color="red">*</font>到期时间：</td>
                    <td> <input name="positiveexpire" type="date" id="HeTongqxsj"  > </td>
                </tr>
                
                
                
                <tr>
                    <td colspan="4" style="text-align: center"> <strong>详细信息</strong><hr></td>
                </tr>
                <tr>
                    <td>合同内容：</td>
                    <td colspan="3" >
                    	 <textarea name="textc" rows="2" cols="20" id="text" class="easyui-textbox" style="height:124px;width:96%;"></textarea>
                     </td>
                </tr>
                <tr>
                    <td>合同备注：
                    </td>
                    <td colspan="3">
                        <textarea name="remark" rows="2" cols="20" id="HtBeiZhu" class="easyui-textbox" style="height:124px;width:96%;"></textarea>
                    </td>
                </tr>
			</tbody>
		</table>
	</form>
	<script type="text/javascript">
	function fenpei(){
		var str = "HT-"
		for(let i = 0 ; i< 8 ; i++){
			str += Math.floor(Math.random()*10);
		}
		$("#HtBianHao").textbox('setValue',str);
	}
		$(function() {
			
			$('#StaffNumer').combogrid({    
			   panelWidth:160,    
			   // fitColumns:true
			   required:true,
			    idField:'id',    
			    textField:'worknum',    
			    url:'archives/all',    
			    columns:[[    
			        {field:'id',title:'id',width:27},    
			        {field:'worknum',title:'编号',width:80},  
			        {field:'name',title:'姓名',width:50}
			    ]] ,
			    onChange:function(n,o){
				$.post("archives/toarchives",{id:n},function(data){
						$("#XingMing10").textbox('setValue',data.name);
						$("#xingbie10").textbox('setValue',data.sex.typename);
						$("#ShenFen").textbox('setValue',data.idcard);
						$("#BuMen").textbox('setValue',data.section.text);
						$("#ZhiWu").textbox('setValue',data.position.name);
						$("#GangWei").textbox('setValue',data.jobs.typename);
					});
				}   
			});  

			
			$('#HeTongqxsj').datebox({required:true});
			$('#HeTongSj').datebox({required:true});
			$('#ShiYongSj').datebox({required:true});
			$('#ShiYongDq').datebox({required:true});
			$('#QinYue').datebox({required:true});
			$('#Nianfen').datebox();
			$('#HeTongQx').datebox();
		})
	</script>
</body>
</html>






