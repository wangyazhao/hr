<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="contractInfoTable"  title="ContractInfo List"
        data-options="url:'contractInfo/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'contractno',width:60">合同编号</th>
            <th data-options="field:'name',width:60">合同名称</th>
            <th data-options="field:'xm',width:50,formatter:xmContractInfoFormatter">员工姓名</th>
            <th data-options="field:'operant',width:50,formatter:bmContractInfoFormatter">员工部门</th>
            <th data-options="field:'expire',width:50,formatter:ztContractInfoFormatter">合同状态</th>
            <th data-options="field:'zt',width:30,formatter:zzContractInfoFormatter">是否转正</th>
            <th data-options="field:'czContractInfo',width:50,formatter:czContractInfoFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<form id="contractInfoCondition1">
	<div id="contractInfotb" style="padding:15px 10px;">
		<div id="titleAssessInfotb" style="padding:10px 10px 15px 0;">
		合同编号: <input type="text" id="HtBianHao5" class="easyui-textbox" data-options="">   
		用户名 : <input id="nameSearch5" class="easyui-combobox" name="archives.id" type="text" data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'archives/all',panelHeight:'auto',panelMaxHeight:250">
		合同名称:<input type="text" id="HtMc5" class="easyui-textbox" data-options="">
		合同状态:<input id="HtZt5" class="easyui-combobox"   data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/contractState',panelHeight:'auto',panelMaxHeight:250,editable:false">
		</div>
		是否转正:<input id="ZhuanZheng5" class="easyui-combobox"   data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/whether',panelHeight:'auto',panelMaxHeight:250,editable:false"></input>
		<a id="btn" href="javascript:void(0)" onclick="setContractInfoCondition();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
		<a id="btn" href="javascript:void(0)" onclick="resetContractInfoCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-undo'">Reset</a>
		<shiro:hasPermission name="contractInfo:add">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_contractInfo();" data-options="iconCls:'icon-add'">添加</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="contractInfo:dels">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="dels_contractInfo();" data-options="iconCls:'icon-remove'">删除</a>
		</shiro:hasPermission>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#contractInfoTable").datagrid({
			pagination : true,
			toolbar : "#contractInfotb",
			idField : "id"
		});
	
		
	})
	
	//设置条件
	function setContractInfoCondition(){ 
		var postData = {contractno : $("#HtBianHao5").val(),
						'archives.id' : $("#nameSearch5").val(),
						name : $("#HtMc5").val(),
						'contractstate.id' : $("#HtZt5").val(),
						'positive.id' : $("#ZhuanZheng5").val()
							};
		$("#contractInfoTable").datagrid("reload",postData);
	}

	//清除条件
	function resetContractInfoCondition(){
		$("#contractInfoCondition1").form("clear");
	}
   
	//姓名
	function xmContractInfoFormatter(value,row,index){
		if(row.archives == null || row.archives.name == null){
			return "-";
		}
		 return row.archives.name; 
	}
	
	//状态
	function ztContractInfoFormatter(value,row,index) {
		if(row.contractstate == null || row.contractstate.typename == null){
			return "-";
		}
		return row.contractstate.typename;
	}
	
	//部门
	function bmContractInfoFormatter(value,row,index) {
		if(row.archives == null || row.archives.section == null || row.archives.section.text == null){
			return "-";
		}
		 return row.archives.section.text; 
	}
	
	//是否转正
	function zzContractInfoFormatter(value,row,index){
		if(row.positive == null || row.positive.typename == null){
			return "-";
		}
		return row.positive.typename;
	}
	
	//格式化操作
	function czContractInfoFormatter(value,row,index){
		var str="<a href='javascript:void(0)' onclick='view_contractInfo("+row.id+");' >查看</a> ";
		if(row.contractstate != null && row.contractstate.id == 71){
			<shiro:hasPermission name="restore:index">
			str += " <a href='javascript:void(0)' onclick='restore_contractInfo("+row.id+");' >恢复</a>   ";
			</shiro:hasPermission>
		}else {
			<shiro:hasPermission name="removeInfo:index">
			str += " <a href='javascript:void(0)' onclick='remove_contractInfo("+row.id+");' >解除</a>   ";
			</shiro:hasPermission>
		}
		<shiro:hasPermission name="positive:index">
		if(row.positive != null && row.positive.id == 1){
			str += " 转正   ";
		}else if (row.positive != null && row.positive.id == 2) {
			str += " <a href='javascript:void(0)' onclick='positive_contractInfo("+row.id+");' >转正</a>   ";
		}	
		</shiro:hasPermission>
			<shiro:hasPermission name="renewInfo:index">
			str += " <a href='javascript:void(0)' onclick='renewInfo_contractInfo("+row.id+");' >续签</a>   ";
			</shiro:hasPermission>
			<shiro:hasPermission name="changeInfo:index">
			str += " <a href='javascript:void(0)' onclick='change_contractInfo("+row.id+");' >变更</a>   ";
			</shiro:hasPermission>
			<shiro:hasPermission name="contractInfo:edit">
		    str += " <a href='javascript:void(0)' onclick='edit_contractInfo("+row.id+");' >修改</a>";
		    </shiro:hasPermission>
		 return str
	}
	
	<shiro:hasPermission name="contractInfo:dels">
	//删除选择的数据行
	function dels_contractInfo(){
		//获取要删除的数据黄
		var contractInfoids = $("#contractInfoTable").datagrid("getSelections");
		if(contractInfoids.length == 0){
			$.messager.alert("提示","请选择要删除的数据行！","warning");
			return;
		}
		$.messager.confirm("提示","确定要删除选中的数据吗？",function(d){
			if(d){
				var postData = "";
				$.each(contractInfoids,function(i){
					postData += "ids="+this.id;
					if(i < contractInfoids.length - 1){
						postData += "&";
					}
				});
				$.post("contractInfo/dels",postData,function(data){
					if(data.result == true){
						// 删除成功后，刷新表格 reload
						$("#contractInfoTable").datagrid("reload");
					}
				})
			}
		})
	}
	</shiro:hasPermission>
	
	//查看
	function view_contractInfo(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:600,
			modal:true,
			href : "contractInfo/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("contractInfo/view",{id:id},function(data){
					$("#contractInfoForm").form("load",data);
					$("#StaffNumer").textbox("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					
					$("#HtLeiXing").textbox('setValue',data.type.typename);
					$("#QiXian").textbox('setValue',data.deadline.typename);
					$("#ZhuanZheng").textbox('setValue',data.positive.typename);
					$("#HtZt").textbox('setValue',data.contractstate.typename);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		})
	}
	
	
	<shiro:hasPermission name="renewInfo:index">
	//续签
	function renewInfo_contractInfo(id){
		
		$("#contractInfoTable").datagrid("clearSelections");
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "续签",
			iconCls : "icon-edit",
			width:900,
			height:600,
			modal:true,
			href : "renewInfo/index?byid="+id,
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
					$("#contractInfoTable").datagrid("reload");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="positive:index">
	//转正
	function positive_contractInfo(id){
		
		$("#contractInfoTable").datagrid("clearSelections");
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "转正",
			iconCls : "icon-edit",
			width:900,
			height:600,
			modal:true,
			href : "positive/index?byid="+id,
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
					$("#contractInfoTable").datagrid("reload");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	
	<shiro:hasPermission name="restore:index">
	//恢复
	function restore_contractInfo(id){
		
		$("#contractInfoTable").datagrid("clearSelections");
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "恢复",
			iconCls : "icon-edit",
			width:900,
			height:600,
			modal:true,
			href : "restore/index?byid="+id,
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
					$("#contractInfoTable").datagrid("reload");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	
	<shiro:hasPermission name="removeInfo:index">
	//解除
	function remove_contractInfo(id){
		
		$("#contractInfoTable").datagrid("clearSelections");
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "解除",
			iconCls : "icon-edit",
			width:900,
			height:600,
			modal:true,
			href : "removeInfo/index?byid="+id,
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
					$("#contractInfoTable").datagrid("reload");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="changeInfo:index">
	//变更
	function change_contractInfo(id){
		
		$("#contractInfoTable").datagrid("clearSelections");
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "变更",
			iconCls : "icon-edit",
			width:900,
			height:600,
			modal:true,
			href : "changeInfo/index?byid="+id,
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
					$("#contractInfoTable").datagrid("reload");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	
	<shiro:hasPermission name="contractInfo:edit">
	//修改
	function edit_contractInfo(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑",
			iconCls : "icon-edit",
			width:700,
			height:600,
			modal:true,
			href : "contractInfo/form",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("contractInfo/view",{id:id},function(data){
					$("#contractInfoForm").form("load",data);
					$("#StaffNumer").combogrid("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					
					$("#HtLeiXing").combobox('setValue',data.type.id);
					$("#QiXian").combobox('setValue',data.deadline.id);
					$("#ZhuanZheng").combobox('setValue',data.positive.id);
					$("#HtZt").combobox('setValue',data.contractstate.id);
				});
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#contractInfoForm").form("submit",{
						url : "contractInfo/edit",
						success : function(data){
							d.dialog("close");
							$("#contractInfoTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="contractInfo:add">
	//添加
	function add_contractInfo(){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "新增",
			iconCls : "icon-add",
			width:700,	
			height:540,
			modal:true,//是否是模态框
			href : "contractInfo/form",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#contractInfoForm").form("submit",{
						url : "contractInfo/add",
						success : function(data){
							d.dialog("close");
							$("#contractInfoTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
</script>
</body>
</html>




