<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="reinstatementTable"  title="Reinstatement List"
        data-options="url:'reinstatement/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'xm',width:60,formatter:xmReinstatementFormatter">员工姓名</th>
            <th data-options="field:'number',width:60,formatter:bmReinstatementFormatter">员工部门</th>
            <th data-options="field:'name',width:50,formatter:lxReinstatementFormatter">复职类型</th>
            <th data-options="field:'time',width:50">复职日期</th>
            <th data-options="field:'czReinstatement',width:50,formatter:czReinstatementFormatter">操作</th>
        </tr>
    </thead>
</table>
<script type="text/javascript">
	$(function(){
		$("#reinstatementTable").datagrid({
			pagination : true,
			toolbar : "#reinstatementtb",
			idField : "id",
			queryParams: {
				'archives.id':${sessionScope.login_user.archives.id } 
			}
		});
	
		
	})
	
	
	//姓名
	function xmReinstatementFormatter(value,row,index){
		if(row.archives == null || row.archives.name == null){
			return "-";
		}
		 return row.archives.name; 
	}
	
	//部门
	function bmReinstatementFormatter(value,row,index) {
		if(row.archives == null || row.archives.section == null || row.archives.section.text == null){
			return "-";
		}
		return row.archives.section.text;
	}
	//类型
	function lxReinstatementFormatter(value,row,index){
		
		if(row.type == null || row.type.typename == null){
			return "-";
		}
		return row.type.typename;
	}
	
	//格式化操作
	function czReinstatementFormatter(value,row,index){
		var str ="<a href='javascript:void(0)' onclick='view_reinstatement("+row.id+");' >查看</a>";
		return str;
		
	}
	
	
	//查看
	function view_reinstatement(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:540,
			modal:true,
			href : "reinstatement/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("reinstatement/view",{id:id},function(data){
					$("#reinstatementForm").form("load",data);
					$("#StaffNumer").textbox("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					
					$("#fuzhitype").textbox('setValue',data.type.typename);
					$("#whether").textbox('setValue',data.whether.typename);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	
	
</script>
</body>
</html>




