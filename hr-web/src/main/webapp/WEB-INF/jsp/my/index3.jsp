<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="titleAssessInfoTable"  title="TitleAssessInfo List"
        data-options="url:'titleAssessInfo/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'xm',width:80,formatter:xmTitleAssessInfoFormatter">姓名</th>
            <th data-options="field:'qdzc',width:80,formatter:qdzcTitleAssessInfoFormatter">取得职称</th>
            <th data-options="field:'time',width:30">取得时间</th>
            <th data-options="field:'qdfs',width:50,formatter:qdfsTitleAssessInfoFormatter">取得方式</th>
            <th data-options="field:'xcsbzc',width:50,formatter:xcsbzcTitleAssessInfoFormatter">下次申报职称</th>
            <th data-options="field:'nexttime',width:30">下次申报时间</th>
            <th data-options="field:'czTitleAssessInfo',width:50,formatter:czTitleAssessInfoFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<script type="text/javascript">
	$(function(){
		$("#titleAssessInfoTable").datagrid({
			pagination : true,
			toolbar : "#titleAssessInfotb",
			idField : "id",
			queryParams: {
				'archives.id':${sessionScope.login_user.archives.id } 
			}
		});
	
		
	})
 
	
	//姓名
	function xmTitleAssessInfoFormatter(value,row,index){
		if(row.archives == null){
			return "-";
		}
		 return row.archives.name; 
	}
	//取得职称
	function qdzcTitleAssessInfoFormatter(value,row,index){
		if( row.way.typename == null){
			return "-";
		}
		return row.name.typename;
	}
	//取得方式
	function qdfsTitleAssessInfoFormatter(value,row,index){
		if( row.way.typename == null){
			return "-";
		}
		return row.way.typename
	}
	//下次申报职称
	function xcsbzcTitleAssessInfoFormatter(value,row,index){
		if( row.nextname.typename == null){
			return "-";
		}
		return row.nextname.typename;
	}
	
	//格式化操作
	function czTitleAssessInfoFormatter(value,row,index){
		var str ="<a href='javascript:void(0)' onclick='view_titleAssessInfo("+row.id+");' >查看</a>  "
		return str;
	}
	
	
	//查看
	function view_titleAssessInfo(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:600,
			modal:true,
			href : "titleAssessInfo/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("titleAssessInfo/view",{id:id},function(data){
					$("#titleAssessInfoForm").form("load",data);
					$("#StaffNumer").textbox("setValue",data.archives.worknum);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					$("#ZhiCheng").textbox('setValue',data.name.typename);
					$("#way").textbox('setValue',data.way.typename);
					$("#XCZhiCheng").textbox('setValue',data.nextname.typename);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	
</script>
</body>
</html>




