<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<div class="easyui-panel" title="设置查询条件" style="padding:15px 10px;">
	<a id="btn" href="javascript:void(0)" onclick="show1();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">合同信息</a>
	<a id="btn" href="javascript:void(0)" onclick="show2();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">职称评定</a>
	<a id="btn" href="javascript:void(0)" onclick="show3();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">证照资料</a>
	<a id="btn" href="javascript:void(0)" onclick="show4();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">奖惩记录</a>
	<a id="btn" href="javascript:void(0)" onclick="show5();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">调动记录</a>
	<a id="btn" href="javascript:void(0)" onclick="show6();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">离职记录</a>
	<a id="btn" href="javascript:void(0)" onclick="show7();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">复职记录</a>
<hr style="margin-top: 15px;"/>
<form action="" id="archivesForm" method="post">
	<div  style="width:810px;margin:0 auto;">
	<table  border="0"   cellpadding="5" cellspacing="0" width="800px">
		<tbody>
			<tr>
				<td align="center" colspan="4" style="height: 26px"><b>基本信息</b><hr/>
					<input name="id"  type="hidden"  >
				</td>
			</tr>
			<tr>
				<td align="right" align="right" style="width: 15%">工号：</td>
				<td><input name="worknum" class="easyui-textbox" readonly="readonly" type="text"></td>
				<td align="right" style="width: 15%">姓名：</td>
				<td><input name="name" class="easyui-textbox" readonly="readonly" type="text" id="XingMing15"  ></td>
			</tr>
			<tr>
				<td align="right">性别：</td>
				<td><input id="xingbie15" class="easyui-textbox" readonly="readonly"></td>
				<td align="right">出生年月：</td>
				<td><input name="birthday"  type= "text"  class="easyui-textbox" readonly="readonly"   id="ChuSheng15" ></td>
			</tr>
			<tr>
				<td align="right">籍贯：</td>
				<td><input id="guanji15" class="easyui-textbox" readonly="readonly" /></td>
				<td align="right">民族：</td>
				<td><input id="minzu15" class="easyui-textbox" readonly="readonly"/></td>
			</tr>
			<tr>
				<td align="right">婚姻状况：</td>
				<td><input id="hunyin15" class="easyui-textbox" readonly="readonly" ></td>
				<td align="right">政治面貌：</td>
				<td><input id="ZhengZhi15" class="easyui-textbox" readonly="readonly"></td>
			</tr>
			<tr>
				<td align="right">健康状况：</td>
				<td><input name="health" type="text" class="easyui-textbox" readonly="readonly" ></td>
				<td align="right">身份证号：</td>
				<td><input name="idcard" type="text" class="easyui-textbox" readonly="readonly" ></td>
			</tr>
			
			
			
			<tr>
				<td colspan="4" style="text-align: center"><strong>个人情况</strong><hr/>
				</td>
			</tr>
			<tr>
				<td align="right">参加工作时间：</td>
				<td><input id="cjgzsj15" name="joinworktime" class="easyui-textbox" readonly="readonly"   type="text"  ></td>
				<td align="right">工作年限：</td>
				<td><input id="gznx15" name="workyear" class="easyui-textbox" readonly="readonly" type="text" ></td>
			</tr>
			<tr>
				<td align="right">学历：</td>
				<td><input id="xueli15" class="easyui-textbox" readonly="readonly"></td>
				<td align="right">毕业院校：</td>
				<td><input  name="finishschool" class="easyui-textbox" readonly="readonly"type="text"></td>
			</tr>
			<tr>
				<td align="right">所学专业：</td>
				<td><input id="suoxuezhuanye15" name="major"class="easyui-textbox" readonly="readonly" type="text" ></td>
				<td align="right">毕业时间：</td>
				<td><input id="biyeshijian15"  name="graduatetime"  class="easyui-textbox" readonly="readonly" type="text"   ></td>
			</tr>
			<tr>
				<td align="right">所学外语：</td>
				<td><input id="sxwaiyu15" name="courseslanguage"class="easyui-textbox" readonly="readonly" type="text" ></td>
				<td align="right">外语等级：</td>
				<td><input id="waiyuDJ15" name="languagedcale"class="easyui-textbox" readonly="readonly" type="text" ></td>
			</tr>
			<tr>
				<td align="right">计算机掌握程度：</td>
				<td><input id="jsjzwcd15" name="masterdegree" class="easyui-textbox" readonly="readonly" type="text"  ></td>
				<td align="right">计算机等级：</td>
				<td><input id="jsjDJ15" name="computerdcale" class="easyui-textbox" readonly="readonly" type="text"  ></td>
			</tr>
			<tr>
				<td align="right">人才分类：</td>
				<td><input id="rencaiFL15" class="easyui-textbox" readonly="readonly" ></td>
				<td align="right">人才等级：</td>
				<td><input id="rencaiDJ15" class="easyui-textbox" readonly="readonly"  ></td>
			</tr>
			<tr>
				<td align="right">所获证书：</td>
				<td colspan="3"><input id="shZhengShu15" name="certificate" class="easyui-textbox" readonly="readonly" type="text"></td>
			</tr>
			<tr>
				<td align="right">教育简历：</td>
				<td colspan="3"><input id="jxJianLi15" name="educationresume" class="easyui-textbox" readonly="readonly" type="text"></td>
			</tr>
			<tr>
				<td align="right">特长：</td>
				<td><input id="techang15" name="specialty" class="easyui-textbox" readonly="readonly" type="text" ></td>
				<td align="right">爱好：</td>
				<td><input id="aihao15" name="hobbys" class="easyui-textbox" readonly="readonly" type="text" ></td>
			</tr>
			<tr>
				<td align="right">电子邮件：</td>
				<td><input id="dianziyoujian15" name="email" class="easyui-textbox" readonly="readonly" ></td>
				<td align="right">联系地址：</td>
				<td><input id="dizhi15" name="address" class="easyui-textbox" readonly="readonly"  type="text" ></td>
			</tr>
			<tr>
				<td align="right">手机号：</td>
				<td><input id="tel115" name="tel" class="easyui-textbox" readonly="readonly" ></td>
				<td align="right">邮政编码：</td>
				<td><input id="youBian15" name="zipcode" class="easyui-textbox" readonly="readonly" type="text" ></td>
			</tr>
			<tr>
				<td align="right">身高：</td>
				<td><input id="shengao15" name="height" class="easyui-textbox" readonly="readonly" type="text" ></td>
				<td align="right">体重：</td>
				<td><input id="tizhong15" name="weight" class="easyui-textbox" readonly="readonly" type="text" ></td>
			</tr>
			<tr>
				<td align="right">血型：</td>
				<td><input id="xiexing15" name="bloodtype" class="easyui-textbox" readonly="readonly" type="text" ></td>
				<td align="right">对工作要求：</td>
				<td><input id="gzyqiu15" name="workrequire" class="easyui-textbox" readonly="readonly" type="text" ></td>
			</tr>
			
			
			
			
			<tr>
				<td colspan="4" style="text-align: center"><strong>单位信息</strong><hr/>
				</td>
			</tr>
			<tr>
				<td align="right">入职日期：</td>
				<td><input  id="ruzhiDate15" class="easyui-textbox" readonly="readonly"  type="text"></td>
				<td align="right">员工部门：</td>
				<td><input id="ygbm15" class="easyui-textbox" readonly="readonly" ></td>
			</tr>
			<tr>
				<td align="right">员工岗位：</td>
				<td><input id="yggw15" class="easyui-textbox" readonly="readonly" /></td>
				<td align="right">员工工种：</td>
				<td><input id="gongzhong15" class="easyui-textbox" readonly="readonly"></td>
			</tr>
			<tr>
				<td align="right">员工职位：</td>
				<td><input id="zhiwu155" class="easyui-textbox" readonly="readonly" >  </td>
				<td align="right">员工职称：</td>
				<td><input id="zhicheng15" class="easyui-textbox" readonly="readonly" > </td>
			</tr>
			<tr>
				<td align="right">基本工资：</td>
				<td><input id="JbGongZ15" name="basicsalary"  class="easyui-textbox" readonly="readonly" type="text"></td>
				<td align="right">员工状态：</td>
				<td><input id="YGztai15" class="easyui-textbox" readonly="readonly" name="status.id"  ></td>
			</tr>
			<tr>
				<td align="right">工资卡开户行：</td>
				<td><input id="bank15" class="easyui-textbox" readonly="readonly" name="bank.id" ></td>
				<td align="right">工资卡帐号：</td>
				<td><input id="ZhangHao15" name="account"  class="easyui-textbox" readonly="readonly" type="text" ></td>
			</tr>
			
			
			<tr>
				<td colspan="4" style="text-align: center"><strong>其他信息</strong><hr/>
				</td>
			</tr>
			<tr>
				<td align="right">社会关系：</td>
				<td colspan="3"><textarea name="socialrelations" class="easyui-textbox" readonly="readonly" rows="2" cols="20"id="Shgx15" style="height: 66px; width: 96%;"></textarea></td>
			</tr>
			<tr>
				<td align="right">工作经历：</td>
				<td colspan="3"><textarea name="workexperience" class="easyui-textbox" readonly="readonly" rows="2" cols="20"id="Gzjl15" style="height: 66px; width: 96%;"></textarea></td>
			</tr>
			<tr>
				<td align="right">学习经历：</td>
				<td colspan="3"><textarea name="learningexperience" class="easyui-textbox" readonly="readonly" rows="2" cols="20"id="Xxjl15" style="height: 66px; width: 96%;"></textarea></td>
			</tr>
			<tr>
				<td align="right">备注：</td>
				<td colspan="3"><textarea name="remark" rows="2" class="easyui-textbox" readonly="readonly" cols="20" id="BeiZhu"15 style="height: 66px; width: 96%;"></textarea></td>
			</tr>
		</tbody>
	</table>
	</div>
</form>
</div>
<script type="text/javascript">
$(function(){
	$.post("archives/view",{id:  ${sessionScope.login_user.archives.id } },function(data){
		$("#archivesForm").form("load",data);
		
		$("#xingbie15").textbox("setValue",data.sex.typename);//性别
		$("#guanji15").textbox("setValue",data.nativeplace.typename);//籍贯
		$("#minzu15").textbox("setValue",data.national.typename );//民族
		$("#hunyin15").textbox("setValue",data.marital.typename );//婚姻状况
		$("#ZhengZhi15").textbox("setValue",data.politicalface.typename );//政治面貌
		
		
		$("#xueli15").textbox("setValue",data.education.typename );//学历
		$("#rencaiFL15").textbox("setValue",data.talenttype.typename);//人才分类
		$("#rencaiDJ15").textbox("setValue",data.talentlevel.typename);//人才等级
		
		
		$("#ruzhiDate15").textbox('setValue',data.entrytime);//入职日期
		$("#ygbm15").textbox("setValue",data.section.text);//员工部门
		$("#yggw15").textbox("setValue",data.jobs.typename);//员工岗位
		$("#gongzhong15").textbox("setValue",data.employeeswork.typename);//员工工种
		$("#zhiwu155").textbox("setValue",data.position.name);//员工职位
		$("#zhicheng15").textbox("setValue",data.jobtitle.typename);//员工职称
		$("#YGztai15").textbox("setValue",data.status.typename);//员工状态
		$("#bank15").textbox("setValue",data.bank.typename);//工资卡开户行
		
		
	});
	
	
	
	
})
function show1() {
		//查看
			var d = $("<div></div>").appendTo("body");
			d.dialog({
				title : "查看",
				iconCls : "icon-back",
				width:1200,
				height:700,
				modal:true,
				href : "user/my2",
				onClose:function(){$(this).dialog("destroy"); },
				buttons:[{
					iconCls:"icon-cancel",
					text:"关闭",
					handler:function(){
						d.dialog("close");
					}
				}]
			})
	}
function show2() {
	//查看
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:1200,
			height:700,
			modal:true,
			href : "user/my3",
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		})
}
function show3() {
	//查看
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:1200,
			height:700,
			modal:true,
			href : "user/my4",
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		})
}
function show4() {
	//查看
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:1200,
			height:700,
			modal:true,
			href : "user/my5",
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		})
}
function show5() {
	//查看
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:1200,
			height:700,
			modal:true,
			href : "user/my6",
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		})
}
function show6() {
	//查看
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:1200,
			height:700,
			modal:true,
			href : "user/my7",
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		})
}
function show7() {
	//查看
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:1200,
			height:700,
			modal:true,
			href : "user/my8",
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		})
}
</script>
</body>
</html>






