<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="rewardsPunishmentsInfoTable"  title="RewardsPunishmentsInfo List"
        data-options="url:'rewardsPunishmentsInfo/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'xm',width:60,formatter:xmRewardsPunishmentsInfoFormatter">员工姓名</th>
            <th data-options="field:'jcxm',width:60,formatter:jcxmRewardsPunishmentsInfoFormatter">奖惩项目</th>
            <th data-options="field:'attribute',width:50">奖惩属性</th>
            <th data-options="field:'time',width:50">奖惩时间</th>
            <th data-options="field:'czRewardsPunishmentsInfo',width:50,formatter:czRewardsPunishmentsInfoFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<script type="text/javascript">
	$(function(){
		$("#rewardsPunishmentsInfoTable").datagrid({
			pagination : true,
			toolbar : "#rewardsPunishmentsInfotb",
			idField : "id",
			queryParams: {
				'archives.id':${sessionScope.login_user.archives.id } 
			}
		});
	
		
	})
	
	//姓名
	function xmRewardsPunishmentsInfoFormatter(value,row,index){
		if(row.archives == null){
			return "-";
		}
		 return row.archives.name; 
	}
	//奖惩项目
	function jcxmRewardsPunishmentsInfoFormatter(value,row,index){
		
		if( row.jcxiangmu == null || row.jcxiangmu.name == null){
			return "-";
		}
		return row.jcxiangmu.name;
	}
	
	//格式化操作
	function czRewardsPunishmentsInfoFormatter(value,row,index){
		var str = "<a href='javascript:void(0)' onclick='view_rewardsPunishmentsInfo("+row.id+");' >查看</a>  ";
		return  str;
	}
	
	
	//查看
	function view_rewardsPunishmentsInfo(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:600,
			modal:true,
			href : "rewardsPunishmentsInfo/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("rewardsPunishmentsInfo/view",{id:id},function(data){
					$("#rewardsPunishmentsInfoForm5").form("load",data);
					$("#StaffNumer19").textbox("setValue",data.archives.id);//id
					$("#XingMing19").textbox('setValue',data.archives.name);
					$("#xingbie19").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen19").textbox('setValue',data.archives.idcard);
					$("#BuMen19").textbox('setValue',data.archives.section.text);
					$("#ZhiWu19").textbox('setValue',data.archives.position.name);
					$("#GangWei19").textbox('setValue',data.archives.jobs.typename);
					
					
					$("#jiangcheng19").textbox('setValue',data.jcxiangmu.name);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	
</script>
</body>
</html>




