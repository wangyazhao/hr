<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="mobilizeTable"  title="Mobilize List"
        data-options="url:'mobilize/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'xm',width:60,formatter:xmMobilizeFormatter">姓名</th>
            <th data-options="field:'title',width:60">调动标题</th>
            <th data-options="field:'namse',width:50,formatter:lxMobilizeFormatter">调动类型</th>
            <th data-options="field:'time',width:50">调动时间</th>
            <th data-options="field:'czMobilize',width:50,formatter:czMobilizeFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<script type="text/javascript">
	$(function(){
		$("#mobilizeTable").datagrid({
			pagination : true,
			toolbar : "#mobilizetb",
			idField : "id",
			queryParams: {
				'archives.id':${sessionScope.login_user.archives.id } 
			}
		});
	
		
	})
	
	
	//姓名
	function xmMobilizeFormatter(value,row,index){
		if(row.archives == null){
			return "-";
		}
		 return row.archives.name; 
	}
	//调动类型
	function lxMobilizeFormatter(value,row,index){
		
		if(row.type == null || row.type.typename == null){
			return "-";
		}
		return row.type.typename;
	}
	
	//格式化操作
	function czMobilizeFormatter(value,row,index){
		var str ="<a href='javascript:void(0)' onclick='view_mobilize("+row.id+");' >查看</a>   ";
		return str;
	}
	
	
	//查看
	function view_mobilize(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:600,
			modal:true,
			href : "mobilize/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("mobilize/view",{id:id},function(data){
					$("#mobilizeForm").form("load",data);
					$("#StaffNumer").textbox("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					
					$("#DiaoDongLX").combobox('setValue',data.type.id);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
</script>
</body>
</html>




