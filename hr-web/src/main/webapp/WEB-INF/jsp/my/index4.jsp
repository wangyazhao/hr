<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="certificateInfoTable"  title="CertificateInfo List"
        data-options="url:'certificateInfo/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'xm',width:60,formatter:xmCertificateInfoFormatter">姓名</th>
            <th data-options="field:'number',width:60">证照编号</th>
            <th data-options="field:'name',width:50">证照名称</th>
            <th data-options="field:'operant',width:50">生效日期</th>
            <th data-options="field:'expire',width:50">到期日期</th>
            <th data-options="field:'zt',width:30,formatter:ztCertificateInfoFormatter">状态</th>
            <th data-options="field:'czCertificateInfo',width:50,formatter:czCertificateInfoFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<script type="text/javascript">
	$(function(){
		$("#certificateInfoTable").datagrid({
			pagination : true,
			toolbar : "#certificateInfotb",
			idField : "id",
			queryParams: {
				'archives.id':${sessionScope.login_user.archives.id } 
			}
		}); 
	})
	
	//姓名
	function xmCertificateInfoFormatter(value,row,index){
		if(row.archives == null || row.archives.name == null){
			return "-";
		}
		 return row.archives.name; 
	}
	//状态
	function ztCertificateInfoFormatter(value,row,index){
		
		if( row.state.typename == null){
			return "-";
		}
		return row.state.typename;
	}
	
	//格式化操作
	function czCertificateInfoFormatter(value,row,index){
		var str ="<a href='javascript:void(0)' onclick='view_certificateInfo("+row.id+");' >查看</a>";
		return   str;
	}
	
	//查看
	function view_certificateInfo(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:600,
			modal:true,
			href : "certificateInfo/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("certificateInfo/view",{id:id},function(data){
					$("#certificateInfoForm").form("load",data);
					$("#StaffNumer").textbox("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					
					$("#Leixing").textbox('setValue',data.type.typename);
					$("#FzJigou").textbox('setValue',data.certificate.typename);
					$("#Zhuantai").textbox('setValue',data.state.typename);
					$("#XianZhi").textbox('setValue',data.periodstate.typename);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	
	
</script>
</body>
</html>




