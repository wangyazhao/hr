<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="contractInfoTable"  title="ContractInfo List"
        data-options="url:'contractInfo/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'contractno',width:40">合同编号</th>
            <th data-options="field:'name',width:40">合同名称</th>
            <th data-options="field:'xm',width:40,formatter:xmContractInfoFormatter">员工姓名</th>
            <th data-options="field:'operant',width:40,formatter:bmContractInfoFormatter">员工部门</th>
            <th data-options="field:'expire',width:40,formatter:ztContractInfoFormatter">合同状态</th>
            <th data-options="field:'zt',width:40,formatter:zzContractInfoFormatter">是否转正</th>
            <th data-options="field:'czContractInfo',width:100,formatter:czContractInfoFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<script type="text/javascript">
	$(function(){
		$("#contractInfoTable").datagrid({
			pagination : true,
			toolbar : "#contractInfotb",
			idField : "id",
			queryParams: {
				'archives.id': ${sessionScope.login_user.archives.id } 
			}
				
		});
	
		
	})
	
   
	//姓名
	function xmContractInfoFormatter(value,row,index){
		if(row.archives == null || row.archives.name == null){
			return "-";
		}
		 return row.archives.name; 
	}
	
	//状态
	function ztContractInfoFormatter(value,row,index) {
		if(row.contractstate == null || row.contractstate.typename == null){
			return "-";
		}
		return row.contractstate.typename;
	}
	
	//部门
	function bmContractInfoFormatter(value,row,index) {
		if(row.archives == null || row.archives.section == null || row.archives.section.text == null){
			return "-";
		}
		 return row.archives.section.text; 
	}
	
	//是否转正
	function zzContractInfoFormatter(value,row,index){
		if(row.positive == null || row.positive.typename == null){
			return "-";
		}
		return row.positive.typename;
	}
	
	//格式化操作
	function czContractInfoFormatter(value,row,index){
		var str="<a href='javascript:void(0)' onclick='view_contractInfo("+row.id+");' >查看</a> ";
			str += " <a href='javascript:void(0)' onclick='restore_contractInfo("+row.id+");' >恢复记录</a>   ";
			str += " <a href='javascript:void(0)' onclick='remove_contractInfo("+row.id+");' >解除记录</a>   ";
			str += " <a href='javascript:void(0)' onclick='positive_contractInfo("+row.id+");' >转正记录</a>   ";
			str += " <a href='javascript:void(0)' onclick='renewInfo_contractInfo("+row.id+");' >续签记录</a>   ";
			str += " <a href='javascript:void(0)' onclick='change_contractInfo("+row.id+");' >变更记录</a>   ";
		 return str
	}
	
	//查看
	function view_contractInfo(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:600,
			modal:true,
			href : "contractInfo/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("contractInfo/view",{id:id},function(data){
					$("#contractInfoForm").form("load",data);
					$("#StaffNumer").textbox("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					
					$("#HtLeiXing").textbox('setValue',data.type.typename);
					$("#QiXian").textbox('setValue',data.deadline.typename);
					$("#ZhuanZheng").textbox('setValue',data.positive.typename);
					$("#HtZt").textbox('setValue',data.contractstate.typename);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		})
	}
	
	
	//续签
	function renewInfo_contractInfo(id){
		
		$("#contractInfoTable").datagrid("clearSelections");
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "续签",
			iconCls : "icon-edit",
			width:900,
			height:600,
			modal:true,
			href : "renewInfo/index?byid="+id,
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
					$("#contractInfoTable").datagrid("reload");
				}
			}]
		});
	}
	
	//转正
	function positive_contractInfo(id){
		
		$("#contractInfoTable").datagrid("clearSelections");
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "转正",
			iconCls : "icon-edit",
			width:900,
			height:600,
			modal:true,
			href : "positive/index?byid="+id,
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
					$("#contractInfoTable").datagrid("reload");
				}
			}]
		});
	}
	
	
	//恢复
	function restore_contractInfo(id){
		
		$("#contractInfoTable").datagrid("clearSelections");
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "恢复",
			iconCls : "icon-edit",
			width:900,
			height:600,
			modal:true,
			href : "restore/index?byid="+id,
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
					$("#contractInfoTable").datagrid("reload");
				}
			}]
		});
	}
	
	
	//解除
	function remove_contractInfo(id){
		
		$("#contractInfoTable").datagrid("clearSelections");
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "解除",
			iconCls : "icon-edit",
			width:900,
			height:600,
			modal:true,
			href : "removeInfo/index?byid="+id,
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
					$("#contractInfoTable").datagrid("reload");
				}
			}]
		});
	}
	
	//变更
	function change_contractInfo(id){
		
		$("#contractInfoTable").datagrid("clearSelections");
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "变更",
			iconCls : "icon-edit",
			width:900,
			height:600,
			modal:true,
			href : "changeInfo/index?byid="+id,
			onClose:function(){$(this).dialog("destroy"); },
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
					$("#contractInfoTable").datagrid("reload");
				}
			}]
		});
	}
	
	
</script>
</body>
</html>




