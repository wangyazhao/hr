<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="departureTable"  title="Departure List"
        data-options="url:'departure/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'xm',width:60,formatter:xmDepartureFormatter">员工姓名</th>
            <th data-options="field:'number',width:60,formatter:bmDepartureFormatter">员工部门</th>
            <th data-options="field:'name',width:50,formatter:lxDepartureFormatter">离职类型</th>
            <th data-options="field:'time',width:50">离职日期</th>
            <th data-options="field:'czDeparture',width:50,formatter:czDepartureFormatter">操作</th>
        </tr>
    </thead>
</table>
<script type="text/javascript">
	$(function(){
		$("#departureTable").datagrid({
			pagination : true,
			toolbar : "#departuretb",
			idField : "id",
			queryParams: {
				'archives.id':${sessionScope.login_user.archives.id } 
			}
		});
	
		
	})
	
	//姓名
	function xmDepartureFormatter(value,row,index){
		if(row.archives == null || row.archives.name == null){
			return "-";
		}
		 return row.archives.name; 
	}
	
	//部门
	function bmDepartureFormatter(value,row,index) {
		if(row.archives == null || row.archives.section == null || row.archives.section.text == null){
			return "-";
		}
		return row.archives.section.text;
	}
	//类型
	function lxDepartureFormatter(value,row,index){
		
		if(row.type == null || row.type.typename == null){
			return "-";
		}
		return row.type.typename;
	}
	
	//格式化操作
	function czDepartureFormatter(value,row,index){
		return "<a href='javascript:void(0)' onclick='view_departure("+row.id+");' >查看</a> ";
	}
	
	//查看
	function view_departure(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:600,
			modal:true,
			href : "departure/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("departure/view",{id:id},function(data){
					$("#departureForm").form("load",data);
					$("#StaffNumer").textbox("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					
					$("#lztype").textbox('setValue',data.type.typename);
					$("#whether").textbox('setValue',data.whether.typename);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	
</script>
</body>
</html>




