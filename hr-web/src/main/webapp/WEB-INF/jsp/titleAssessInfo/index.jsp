<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="titleAssessInfoTable"  title="TitleAssessInfo List"
        data-options="url:'titleAssessInfo/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'xm',width:80,formatter:xmTitleAssessInfoFormatter">姓名</th>
            <th data-options="field:'qdzc',width:80,formatter:qdzcTitleAssessInfoFormatter">取得职称</th>
            <th data-options="field:'time',width:30">取得时间</th>
            <th data-options="field:'qdfs',width:50,formatter:qdfsTitleAssessInfoFormatter">取得方式</th>
            <th data-options="field:'xcsbzc',width:50,formatter:xcsbzcTitleAssessInfoFormatter">下次申报职称</th>
            <th data-options="field:'nexttime',width:30">下次申报时间</th>
            <th data-options="field:'czTitleAssessInfo',width:50,formatter:czTitleAssessInfoFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<form id="titleAssessInfoCondition1">
	<div id="titleAssessInfotb" style="padding:15px 10px;">
		用 户 名  :<input id="xingming5" class="easyui-combobox" data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'archives/all',panelHeight:'auto',panelMaxHeight:250"> 
		获得职称:<input id="ZhiCheng5" class="easyui-combobox" data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/zhiCheng',panelHeight:'auto',panelMaxHeight:250"> 
		获得方式:<input id="way5" class="easyui-combobox" name="way.id"   data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/way',panelHeight:'auto',panelMaxHeight:250,editable:false">
 		<a id="btn" href="javascript:void(0)" onclick="setTitleAssessInfoCondition();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
		<a id="btn" href="javascript:void(0)" onclick="resetTitleAssessInfoCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-undo'">Reset</a>
 		<shiro:hasPermission name="titleAssessInfo:add">
 		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_titleAssessInfo();" data-options="iconCls:'icon-add'">添加</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="titleAssessInfo:dels">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="dels_titleAssessInfo();" data-options="iconCls:'icon-remove'">删除</a>
		</shiro:hasPermission>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#titleAssessInfoTable").datagrid({
			pagination : true,
			toolbar : "#titleAssessInfotb",
			idField : "id"
		});
	
		
	})
 
	//设置条件
	function setTitleAssessInfoCondition(){ 
		var postData = {'archives.id':$("#xingming5").val(),
						'name.id': $("#ZhiCheng5").val(),
						'way.id' : $("#way5").val() 
						};
		$("#titleAssessInfoTable").datagrid("reload",postData);
	}

	//清除条件
	function resetTitleAssessInfoCondition(){
		$("#titleAssessInfoCondition1").form("clear");
	}
	
	//姓名
	function xmTitleAssessInfoFormatter(value,row,index){
		if(row.archives == null){
			return "-";
		}
		 return row.archives.name; 
	}
	//取得职称
	function qdzcTitleAssessInfoFormatter(value,row,index){
		if( row.way.typename == null){
			return "-";
		}
		return row.name.typename;
	}
	//取得方式
	function qdfsTitleAssessInfoFormatter(value,row,index){
		if( row.way.typename == null){
			return "-";
		}
		return row.way.typename
	}
	//下次申报职称
	function xcsbzcTitleAssessInfoFormatter(value,row,index){
		if( row.nextname.typename == null){
			return "-";
		}
		return row.nextname.typename;
	}
	
	//格式化操作
	function czTitleAssessInfoFormatter(value,row,index){
		var str ="<a href='javascript:void(0)' onclick='view_titleAssessInfo("+row.id+");' >查看</a>  "
			<shiro:hasPermission name="titleAssessInfo:edit">
			str +="<a href='javascript:void(0)' onclick='edit_titleAssessInfo("+row.id+");' >修改</a>";
			</shiro:hasPermission>
		return str;
	}
	
	<shiro:hasPermission name="titleAssessInfo:dels">
	//删除选择的数据行
	function dels_titleAssessInfo(){
		//获取要删除的数据黄
		var titleAssessInfoids = $("#titleAssessInfoTable").datagrid("getSelections");
		if(titleAssessInfoids.length == 0){
			$.messager.alert("提示","请选择要删除的数据行！","warning");
			return;
		}
		$.messager.confirm("提示","确定要删除选中的员工及其相关数据吗？",function(d){
			if(d){
				var postData = "";
				$.each(titleAssessInfoids,function(i){
					postData += "ids="+this.id;
					if(i < titleAssessInfoids.length - 1){
						postData += "&";
					}
				});
				$.post("titleAssessInfo/dels",postData,function(data){
					if(data.result == true){
						// 删除成功后，刷新表格 reload
						$("#titleAssessInfoTable").datagrid("reload");
					}
				})
			}
		})
	}
	</shiro:hasPermission>
	
	//查看
	function view_titleAssessInfo(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:600,
			modal:true,
			href : "titleAssessInfo/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("titleAssessInfo/view",{id:id},function(data){
					$("#titleAssessInfoForm").form("load",data);
					$("#StaffNumer").textbox("setValue",data.archives.worknum);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					$("#ZhiCheng").textbox('setValue',data.name.typename);
					$("#way").textbox('setValue',data.way.typename);
					$("#XCZhiCheng").textbox('setValue',data.nextname.typename);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	<shiro:hasPermission name="titleAssessInfo:edit">
	//修改
	function edit_titleAssessInfo(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑用户",
			iconCls : "icon-edit",
			width:700,
			height:600,
			modal:true,
			href : "titleAssessInfo/form",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("titleAssessInfo/view",{id:id},function(data){
					$("#titleAssessInfoForm").form("load",data);
					$("#StaffNumer").combogrid("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					$("#ZhiCheng").combobox('setValue',data.name.id);
					$("#way").combobox('setValue',data.way.id);
					$("#XCZhiCheng").combobox('setValue',data.nextname.id);
				});
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#titleAssessInfoForm").form("submit",{
						url : "titleAssessInfo/edit",
						success : function(data){
							d.dialog("close");
							$("#titleAssessInfoTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="titleAssessInfo:add">
	//添加
	function add_titleAssessInfo(){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "新增",
			iconCls : "icon-add",
			width:700,	
			height:540,
			modal:true,//是否是模态框
			href : "titleAssessInfo/form",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#titleAssessInfoForm").form("submit",{
						url : "titleAssessInfo/add",
						success : function(data){
							d.dialog("close");
							$("#titleAssessInfoTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
</script>
</body>
</html>




