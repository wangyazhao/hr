<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div  id="titleAssessInfoForm">
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
					<td align="center" colspan="4"><b>员工信息</b><input type="hidden" name="id" ></td>
				</tr>
				<tr>
					<td class="form-item" style="width: 20%">员工工号：
					</td>
					<td style="width: 30%"><input id="StaffNumer"  type="text" class="easyui-textbox" readonly="readonly"></td>
					<td class="form-item" style="width: 20%">员工姓名：</td>
					<td style="width: 30%"><input type="text"id="XingMing" class="easyui-textbox" readonly="readonly"></td>
				</tr>
				<tr>
					<td class="form-item">性别：</td>
					<td><input id="xingbie" class="easyui-textbox" type="text" readonly="readonly"  ></td>
					<td class="form-item">员工身份证：</td>
					<td><input id="ShenFen" class="easyui-textbox" type="text"  readonly="readonly" ></td>
				</tr>
				<tr>
					<td class="form-item">所在部门名称：</td>
					<td colspan="3"><input id="BuMen"  class="easyui-textbox" type="text" readonly="readonly"  ></td>
				</tr>
				<tr>
					<td class="form-item">岗位：</td>
					<td><input  id="GangWei"  class="easyui-textbox" type="text" readonly="readonly"  ></td>
					<td class="form-item">职务：</td>
					<td><input  id="ZhiWu"  class="easyui-textbox" type="text" readonly="readonly"  ></td>
				</tr>
				
				
				
				<tr>
					<td colspan="4" style="text-align: center"><strong>职称评定信息</strong><hr/></td>
				</tr>
				<tr>
					<td class="form-item">取得职称：</td>
					<td colspan="3"><input id="ZhiCheng" class="easyui-textbox" >  </td>
				</tr>
				<tr>
					<td class="form-item">取得方式：</td>
					<td><input id="way"      class="easyui-textbox" type="text" readonly="readonly" ></td>
					<td class="form-item">取得时间：</td>
					<td><input name="time" id="ShiJian"  class="easyui-textbox" type="text" readonly="readonly" ></td>
				</tr>
				<tr>
					<td class="form-item">下次申报职称：</td>
					<td><input id="XCZhiCheng"    class="easyui-textbox" type="text" readonly="readonly" ></td>
					<td class="form-item">下次申报时间：</td>
					<td><input name="nexttime"  id="XCShiJian"  class="easyui-textbox" type="text" readonly="readonly" > </td>
				</tr>
				<tr>
					<td class="form-item">备注信息：</td>
					<td colspan="3"><textarea name="remark"   class="easyui-textbox"  readonly="readonly"   rows="5" cols="50" id="BeiZhu" style="height: 66px; width: 96%;"></textarea></td>
				</tr>
			</tbody>
		</table>
	</div>

</body>
</html>






