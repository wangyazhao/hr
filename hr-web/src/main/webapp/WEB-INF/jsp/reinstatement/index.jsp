<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="reinstatementTable"  title="Reinstatement List"
        data-options="url:'reinstatement/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'xm',width:60,formatter:xmReinstatementFormatter">员工姓名</th>
            <th data-options="field:'number',width:60,formatter:bmReinstatementFormatter">员工部门</th>
            <th data-options="field:'name',width:50,formatter:lxReinstatementFormatter">复职类型</th>
            <th data-options="field:'time',width:50">复职日期</th>
            <th data-options="field:'czReinstatement',width:50,formatter:czReinstatementFormatter">操作</th>
        </tr>
    </thead>
</table>
<form id="reinstatementCondition1">
	<div id="reinstatementtb" style="padding:15px 10px;">
		用户名 : <input id="nameSearch5" class="easyui-combobox" name="archives.id" type="text" data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'archives/all',panelHeight:'auto',panelMaxHeight:250">
		复职类型 :<input id="fuzhitype5" name="type.id" type="text" class="easyui-combobox" data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/reinstatement',panelHeight:'auto',panelMaxHeight:250,editable:false">
		是否恢复合同 :<input id="whether5" name="whether.id" type="text" class="easyui-combobox" data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/whether',panelHeight:'auto',panelMaxHeight:250,editable:false">
		<a id="btn" href="javascript:void(0)" onclick="setReinstatementCondition();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
		<a id="btn" href="javascript:void(0)" onclick="resetReinstatementCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-undo'">Reset</a>
		
		<shiro:hasPermission name="reinstatement:add">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_reinstatement();" data-options="iconCls:'icon-add'">添加</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="reinstatement:dels">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="dels_reinstatement();" data-options="iconCls:'icon-remove'">删除</a>
		</shiro:hasPermission>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#reinstatementTable").datagrid({
			pagination : true,
			toolbar : "#reinstatementtb",
			idField : "id"
		});
	
		
	})
	
	//设置条件
	function setReinstatementCondition(){ 
		var postData = {'archives.id' : $("#nameSearch5").val(),
						'type.id' : $("#fuzhitype5").val(),
							'whether.id'  :$("#whether5").val()};
		$("#reinstatementTable").datagrid("reload",postData);
	}

	//清除条件
	function resetReinstatementCondition(){
		$("#reinstatementCondition1").form("clear");
	}
	
	//姓名
	function xmReinstatementFormatter(value,row,index){
		if(row.archives == null || row.archives.name == null){
			return "-";
		}
		 return row.archives.name; 
	}
	
	//部门
	function bmReinstatementFormatter(value,row,index) {
		if(row.archives == null || row.archives.section == null || row.archives.section.text == null){
			return "-";
		}
		return row.archives.section.text;
	}
	//类型
	function lxReinstatementFormatter(value,row,index){
		
		if(row.type == null || row.type.typename == null){
			return "-";
		}
		return row.type.typename;
	}
	
	//格式化操作
	function czReinstatementFormatter(value,row,index){
		var str ="<a href='javascript:void(0)' onclick='view_reinstatement("+row.id+");' >查看</a>";
			<shiro:hasPermission name="reinstatement:edit">
			str +="   <a href='javascript:void(0)' onclick='edit_reinstatement("+row.id+");' >修改</a>";
			</shiro:hasPermission>
		return str;
		
	}
	
	<shiro:hasPermission name="reinstatement:dels">
	//删除选择的数据行
	function dels_reinstatement(){
		//获取要删除的数据黄
		var reinstatementids = $("#reinstatementTable").datagrid("getSelections");
		if(reinstatementids.length == 0){
			$.messager.alert("提示","请选择要删除的数据行！","warning");
			return;
		}
		$.messager.confirm("提示","确定要删除选中的员工及其相关数据吗？",function(d){
			if(d){
				var postData = "";
				$.each(reinstatementids,function(i){
					postData += "ids="+this.id;
					if(i < reinstatementids.length - 1){
						postData += "&";
					}
				});
				$.post("reinstatement/dels",postData,function(data){
					if(data.result == true){
						// 删除成功后，刷新表格 reload
						$("#reinstatementTable").datagrid("reload");
					}
				})
			}
		})
	}
	</shiro:hasPermission>
	
	//查看
	function view_reinstatement(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:540,
			modal:true,
			href : "reinstatement/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("reinstatement/view",{id:id},function(data){
					$("#reinstatementForm").form("load",data);
					$("#StaffNumer").textbox("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					
					$("#fuzhitype").textbox('setValue',data.type.typename);
					$("#whether").textbox('setValue',data.whether.typename);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	
	<shiro:hasPermission name="reinstatement:edit">
	//修改
	function edit_reinstatement(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑用户",
			iconCls : "icon-edit",
			width:700,
			height:540,
			modal:true,
			href : "reinstatement/form",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("reinstatement/view",{id:id},function(data){
					$("#reinstatementForm").form("load",data);
					$("#StaffNumer").combogrid("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
				    $("#fuzhitype").combobox('setValue',data.type.id);
					$("#whether").combobox('setValue',data.whether.id);
				});
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#reinstatementForm").form("submit",{
						url : "reinstatement/edit",
						success : function(data){
							d.dialog("close");
							$("#reinstatementTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="reinstatement:add">
	//添加
	function add_reinstatement(){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "新增",
			iconCls : "icon-add",
			width:700,	
			height:540,
			modal:true,//是否是模态框
			href : "reinstatement/form",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#reinstatementForm").form("submit",{
						url : "reinstatement/add",
						success : function(data){
							d.dialog("close");
							$("#reinstatementTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
</script>
</body>
</html>




