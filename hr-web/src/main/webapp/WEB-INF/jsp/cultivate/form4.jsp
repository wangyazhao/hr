<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="" id="auditForm" method="post">
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
					<td align="center" colspan="4"><b>培训计划审核</b><input type="hidden" name="cultivate.id" value="${param.byid}" ><hr/></td>
				</tr>
                <tr>
                    <td>是否通过： </td>
                    <td> <input name="whether.id" id="pxlx"  type="text" class="easyui-combobox"  data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/whether',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
                </tr>
                <tr>
                    <td>意见或者建议:</td>
                    <td colspan="3"><input name="opinion" class="easyui-textbox" data-options="mutiline:true" style="width:100%;height:100px"></td>
                </tr>
                
			</tbody>
		</table>
	</form>
	<script type="text/javascript">
		
	</script>
</body>
</html>






