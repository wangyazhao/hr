<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="" id="cultivateForm" method="post">
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
					<td align="center" colspan="4"><b>培训计划</b><input type="hidden" name="id" ><hr/></td>
				</tr>
                <tr>
                    <td>培训计划编号： </td>
                    <td>  <input name="no" type="text" id="HtBianHao" class="easyui-textbox" data-options="required:true"> <img src="easyui/themes/icons/large_chart.png" onclick="fenpei()"  title="分配编号" style="vertical-align: middle;"/> </td>
                    <td>培训计划名称：</td>
                    <td> <input name="name" type="text"  class="easyui-textbox" data-options="required:true"></td>
                </tr>
                <tr>
                    <td>培训时间： </td>
                    <td><input name="time" type="date" id="pxsj"></input></td>
                    <td>培训结束时间： </td>
                    <td><input name="outtime" type="date" id="otpxsj"></input></td>
                </tr>
                <tr>
                   	<td>主办部门：</td>
                   	<td> <input id="zbbm2" class="easyui-combotree" name="section.id" data-options="required:true,hasDownArrow:true,animate:true,valueField:'id',textField:'typename',url:'section/list',panelHeight:'auto',panelMaxHeight:250,editable:false,lines:'true'"></td>
                    <td>负责人：</td>
                    <td><input name="head" id="fuzeren" type="text"  class="easyui-textbox" data-options="required:true"> <img src="easyui/themes/icons/large_chart.png" onclick="head1()"  title="负责人" style="vertical-align: middle;"/></td>
                </tr>
                <tr>
                    <td>培训机构联系方式： </td>
                    <td><input name="tel" type="text" class="easyui-textbox"></td>
                    <td>培训地点：</td>
                    <td><input name="place" type="text"  class="easyui-textbox" data-options="required:true">  </td>
                </tr>
                <tr>
                    <td>审批人：<input name="approver.id" type="hidden" id="shrr"></td>
                    <td><input  id="shr"  type="text" class="easyui-textbox" data-options="editable:false,required:true"><img src="easyui/themes/icons/large_chart.png" onclick="head2()"  title="审批人" style="vertical-align: middle;"/> </td>
                    <td>培训方式： </td>
                    <td><input name="way.id" id="pxfs" type="text" class="easyui-combobox"  data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/cultivateway',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
                </tr>
                <tr>
                    <td>培训类型：</td>
                    <td><input name="type.id" id="pxlx"  type="text" class="easyui-combobox"  data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/cultivatetype',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
                    <td>预算金额： </td>
                    <td><input name="budget" type="text" class="easyui-textbox" data-options="required:true"></td>
                </tr>
                <tr>
                    <td>培训人员：</td>
                    <td colspan="3"><input id="pxrjs"  name="aid" type="text"></td>
                </tr>
                <tr>
                    <td>培训说明：</td>
                    <td colspan="3"><input name="cultivatremark" class="easyui-textbox" data-options="mutiline:true" style="width:90%;height:60px"></td>
                </tr>
                <tr>
                    <td>培训机构信息：</td>
                    <td colspan="3"><input name="info" class="easyui-textbox" data-options="mutiline:true" style="width:90%;height:60px"></td>
                </tr>
                <tr>
                	<td>附件：</td>
                	<td>
                		<div class="field " >
		          		 
		          		 <input type="text" name="attachment"  readonly="readonly"  id="attachmentFile"  class="easyui-textbox" />
		          			<input type="file" name="fileName"  style="display: none;"  id="shangchuan"/>
		          			<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#shangchuan').click();" data-options="iconCls:'icon-add'">上传</a>
		        		</div>
                	</td>
                </tr>
                 
                
			</tbody>
		</table>
	</form>
	<script type="text/javascript">
	function fenpei(){
		var str = "JH-"
		for(let i = 0 ; i< 8 ; i++){
			str += Math.floor(Math.random()*10);
		}
		$("#HtBianHao").textbox('setValue',str);
	}
	//添加负责人
	function head1(){
		var d = $("<div id='form2'></div>").appendTo("body");
		d.dialog({
			title : "负责人",
			iconCls : "icon-add",
			width:520,	
			height:380,
			modal:true,//是否是模态框
			href : "cultivate/form2",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	//添加审核人
	function head2(){
		var d = $("<div id='form3'></div>").appendTo("body");
		d.dialog({
			title : "审核人",
			iconCls : "icon-add",
			width:520,	
			height:380,
			modal:true,//是否是模态框
			href : "cultivate/form3",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
		$(function() {
			
			
			
			$('#pxrjs').combogrid({    
				   panelWidth:160,    
				   // fitColumns:true
				   required:true,
				   multiple:true,
				    idField:'id',    
				    textField:'name',    
				    url:'archives/all',    
				    columns:[[    
				        {field:'id',title:'id',width:27},    
				        {field:'worknum',title:'编号',width:80},  
				        {field:'name',title:'姓名',width:50}
				    ]] 
				});  
			
			$("#shangchuan").fileupload({
				url : 'hr/upload',
				dataType : 'json',
				done : function(e,data){
					
					if(data.result.r){
						$("#attachmentFile").textbox('setValue',data.result.name);
						alert("上传成功")
					}else{
						alert("上传失败")
					}
					
					
				}
			})
			
			
			
			$('#StaffNumer').combogrid({    
			   panelWidth:160,    
			   // fitColumns:true
			   required:true,
			    idField:'id',    
			    textField:'worknum',    
			    url:'archives/all',    
			    columns:[[    
			        {field:'id',title:'id',width:27},    
			        {field:'worknum',title:'编号',width:80},  
			        {field:'name',title:'姓名',width:50}
			    ]] ,
			    onChange:function(n,o){
				$.post("archives/toarchives",{id:n},function(data){
						$("#XingMing").textbox('setValue',data.name);
						$("#xingbie").textbox('setValue',data.sex.typename);
						$("#ShenFen").textbox('setValue',data.idcard);
						$("#BuMen").textbox('setValue',data.section.text);
						$("#ZhiWu").textbox('setValue',data.position.name);
						$("#GangWei").textbox('setValue',data.jobs.typename);
					});
				}   
			});  

			
			$('#pxsj').datebox({required:true});
			$('#otpxsj').datebox({required:true});
		})
		
		
		function selname(th){
			$("#fuzeren").textbox('setValue',$(th).html())
			
			$("#form2").dialog("destroy");
		} 
		function selname1(th){
			$("#shr").textbox('setValue',$(th).html())
			$("#shrr").val($(th).attr("dateid"))
		$("#form3").dialog("destroy");
		} 
		
	</script>
</body>
</html>






