<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="js/jquery.ui.widget.js"></script>
<script type="text/javascript" src="js/jquery.fileupload.js"></script>
</head>
<body>
<table id="cultivateTable"  title="Cultivate List"
        data-options="url:'cultivate/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'no',width:60">计划编号</th>
            <th data-options="field:'name',width:60">计划名称</th>
            <th data-options="field:'time',width:50">培训时间</th>
            <th data-options="field:'place',width:50">培训地点</th>
            <th data-options="field:'zt',width:50,formatter:ctCultivateFormatter">状态</th>
            <th data-options="field:'czCultivate',width:50,formatter:czCultivateFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<form id="cultivateCondition1">
	<div id="cultivatetb" style="padding:15px 10px;">
 		培训计划名称：<input id="pxjh5"  type="text" class="easyui-textbox">
 		主办部门：<input id="zbbm5" class="easyui-combotree" data-options="hasDownArrow:true,animate:true,valueField:'id',textField:'typename',url:'section/list',panelHeight:'auto',panelMaxHeight:250,editable:false,lines:'true'">
 		负责人：<input id="fuzeren5" type="text"  class="easyui-textbox" data-options="">
 		培训地点：<input id="pxdd5" type="text"  class="easyui-textbox" data-options="">
 		培训方式：<input id="pxfs5" type="text" class="easyui-combobox"  data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/cultivateway',panelHeight:'auto',panelMaxHeight:250,editable:false">
 		培训类型：<input id="pxlx5"  type="text" class="easyui-combobox"  data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/cultivatetype',panelHeight:'auto',panelMaxHeight:250,editable:false">
 		<a id="btn" href="javascript:void(0)" onclick="setCultivateCondition();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
		<a id="btn" href="javascript:void(0)" onclick="resetCultivateCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-undo'">Reset</a>
		<shiro:hasPermission name="cultivate:add">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_cultivate();" data-options="iconCls:'icon-add'">添加</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="cultivate:dels">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="dels_cultivate();" data-options="iconCls:'icon-remove'">删除</a>
		</shiro:hasPermission>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#cultivateTable").datagrid({
			pagination : true,
			toolbar : "#cultivatetb",
			idField : "id"
		});
	
		
	})
	
	//设置条件
	function setCultivateCondition(){ 
		var postData = {name : $("#pxjh5").val(),
				'section.id' : $("#zbbm5").val(),
				head : $("#fuzeren5").val(),
				place : $("#pxdd5").val(),
				'way.id' : $("#pxfs5").val(),
				'type.id' : $("#pxlx5").val()
									};
		$("#cultivateTable").datagrid("reload",postData);
	}

	//清除条件
	function resetCultivateCondition(){
		$("#cultivateCondition1").form("clear");
	}
	
	//状态
	function ctCultivateFormatter(value,row,index) {
		if(row.statu == null || row.statu.typename == null){
			return "-";
		}
		return row.statu.typename;
	}
	
	//格式化操作
	function czCultivateFormatter(value,row,index){
		var str="<a href='javascript:void(0)' onclick='view_cultivate("+row.id+");' >查看</a> ";
			<shiro:hasPermission name="cultivate:edit">
			if(row.statu != null && row.statu.id == 143){
				str += " <a href='javascript:void(0)' onclick='edit_cultivate("+row.id+");' >修改</a>";
			}
			</shiro:hasPermission>
		 return str
	}
	<shiro:hasPermission name="cultivate:dels">
	//删除选择的数据行
	function dels_cultivate(){
		//获取要删除的数据黄
		var cultivateids = $("#cultivateTable").datagrid("getSelections");
		if(cultivateids.length == 0){
			$.messager.alert("提示","请选择要删除的数据行！","warning");
			return;
		}
		$.messager.confirm("提示","确定要删除选中的数据吗？",function(d){
			if(d){
				var postData = "";
				$.each(cultivateids,function(i){
					postData += "ids="+this.id;
					if(i < cultivateids.length - 1){
						postData += "&";
					}
				});
				$.post("cultivate/dels",postData,function(data){
					if(data.result == true){
						// 删除成功后，刷新表格 reload
						$("#cultivateTable").datagrid("reload");
					}
				})
			}
		})
	}
	</shiro:hasPermission>
	
	//查看
	function view_cultivate(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:610,
			modal:true,
			href : "cultivate/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("cultivate/view",{id:id},function(data){
					$("#cultivateForm").form("load",data);
					$("#cultivateForm").form("load",data);
					$("#shr").textbox('setValue',data.approver.name);
					$("#pxfs").textbox('setValue',data.way.typename);
					$("#pxlx").textbox('setValue',data.type.typename);
					$("#pxlx").textbox('setValue',data.type.typename);
					$("#zbbm2").textbox('setValue',data.section.text);
					$("#shenhezt").textbox('setValue',data.statu.typename);
					if(data.statu.id != 143){
		        		
		        		
						$("#addshenhe").html("")
						
					}
					var $idaa = ${sessionScope.login_user.archives.id }+"";
					if($idaa == null || $idaa == "" || $idaa != data.approver.id){
						$("#addshenhe").html("")
					}
					$("#fujian").attr("href","hr/download?name="+data.attachment);
					$("#fujian").html(data.attachment);
					var str = '';
					console.log(data.audit.length)
					if(data.audit.length >= 1){
						str += '<tr>';
						str += '<td align="center" colspan="4"><b>审核记录</b> <hr/></td>';
						str += '</tr>';
					}
					$.each(data.audit,function(i){
						str +=  '<tr>';
						str +=  '<td>是否通过： </td>';
						str +=  '<td> <input type="text" value="'+this.whether.typename+'" class="op" readonly="readonly" ></td>';
						str +=  '<td>审核时间： </td>';
						str +=  '<td> <input type="text" value="'+this.time+'" class="op" readonly="readonly" ></td>';
						str +=  '</tr>';
		                str +=  '<tr>';
		                str +=  '<td>意见或者建议:</td>';
		                str +=  '<td colspan="3"><input value="'+1+'" class="op" style="width:100%;height:100px"></td>';
		                str +=  '<tr>';
					});
					
					$(str).appendTo("#totbody")
					
					var $str ='';
					$.each(data.archives,function(){
						$str += this.name+'  '
					})
					
					
					$('#pxry5').textbox('setValue', $str);
					
				});
			},onLoadSuccess:function(){
				
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
					$("#cultivateTable").datagrid("reload");
				}
			}]
		})
	}
	
	
	
	<shiro:hasPermission name="cultivate:edit">
	//修改
	function edit_cultivate(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑",
			iconCls : "icon-edit",
			width:700,
			height:600,
			modal:true,
			href : "cultivate/form",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("cultivate/view",{id:id},function(data){
					$("#cultivateForm").form("load",data);
					$("#shr").textbox('setValue',data.approver.name);
					$("#pxfs").combobox('setValue',data.way.id);
					$("#pxlx").combobox('setValue',data.type.id);
					$("#zbbm2").combotree('setValue',data.section.id);
					var $arr = new Array();
					$.each(data.archives,function(){
						$arr.push(this.id)
					})
					
					
					$('#pxrjs').combogrid('setValues', $arr);
				});
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#cultivateForm").form("submit",{
						url : "cultivate/edit",
						success : function(data){
							d.dialog("close");
							$("#cultivateTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="cultivate:add">
	//添加
	function add_cultivate(){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "新增",
			iconCls : "icon-add",
			width:700,	
			height:540,
			modal:true,//是否是模态框
			href : "cultivate/form",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#cultivateForm").form("submit",{
						url : "cultivate/add",
						success : function(data){
							d.dialog("close");
							$("#cultivateTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
</script>
</body>
</html>




