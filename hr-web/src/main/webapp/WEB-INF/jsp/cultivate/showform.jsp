<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="" id="cultivateForm" method="post">
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody id="totbody">
				<tr>
					<td align="center" colspan="4"><b>培训计划</b><input type="hidden" name="id" id="byid" ><hr/></td>
				</tr>
                <tr>
                    <td>培训计划编号： </td>
                    <td>  <input name="no" type="text" id="HtBianHao"  type="text"class="easyui-textbox"  readonly="readonly"> </td>
                    <td>培训计划名称：</td>
                    <td> <input name="name" type="text"   type="text"class="easyui-textbox"  readonly="readonly"></td>
                </tr>
                <tr>
                    <td>培训时间： </td>
                    <td><input name="time" type="text" id="pxsj"  type="text"class="easyui-textbox"  readonly="readonly"></input></td>
                    <td>培训结束时间： </td>
                    <td><input name="outtime" type="text" id="otpxsj"  type="text"class="easyui-textbox"  readonly="readonly"/></td>
                </tr>
                <tr>
                   	<td>主办部门：</td>
                   	<td> <input id="zbbm2"  type="text"class="easyui-textbox"  readonly="readonly"></td>
                    <td>负责人：</td>
                    <td><input name="head" id="fuzeren" type="text"  type="text"class="easyui-textbox"  readonly="readonly"></td>
                </tr>
                <tr>
                    <td>培训机构联系方式： </td>
                    <td><input name="tel" type="text"  type="text"class="easyui-textbox"  readonly="readonly"></td>
                    <td>培训地点：</td>
                    <td><input name="place" type="text"   type="text"class="easyui-textbox"  readonly="readonly">  </td>
                </tr>
                <tr>
                    <td>审批人：<input name="approver.id" type="hidden" id="shrr"></td>
                    <td><input  id="shr"  type="text"  type="text"class="easyui-textbox"  readonly="readonly"></td>
                    <td>培训方式： </td>
                    <td><input name="way.id" id="pxfs" type="text" type="text"class="easyui-textbox"  readonly="readonly"></td>
                </tr>
                <tr>
                    <td>培训类型：</td>
                    <td><input name="type.id" id="pxlx"  type="text"  type="text"class="easyui-textbox"  readonly="readonly"></td>
                    <td>预算金额： </td>
                    <td><input name="budget" type="text"  type="text"class="easyui-textbox"  readonly="readonly"></td>
                </tr>
                <tr>
                    <td>审核状态：</td>
                    <td colspan="3"><input id="shenhezt" class="easyui-textbox"  readonly="readonly" ></td>
                </tr>
                <tr>
                    <td>培训人员：</td>
                    <td colspan="3"><input id="pxry5" class="easyui-textbox"  readonly="readonly"  data-options="mutiline:true" style="width:90%;height:60px"></td>
                </tr>
                <tr>
                    <td>培训说明：</td>
                    <td colspan="3"><input name="cultivatremark"  type="text"class="easyui-textbox"  readonly="readonly" data-options="mutiline:true" style="width:90%;height:60px"></td>
                </tr>
                <tr>
                    <td>培训机构信息：</td>
                    <td colspan="3"><input name="info"  type="text"class="easyui-textbox"  readonly="readonly" data-options="mutiline:true" style="width:90%;height:60px"></td>
                </tr>
                <tr>
                	<td>附件：   </td>
                	<td colspan="2"><a id="fujian" href="hr/download?name=">1</a></td>
                	<td id="addshenhe"><a href="javascript:void(0)" class="easyui-linkbutton" onclick="audit_cultivate();" data-options="iconCls:'icon-add'">审核</a>  </td>
                </tr>
                 
                
			</tbody>
		</table>
	</form>
	<script type="text/javascript">
	//添加
	function audit_cultivate(){
		var $id = $("#byid").val();
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "审核",
			iconCls : "icon-add",
			width:500,	
			height:300,
			modal:true,//是否是模态框
			href : "cultivate/form4?byid="+$id,
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#auditForm").form("submit",{
						url : "cultivate/addaudit",
						success : function(data){
							d.dialog("close");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</script>
		
</body>
</html>






