<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
<base href="<%=basePath%>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
<link rel="stylesheet" type="text/css" href="easyui/themes/gray/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<title>Insert title here</title>
</head>
<body>
<div class="easyui-panel" title="审核人" style="width:500px;height:300px;padding:10px;">
		<div class="easyui-layout" data-options="fit:true">
			<div data-options="region:'west',split:true" style="width:200px;padding:10px">
				<ul id="sectionTrees" data-options="url:'section/list',lines:'true',animate:'true'"></ul>
			</div>
			<div data-options="region:'center'" style="padding:10px">
			<p id="p1" align="center">暂未选择部门</p>
			 <ul id="ul" style="list-style-type: none;">
			 </ul>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$(function(){
		$("#sectionTrees").tree({
			onClick:function(node){
				
				$.post("user/reviewer",{id:node.id},function(data){
					var str = '';
					$.each(data,function(){
						str +='<li onclick="selname1(this)" dateid="'+this.archives.id+'"  style="margin: 4px 50px;line-height: 20px ">'+this.archives.name+'</li>';
					});
					$("#ul").html(str);
					});
				$("#p1").html(node.text)
				}   
		});
		
		
	})
	
	</script>
</body>
</html>






