<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div  id="certificateInfoForm" >
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
					<td align="center" colspan="4"><b>员工信息</b><input type="hidden" name="id" ><hr/></td>
				</tr>
				<tr>
					<td class="form-item" style="width: 20%">员工工号：
					</td>
					<td style="width: 30%"><input id="StaffNumer"  type="text" class="easyui-textbox" readonly="readonly"></td>
					<td class="form-item" style="width: 20%">员工姓名：</td>
					<td style="width: 30%"><input type="text"id="XingMing" class="easyui-textbox" readonly="readonly"></td>
				</tr>
				<tr>
					<td class="form-item">性别：</td>
					<td><input id="xingbie" class="easyui-textbox" type="text" readonly="readonly"  ></td>
					<td class="form-item">员工身份证：</td>
					<td><input id="ShenFen" class="easyui-textbox" type="text"  readonly="readonly" ></td>
				</tr>
				<tr>
					<td class="form-item">所在部门名称：</td>
					<td colspan="3"><input id="BuMen"  class="easyui-textbox" type="text" readonly="readonly"  ></td>
				</tr>
				<tr>
					<td class="form-item">岗位：</td>
					<td><input  id="GangWei"  class="easyui-textbox" type="text" readonly="readonly"  ></td>
					<td class="form-item">职务：</td>
					<td><input  id="ZhiWu"  class="easyui-textbox" type="text" readonly="readonly"  ></td>
				</tr>
				
				
				
				
				<tr>
					<td colspan="4" style="text-align: center"><strong>证照信息</strong><hr/>
					<input type="hidden" name="id" />
					</td>
				</tr>
				<tr>
					<td class="form-item">证照类型：</td>
					<td><input id="Leixing" class="easyui-textbox" name="type.id" readonly="readonly"  ></td>
					<td class="form-item"><font color="red">*</font>证照编号：</td>
					<td><input id="Bianhao" name="number" type="text" class="easyui-textbox" readonly="readonly" ></td>
				</tr>
				<tr>
					<td class="form-item"><font color="red">*</font>证照名称：</td>
					<td><input  id="Mingcheng" name="name" type="text" class="easyui-textbox" readonly="readonly"  ></td>
					<td class="form-item">发证机构：</td>
					<td><input id="FzJigou" name="certificate.id" type="text" class="easyui-textbox" readonly="readonly" ></td>
				</tr>
				<tr>
					<td class="form-item"><font color="red">*</font>取证日期：</td>
					<td><input name="time" type="text"  class="easyui-textbox" id="QzRiQi" readonly="readonly"></td>
					<td class="form-item"><font color="red">*</font>生效日期：</td>
					<td><input name="operant" type="text" id="SxRiQi" class="easyui-textbox" readonly="readonly"></td>
				</tr>
				<tr>
					<td class="form-item">有无期限：</td>
					<td><input id="XianZhi" class="easyui-textbox" readonly="readonly"></td>
					<td class="form-item"><font color="red">*</font>到期日期：</td>
					<td><input type="text" id="DqRiQi" class="easyui-textbox" readonly="readonly"></td>
				</tr>
				<tr>
					<td class="form-item"><font color="red">*</font>状态：</td>
					<td colspan="3"><input id="Zhuantai"  class="easyui-textbox" readonly="readonly"></td>
				</tr>
				<tr>
					<td class="form-item">备注：</td>
					<td colspan="3"><textarea name="remark" rows="2" cols="20"
							id="Beizhu"  class="easyui-textbox" readonly="readonly" style="height: 60px; width: 96%;"></textarea>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>






