<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="certificateInfoTable"  title="CertificateInfo List"
        data-options="url:'certificateInfo/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
       		<th data-options="field:'xm',width:60,formatter:xmCertificateInfoFormatter">姓名</th>
            <th data-options="field:'number',width:60">证照编号</th>
            <th data-options="field:'name',width:50">证照名称</th>
            <th data-options="field:'operant',width:50">生效日期</th>
            <th data-options="field:'expire',width:50">到期日期</th>
            <th data-options="field:'zt',width:30,formatter:ztCertificateInfoFormatter">状态</th>
            <th data-options="field:'czCertificateInfo',width:50,formatter:czCertificateInfoFormatter">操作</th>
           
        </tr>
    </thead>
</table>
<form id="certificateInfoCondition1">
	<div id="certificateInfotb" style="padding:15px 10px;">
		用户名 : <input id="nameSearch5" class="easyui-combobox" type="text" data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'archives/all',panelHeight:'auto',panelMaxHeight:250">
		证照类型:<input id="Leixing5" class="easyui-combobox" data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/zztype',panelHeight:'auto',panelMaxHeight:250,editable:false">
		状 态 :<input id="Zhuantai5" class="easyui-combobox" data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/zhuangtai',panelHeight:'auto',panelMaxHeight:250,editable:false">
		证件编号:<input id="zzbh5" type="text" class="easyui-textbox">
		<a id="btn" href="javascript:void(0)" onclick="setCertificateInfoCondition();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
		<a id="btn" href="javascript:void(0)" onclick="resetCertificateInfoCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-undo'">Reset</a>
		
		<shiro:hasPermission name="certificateInfo:add">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_certificateInfo();" data-options="iconCls:'icon-add'">添加</a>
		</shiro:hasPermission>
		<shiro:hasPermission name="certificateInfo:dels">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="dels_certificateInfo();" data-options="iconCls:'icon-remove'">删除</a>
		</shiro:hasPermission>
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#certificateInfoTable").datagrid({
			pagination : true,
			toolbar : "#certificateInfotb",
			idField : "id"
		}); 
	})
	
	//设置条件
	function setCertificateInfoCondition(){ 
		var postData = {'archives.id' : $("#nameSearch5").val(),
						   'type.id'  : $("#Leixing5").val(),
						   'state.id' : $("#Zhuantai5").val(), 
						      number  : $("#zzbh5").val()
						      };
		$("#certificateInfoTable").datagrid("reload",postData);
	}

	//清除条件
	function resetCertificateInfoCondition(){
		$("#certificateInfoCondition1").form("clear");
	}
	
	//姓名
	function xmCertificateInfoFormatter(value,row,index){
		if(row.archives == null || row.archives.name == null){
			return "-";
		}
		 return row.archives.name; 
	}
	//状态
	function ztCertificateInfoFormatter(value,row,index){
		
		if( row.state.typename == null){
			return "-";
		}
		return row.state.typename;
	}
	
	//格式化操作
	function czCertificateInfoFormatter(value,row,index){
		var str ="<a href='javascript:void(0)' onclick='view_certificateInfo("+row.id+");' >查看</a>";
			<shiro:hasPermission name="certificateInfo:dels">
			str += " <a href='javascript:void(0)' onclick='edit_certificateInfo("+row.id+");' >修改</a>";
			</shiro:hasPermission>
		return   str;
	}
	<shiro:hasPermission name="certificateInfo:dels">
	//删除选择的数据行
	function dels_certificateInfo(){
		//获取要删除的数据黄
		var certificateInfoids = $("#certificateInfoTable").datagrid("getSelections");
		if(certificateInfoids.length == 0){
			$.messager.alert("提示","请选择要删除的数据行！","warning");
			return;
		}
		$.messager.confirm("提示","确定要删除选中的员工及其相关数据吗？",function(d){
			if(d){
				var postData = "";
				$.each(certificateInfoids,function(i){
					postData += "ids="+this.id;
					if(i < certificateInfoids.length - 1){
						postData += "&";
					}
				});
				$.post("certificateInfo/dels",postData,function(data){
					if(data.result == true){
						// 删除成功后，刷新表格 reload
						$("#certificateInfoTable").datagrid("reload");
					}
				})
			}
		})
	}
	</shiro:hasPermission>
	
	//查看
	function view_certificateInfo(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "查看",
			iconCls : "icon-back",
			width:700,
			height:600,
			modal:true,
			href : "certificateInfo/showform",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("certificateInfo/view",{id:id},function(data){
					$("#certificateInfoForm").form("load",data);
					$("#StaffNumer").textbox("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					
					$("#Leixing").textbox('setValue',data.type.typename);
					$("#FzJigou").textbox('setValue',data.certificate.typename);
					$("#Zhuantai").textbox('setValue',data.state.typename);
					$("#XianZhi").textbox('setValue',data.periodstate.typename);
				});
			},
			buttons:[{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	
	<shiro:hasPermission name="certificateInfo:edit">
	//修改
	function edit_certificateInfo(id){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "编辑用户",
			iconCls : "icon-edit",
			width:700,
			height:600,
			modal:true,
			href : "certificateInfo/form",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
				$.post("certificateInfo/view",{id:id},function(data){
					$("#certificateInfoForm").form("load",data);
					$("#StaffNumer").combogrid("setValue",data.archives.id);//id
					$("#XingMing").textbox('setValue',data.archives.name);
					$("#xingbie").textbox('setValue',data.archives.sex.typename);
					$("#ShenFen").textbox('setValue',data.archives.idcard);
					$("#BuMen").textbox('setValue',data.archives.section.text);
					$("#ZhiWu").textbox('setValue',data.archives.position.name);
					$("#GangWei").textbox('setValue',data.archives.jobs.typename);
					
					$("#Leixing").combobox('setValue',data.type.id);
					$("#FzJigou").combobox('setValue',data.certificate.id);
					$("#Zhuantai").combobox('setValue',data.state.id);
					$("#XianZhi").combobox('setValue',data.periodstate.id);
				});
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#certificateInfoForm").form("submit",{
						url : "certificateInfo/edit",
						success : function(data){
							d.dialog("close");
							$("#certificateInfoTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="certificateInfo:add">
	//添加
	function add_certificateInfo(){
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "新增",
			iconCls : "icon-add",
			width:700,	
			height:540,
			modal:true,//是否是模态框
			href : "certificateInfo/form",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#certificateInfoForm").form("submit",{
						url : "certificateInfo/add",
						success : function(data){
							d.dialog("close");
							$("#certificateInfoTable").datagrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	}
	</shiro:hasPermission>
	
	
</script>
</body>
</html>




