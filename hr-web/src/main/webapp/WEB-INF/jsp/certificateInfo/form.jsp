<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="" id="certificateInfoForm" method="post">
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
					<td align="center" colspan="4"><b>员工信息</b><input type="hidden" name="id" ><hr/></td>
				</tr>
				<tr>
					<td class="form-item" style="width: 20%"><font color="red">*</font>员工工号：
					</td>
					<td style="width: 30%"><input id="StaffNumer" name="archives.id" type="text"><img src="easyui/themes/icons/search.png" style="vertical-align: middle;"/></td>
					<td class="form-item" style="width: 20%">员工姓名：</td>
					<td style="width: 30%"><input type="text"id="XingMing" class="easyui-textbox" readonly="readonly"></td>
				</tr>
				<tr>
					<td class="form-item">性别：</td>
					<td><input id="xingbie" class="easyui-textbox"  ></td>
					<td class="form-item">员工身份证：</td>
					<td><input id="ShenFen" class="easyui-textbox" type="text"  readonly="readonly" ></td>
				</tr>
				<tr>
					<td class="form-item">所在部门名称：</td>
					<td colspan="3"><input class="easyui-textbox" type="text" id="BuMen" readonly="readonly" ></td>
				</tr>
				<tr>
					<td class="form-item">岗位：</td>
					<td><input class="easyui-textbox" type="text" id="GangWei" readonly="readonly" ></td>
					<td class="form-item">职务：</td>
					<td><input class="easyui-textbox" type="text" id="ZhiWu" readonly="readonly" ></td>
				</tr>
				
				
				
				
				<tr>
					<td colspan="4" style="text-align: center"><strong>证照信息</strong><hr/>
					<input type="hidden" name="id" />
					</td>
				</tr>
				<tr>
					<td class="form-item">证照类型：</td>
					<td><input id="Leixing" class="easyui-combobox" name="type.id"   data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/zztype',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
					<td class="form-item"><font color="red">*</font>证照编号：</td>
					<td><input id="Bianhao" name="number" id="zzbh" type="text" class="easyui-textbox" data-options="required:true"></td>
				</tr>
				<tr>
					<td class="form-item"><font color="red">*</font>证照名称：</td>
					<td><input  id="Mingcheng" name="name" type="text" class="easyui-textbox" data-options="required:true" ></td>
					<td class="form-item">发证机构：</td>
					<td><input id="FzJigou" name="certificate.id" type="text" class="easyui-combobox" data-options="hasDownArrow:true,valueField:'id',textField:'typename',url:'data/certificate',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
				</tr>
				<tr>
					<td class="form-item"><font color="red">*</font>取证日期：</td>
					<td><input name="time" type="text" id="QzRiQi"></td>
					<td class="form-item"><font color="red">*</font>生效日期：</td>
					<td><input name="operant" type="text" id="SxRiQi"></td>
				</tr>
				<tr>
					<td class="form-item">有无期限：</td>
					<td><input id="XianZhi" name="periodstate.id" class="easyui-combobox"   data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/without',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
					<td class="form-item"><font color="red">*</font>到期日期：</td>
					<td><input name="expire" type="text" id="DqRiQi"></td>
				</tr>
				<tr>
					<td class="form-item"><font color="red">*</font>状态：</td>
					<td colspan="3"><input id="Zhuantai" class="easyui-combobox" name="state.id"   data-options="required:true,hasDownArrow:true,valueField:'id',textField:'typename',url:'data/zhuangtai',panelHeight:'auto',panelMaxHeight:250,editable:false"></td>
				</tr>
				<tr>
					<td class="form-item">备注：</td>
					<td colspan="3"><textarea name="remark" rows="2" cols="20"
							id="Beizhu" class="mytextarea" style="height: 60px; width: 96%;"></textarea>
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	<script type="text/javascript">
		$(function() {
			
			
			$('#StaffNumer').combogrid({    
			   panelWidth:160,    
			   // fitColumns:true
			   required:true,
			    idField:'id',    
			    textField:'worknum',    
			    url:'archives/all',    
			    columns:[[    
			        {field:'id',title:'id',width:27},    
			        {field:'worknum',title:'编号',width:80},  
			        {field:'name',title:'姓名',width:50}
			    ]] ,
			    onChange:function(n,o){
				$.post("archives/toarchives",{id:n},function(data){
						$("#XingMing").textbox('setValue',data.name);
						$("#xingbie").textbox('setValue',data.sex.typename);
						$("#ShenFen").textbox('setValue',data.idcard);
						$("#BuMen").textbox('setValue',data.section.text);
						$("#ZhiWu").textbox('setValue',data.position.name);
						$("#GangWei").textbox('setValue',data.jobs.typename);
					});
				}   
			});  

			
			 
			$('#QzRiQi').datebox({required:true});
			$('#SxRiQi').datebox({required:true});
			$('#DqRiQi').datebox({required:true});
			
		})
	</script>
</body>
</html>






