<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<base href="<%=basePath%>">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<table id="logTable"  title="Log List"
        data-options="url:'log/list',fitColumns:true,striped:true,rownumbers:true,iconCls:'icon-search'">
    <thead>
        <tr>
        	<th data-options="field:'tyu',checkbox:true"></th>
       		<th data-options="field:'id',width:20,sortable:true,order:'desc'">Id</th>
            <th data-options="field:'usercode',width:60,sortable:true">用户编码</th>
            <th data-options="field:'username',width:50,sortable:true">登陆账号</th>
       		<th data-options="field:'time',width:60,sortable:true">操作日期</th>
            <th data-options="field:'ip',width:50,sortable:true">IP地址</th>
            <th data-options="field:'port',width:50,sortable:true">端口号</th>
            <th data-options="field:'module',width:50">执行操作</th>
           
        </tr>
    </thead>
</table>
<form id="log">
	<div id="logtb" style="padding:15px 10px;">
		用户名 : <input id="nameSearch" class="easyui-combobox" name="archives.id" type="text" data-options="hasDownArrow:true,valueField:'id',textField:'name',url:'archives/all',panelHeight:'auto',panelMaxHeight:250">
		<a id="btn" href="javascript:void(0)" onclick="setLogCondition();" class="easyui-linkbutton" data-options="iconCls:'icon-search'">Search</a>
		<a id="btn" href="javascript:void(0)" onclick="resetLogCondition()" class="easyui-linkbutton" data-options="iconCls:'icon-undo'">Reset</a>
		
	</div>
</form>
<script type="text/javascript">
	$(function(){
		$("#logTable").datagrid({
			pagination : true,
			toolbar : "#logtb",
			idField : "id"
		});
	
		
	})
	
	//设置条件
	function setLogCondition(){
		var postData = {id : $("#nameSearch").val()}; 
		$("#logTable").datagrid("reload",postData);
	}

	//清除条件
	function resetLogCondition(){
		$("#logCondition1").form("clear");
	}
	
	
	
</script>
</body>
</html>




