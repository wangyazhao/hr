<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<base href="<%=basePath%>">
	<meta charset="UTF-8">
	<title>Document</title> 
</head>
<body>
<script type="text/javascript" src="js/Chart.min.js"></script>
	<div id="tt" class="easyui-tabs" >  
   		<div title="性别比例分析" data-options="fit:true, border:true,iconCls:'icon-reload'"  style="padding:20px;display:none;">   
		    <div style="height: 500px;width: 260px; float: left;">
				<ul id="sectionTree" data-options="url:'section/list',lines:'true',animate:'true'"></ul>
			</div>
		    <div style="width:50%;height:100%; float: left;">
		        <canvas id="myChart1"></canvas>
		    </div>
	    </div> 
       <div title="学历比例分析" data-options="fit:true, border:true,iconCls:'icon-reload'"  style="padding:20px;display:none;">   
		    <div style="height: 500px;width: 260px; float: left;">
				<ul id="sectionTree2" data-options="url:'section/list',lines:'true',animate:'true'"></ul>
			</div>
		    <div style="width:50%;height:100%; float: left;">
		        <canvas id="myChart2"></canvas>
		    </div>
	    </div>  
	    <div title="人才分类分析" data-options="fit:true, border:true,iconCls:'icon-reload'"  style="padding:20px;display:none;">   
		    <div style="height: 500px;width: 260px; float: left;">
				<ul id="sectionTree3" data-options="url:'section/list',lines:'true',animate:'true'"></ul>
			</div>
		    <div style="width:50%;height:100%; float: left;">
		        <canvas id="myChart3"></canvas>
		    </div>
	    </div>
	    <div title="人才等级分析" data-options="fit:true, border:true,iconCls:'icon-reload'"  style="padding:20px;display:none;">   
		    <div style="height: 500px;width: 260px; float: left;">
				<ul id="sectionTree4" data-options="url:'section/list',lines:'true',animate:'true'"></ul>
			</div>
		    <div style="width:50%;height:100%; float: left;">
		        <canvas id="myChart4"></canvas>
		    </div>
	    </div>
	    <div title="工种分析" data-options="fit:true, border:true,iconCls:'icon-reload'"  style="padding:20px;display:none;">   
		    <div style="height: 500px;width: 260px; float: left;">
				<ul id="sectionTree5" data-options="url:'section/list',lines:'true',animate:'true'"></ul>
			</div>
		    <div style="width:50%;height:100%; float: left;">
		        <canvas id="myChart5"></canvas>
		    </div>
	    </div>
	    <div title="员工状态分析" data-options="fit:true, border:true,iconCls:'icon-reload'"  style="padding:20px;display:none;">   
		    <div style="height: 500px;width: 260px; float: left;">
				<ul id="sectionTree6" data-options="url:'section/list',lines:'true',animate:'true'"></ul>
			</div>
		    <div style="width:50%;height:100%; float: left;">
		        <canvas id="myChart6"></canvas>
		    </div>
	    </div>
	    <div title="职称分析" data-options="fit:true, border:true,iconCls:'icon-reload'"  style="padding:20px;display:none;">   
		    <div style="height: 500px;width: 260px; float: left;">
				<ul id="sectionTree7" data-options="url:'section/list',lines:'true',animate:'true'"></ul>
			</div>
		    <div style="width:50%;height:100%; float: left;">
		        <canvas id="myChart7"></canvas>
		    </div>
	    </div>
	    <div title="职务分析" data-options="fit:true, border:true,iconCls:'icon-reload'"  style="padding:20px;display:none;">   
		    <div style="height: 500px;width: 260px; float: left;">
				<ul id="sectionTree8" data-options="url:'section/list',lines:'true',animate:'true'"></ul>
			</div>
		    <div style="width:50%;height:100%; float: left;">
		        <canvas id="myChart8"></canvas>
		    </div>
	    </div>
	    <div title="婚姻状况分析" data-options="fit:true, border:true,iconCls:'icon-reload'"  style="padding:20px;display:none;">   
		    <div style="height: 500px;width: 260px; float: left;">
				<ul id="sectionTree9" data-options="url:'section/list',lines:'true',animate:'true'"></ul>
			</div>
		    <div style="width:50%;height:100%; float: left;">
		        <canvas id="myChart9"></canvas>
		    </div>
	    </div>  
	</div>  

<script type="text/javascript">
var dataName = new Array()
var dataArr = new Array()
$(function() {
	
	//性别
	$("#sectionTree").tree({
		onClick:function(node){
			 $.post("archives/bysex",{sid:node.id},function(data){
				 loadingdata(data,"myChart1")
			 })
		}
	})
	 $.post("archives/bysex",function(data){
		 loadingdata(data,"myChart1")
	})
	
	
	//学历
	 $.post("archives/byeducation",function(data){
		 loadingdata(data,"myChart2")
	})
	
	$("#sectionTree2").tree({
		onClick:function(node){
			 $.post("archives/byeducation",{sid:node.id},function(data){
				 loadingdata(data,"myChart2")
			 })
		}
	})
	//人才分类
	$("#sectionTree3").tree({
		onClick:function(node){
			 $.post("archives/bytalentType",{sid:node.id},function(data){
				 loadingdata(data,"myChart3")
			 })
		}
	})
	 $.post("archives/bytalentType",function(data){
		 loadingdata(data,"myChart3")
	})
	
	//人才等级
	$("#sectionTree4").tree({
		onClick:function(node){
			 $.post("archives/byTalentLevel",{sid:node.id},function(data){
				 loadingdata(data,"myChart4")
			 })
		}
	})
	 $.post("archives/byTalentLevel",function(data){
		 loadingdata(data,"myChart4")
	})
	//工种分析
	$("#sectionTree5").tree({
		onClick:function(node){
			 $.post("archives/byemployeesWork",{sid:node.id},function(data){
				 loadingdata(data,"myChart5")
			 })
		}
	})
	 $.post("archives/byemployeesWork",function(data){
		 loadingdata(data,"myChart5")
	})
	//员工状态
	$("#sectionTree6").tree({
		onClick:function(node){
			 $.post("archives/bystatus",{sid:node.id},function(data){
				 loadingdata(data,"myChart6")
			 })
		}
	})
	 $.post("archives/bystatus",function(data){
		 loadingdata(data,"myChart6")
	})
	//职称分析
	$("#sectionTree7").tree({
		onClick:function(node){
			 $.post("archives/byjobTitle",{sid:node.id},function(data){
				 loadingdata(data,"myChart7")
			 })
		}
	})
	 $.post("archives/byjobTitle",function(data){
		 loadingdata(data,"myChart7")
	})
	//职务分析
	$("#sectionTree8").tree({
		onClick:function(node){
			 $.post("archives/byposition",{sid:node.id},function(data){
				 loadingdata2(data,"myChart8")
			 })
		}
	})
	 $.post("archives/byposition",function(data){
		 loadingdata2(data,"myChart8")
	})
	//婚姻
	$("#sectionTree9").tree({
		onClick:function(node){
			 $.post("archives/bymarital",{sid:node.id},function(data){
				 loadingdata(data,"myChart9")
			 })
		}
	})
	 $.post("archives/bymarital",function(data){
		 loadingdata(data,"myChart9")
	})
	
	
	
	
	
})

function loadingdata(data,str) {
	dataName = new Array()
    dataArr = new Array()
	 if (data == null || data.length == 0) {
		 dataName = new Array("暂无数据")
		 dataArr = new Array('0')
	}else{
		$.each(data,function(i){
			dataName.push(this.name.typename);
			 dataArr.push(this.num)
       	})
	}
	myChart1( dataName, dataArr,str);
}
function loadingdata2(data,str) {
	dataName = new Array()
    dataArr = new Array()
	 if (data == null || data.length == 0) {
		 dataName = new Array("暂无数据")
		 dataArr = new Array('0')
	}else{
		$.each(data,function(i){
			dataName.push(this.position.name);
			 dataArr.push(this.num)
       	})
	}
	myChart1( dataName, dataArr,str);
}

function myChart1( dataName, dataArr,str) {
	var $parent = $("#"+str).parent();
	//$parent.remove();
	var $node = $('<canvas id="'+str+'"></canvas>');

	$parent.html($node);

	var MyHuanChart = new Chart($node.get(0), {
         type: 'doughnut',
         data: {
             labels: dataName,
             datasets: [{
                 data: dataArr,
                 backgroundColor: ['rgb(54, 162, 235)', 'rgb(255, 205, 86)','rgb(221,77,64)', 'rgb(24,161,95)',  'rgb(201, 203, 207)','rgba(255, 99, 132, 0.2)',
                     'rgba(54, 162, 235, 0.2)',
                     'rgba(255, 206, 86, 0.2)',
                     'rgba(75, 192, 192, 0.2)',
                     'rgba(153, 102, 255, 0.2)',
                     'rgba(255, 159, 64, 0.2)']}]
         }
     })

}
</script>
</body>
</html>

