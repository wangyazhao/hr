<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
 	<form action="" id="permissionForm" method="post">
		<input type="hidden" name="id" />
		<table  border="0" cellpadding="5"  cellspacing="0"width="100%">
			<tbody>
				<tr>
					<td style="width: 25%">父级权限:</td>
					<td><input id="parentid" class="easyui-combotree" name="parentid"  value="0"
			 data-options="hasDownArrow:true,valueField:'id',textField:'text',url:'rolepermission/alls',panelHeight:'auto',editable:false,panelMaxHeight:250,panelMinWidth:150"></td>
				</tr>
				<tr>
					<td>资源类型:</td>
					<td><input class="easyui-combobox" name="type" style="width: auto;min-width: 100px;"  data-options="hasDownArrow:true,valueField:'typename',textField:'typename',url:'data/percode',panelHeight:'auto',panelMaxHeight:250,panelMinWidth:150"></td>
				</tr>
				<tr>
					<td>权限名称:</td>
					<td><input type="text" name="text" class="easyui-textbox"  /></td>
				</tr>
				<tr>
					<td>权限代码:</td>
					<td><input type="text" name="percode" class="easyui-textbox"  /></td>
				</tr>
				<tr>
					<td>Url:</td>
					<td><input type="text" name="url" class="easyui-textbox"  /></td>
				</tr>
				<tr>
					<td>状态:</td>
					<td><input name="available" value="1" class="easyui-switchbutton" data-options="onText:'Yes',offText:'No'"></td>
				</tr>
			</tbody>
		</table>
	</form>
</body>

</html>






