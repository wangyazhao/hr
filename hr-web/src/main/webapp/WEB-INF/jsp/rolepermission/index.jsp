<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
	<meta charset="UTF-8">
<title>角色授权</title>
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
<link rel="stylesheet" type="text/css" href="easyui/themes/material/easyui.css"/>
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
</head>
<body>
<script type="text/javascript"> 
 	$(function(){
 		$("#permissionTable").treegrid({
			pagination : true,
			toolbar:"#tb",
			idField:"id",
			treeField:"text",
			animate:true,
			pagination:false,
			onLoadSuccess : function(){
				//$(this).treegrid("collapseAll");
			},
			loadFilter : function(data){
				$.each(data,function(){
					this.state = "open";
				});
				return data;
			}
 		})
 	});
 	
 	function postFormatter(value,row,index){
 		if(row.available==1){
 			return "可用";
 		}
 		return "不可用";
 	}
 	
 	<shiro:hasPermission name="rolepermission:add">
 	function add_role(){
 		var d = $("<div></div>").appendTo("body");
 		
		d.dialog({
			title : "添加权限",
			iconCls : "icon-add",
			width:400,	
			height:300,
			modal:true,//是否是模态框
			href : "rolepermission/toadd",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#permissionForm").form("submit",{
						url : "rolepermission/add",
						success : function(data){
							d.dialog("close");
							$("#permissionTable").treegrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
 	}
 	</shiro:hasPermission>
 	
 	<shiro:hasPermission name="rolepermission:add">
 	function add_role1(){
 		var row = $("#permissionTable").datagrid("getSelected");
		if(row == null){
			return;
		}
 		var d = $("<div></div>").appendTo("body");
 		
		d.dialog({
			title : "添加权限",
			iconCls : "icon-add",
			width:400,	
			height:300,
			modal:true,//是否是模态框
			href : "rolepermission/toadd",
			onClose:function(){$(this).dialog("destroy"); },//destroy销毁
			onLoad:function(){
				$("#parentid").combotree("setValue",row.id);
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){//点击确定按钮的操作
					$("#permissionForm").form("submit",{
						url : "rolepermission/add",
						success : function(data){
							d.dialog("close");
							$("#permissionTable").treegrid("reload");
						}
					});
				}
			},{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
 	}
 	</shiro:hasPermission>
 	
 	<shiro:hasPermission name="rolepermission:edit">
	//修改权限
	function edit_role(){
		var row = $("#permissionTable").datagrid("getSelected");
		if(row == null){
			return;
		}
		var d = $("<div></div>").appendTo("body");
		d.dialog({
			title : "分配权限",
			iconCls : "icon-edit",
			width:400,
			height:300,
			modal:true,
			href : "rolepermission/toadd",
			onClose:function(){$(this).dialog("destroy"); },
			onLoad:function(){
				//发送异步请求，查询数据
	  			$.post("rolepermission/view",{id:row.id},function(data){ 
					$("#permissionForm").form("load",data); 
 				});  
			},
			buttons:[{
				iconCls:"icon-ok",
				text:"确定",
				handler:function(){
					$("#permissionForm").form("submit",{
						url : "rolepermission/edit",
						success : function(data){
							d.dialog("close"); 
							$("#permissionTable").treegrid("reload");
						}
					});
				}
			},
			<shiro:hasPermission name="rolepermission:del">
			{
				iconCls:"icon-remove",
				text:"删除",
				handler:function(){
					$.messager.confirm("提示","确定要删除选中的数据吗？",function(da){
						if(da){
							$.post("rolepermission/del",{id:row.id},function(data){
								if(data.result == true){
									// 删除成功后，刷新表格 reload
									d.dialog("close");
									$("#permissionTable").treegrid("reload");
								}
							})
						}
					})
				}
			},
			</shiro:hasPermission>
			{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					d.dialog("close");
				}
			}]
		});
	} 
	</shiro:hasPermission>
</script>  
 <table id="permissionTable" title="Permission List"
        data-options="url:'rolepermission/all',fitColumns:true,striped:true,iconCls:'icon-search'">
    <thead>
        <tr>
            <th data-options="field:'text',width:100,sortable:true">Text</th>
            <th data-options="field:'available',width:100,sortable:true,formatter:postFormatter">Available</th>
            <th data-options="field:'url',width:50">Url</th>
        </tr>
    </thead>
</table>
<div id="tb">
	<shiro:hasPermission name="rolepermission:add">
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_role();" data-options="iconCls:'icon-add',plain:true">添加</a>
	</shiro:hasPermission>
	<shiro:hasPermission name="rolepermission:edit">
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="edit_role();" data-options="iconCls:'icon-edit',plain:true">修改</a>
	</shiro:hasPermission>
	<shiro:hasPermission name="rolepermission:add">
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="add_role1();" data-options="iconCls:'icon-add',plain:true">在此节点添加</a>
	</shiro:hasPermission>
 </div>
 
</body>
</html>


 


