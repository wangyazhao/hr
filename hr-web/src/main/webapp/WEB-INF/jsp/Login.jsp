<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html >
<html>
<head>
<base href="<%=basePath%>">
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<meta name="keywords"
	content="Flat Dark Web Login Form Responsive Templates, Iphone Widget Template, Smartphone login forms,Login form, Widget Template, Responsive Templates, a Ipad 404 Templates, Flat Responsive Templates" />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<%@include file="/common.jsp"%>
<script type="text/javascript">
var ai = 0;
function login(){
	$('#ff').form('submit', {
	    url:"user/login",
	    onSubmit: function(){
	    	//验证输入是否合法
	    	
	    	
	    },
	    success:function(data){
			if(data==-2){
				console.log("用户不存在");
			}else if(data==-1){
				console.log("密码错误");
			}else{
				location.href = "hr/main";
				console.log("登录成功");
			}
	    }
	});
}
		

</script>
</head>
<body>
	<!--SIGN UP-->
	<h1>人力资源管理系统</h1>
	<div class="login-form">
		<div class="head-info">
			<label class="lbl-1"> </label> <label class="lbl-2"> </label> <label class="lbl-3"> </label>
		</div>
		<div class="clear"></div>
		<div class="avtar">
			<img src="images/avtar.png" />
		</div>
		<form id="ff" action="hr/login" method="post">
			<input type="text" class="text" name="username" placeholder="用户名">
			<div class="key">
				<input type="password" name="password" placeholder="密码">
			</div>
			<div class="signin">
				<input type="submit" class="submitButton" value="Login" >
			</div>
		</form>

	</div>
	<div class="copy-rights">
		<p>Copyright &copy; 2015.Company name All rights reserved.More
			Templates - Collect from</p>
	</div>

</body>
</html>