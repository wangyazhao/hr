/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.28 : Database - hr
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hr` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hr`;

/*Table structure for table `archives` */

DROP TABLE IF EXISTS `archives`;

CREATE TABLE `archives` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `worknum` varchar(50) DEFAULT NULL COMMENT '工号',
  `sex` int(5) DEFAULT NULL COMMENT '性别',
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `birthday` date DEFAULT NULL COMMENT '出生年月',
  `marital` int(5) DEFAULT NULL COMMENT '婚否',
  `politicalFace` int(5) DEFAULT NULL COMMENT '政治面貌',
  `health` varchar(50) DEFAULT NULL COMMENT '健康状况',
  `IDCard` varchar(50) DEFAULT NULL COMMENT '身份证号',
  `salt` varchar(50) DEFAULT NULL COMMENT '盐值',
  `nativePlace` int(5) DEFAULT NULL COMMENT '籍贯',
  `national` int(5) DEFAULT NULL COMMENT '民族',
  `section` int(11) DEFAULT NULL COMMENT '部门',
  `joinWorkTime` date DEFAULT NULL COMMENT '工作时间',
  `workYear` varchar(20) DEFAULT NULL COMMENT '工作年限',
  `education` int(5) DEFAULT NULL COMMENT '学历',
  `finishSchool` varchar(50) DEFAULT NULL COMMENT '毕业院校',
  `major` varchar(50) DEFAULT NULL COMMENT '专业',
  `graduateTime` date DEFAULT NULL COMMENT '毕业时间',
  `coursesLanguage` varchar(50) DEFAULT NULL COMMENT '所学外语',
  `languageDcale` varchar(50) DEFAULT NULL COMMENT '语言等级',
  `MasterDegree` varchar(50) DEFAULT NULL COMMENT '掌握计算机程度',
  `computerDcale` varchar(50) DEFAULT NULL COMMENT '计算机等级',
  `certificate` varchar(100) DEFAULT NULL COMMENT '所获证书',
  `educationResume` varchar(200) DEFAULT NULL COMMENT '教育简历',
  `specialty` varchar(50) DEFAULT NULL COMMENT '特长',
  `hobbys` varchar(50) DEFAULT NULL COMMENT '爱好',
  `address` varchar(200) DEFAULT NULL COMMENT '联系地址',
  `tel` varchar(50) DEFAULT NULL COMMENT '电话',
  `height` varchar(50) DEFAULT NULL COMMENT '身高',
  `weight` varchar(50) DEFAULT NULL COMMENT '体重',
  `bloodType` varchar(50) DEFAULT NULL COMMENT '血型',
  `workRequire` varchar(200) DEFAULT NULL COMMENT '工作要求',
  `zipCode` varchar(50) DEFAULT NULL COMMENT '邮政编码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `talentType` int(5) DEFAULT NULL COMMENT '人才分类',
  `TalentLevel` int(5) DEFAULT NULL COMMENT '人才分级',
  `socialRelations` varchar(200) DEFAULT NULL COMMENT '社会关系',
  `workExperience` varchar(200) DEFAULT NULL COMMENT '工作经历',
  `learningExperience` varchar(200) DEFAULT NULL COMMENT '学习经历',
  `accessory` varchar(200) DEFAULT NULL COMMENT '附件',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `entryTime` date DEFAULT NULL COMMENT '入职日期',
  `jobs` int(5) DEFAULT NULL COMMENT '员工岗位',
  `employeesWork` int(5) DEFAULT NULL COMMENT '员工工种',
  `position` int(11) DEFAULT NULL COMMENT '员工职务',
  `jobTitle` int(5) DEFAULT NULL COMMENT '员工职称',
  `basicSalary` varchar(50) DEFAULT NULL COMMENT '基本工资',
  `status` int(5) DEFAULT NULL COMMENT '员工状态',
  `bank` int(5) DEFAULT NULL COMMENT '工资卡开户行',
  `account` varchar(50) DEFAULT NULL COMMENT '工资卡帐号',
  `photo` varchar(50) DEFAULT NULL COMMENT '照片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

/*Data for the table `archives` */

insert  into `archives`(`id`,`worknum`,`sex`,`name`,`birthday`,`marital`,`politicalFace`,`health`,`IDCard`,`salt`,`nativePlace`,`national`,`section`,`joinWorkTime`,`workYear`,`education`,`finishSchool`,`major`,`graduateTime`,`coursesLanguage`,`languageDcale`,`MasterDegree`,`computerDcale`,`certificate`,`educationResume`,`specialty`,`hobbys`,`address`,`tel`,`height`,`weight`,`bloodType`,`workRequire`,`zipCode`,`email`,`talentType`,`TalentLevel`,`socialRelations`,`workExperience`,`learningExperience`,`accessory`,`remark`,`entryTime`,`jobs`,`employeesWork`,`position`,`jobTitle`,`basicSalary`,`status`,`bank`,`account`,`photo`) values (17,'21',89,'王坤','2018-04-03',86,13,'良好','130428199102281912',NULL,5,14,1,'2018-04-19','20',63,'北大青鸟','计算机','2018-04-24','英语','三级','初级','四级','架构师','大学毕业','平面设计','计算机','河北省邯郸市','18303013109','188','52','O','不加班','056000','1664747666@qq.com',33,30,'asdasd','qweq','三年基础Java',NULL,'18年应届毕业生','2018-04-11',102,34,2,39,'123',106,48,'627381726846518273727',''),(20,'AX-101',89,'楚文磊','1991-04-24',86,11,'良好','136456199650244424',NULL,7,14,2,'2016-01-18','3年',58,'北大青鸟','计算机','2018-04-11','英语','三级','高级','四级','架构师','电脑','腿','计算机','永年县名关镇','18393929321','173','52','O','给钱','056000','2341231231@qq.com',32,28,'工人','Java工程师','三年基础Java',NULL,'有能力，贼强','2018-04-13',101,34,2,37,'123',107,46,'627381726473718273727',NULL),(21,'AX-102',89,'王龙涛','2018-05-02',86,12,'良好','176786199402245634',NULL,7,14,1,'2017-03-22','2年',58,'南京大学','计算机','2018-07-12','英语','一级','高级','四级','平面设计师证','大学毕业','平面设计','打游戏','邢台市隆尧县','13434453424','155','49','A','轻松','056000','2342342344@qq.com',32,28,'农民','Java工程师','三年基础Java',NULL,'18年应届毕业生','2017-02-01',104,34,2,37,'6000',107,52,'212453463645474767567',NULL),(22,'AX-103',89,'龙阳','1999-11-26',87,13,'良好','186345199102256753',NULL,6,15,2,'2018-04-03','3年',57,'复旦大学','计算机','2018-08-16','英语','三级','高级','四级','软件工程师','大学毕业','运行维护','篮球','河北省邯郸市','13453453567','187','45','AB','不加班','056000','2123123423@qq.com',31,29,'农民','Java工程师','三年基础Java',NULL,'适应能力强','2018-08-15',103,35,2,36,'1111',106,45,'324224253564756758786',NULL),(23,'AX-104',90,'王文鹏','1999-07-23',87,12,'较差','184564199302234445',NULL,7,16,1,'2018-02-21','3年',58,'华东理工大学','计算机','2018-03-11','英语','三级','高级','四级','软件工程师','大学毕业','后端开发','乒乓球','河北省邯郸市','18345345322','176','46','A','不加班','200003','1231233635@qq.com',32,30,'农民','Java工程师','三年基础Java',NULL,'有较强的学习能力','2018-04-20',105,35,2,37,'6000',106,46,'899678656345342362443',NULL),(24,'AX-105',90,'申日杨','1998-11-05',86,11,'良好','130428199701231534',NULL,7,17,1,'2018-04-03','3年',59,'北京工业大学','计算机','2018-10-26','英语','三级','高级','四级','软件工程师','大学毕业','运行维护','打游戏','河北省邯郸市','13454456456','175','47','O','不加班','056000','5134134233@qq.com',33,29,'农民','Java工程师','三年基础Java',NULL,'有比较好的开发经验','2018-07-19',100,34,2,38,'6000',107,47,'523423425645745745734',NULL),(25,'AX-106',89,'梁浩伟','1994-04-06',88,13,'较差','125428199805423424',NULL,8,18,2,'2018-01-17','3年',60,'南京师范大学','计算机','2018-10-20','英语','三级','高级','四级','软件工程师','大学毕业','后端开发','篮球','河北省邯郸市','18634534543','165','48','AB','不加班','056000','3545376456@qq.com',31,30,'农民','Java工程师','三年基础Java',NULL,'开发阅历丰富','2018-04-01',104,34,2,39,'6000',106,48,'514323523526467586856',NULL),(26,'AX-107',89,'杜小飞','1994-07-22',87,11,'良好','156428199405648565',NULL,9,19,2,'2014-06-03','3年',61,'中国医科大学','计算机','2015-07-23','英语','三级','高级','四级','软件工程师','大学毕业','运行维护','打游戏','河北省邯郸市','15433543445','187','49','A','不加班','056000','5134234234@qq.com',32,30,'农民','Java工程师','三年基础Java',NULL,'适应能力强','2018-04-01',103,34,2,40,'3500',106,49,'223425235254574675678',NULL),(27,'AX-108',90,'赵英宏','1999-04-06',86,13,'较差','130428199304554345',NULL,10,20,2,'2015-07-17','3年',62,'西北大学','计算机','2017-09-23','英语','三级','高级','四级','软件工程师','大学毕业','后端开发','乒乓球','河北省邯郸市','18345345645','166','50','AB','不加班','200003','2341434523@qq.com',33,30,'农民','Java工程师','三年基础Java',NULL,'有比较好的开发经验','2018-08-25',104,34,2,41,'6000',106,50,'675856867456345252352',NULL),(28,'AX-109',89,'张睿','1998-04-06',86,12,'良好','156428199304554463',NULL,7,21,1,'2011-07-07','3年',63,'四川大学','计算机','2017-09-22','英语','三级','高级','四级','软件工程师','大学毕业','运行维护','打游戏','河北省邯郸市','18303013109','177','51','AB','不加班','056000','5234241134@qq.com',31,30,'农民','Java工程师','三年基础Java',NULL,'开发阅历丰富','2018-09-21',103,34,2,42,'6000',106,51,'534634634756853452323',NULL),(29,'AX-110',89,'李绍祥','1999-01-27',87,11,'良好','124428199304554345',NULL,8,22,1,'2016-12-30','3年',64,'北京化工大学','计算机','2018-07-13','英语','三级','高级','四级','软件工程师','大学毕业','后端开发','篮球','河北省邯郸市','18303013109','176','52','A','不加班','056000','5675675673@qq.com',32,30,'农民','Java工程师','三年基础Java',NULL,'有比较好的开发经验','2018-04-01',105,35,2,43,'6000',106,52,'356345362351315364574',NULL),(30,'AX-111',90,'罗达','1998-04-06',88,13,'良好','144428199304554345',NULL,6,23,2,'2018-04-07','3年',58,'燕山大学','计算机','2018-07-18','英语','三级','高级','四级','软件工程师','大学毕业','运行维护','打游戏','河北省邯郸市','18303013109','187','53','AB','不加班','056000','2613413423@qq.com',33,30,'农民','Java工程师','三年基础Java',NULL,'18年应届毕业生','2018-10-05',104,34,2,44,'5500',106,53,'342342545346346345278',NULL),(31,'AX-112',89,'胡亚捷','1996-12-06',86,12,'良好','177428199304554345',NULL,9,24,1,'2010-12-18','3年',63,'河北工业大学','计算机','2014-04-25','英语','三级','高级','四级','软件工程师','大学毕业','运行维护','篮球','河北省邯郸市','18345345677','178','54','O','不加班','200003','3452342342@qq.com',31,29,'农民','Java工程师','三年基础Java',NULL,'适应能力强','2018-08-25',103,34,2,39,'6000',107,54,'345363656456457589867',NULL),(32,'AX-113',89,'朱丽倩','1993-05-21',87,11,'较差','134428199304554345',NULL,5,25,2,'2014-06-12','3年',63,'河北师范大学','计算机','2018-02-21','英语','三级','高级','四级','软件工程师','大学毕业','后端开发','乒乓球','河北省邯郸市','18303013109','167','55','O','不加班','056000','2342342346@qq.com',32,30,'农民','北京Java工程师','三年基础Java',NULL,'18年应届毕业生','2018-04-01',104,34,2,39,'6000',106,55,'364574575232453453453',NULL),(33,'AX-114',90,'高大山','1995-04-06',88,12,'良好','163428199304554345',NULL,8,26,2,'2010-02-11','3年',58,'太原理工大学','计算机','2011-02-24','英语','三级','高级','四级','软件工程师','大学毕业','后端开发','打游戏','河北省邯郸市','18303013109','166','56','A','不加班','056000','5675675676@qq.com',33,30,'农民','Java工程师','三年基础Java',NULL,'有比较好的开发经验','2011-12-30',105,35,2,39,'6000',106,56,'113212423546363634553',NULL),(34,'AX-115',89,'甘铁生','1989-02-06',86,13,'良好','154428199304554345',NULL,6,21,2,'2015-07-03','3年',57,'大连海事大学','计算机','2016-11-14','英语','三级','高级','四级','软件工程师','大学毕业','运行维护','乒乓球','河北省邯郸市','18303013109','157','52','O','不加班','200003','3452323446@qq.com',31,30,'农民','Java工程师','三年基础Java',NULL,'适应能力强','2018-04-01',104,34,2,39,'7600',106,51,'235346345377567585674',NULL),(35,'AX-116',90,'程孝先','1990-10-06',86,12,'较差','123428199304554345',NULL,7,21,1,'2014-06-03','3年',58,'辽宁大学','计算机','2015-02-12','英语','三级','高级','四级','软件工程师','大学毕业','后端开发','打游戏','河北省邯郸市','18303013109','167','52','O','不加班','056000','4564575764@qq.com',32,29,'农民','Java工程师','三年基础Java',NULL,'有比较好的开发经验','2018-06-08',100,35,2,40,'6000',106,51,'423423565756757345452',NULL),(36,'AX-117',90,'赵大华','1999-11-12',87,11,'较差','175428199304554345',NULL,8,18,1,'2012-01-28','3年',57,'大连理工大学','计算机','2015-02-14','英语','三级','高级','四级','软件工程师','大学毕业','后端开发','篮球','河北省邯郸市','18303013109','188','53','A','不加班','056000','2323423423@qq.com',33,30,'农民','Java工程师','三年基础Java',NULL,'适应能力强','2018-08-26',105,34,2,40,'6000',107,51,'678768746524354534534',NULL),(37,'AX-118',89,'孙婉宛','1999-10-22',88,11,'良好','135428199304554345',NULL,6,18,1,'2015-07-31','3年',63,'同济大学','计算机','2016-04-02','英语','三级','高级','四级','软件工程师','大学毕业','运行维护','篮球','河北省邯郸市','13543453634','156','52','O','不加班','200003','2341431342@qq.com',31,30,'农民','Java工程师','三年基础Java',NULL,'开发阅历丰富','2018-10-27',100,34,2,40,'6000',106,51,'864564245234234242342',NULL),(38,'AX-119',89,'孙玉轩','1994-12-02',86,13,'良好','124564199302234445',NULL,7,14,1,'2015-07-03','3年',57,'上海外国语大学','计算机','2016-11-24','英语','三级','高级','四级','软件工程师','大学毕业','后端开发','打游戏','河北省邯郸市','15645647334','176','53','A','不加班','056000','2342645645@qq.com',32,29,'农民','Java工程师','三年基础Java',NULL,'18年应届毕业生','2018-06-29',100,34,2,40,'8600',107,53,'134265645645242343422',NULL),(39,'AX-120',89,'王子久','1999-07-23',88,12,'较差','175428199304554345',NULL,8,14,1,'2005-08-21','3年',58,'复旦大学','计算机','2006-04-02','英语','三级','高级','四级','软件工程师','大学毕业','后端开发','乒乓球','河北省邯郸市','18453453452','167','53','A','不加班','056000','3452342346@qq.com',33,30,'农民','Java工程师','三年基础Java',NULL,'有较强的学习能力','2018-04-01',105,35,2,43,'6000',106,53,'434234265645634524523',NULL),(40,'AX-121',90,'余克勤','1999-11-26',87,13,'较差','146428199304554345',NULL,9,14,2,'2013-06-19','3年',57,'上海交通大学','计算机','2014-04-02','英语','三级','高级','四级','软件工程师','大学毕业','运行维护','打游戏','河北省邯郸市','18323423456','179','56','O','不加班','200003','3634534521@qq.com',31,30,'农民','Java工程师','三年基础Java',NULL,'适应能力强','2018-06-22',100,34,2,43,'6000',106,52,'142342342342232342665',NULL),(41,'AX-122',89,'李林臻','1995-09-02',87,12,'良好','123428199304554345',NULL,5,21,1,'2014-06-03','3年',58,'辽宁大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','后端开发','打游戏','河北省邯郸市','17645634522','188','53','AB','不加班','200003','2613413423@qq.com',32,29,'农民','Java工程师','三年基础Java',NULL,'有比较好的开发经验','2018-08-26',104,35,2,43,'50000',107,50,'234252352352342342344',NULL),(42,'AX-123',89,'王燕','1998-07-16',87,12,'良好','123428199304554345',NULL,5,21,2,'2015-07-03','3年',58,'燕山大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','后端开发','篮球','河北省邯郸市','18323423456','167','52','O','不加班','056000','3452342346@qq.com',31,28,'农民','Java工程师','三年基础Java',NULL,'适应能力强','2018-04-20',105,34,2,43,'8000',106,52,'645634525234142342342',NULL),(43,'AX-124',89,'周克涛','1989-07-28',86,11,'良好','123428199304554345',NULL,6,18,1,'2018-01-17','3年',63,'辽宁大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','后端开发','乒乓球','河北省邯郸市','18323423456','188','53','AB','不加班','200003','5675675673@qq.com',31,29,'农民','Java工程师','三年基础Java',NULL,'有较强的学习能力','2018-08-26',105,35,2,44,'8000',106,52,'534537363453452323424',NULL),(44,'AX-125',89,'卢兰凤','1995-08-26',87,13,'良好','123428199304554345',NULL,7,18,1,'2015-07-03','3年',63,'四川大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','后端开发','网球','河北省邯郸市','18323423456','156','55','A','不加班','200003','5675675673@qq.com',31,28,'农民','Java工程师','三年基础Java',NULL,'适应能力强','2018-04-01',101,34,2,36,'8000',107,50,'624234234645767567354',NULL),(45,'AX-126',89,'张荷','1999-07-23',86,13,'良好','123428199304554345',NULL,8,21,1,'2018-01-17','3年',57,'辽宁大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','运行维护','网球','河北省邯郸市','16433546345','167','55','O','不加班','200003','1231233635@qq.com',32,29,'农民','Java工程师','三年基础Java',NULL,'有比较好的开发经验','2018-10-05',105,35,2,44,'8000',106,56,'623525234234234234456',NULL),(46,'AX-127',89,'邓天','1994-06-23',88,12,'良好','123428199304554345',NULL,7,18,2,'2018-01-17','3年',63,'南京大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','后端开发','打游戏','河北省邯郸市','17456563453','156','55','AB','不加班','200003','5675675673@qq.com',33,30,'农民','Java工程师','三年基础Java',NULL,'适应能力强','2018-08-26',104,34,2,36,'8000',106,56,'756734354245234234134',NULL),(47,'AX-128',89,'刘嘉佳','1994-07-15',86,13,'良好','123428199304554345',NULL,8,21,2,'2014-06-03','3年',57,'上海外国语大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','后端开发','打游戏','河北省邯郸市','15345363446','188','55','O','不加班','200003','3452342346@qq.com',33,28,'农民','Java工程师','三年基础Java',NULL,'有比较好的开发经验','2018-04-01',104,35,2,44,'8000',106,56,'634523524534534523423',NULL),(48,'AX-129',89,'陈秀春','1994-06-02',86,11,'良好','123428199304554345',NULL,8,21,1,'2018-01-17','3年',57,'太原理工大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','运行维护','篮球','河北省邯郸市','17645435653','167','52','A','不加班','200003','1231233635@qq.com',32,28,'农民','Java工程师','三年基础Java',NULL,'有比较好的开发经验','2018-04-20',101,34,2,43,'8000',106,50,'134232342534535645646',NULL),(49,'AX-130',90,'许青云','1993-11-27',86,12,'良好','123428199304554345',NULL,9,21,1,'2018-02-21','3年',58,'四川大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','后端开发','网球','河北省邯郸市','18573738482','156','55','AB','不加班','056000','2613413423@qq.com',33,30,'农民','Java工程师','三年基础Java',NULL,'有较强的学习能力','2018-10-05',103,34,2,36,'8000',106,56,'634523425234123423543',NULL),(50,'AX-131',89,'周彦楠','1994-06-24',88,11,'较差','123428199304554345',NULL,8,18,2,'2015-07-03','3年',63,'复旦大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','运行维护','打游戏','河北省邯郸市','18323423456','156','52','O','不加班','200003','3452342346@qq.com',33,28,'农民','Java工程师','三年基础Java',NULL,'适应能力强','2018-08-26',103,34,6,44,'333333',107,50,'634523423412233534636',NULL),(51,'AX-132',89,'王盼盼','1999-07-23',86,12,'良好','123428199304554345',NULL,9,21,2,'2018-02-21','3年',58,'上海交通大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','运行维护','篮球','河北省邯郸市','17494838472','156','56','A','不加班','200003','1231233635@qq.com',33,30,'农民','Java工程师','三年基础Java',NULL,'有较强的学习能力','2018-10-05',100,34,2,43,'8000',107,51,'867567456456456344244',NULL),(52,'AX-133',90,'茹然','1995-08-05',88,12,'良好','123428199304554345',NULL,7,18,1,'2015-07-03','3年',63,'南京大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','后端开发','网球','河北省邯郸市','18435345355','167','55','O','不加班','200003','5675675673@qq.com',32,29,'农民','Java工程师','三年基础Java',NULL,'18年应届毕业生','2018-04-01',103,35,2,43,'8000',106,51,'142334534536364523423',NULL),(53,'AX-134',89,'李娜','1993-06-24',88,11,'良好','134438199304554345',NULL,9,21,1,'2018-02-21','3年',57,'太原理工大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','后端开发','篮球','河北省邯郸市','19844534534','156','52','O','不加班','056000','2613413423@qq.com',33,30,'农民','Java工程师','三年基础Java',NULL,'18年应届毕业生','2018-10-05',101,35,2,44,'8000',106,51,'234266564564634523445',NULL),(54,'AX-135',90,'袁嫚玲','1994-05-21',86,12,'良好','152428199404554345',NULL,10,21,2,'2014-06-03','3年',58,'辽宁大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','运行维护','网球','河北省邯郸市','17535345345','156','56','A','不加班','200003','3452342346@qq.com',33,30,'农民','Java工程师','三年基础Java',NULL,'有较强的学习能力','2018-04-20',103,34,2,36,'8000',107,50,'568678678678456345345',NULL),(55,'AX-136',89,'沈雨露','1995-02-28',86,11,'较差','134428199504554345',NULL,8,21,1,'2018-02-21','3年',57,'太原理工大学','计算机',NULL,'英语','三级','高级','四级','软件工程师','大学毕业','后端开发','打游戏','河北省邯郸市','17546546546','167','56','AB','不加班','056000','1231233635@qq.com',32,29,'农民','Java工程师','三年基础Java',NULL,'有比较好的开发经验','2018-04-01',101,35,2,44,'8000',106,51,'235345345234234232664',NULL),(57,'GH-93624058',89,'请问','2018-04-10',87,13,'阿斯顿','15884685484',NULL,7,14,20,'2018-04-03','5',60,'','','2018-04-17','','','','','','','','','','1586484651','','','','','','123@qq.com',32,28,'','','',NULL,'','2018-04-10',102,34,3,40,'6000',106,49,'34538486345644',NULL);

/*Table structure for table `audit` */

DROP TABLE IF EXISTS `audit`;

CREATE TABLE `audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '审核',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间',
  `opinion` varchar(500) DEFAULT NULL COMMENT '意见',
  `whether` int(5) DEFAULT NULL COMMENT '是否通过',
  `cultivate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `audit` */

insert  into `audit`(`id`,`time`,`opinion`,`whether`,`cultivate`) values (1,'2018-04-04 09:02:34','计划完美',1,1),(2,'2018-04-04 10:08:38','asd',1,3),(3,'2018-04-06 22:07:31','849561',2,7),(4,'2018-04-07 11:32:33','123',1,10),(5,'2018-04-07 12:01:01','haixing',1,11);

/*Table structure for table `certificateinfo` */

DROP TABLE IF EXISTS `certificateinfo`;

CREATE TABLE `certificateinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '证照信息',
  `type` int(5) DEFAULT NULL COMMENT '证照类型',
  `number` varchar(50) DEFAULT NULL COMMENT '证照编号',
  `name` varchar(50) DEFAULT NULL COMMENT '证照名称',
  `certificate` int(5) DEFAULT NULL COMMENT '发证机构',
  `time` varchar(50) DEFAULT NULL COMMENT '取证日期',
  `operant` varchar(50) DEFAULT NULL COMMENT '生效日期',
  `periodState` int(5) DEFAULT NULL COMMENT '有无期限',
  `expire` varchar(50) DEFAULT NULL COMMENT '到期日期',
  `state` int(5) DEFAULT NULL COMMENT '状态',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `accessory` varchar(200) DEFAULT NULL COMMENT '附件',
  `archives` int(11) DEFAULT NULL COMMENT '员工',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='证照信息';

/*Data for the table `certificateinfo` */

insert  into `certificateinfo`(`id`,`type`,`number`,`name`,`certificate`,`time`,`operant`,`periodState`,`expire`,`state`,`remark`,`accessory`,`archives`) values (6,110,'786453','4651',81,'2018-04-05','2018-04-03',3,'2018-04-04',115,'456',NULL,17),(7,110,'2523','22342',84,'2018-04-10','2018-04-10',3,'2020-04-15',115,'百日相互',NULL,33),(8,110,'453453','身份证',84,'2018-04-13','2018-04-13',3,'2021-04-14',115,'asd',NULL,57);

/*Table structure for table `changeinfo` */

DROP TABLE IF EXISTS `changeinfo`;

CREATE TABLE `changeinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `time` varchar(50) DEFAULT NULL COMMENT '变更时间',
  `type` int(5) DEFAULT NULL COMMENT '变更类别',
  `updateText` text COMMENT '合同内容',
  `contractInfo` int(11) DEFAULT NULL COMMENT '合同信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='合同变更信息';

/*Data for the table `changeinfo` */

insert  into `changeinfo`(`id`,`time`,`type`,`updateText`,`contractInfo`) values (4,'2018-03-01',78,']aaa,a\"}',2),(5,'2018-03-13',76,'增加了一个条款',3),(6,'2018-03-14',77,'ad',2),(7,'2018-04-12',78,'大苏打',6);

/*Table structure for table `contractinfo` */

DROP TABLE IF EXISTS `contractinfo`;

CREATE TABLE `contractinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `contractNo` varchar(50) DEFAULT NULL COMMENT '合同编号',
  `name` varchar(50) DEFAULT NULL COMMENT '合同名称',
  `type` int(5) DEFAULT NULL COMMENT '合同类型',
  `deadline` int(5) DEFAULT NULL COMMENT '有无期限',
  `positive` int(5) DEFAULT NULL COMMENT '是否转正',
  `signing` date DEFAULT NULL COMMENT '签约时间',
  `contractYear` varchar(50) DEFAULT NULL COMMENT '合同年份',
  `textc` text COMMENT '合同内容',
  `accessory` varchar(200) DEFAULT NULL COMMENT '附件',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `trialPeriod` varchar(50) DEFAULT NULL COMMENT '试用期限',
  `trialWage` varchar(50) DEFAULT NULL COMMENT '试用基本工资',
  `trialOperant` date DEFAULT NULL COMMENT '试用生效时间',
  `trialExpire` date DEFAULT NULL COMMENT '试用到期时间',
  `contracPeriod` varchar(50) DEFAULT NULL COMMENT '合同期限',
  `positiveWage` varchar(50) DEFAULT NULL COMMENT '转正基本工资',
  `positiveOperant` date DEFAULT NULL COMMENT '生效时间',
  `positiveExpire` date DEFAULT NULL COMMENT '到期时间',
  `contractState` int(5) DEFAULT NULL COMMENT '合同状态',
  `archives` int(11) DEFAULT NULL COMMENT '员工',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='合同信息';

/*Data for the table `contractinfo` */

insert  into `contractinfo`(`id`,`contractNo`,`name`,`type`,`deadline`,`positive`,`signing`,`contractYear`,`textc`,`accessory`,`remark`,`trialPeriod`,`trialWage`,`trialOperant`,`trialExpire`,`contracPeriod`,`positiveWage`,`positiveOperant`,`positiveExpire`,`contractState`,`archives`) values (6,'11','开发',73,3,2,'2018-04-11','2018-04-12','人才',NULL,'','5年','1000','2018-04-18','2018-05-01','2018-05-01','2000','2018-07-13','2018-08-23',69,17),(7,'HT-82314800','123',72,3,1,'2018-04-04','2018-04-05','123',NULL,'123','3','500','2018-04-05','2018-04-26','2018-04-06','8000','2018-04-26','2018-04-04',71,20),(8,'HT-95317152','2324',73,3,1,'2018-04-10','2018-04-11','awiudhaijod',NULL,'qwrqwr','4534','33333','2018-04-10','2018-06-21','2018-04-11','666666','2018-04-17','2018-03-28',71,22),(9,'HT-21670811','fsd',72,3,1,'2018-04-10','2018-04-11','sannian',NULL,'','3','3000','2018-04-13','2018-04-13','2018-06-22','6000','2018-04-19','2021-04-21',69,57);

/*Table structure for table `cultivate` */

DROP TABLE IF EXISTS `cultivate`;

CREATE TABLE `cultivate` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '培训计划',
  `name` varchar(50) DEFAULT NULL COMMENT '培训计划名称',
  `time` date DEFAULT NULL COMMENT '时间',
  `section` int(11) DEFAULT NULL COMMENT '主办部门',
  `num` int(5) DEFAULT NULL COMMENT '计划参与培训人数',
  `budget` varchar(50) DEFAULT NULL COMMENT '培训预算',
  `place` varchar(50) DEFAULT NULL COMMENT '培训地点',
  `tel` varchar(50) DEFAULT NULL COMMENT '培训机构联系方式',
  `approver` int(11) DEFAULT NULL COMMENT '审批人',
  `cultivatremark` varchar(500) DEFAULT NULL COMMENT '培训说明',
  `attachment` varchar(50) DEFAULT NULL COMMENT '附件',
  `info` varchar(500) DEFAULT NULL COMMENT '培训机构相关信息',
  `no` varchar(50) DEFAULT NULL COMMENT '编号',
  `statu` int(5) DEFAULT NULL COMMENT '状态',
  `head` varchar(50) DEFAULT NULL COMMENT '负责人',
  `type` int(5) DEFAULT NULL COMMENT '培训类型',
  `way` int(5) DEFAULT NULL COMMENT '培训方式',
  `outtime` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `cultivate` */

insert  into `cultivate`(`id`,`name`,`time`,`section`,`num`,`budget`,`place`,`tel`,`approver`,`cultivatremark`,`attachment`,`info`,`no`,`statu`,`head`,`type`,`way`,`outtime`) values (7,'123','2018-04-07',2,NULL,'123','123','123',17,'123','','123','JH-52356975',144,'楚文磊',135,139,'2018-04-06'),(8,'ad','2018-04-12',2,NULL,'a','a','a',17,'','ceshi1.docx','','JH-33327568',143,'王文鹏',135,139,'2018-04-05'),(9,'awef','2018-04-24',18,NULL,'2342','werwe','wwer',20,'werwer','','234234','JH-22351638',143,'杜小飞',136,140,'2018-04-27'),(10,'123','2018-04-05',2,NULL,'1','123','123',20,'12','ceshi2.docx','123','JH-47290728',145,'楚文磊',135,139,'2018-04-04'),(11,'asd','2018-04-12',20,NULL,'2000','asdas','45343453',20,'asdja','测试1.docx','','JH-56221435',145,'请问',136,140,'2018-04-20');

/*Table structure for table `cultivatearchives` */

DROP TABLE IF EXISTS `cultivatearchives`;

CREATE TABLE `cultivatearchives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cultivate` int(11) DEFAULT NULL,
  `archives` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*Data for the table `cultivatearchives` */

insert  into `cultivatearchives`(`id`,`cultivate`,`archives`) values (5,5,17),(6,5,18),(7,6,17),(8,6,18),(13,4,18),(14,7,17),(15,7,20),(20,9,23),(21,9,22),(22,8,17),(23,8,20),(24,8,21),(25,8,23),(26,10,17),(27,10,20),(28,11,36),(29,11,37),(30,11,38);

/*Table structure for table `datadictionary` */

DROP TABLE IF EXISTS `datadictionary`;

CREATE TABLE `datadictionary` (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT '数据字典',
  `typeCode` varchar(50) DEFAULT NULL COMMENT '类型代码',
  `typeName` varchar(50) DEFAULT NULL COMMENT '类型名字',
  `valueId` int(5) DEFAULT NULL COMMENT '值id',
  `valueName` varchar(50) DEFAULT NULL COMMENT '值名字',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf8 COMMENT='数据字典';

/*Data for the table `datadictionary` */

insert  into `datadictionary`(`id`,`typeCode`,`typeName`,`valueId`,`valueName`) values (1,'whether','是',NULL,NULL),(2,'whether','否',NULL,NULL),(3,'without','有',NULL,NULL),(4,'without','无',NULL,NULL),(5,'nativePlace','北京',NULL,NULL),(6,'nativePlace','上海',NULL,NULL),(7,'nativePlace','河北',NULL,NULL),(8,'nativePlace','深圳',NULL,NULL),(9,'nativePlace','天津',NULL,NULL),(10,'nativePlace','重庆',NULL,NULL),(11,'politicalFace','党员',NULL,NULL),(12,'politicalFace','团员',NULL,NULL),(13,'politicalFace','群众',NULL,NULL),(14,'national','汉族',NULL,NULL),(15,'national','满族',NULL,NULL),(16,'national','藏族',NULL,NULL),(17,'national','回族',NULL,NULL),(18,'national','壮族',NULL,NULL),(19,'national','维吾尔族',NULL,NULL),(20,'national','朝鲜族',NULL,NULL),(21,'national','蒙古族',NULL,NULL),(22,'national','白族',NULL,NULL),(23,'national','傣族',NULL,NULL),(24,'national','高山族',NULL,NULL),(25,'national','苗族',NULL,NULL),(26,'national','怒族',NULL,NULL),(27,'rcFenJi','处级',NULL,NULL),(28,'rcFenJi','科级',NULL,NULL),(29,'rcFenJi','部级',NULL,NULL),(30,'rcFenJi','工人',NULL,NULL),(31,'rcFenLei','高级',NULL,NULL),(32,'rcFenLei','中级',NULL,NULL),(33,'rcFenLei','初级',NULL,NULL),(34,'employeesWork','合同工',NULL,NULL),(35,'employeesWork','临时工',NULL,NULL),(36,'zhiCheng','初级工程师',NULL,NULL),(37,'zhiCheng','中级工程师',NULL,NULL),(38,'zhiCheng','高级工程师',NULL,NULL),(39,'zhiCheng','助理会计师',NULL,NULL),(40,'zhiCheng','会计师',NULL,NULL),(41,'zhiCheng','高级会计师',NULL,NULL),(42,'zhiCheng','系统分析师',NULL,NULL),(43,'zhiCheng','助理分析师',NULL,NULL),(44,'zhiCheng','分析员',NULL,NULL),(45,'bank','中国工商银行',NULL,NULL),(46,'bank','中国建设银行',NULL,NULL),(47,'bank','中国农业银行',NULL,NULL),(48,'bank','中国邮储银行',NULL,NULL),(49,'bank','中国银行',NULL,NULL),(50,'bank','交通银行',NULL,NULL),(51,'bank','招商银行',NULL,NULL),(52,'bank','中信银行',NULL,NULL),(53,'bank','民生银行',NULL,NULL),(54,'bank','浦发银行',NULL,NULL),(55,'bank','光大银行',NULL,NULL),(56,'bank','华夏银行',NULL,NULL),(57,'education','小学',NULL,NULL),(58,'education','初中',NULL,NULL),(59,'education','高中',NULL,NULL),(60,'education','中专',NULL,NULL),(61,'education','大专',NULL,NULL),(62,'education','本科',NULL,NULL),(63,'education','硕士',NULL,NULL),(64,'education','博士',NULL,NULL),(65,'way','现场考核',NULL,NULL),(66,'way','网上考核',NULL,NULL),(67,'contractState','试用中',NULL,NULL),(68,'contractState','试用到期',NULL,NULL),(69,'contractState','生效中',NULL,NULL),(70,'contractState','已到期',NULL,NULL),(71,'contractState','已解除',NULL,NULL),(72,'contractType','劳动合同',NULL,NULL),(73,'contractType','保密合同',NULL,NULL),(74,'renewType','正常延期',NULL,NULL),(75,'renewType','项目延期',NULL,NULL),(76,'changeType','条款变更',NULL,NULL),(77,'changeType','联系方式变更',NULL,NULL),(78,'changeType','金额变更',NULL,NULL),(79,'changeType','联系人变更',NULL,NULL),(80,'changeType','其它变更',NULL,NULL),(81,'certificate','国家教育部',NULL,NULL),(82,'certificate','人力资源和社会保障部',NULL,NULL),(83,'certificate','国家信息化协会',NULL,NULL),(84,'certificate','公安局',NULL,NULL),(85,'certificate','车辆管理所',NULL,NULL),(86,'marital','未婚',NULL,NULL),(87,'marital','已婚',NULL,NULL),(88,'marital','离异',NULL,NULL),(89,'sex','男',NULL,NULL),(90,'sex','女',NULL,NULL),(91,'bank','邯郸银行',NULL,NULL),(92,'sectionType','管理',NULL,NULL),(93,'sectionType','生产',NULL,NULL),(94,'sectionType','营销',NULL,NULL),(95,'sectionType','运输',NULL,NULL),(96,'sectionType','工作',NULL,NULL),(97,'sectionPermissions','允许管理子部们数据',NULL,NULL),(98,'sectionPermissions','不允许管理子部们数据',NULL,NULL),(99,'gangwei','经理',NULL,NULL),(100,'gangwei','主任',NULL,NULL),(101,'gangwei','科长',NULL,NULL),(102,'gangwei','总监',NULL,NULL),(103,'gangwei','外聘',NULL,NULL),(104,'gangwei','职员',NULL,NULL),(105,'gangwei','其它',NULL,NULL),(106,'ygzt','在职',NULL,NULL),(107,'ygzt','离职',NULL,NULL),(108,'jc','奖励',NULL,NULL),(109,'jc','惩罚',NULL,NULL),(110,'ZZtype','身份证',NULL,NULL),(111,'ZZtype','驾驶证',NULL,NULL),(112,'ZZtype','营业执照',NULL,NULL),(113,'ZZtype','职业资格证',NULL,NULL),(114,'ZZtype','护照',NULL,NULL),(115,'zhuangtai','已生效',NULL,NULL),(116,'zhuangtai','未生效',NULL,NULL),(117,'zhuangtai','已到期',NULL,NULL),(118,'ddtype','晋升',NULL,NULL),(119,'ddtype','降级',NULL,NULL),(120,'ddtype','平调',NULL,NULL),(121,'ddtype','轮岗',NULL,NULL),(122,'ddtype','工资调整',NULL,NULL),(123,'departure','辞退',NULL,NULL),(124,'departure','外调',NULL,NULL),(125,'departure','退休',NULL,NULL),(126,'departure','死亡',NULL,NULL),(127,'reinstatement','回调',NULL,NULL),(128,'reinstatement','复原',NULL,NULL),(129,'jctype','离职',NULL,NULL),(130,'jctype','调岗',NULL,NULL),(131,'positivetype','试用到期',NULL,NULL),(132,'positivetype','提前转正',NULL,NULL),(133,'percode','permission',NULL,NULL),(134,'percode','menu',NULL,NULL),(135,'cultivatetype','讨论',NULL,NULL),(136,'cultivatetype','讲座',NULL,NULL),(137,'cultivatetype','演练',NULL,NULL),(138,'cultivatetype','座谈',NULL,NULL),(139,'cultivateway','内部现场培训',NULL,NULL),(140,'cultivateway','内部网络培训',NULL,NULL),(141,'cultivateway','国内现场培训',NULL,NULL),(142,'cultivateway','国内网络培训',NULL,NULL),(143,'cultivatezt','未审核',NULL,NULL),(144,'cultivatezt','审核未通过',NULL,NULL),(145,'cultivatezt','审核通过',NULL,NULL);

/*Table structure for table `departure` */

DROP TABLE IF EXISTS `departure`;

CREATE TABLE `departure` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '离职表',
  `time` date DEFAULT NULL COMMENT '离职时间',
  `moneytime` date DEFAULT NULL COMMENT '工资截至时间',
  `gowhere` varchar(100) DEFAULT NULL COMMENT '去向',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `archives` int(11) DEFAULT NULL COMMENT '员工',
  `type` int(5) DEFAULT NULL COMMENT '离职类型',
  `whether` int(5) DEFAULT NULL COMMENT '是否解除合同',
  `jctype` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `departure` */

insert  into `departure`(`id`,`time`,`moneytime`,`gowhere`,`remark`,`archives`,`type`,`whether`,`jctype`) values (3,'2018-04-06','2018-03-29','23','121',17,124,1,NULL),(4,'2018-04-03','2018-03-29','123','123',17,124,1,129),(5,'2018-04-01','2018-04-04','a','a',17,123,2,129),(6,'2018-04-05','2018-04-04','1','12',17,124,1,130),(7,'2018-04-05','2018-04-04','1','1',17,124,2,NULL),(8,'2018-04-05','2018-04-10','1','123',17,124,2,NULL),(9,'2018-04-11','2018-04-11','天堂','啊实打实的',28,126,2,NULL),(10,'2018-04-13','2018-04-19','北京','委任为',57,125,1,129),(11,'2018-04-04','2018-04-03','123','123',20,123,1,130);

/*Table structure for table `employedinfo` */

DROP TABLE IF EXISTS `employedinfo`;

CREATE TABLE `employedinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `time` varchar(50) DEFAULT NULL COMMENT '入职日期',
  `current` varchar(50) DEFAULT NULL COMMENT '目前月薪',
  `expect` varchar(50) DEFAULT NULL COMMENT '期望月薪',
  `section` int(11) DEFAULT NULL COMMENT '应聘部门',
  `jobs` int(5) DEFAULT NULL COMMENT '应聘岗位',
  `position` int(5) DEFAULT NULL COMMENT '员工职务',
  `source` int(5) DEFAULT NULL COMMENT '招聘来源',
  `way` int(5) DEFAULT NULL COMMENT '员工工种',
  `result` int(5) DEFAULT NULL COMMENT '评审结果过',
  `photo` varchar(50) DEFAULT NULL COMMENT '相片',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `state` int(5) DEFAULT NULL COMMENT '储备=1/录入=0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='录用信息';

/*Data for the table `employedinfo` */

/*Table structure for table `interview` */

DROP TABLE IF EXISTS `interview`;

CREATE TABLE `interview` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '面试主题',
  `subject` varchar(50) DEFAULT NULL COMMENT '主题',
  `text` varchar(500) DEFAULT NULL COMMENT 'text',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `interviewInfo_id` int(11) DEFAULT NULL COMMENT '面试人员信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='面试主题';

/*Data for the table `interview` */

/*Table structure for table `interviewinfo` */

DROP TABLE IF EXISTS `interviewinfo`;

CREATE TABLE `interviewinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '面试人员信息',
  `name` char(10) DEFAULT NULL COMMENT '姓名',
  `br` char(10) DEFAULT NULL,
  `nativePlace` int(5) DEFAULT NULL COMMENT '籍贯',
  `education` int(5) DEFAULT NULL COMMENT '学历',
  `major` varchar(50) DEFAULT NULL COMMENT '所学专业',
  `tel` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `adderss` varchar(50) DEFAULT NULL COMMENT '居住地址',
  `email` varchar(50) DEFAULT NULL COMMENT 'Email',
  `armerk` varchar(300) DEFAULT NULL COMMENT '备注',
  `interviewsubject_id` int(11) DEFAULT NULL COMMENT '面试主题',
  `RecruitmentDepartment` int(11) DEFAULT NULL COMMENT '招聘部门',
  `employeesWork` int(5) DEFAULT NULL COMMENT '工种',
  `time` varchar(50) DEFAULT NULL COMMENT '面试时间',
  `jobs` int(5) NOT NULL COMMENT '面试岗位',
  `way` int(5) DEFAULT NULL COMMENT '面试方式',
  `result` int(5) DEFAULT NULL COMMENT '面试结果',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='面试人员信息';

/*Data for the table `interviewinfo` */

/*Table structure for table `loginlog` */

DROP TABLE IF EXISTS `loginlog`;

CREATE TABLE `loginlog` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '登陆日志',
  `usercode` varchar(50) DEFAULT NULL COMMENT '用户',
  `IP` varchar(100) DEFAULT NULL COMMENT 'IP',
  `port` varchar(50) DEFAULT NULL COMMENT '端口号',
  `module` varchar(50) DEFAULT NULL COMMENT '模块',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间',
  `username` varchar(50) DEFAULT NULL COMMENT '登陆账号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=789 DEFAULT CHARSET=utf8 COMMENT='登陆日志';

/*Data for the table `loginlog` */

insert  into `loginlog`(`id`,`usercode`,`IP`,`port`,`module`,`time`,`username`) values (1,'GLY-0000001','0:0:0:0:0:0:0:1',NULL,NULL,'2018-04-06 00:03:57','admin'),(2,'GLY-0000001','0:0:0:0:0:0:0:1',NULL,NULL,'2018-04-06 00:04:04','admin'),(3,'GLY-0000001','0:0:0:0:0:0:0:1',NULL,NULL,'2018-04-06 00:18:26','admin'),(4,'GLY-0000001','0:0:0:0:0:0:0:1',NULL,NULL,'2018-04-06 00:22:10','admin'),(5,'GLY-0000001','0:0:0:0:0:0:0:1',NULL,NULL,'2018-04-06 00:26:45','admin'),(6,'GLY-0000001','0:0:0:0:0:0:0:1',NULL,NULL,'2018-04-06 00:29:41','admin'),(7,'GLY-0000001','0:0:0:0:0:0:0:1',NULL,NULL,'2018-04-06 00:29:49','admin'),(8,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 00:48:30','admin'),(9,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-06 00:48:44','admin'),(10,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-06 00:48:49','admin'),(11,'GLY-0000001','127.0.0.1','8888','日志管理','2018-04-06 00:53:54','admin'),(12,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限管理','2018-04-06 00:54:08','admin'),(13,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 01:00:05','admin'),(14,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-06 01:00:12','admin'),(15,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 01:02:07','admin'),(16,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-06 01:02:12','admin'),(17,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 01:02:22','admin'),(18,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 01:03:07','admin'),(19,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 01:03:15','admin'),(20,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-06 01:03:18','admin'),(21,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 01:03:38','admin'),(22,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-06 01:03:41','admin'),(23,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 01:05:13','admin'),(24,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-06 01:05:17','admin'),(25,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 01:06:56','admin'),(26,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-06 01:07:01','admin'),(27,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 01:07:11','admin'),(28,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-06 01:07:25','admin'),(29,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 01:11:17','admin'),(30,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 01:11:44','wang'),(31,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 01:12:06','wang'),(32,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 01:13:05','wang'),(33,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:02:09','admin'),(34,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:03:19','admin'),(35,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:05:32','admin'),(36,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:06:06','admin'),(37,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:06:18','admin'),(38,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:07:05','admin'),(39,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:07:26','admin'),(40,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:08:08','admin'),(41,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:08:15','admin'),(42,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:08:36','admin'),(43,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:09:33','admin'),(44,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:09:50','admin'),(45,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:10:50','admin'),(46,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:12:15','admin'),(47,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:12:42','admin'),(48,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 13:13:19','admin'),(49,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 14:53:01','admin'),(50,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 14:54:24','admin'),(51,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 14:54:57','admin'),(52,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 14:55:29','admin'),(53,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 14:55:38','admin'),(54,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 14:55:50','admin'),(55,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:01:54','admin'),(56,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:05:51','admin'),(57,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:06:16','admin'),(58,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:06:53','admin'),(59,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:07:42','admin'),(60,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:08:21','admin'),(61,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:09:54','admin'),(62,'GLY-0000001','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-06 15:14:54','admin'),(63,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:14:59','admin'),(64,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:16:47','admin'),(65,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:18:45','admin'),(66,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:19:25','admin'),(67,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:19:36','admin'),(68,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:20:07','admin'),(69,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:20:37','admin'),(70,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:21:02','admin'),(71,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:21:14','admin'),(72,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:21:38','admin'),(73,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:21:55','admin'),(74,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:53:42','admin'),(75,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:54:14','admin'),(76,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:54:33','admin'),(77,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:55:15','admin'),(78,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 15:56:12','admin'),(79,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:01:51','admin'),(80,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:02:07','admin'),(81,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:02:59','admin'),(82,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:04:34','admin'),(83,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:05:03','admin'),(84,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:11:24','admin'),(85,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:12:10','admin'),(86,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:13:14','admin'),(87,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:15:51','admin'),(88,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:16:19','admin'),(89,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:16:50','admin'),(90,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:17:03','admin'),(91,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:20:19','admin'),(92,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:21:22','admin'),(93,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:22:13','admin'),(94,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:23:56','admin'),(95,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:25:48','admin'),(96,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:31:11','admin'),(97,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:41:14','admin'),(98,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:41:47','admin'),(99,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:42:02','admin'),(100,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:42:24','admin'),(101,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:43:23','admin'),(102,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:44:04','admin'),(103,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:44:19','admin'),(104,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:44:29','admin'),(105,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:44:55','admin'),(106,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:46:02','admin'),(107,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:46:20','admin'),(108,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:47:21','admin'),(109,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:47:43','admin'),(110,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:50:44','admin'),(111,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:51:14','admin'),(112,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:51:25','admin'),(113,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:51:31','admin'),(114,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:52:03','admin'),(115,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:52:12','admin'),(116,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:56:52','admin'),(117,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:57:39','admin'),(118,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:57:51','admin'),(119,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:58:03','admin'),(120,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:58:44','admin'),(121,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:59:24','admin'),(122,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:59:33','admin'),(123,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 16:59:44','admin'),(124,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:00:09','admin'),(125,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:00:47','admin'),(126,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:01:26','admin'),(127,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:03:20','admin'),(128,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:03:40','admin'),(129,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:04:07','admin'),(130,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:05:25','admin'),(131,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:08:53','admin'),(132,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:09:55','admin'),(133,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:14:25','admin'),(134,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:14:51','admin'),(135,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:16:07','admin'),(136,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:19:07','admin'),(137,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:19:25','admin'),(138,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:25:09','admin'),(139,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:26:24','admin'),(140,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:27:35','admin'),(141,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:27:49','admin'),(142,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:28:16','admin'),(143,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:29:01','admin'),(144,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:33:58','admin'),(145,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:34:12','admin'),(146,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:34:20','admin'),(147,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:34:44','admin'),(148,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:36:36','admin'),(149,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:37:08','admin'),(150,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:37:15','admin'),(151,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:40:45','admin'),(152,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:40:51','admin'),(153,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:43:01','admin'),(154,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:43:14','admin'),(155,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:43:37','admin'),(156,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:44:20','admin'),(157,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:44:43','admin'),(158,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:45:05','admin'),(159,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:45:59','admin'),(160,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:46:23','admin'),(161,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:46:52','admin'),(162,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:47:06','admin'),(163,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:47:19','admin'),(164,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:47:32','admin'),(165,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:47:41','admin'),(166,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:53:28','admin'),(167,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:53:48','admin'),(168,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:54:28','admin'),(169,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:54:51','admin'),(170,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:55:14','admin'),(171,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:55:25','admin'),(172,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:55:42','admin'),(173,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 17:58:06','admin'),(174,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:02:04','admin'),(175,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:04:10','admin'),(176,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:05:51','admin'),(177,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:09:27','admin'),(178,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:09:53','admin'),(179,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:10:08','admin'),(180,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:11:20','admin'),(181,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:11:42','admin'),(182,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:13:55','admin'),(183,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:15:23','admin'),(184,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:16:31','admin'),(185,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:16:55','admin'),(186,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:19:00','admin'),(187,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:20:50','admin'),(188,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:21:06','admin'),(189,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:22:07','admin'),(190,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:22:32','admin'),(191,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:35:24','admin'),(192,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:37:07','admin'),(193,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:37:46','admin'),(194,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:38:48','admin'),(195,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:47:31','root'),(196,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:48:39','root'),(197,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:50:44','root'),(198,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:50:54','root'),(199,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:53:43','root'),(200,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:57:30','root'),(201,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 18:58:29','root'),(202,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:44:06','root'),(203,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:44:37','root'),(204,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:45:04','root'),(205,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:45:41','root'),(206,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:48:35','root'),(207,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:50:37','root'),(208,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:51:10','root'),(209,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:51:36','root'),(210,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:52:51','root'),(211,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:53:38','root'),(212,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:53:47','root'),(213,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:54:27','root'),(214,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:54:38','root'),(215,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:55:58','root'),(216,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:56:27','root'),(217,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:57:49','root'),(218,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:58:21','root'),(219,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:59:06','root'),(220,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:59:44','root'),(221,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 19:59:56','root'),(222,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:00:11','root'),(223,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:07:59','root'),(224,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:09:37','root'),(225,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:10:20','root'),(226,'GL-00000002','0:0:0:0:0:0:0:1','8888','职务管理','2018-04-06 20:11:23','root'),(227,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:16:46','admin'),(228,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:19:25','root'),(229,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:22:17','root'),(230,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:23:27','root'),(231,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:23:32','root'),(232,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:24:21','root'),(233,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:27:09','root'),(234,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:29:11','root'),(235,'GL-00000002','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-06 20:30:43','root'),(236,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:31:45','root'),(237,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:33:58','root'),(238,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:34:38','root'),(239,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:36:26','root'),(240,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:36:40','root'),(241,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:36:57','root'),(242,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:37:39','root'),(243,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 20:38:37','root'),(244,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:07:33','admin'),(245,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-06 21:07:43','admin'),(246,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-06 21:07:52','admin'),(247,'GLY-0000001','0:0:0:0:0:0:0:1','8888','分配权限','2018-04-06 21:09:25','admin'),(248,'GLY-0000001','0:0:0:0:0:0:0:1','8888','职务管理','2018-04-06 21:09:27','admin'),(249,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限管理','2018-04-06 21:09:31','admin'),(250,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增权限','2018-04-06 21:10:15','admin'),(251,'GLY-0000001','0:0:0:0:0:0:0:1','8888','分配权限','2018-04-06 21:10:41','admin'),(252,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员信息','2018-04-06 21:11:04','admin'),(253,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员附加角色','2018-04-06 21:11:21','admin'),(254,'GLY-0000001','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-06 21:14:25','admin'),(255,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:20:26','admin'),(256,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-06 21:20:34','admin'),(257,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:20:55','admin'),(258,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:21:10','admin'),(259,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:22:40','admin'),(260,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:23:23','admin'),(261,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:25:18','admin'),(262,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:25:25','admin'),(263,'GLY-0000001','0:0:0:0:0:0:0:1','8888','移除培训计划','2018-04-06 21:26:26','admin'),(264,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增培训计划','2018-04-06 21:27:32','admin'),(265,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:28:48','admin'),(266,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:28:53','admin'),(267,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增培训计划','2018-04-06 21:30:08','admin'),(268,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增培训计划','2018-04-06 21:34:50','admin'),(269,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:36:50','admin'),(270,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:37:00','admin'),(271,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:37:18','admin'),(272,'GLY-0000001','0:0:0:0:0:0:0:1','8888','职务管理','2018-04-06 21:37:22','admin'),(273,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-06 21:37:23','admin'),(274,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩项目','2018-04-06 21:37:26','admin'),(275,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-06 21:37:28','admin'),(276,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:38:19','admin'),(277,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:39:07','admin'),(278,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:40:19','admin'),(279,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:43:14','admin'),(280,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-06 21:43:18','admin'),(281,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:43:23','admin'),(282,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:45:04','admin'),(283,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:45:10','admin'),(284,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增培训计划','2018-04-06 21:45:39','admin'),(285,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:50:20','wang'),(286,'HD—003','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:50:33','wang'),(287,'HD—003','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-06 21:50:39','wang'),(288,'HD—003','0:0:0:0:0:0:0:1','8888','证照资料','2018-04-06 21:50:43','wang'),(289,'HD—003','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:50:48','wang'),(290,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:50:59','admin'),(291,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:51:37','admin'),(292,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:51:40','admin'),(293,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:51:48','admin'),(294,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:52:29','wang'),(295,'HD—003','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:52:31','wang'),(296,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 21:52:42','admin'),(297,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 21:52:48','admin'),(298,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:00:47','admin'),(299,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:01:46','wang'),(300,'HD—003','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:01:48','wang'),(301,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:02:13','admin'),(302,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:02:19','admin'),(303,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:02:24','wang'),(304,'HD—003','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:02:26','wang'),(305,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:03:13','admin'),(306,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:03:18','admin'),(307,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:03:47','wang'),(308,'HD—003','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:03:49','wang'),(309,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:05:52','admin'),(310,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:05:56','admin'),(311,'HD—003','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:06:16','wang'),(312,'GL-00000002','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:06:45','root'),(313,'GL-00000002','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:06:52','root'),(314,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户添加','2018-04-06 22:10:03','admin'),(315,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员附加角色','2018-04-06 22:10:12','admin'),(316,'YG-90955653','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:10:23','123'),(317,'YG-90955653','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:10:25','123'),(318,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:10:39','admin'),(319,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增培训计划','2018-04-06 22:11:27','admin'),(320,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:12:36','admin'),(321,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:13:02','admin'),(322,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:13:13','wang'),(323,'HD—003','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:13:16','wang'),(324,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:13:42','admin'),(325,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:13:51','wang'),(326,'HD—003','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:13:53','wang'),(327,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:14:07','wang'),(328,'HD—003','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:14:09','wang'),(329,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:16:09','admin'),(330,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:16:21','admin'),(331,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:16:32','wang'),(332,'HD—003','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:16:34','wang'),(333,'YG-90955653','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:16:56','123'),(334,'YG-90955653','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:16:58','123'),(335,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:23:31','admin'),(336,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-06 22:23:35','admin'),(337,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户修改','2018-04-06 22:23:42','admin'),(338,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-06 22:23:56','admin'),(339,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:24:12','admin'),(340,'GLY-0000001','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-06 22:24:48','admin'),(341,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:26:10','admin'),(342,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限管理','2018-04-06 22:26:15','admin'),(343,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增权限','2018-04-06 22:26:50','admin'),(344,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-06 22:32:18','admin'),(345,'GLY-0000001','0:0:0:0:0:0:0:1','8888','分配权限','2018-04-06 22:32:23','admin'),(346,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:40:05','admin'),(347,'GLY-0000001','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-06 22:40:20','admin'),(348,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员信息','2018-04-06 22:42:14','admin'),(349,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:42:48','admin'),(350,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-06 22:42:51','admin'),(351,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员信息','2018-04-06 22:43:01','admin'),(352,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-06 22:44:42','admin'),(353,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 22:59:19','admin'),(354,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:09:48','admin'),(355,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:15:49','admin'),(356,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:17:57','admin'),(357,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:19:04','admin'),(358,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:19:13','admin'),(359,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:19:49','admin'),(360,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:20:35','admin'),(361,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-06 23:23:38','admin'),(362,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:24:42','admin'),(363,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:25:29','admin'),(364,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:25:47','admin'),(365,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:26:22','admin'),(366,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:26:51','admin'),(367,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:27:04','admin'),(368,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:27:11','admin'),(369,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:44:53','admin'),(370,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:45:14','admin'),(371,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:45:37','admin'),(372,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:46:12','admin'),(373,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:46:27','admin'),(374,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:48:12','admin'),(375,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:49:26','admin'),(376,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:49:40','admin'),(377,'GLY-0000001','0:0:0:0:0:0:0:1','8888','解除合同','2018-04-06 23:49:58','admin'),(378,'GLY-0000001','0:0:0:0:0:0:0:1','8888','转正记录','2018-04-06 23:50:04','admin'),(379,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同续签记录','2018-04-06 23:50:07','admin'),(380,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同续签记录','2018-04-06 23:50:14','admin'),(381,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同变更记录','2018-04-06 23:50:16','admin'),(382,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同续签记录','2018-04-06 23:50:19','admin'),(383,'GLY-0000001','0:0:0:0:0:0:0:1','8888','转正记录','2018-04-06 23:50:21','admin'),(384,'GLY-0000001','0:0:0:0:0:0:0:1','8888','解除合同','2018-04-06 23:50:25','admin'),(385,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:50:53','wang'),(386,'HD—003','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-06 23:50:56','wang'),(387,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-06 23:51:12','admin'),(388,'GLY-0000001','0:0:0:0:0:0:0:1','8888','分配权限','2018-04-06 23:51:19','admin'),(389,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:51:29','wang'),(390,'HD—003','0:0:0:0:0:0:0:1','8888','解除合同','2018-04-06 23:51:41','wang'),(391,'HD—003','0:0:0:0:0:0:0:1','8888','转正记录','2018-04-06 23:51:45','wang'),(392,'HD—003','0:0:0:0:0:0:0:1','8888','合同续签记录','2018-04-06 23:51:48','wang'),(393,'HD—003','0:0:0:0:0:0:0:1','8888','合同变更记录','2018-04-06 23:51:51','wang'),(394,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-06 23:52:12','admin'),(395,'GLY-0000001','0:0:0:0:0:0:0:1','8888','分配角色','2018-04-06 23:52:42','admin'),(396,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-06 23:52:44','admin'),(397,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限管理','2018-04-06 23:52:46','admin'),(398,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-06 23:53:00','admin'),(399,'GLY-0000001','0:0:0:0:0:0:0:1','8888','分配权限','2018-04-06 23:53:09','admin'),(400,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-06 23:53:21','admin'),(401,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:53:35','wang'),(402,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:54:34','wang'),(403,'HD—003','0:0:0:0:0:0:0:1','8888','解除合同','2018-04-06 23:54:42','wang'),(404,'HD—003','0:0:0:0:0:0:0:1','8888','合同续签记录','2018-04-06 23:54:45','wang'),(405,'HD—003','0:0:0:0:0:0:0:1','8888','转正记录','2018-04-06 23:54:47','wang'),(406,'HD—003','0:0:0:0:0:0:0:1','8888','合同变更记录','2018-04-06 23:54:49','wang'),(407,'GLY-0000001','0:0:0:0:0:0:0:1','8888','证照资料','2018-04-06 23:55:21','admin'),(408,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-06 23:57:48','wang'),(409,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-06 23:58:02','admin'),(410,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-06 23:59:27','admin'),(411,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增合同','2018-04-07 00:00:16','admin'),(412,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护合同','2018-04-07 00:00:33','admin'),(413,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:02:50','admin'),(414,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:02:59','wang'),(415,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:04:48','wang'),(416,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:10:08','wang'),(417,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:12:35','wang'),(418,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:12:55','admin'),(419,'GLY-0000001','0:0:0:0:0:0:0:1','8888','职称评定','2018-04-07 00:13:06','admin'),(420,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 00:33:06','admin'),(421,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:42:49','admin'),(422,'GLY-0000001','0:0:0:0:0:0:0:1','8888','职称评定','2018-04-07 00:42:56','admin'),(423,'GLY-0000001','0:0:0:0:0:0:0:1','8888','证照资料','2018-04-07 00:43:17','admin'),(424,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 00:43:55','admin'),(425,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工调动','2018-04-07 00:44:40','admin'),(426,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 00:45:09','admin'),(427,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工复职','2018-04-07 00:46:21','admin'),(428,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:53:38','admin'),(429,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:53:47','wang'),(430,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:55:05','wang'),(431,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:56:01','wang'),(432,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:57:36','wang'),(433,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:58:00','wang'),(434,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:58:14','wang'),(435,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:58:41','wang'),(436,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:58:58','wang'),(437,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:59:15','wang'),(438,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:59:25','wang'),(439,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 00:59:38','wang'),(440,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:04:31','wang'),(441,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:05:07','wang'),(442,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:06:06','wang'),(443,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:06:35','wang'),(444,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:08:53','wang'),(445,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:09:32','wang'),(446,'HD—003','0:0:0:0:0:0:0:1','8888','解除合同','2018-04-07 01:09:54','wang'),(447,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:10:54','admin'),(448,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩项目','2018-04-07 01:11:03','admin'),(449,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 01:11:06','admin'),(450,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护奖惩记录','2018-04-07 01:11:21','admin'),(451,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:17:05','wang'),(452,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 01:17:42','admin'),(453,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:19:52','wang'),(454,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:20:12','wang'),(455,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:22:34','wang'),(456,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 01:22:59','admin'),(457,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:25:31','admin'),(458,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 01:25:50','admin'),(459,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:27:09','wang'),(460,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:28:16','wang'),(461,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:30:05','wang'),(462,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:30:17','wang'),(463,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:30:41','wang'),(464,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:30:51','wang'),(465,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:31:51','wang'),(466,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:33:09','admin'),(467,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 01:33:15','admin'),(468,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 01:33:15','admin'),(469,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:33:48','admin'),(470,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 01:33:53','admin'),(471,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 01:33:59','admin'),(472,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:34:47','wang'),(473,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:35:47','wang'),(474,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:38:51','wang'),(475,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:39:37','wang'),(476,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 01:40:02','admin'),(477,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:41:10','wang'),(478,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:42:01','wang'),(479,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:50:25','wang'),(480,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:50:28','admin'),(481,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 01:50:35','admin'),(482,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 01:50:40','admin'),(483,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 01:51:34','admin'),(484,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 01:51:38','admin'),(485,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 01:51:40','admin'),(486,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员信息','2018-04-07 01:55:06','admin'),(487,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限管理','2018-04-07 01:55:10','admin'),(488,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 01:55:20','admin'),(489,'GLY-0000001','0:0:0:0:0:0:0:1','8888','分配权限','2018-04-07 01:55:26','admin'),(490,'GLY-0000001','0:0:0:0:0:0:0:1','8888','分配权限','2018-04-07 01:55:47','admin'),(491,'GLY-0000001','0:0:0:0:0:0:0:1','8888','分配权限','2018-04-07 01:56:35','admin'),(492,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-07 02:00:02','admin'),(493,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 02:00:26','admin'),(494,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 02:21:20','admin'),(495,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 02:26:39','admin'),(496,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 09:23:56','admin'),(497,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 09:24:04','admin'),(498,'GLY-0000001','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-07 09:24:13','admin'),(499,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 09:24:20','admin'),(500,'GLY-0000001','0:0:0:0:0:0:0:1','8888','职称评定','2018-04-07 09:24:23','admin'),(501,'GLY-0000001','0:0:0:0:0:0:0:1','8888','证照资料','2018-04-07 09:24:24','admin'),(502,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 09:24:25','admin'),(503,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 09:24:30','admin'),(504,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 09:24:39','admin'),(505,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工复职','2018-04-07 09:24:43','admin'),(506,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 09:24:52','admin'),(507,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩项目','2018-04-07 09:24:55','admin'),(508,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 09:24:58','admin'),(509,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工调动','2018-04-07 09:25:00','admin'),(510,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 09:25:03','admin'),(511,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 09:25:40','admin'),(512,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 09:25:47','admin'),(513,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 09:27:43','admin'),(514,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 09:27:55','admin'),(515,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 09:27:58','admin'),(516,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 09:29:10','admin'),(517,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 09:29:20','admin'),(518,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 09:29:35','admin'),(519,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 09:29:39','admin'),(520,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 09:30:17','admin'),(521,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 09:30:19','admin'),(522,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 09:31:16','admin'),(523,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 09:31:19','admin'),(524,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工复职','2018-04-07 09:31:26','admin'),(525,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩项目','2018-04-07 09:31:28','admin'),(526,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 09:31:29','admin'),(527,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工调动','2018-04-07 09:31:29','admin'),(528,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 09:31:36','admin'),(529,'GLY-0000001','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-07 09:32:02','admin'),(530,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 09:36:48','admin'),(531,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限管理','2018-04-07 09:36:51','admin'),(532,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限维护','2018-04-07 09:37:13','admin'),(533,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 09:37:41','wang'),(534,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 09:37:54','admin'),(535,'GLY-0000001','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-07 09:38:01','admin'),(536,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员信息','2018-04-07 09:38:43','admin'),(537,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限管理','2018-04-07 09:38:52','admin'),(538,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限维护','2018-04-07 09:39:39','admin'),(539,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 09:39:45','admin'),(540,'GLY-0000001','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-07 09:39:49','admin'),(541,'GLY-0000001','0:0:0:0:0:0:0:1','8888','职称评定','2018-04-07 09:41:00','admin'),(542,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增证照资料','2018-04-07 09:41:54','admin'),(543,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护证照资料','2018-04-07 09:42:32','admin'),(544,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护证照资料','2018-04-07 09:42:44','admin'),(545,'GLY-0000001','0:0:0:0:0:0:0:1','8888','证照资料','2018-04-07 09:42:52','admin'),(546,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 09:58:42','admin'),(547,'GLY-0000001','0:0:0:0:0:0:0:1','8888','证照资料','2018-04-07 09:58:47','admin'),(548,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 09:59:07','admin'),(549,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增合同','2018-04-07 10:00:19','admin'),(550,'GLY-0000001','0:0:0:0:0:0:0:1','8888','转正记录','2018-04-07 10:00:37','admin'),(551,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增转正记录','2018-04-07 10:00:51','admin'),(552,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护合同','2018-04-07 10:01:08','admin'),(553,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同变更记录','2018-04-07 10:01:16','admin'),(554,'GLY-0000001','0:0:0:0:0:0:0:1','8888','转正记录','2018-04-07 10:01:23','admin'),(555,'GLY-0000001','0:0:0:0:0:0:0:1','8888','解除合同','2018-04-07 10:01:27','admin'),(556,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增解除记录','2018-04-07 10:01:39','admin'),(557,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 10:01:54','admin'),(558,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增培训计划','2018-04-07 10:03:26','admin'),(559,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护培训计划','2018-04-07 10:04:00','admin'),(560,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 10:04:18','admin'),(561,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护员工离职记录','2018-04-07 10:05:04','admin'),(562,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护员工离职记录','2018-04-07 10:05:09','admin'),(563,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:06:57','admin'),(564,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 10:07:00','admin'),(565,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:08:58','admin'),(566,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 10:09:03','admin'),(567,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 10:09:59','admin'),(568,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 10:11:31','admin'),(569,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:12:08','admin'),(570,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 10:12:11','admin'),(571,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增员工离职记录','2018-04-07 10:12:23','admin'),(572,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 10:13:45','admin'),(573,'GLY-0000001','0:0:0:0:0:0:0:1','8888','转正记录','2018-04-07 10:14:23','admin'),(574,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增合同恢复记录','2018-04-07 10:17:16','admin'),(575,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:20:04','admin'),(576,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 10:20:10','admin'),(577,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增员工离职记录','2018-04-07 10:20:22','admin'),(578,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:21:49','admin'),(579,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 10:21:52','admin'),(580,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工复职','2018-04-07 10:21:55','admin'),(581,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 10:22:33','admin'),(582,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:26:10','admin'),(583,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:28:15','admin'),(584,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工调动','2018-04-07 10:28:20','admin'),(585,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 10:28:22','admin'),(586,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:30:39','admin'),(587,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 10:30:46','admin'),(588,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 10:30:52','admin'),(589,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增员工离职记录','2018-04-07 10:31:38','admin'),(590,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工复职','2018-04-07 10:31:41','admin'),(591,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:33:55','admin'),(592,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工复职','2018-04-07 10:34:15','admin'),(593,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工复职','2018-04-07 10:35:27','admin'),(594,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增员工复职记录','2018-04-07 10:35:37','admin'),(595,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护员工复职记录','2018-04-07 10:35:48','admin'),(596,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增员工复职记录','2018-04-07 10:36:06','admin'),(597,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩项目','2018-04-07 10:36:19','admin'),(598,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增奖惩项目','2018-04-07 10:36:29','admin'),(599,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护奖惩项目','2018-04-07 10:36:33','admin'),(600,'GLY-0000001','0:0:0:0:0:0:0:1','8888','移除奖惩项目','2018-04-07 10:36:35','admin'),(601,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 10:36:37','admin'),(602,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增奖惩记录','2018-04-07 10:37:01','admin'),(603,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 10:38:07','admin'),(604,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护奖惩记录','2018-04-07 10:38:29','admin'),(605,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工调动','2018-04-07 10:38:34','admin'),(606,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增调动','2018-04-07 10:39:02','admin'),(607,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护调动','2018-04-07 10:39:09','admin'),(608,'GLY-0000001','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-07 10:39:19','admin'),(609,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-07 10:40:46','admin'),(610,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 10:41:06','admin'),(611,'GLY-0000001','0:0:0:0:0:0:0:1','8888','部门管理','2018-04-07 10:41:07','admin'),(612,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员信息','2018-04-07 10:41:08','admin'),(613,'GLY-0000001','0:0:0:0:0:0:0:1','8888','部门管理','2018-04-07 10:41:36','admin'),(614,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 10:41:37','admin'),(615,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员信息','2018-04-07 10:41:42','admin'),(616,'GLY-0000001','0:0:0:0:0:0:0:1','8888','部门管理','2018-04-07 10:41:55','admin'),(617,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员信息','2018-04-07 10:41:55','admin'),(618,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-07 10:41:57','admin'),(619,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 10:42:03','admin'),(620,'GLY-0000001','0:0:0:0:0:0:0:1','8888','部门添加','2018-04-07 10:42:32','admin'),(621,'GLY-0000001','0:0:0:0:0:0:0:1','8888','部门修改','2018-04-07 10:42:41','admin'),(622,'GLY-0000001','0:0:0:0:0:0:0:1','8888','部门添加','2018-04-07 10:42:57','admin'),(623,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限管理','2018-04-07 10:47:29','admin'),(624,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限维护','2018-04-07 10:49:52','admin'),(625,'GLY-0000001','0:0:0:0:0:0:0:1','8888','部门管理','2018-04-07 10:49:57','admin'),(626,'GLY-0000001','0:0:0:0:0:0:0:1','8888','部门删除','2018-04-07 10:50:05','admin'),(627,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:50:35','wang'),(628,'HD—003','0:0:0:0:0:0:0:1','8888','解除合同','2018-04-07 10:50:47','wang'),(629,'HD—003','0:0:0:0:0:0:0:1','8888','转正记录','2018-04-07 10:50:49','wang'),(630,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:53:50','admin'),(631,'GLY-0000001','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-07 10:54:26','admin'),(632,'GLY-0000001','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-07 10:54:48','admin'),(633,'GLY-0000001','0:0:0:0:0:0:0:1','8888','职称评定','2018-04-07 10:54:56','admin'),(634,'GLY-0000001','0:0:0:0:0:0:0:1','8888','证照资料','2018-04-07 10:55:21','admin'),(635,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 10:55:29','admin'),(636,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 10:55:41','admin'),(637,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩项目','2018-04-07 10:56:02','admin'),(638,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限管理','2018-04-07 10:56:11','admin'),(639,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限维护','2018-04-07 10:56:27','admin'),(640,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:56:34','admin'),(641,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:56:40','admin'),(642,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限管理','2018-04-07 10:56:49','admin'),(643,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 10:57:09','admin'),(644,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩项目','2018-04-07 10:57:12','admin'),(645,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-07 10:57:14','admin'),(646,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩项目','2018-04-07 10:57:20','admin'),(647,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增奖惩项目','2018-04-07 10:57:27','admin'),(648,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护奖惩项目','2018-04-07 10:57:33','admin'),(649,'GLY-0000001','0:0:0:0:0:0:0:1','8888','移除奖惩项目','2018-04-07 10:57:36','admin'),(650,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工调动','2018-04-07 10:58:03','admin'),(651,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增调动','2018-04-07 10:59:09','admin'),(652,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增调动','2018-04-07 10:59:33','admin'),(653,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增调动','2018-04-07 10:59:49','admin'),(654,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增调动','2018-04-07 11:00:14','admin'),(655,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增调动','2018-04-07 11:00:31','admin'),(656,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增调动','2018-04-07 11:01:17','admin'),(657,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工调动','2018-04-07 11:02:25','admin'),(658,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增调动','2018-04-07 11:02:45','admin'),(659,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增调动','2018-04-07 11:03:05','admin'),(660,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工调动','2018-04-07 11:03:30','admin'),(661,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增调动','2018-04-07 11:03:50','admin'),(662,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护调动','2018-04-07 11:03:59','admin'),(663,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 11:04:08','admin'),(664,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增奖惩记录','2018-04-07 11:04:28','admin'),(665,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护奖惩记录','2018-04-07 11:04:41','admin'),(666,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工复职','2018-04-07 11:04:56','admin'),(667,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 11:04:59','admin'),(668,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增员工离职记录','2018-04-07 11:05:32','admin'),(669,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护员工离职记录','2018-04-07 11:05:45','admin'),(670,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工复职','2018-04-07 11:06:09','admin'),(671,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 11:06:09','admin'),(672,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工调动','2018-04-07 11:06:10','admin'),(673,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 11:06:13','admin'),(674,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 11:06:14','admin'),(675,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 11:06:15','admin'),(676,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 11:06:26','admin'),(677,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-07 11:06:35','admin'),(678,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 11:06:55','admin'),(679,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 11:07:23','admin'),(680,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-07 11:07:25','admin'),(681,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员信息','2018-04-07 11:07:30','admin'),(682,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 11:07:33','admin'),(683,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 11:08:50','admin'),(684,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 11:08:53','admin'),(685,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 11:09:47','admin'),(686,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 11:09:49','admin'),(687,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员信息','2018-04-07 11:10:44','admin'),(688,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-07 11:10:49','admin'),(689,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 11:12:44','admin'),(690,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 11:12:46','admin'),(691,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-07 11:12:51','admin'),(692,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员信息','2018-04-07 11:12:52','admin'),(693,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员附加角色','2018-04-07 11:13:05','admin'),(694,'GLY-0000001','0:0:0:0:0:0:0:1','8888','部门管理','2018-04-07 11:13:12','admin'),(695,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩项目','2018-04-07 11:13:16','admin'),(696,'GLY-0000001','0:0:0:0:0:0:0:1','8888','职务管理','2018-04-07 11:13:19','admin'),(697,'GLY-0000001','0:0:0:0:0:0:0:1','8888','权限管理','2018-04-07 11:13:19','admin'),(698,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护奖惩项目','2018-04-07 11:13:35','admin'),(699,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护奖惩项目','2018-04-07 11:13:42','admin'),(700,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-07 11:13:54','admin'),(701,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-07 11:13:59','admin'),(702,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 11:20:43','wang'),(703,'HD—003','0:0:0:0:0:0:0:1','8888','维护续签记录','2018-04-07 11:21:30','wang'),(704,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 11:23:39','wang'),(705,'HD—003','0:0:0:0:0:0:0:1','8888','维护续签记录','2018-04-07 11:23:44','wang'),(706,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 11:28:20','admin'),(707,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 11:28:29','admin'),(708,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增培训计划','2018-04-07 11:29:15','admin'),(709,'YG-90955653','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 11:30:12','123'),(710,'YG-90955653','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 11:30:21','123'),(711,'YG-90955653','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 11:30:23','123'),(712,'HD—003','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 11:31:44','wang'),(713,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 11:40:18','admin'),(714,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 11:40:35','admin'),(715,'GLY-0000001','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-07 11:40:46','admin'),(716,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 11:42:45','admin'),(717,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增合同','2018-04-07 11:44:11','admin'),(718,'GLY-0000001','0:0:0:0:0:0:0:1','8888','转正记录','2018-04-07 11:44:28','admin'),(719,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同变更记录','2018-04-07 11:45:24','admin'),(720,'GLY-0000001','0:0:0:0:0:0:0:1','8888','维护合同','2018-04-07 11:46:39','admin'),(721,'GLY-0000001','0:0:0:0:0:0:0:1','8888','转正记录','2018-04-07 11:46:42','admin'),(722,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增转正记录','2018-04-07 11:46:53','admin'),(723,'GLY-0000001','0:0:0:0:0:0:0:1','8888','职称评定','2018-04-07 11:47:32','admin'),(724,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增证照资料','2018-04-07 11:47:59','admin'),(725,'GLY-0000001','0:0:0:0:0:0:0:1','8888','证照资料','2018-04-07 11:48:16','admin'),(726,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 11:49:47','admin'),(727,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增员工离职记录','2018-04-07 11:50:46','admin'),(728,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 11:50:57','admin'),(729,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工复职','2018-04-07 11:51:28','admin'),(730,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增员工复职记录','2018-04-07 11:51:56','admin'),(731,'GLY-0000001','0:0:0:0:0:0:0:1','8888','合同管理','2018-04-07 11:52:03','admin'),(732,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工调动','2018-04-07 11:52:17','admin'),(733,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增调动','2018-04-07 11:52:41','admin'),(734,'GLY-0000001','0:0:0:0:0:0:0:1','8888','人事管理','2018-04-07 11:52:53','admin'),(735,'GLY-0000001','0:0:0:0:0:0:0:1','8888','奖惩管理','2018-04-07 11:53:38','admin'),(736,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增奖惩记录','2018-04-07 11:53:59','admin'),(737,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增奖惩记录','2018-04-07 11:54:23','admin'),(738,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员信息','2018-04-07 11:55:21','admin'),(739,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员附加角色','2018-04-07 11:55:46','admin'),(740,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 11:55:53','admin'),(741,'GLY-0000001','0:0:0:0:0:0:0:1','8888','分配权限','2018-04-07 11:56:00','admin'),(742,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 11:56:07','admin'),(743,'GLY-0000001','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 11:56:24','admin'),(744,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-07 11:56:38','admin'),(745,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户添加','2018-04-07 11:57:03','admin'),(746,'GLY-0000001','0:0:0:0:0:0:0:1','8888','分配角色','2018-04-07 11:57:22','admin'),(747,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增培训计划','2018-04-07 11:58:49','admin'),(748,'YG-12254374','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 11:59:04','qwe'),(749,'YG-12254374','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 11:59:14','qwe'),(750,'YG-90955653','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 11:59:47','123'),(751,'YG-90955653','0:0:0:0:0:0:0:1','8888','培训计划','2018-04-07 11:59:52','123'),(752,'YG-12254374','0:0:0:0:0:0:0:1','8888','维护续签记录','2018-04-07 12:03:13','qwe'),(753,'GLY-0000001','0:0:0:0:0:0:0:1','8888','角色管理','2018-04-07 12:03:46','admin'),(754,'GLY-0000001','0:0:0:0:0:0:0:1','8888','日志管理','2018-04-07 12:03:48','admin'),(755,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:07:38','admin'),(756,'GLY-0000001','0:0:0:0:0:0:0:1','8888','用户管理','2018-04-07 15:07:45','admin'),(757,'GLY-0000001','0:0:0:0:0:0:0:1','8888','管理员信息','2018-04-07 15:07:51','admin'),(758,'GLY-0000001','0:0:0:0:0:0:0:1','8888','职称评定','2018-04-07 15:07:54','admin'),(759,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增证照资料','2018-04-07 15:08:11','admin'),(760,'GLY-0000001','0:0:0:0:0:0:0:1','8888','员工离职','2018-04-07 15:08:45','admin'),(761,'GLY-0000001','0:0:0:0:0:0:0:1','8888','新增员工离职记录','2018-04-07 15:09:04','admin'),(762,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:11:40','admin'),(763,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:20:39','admin'),(764,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:32:43','admin'),(765,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:32:57','admin'),(766,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:34:11','admin'),(767,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:34:50','admin'),(768,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:35:17','admin'),(769,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:35:29','admin'),(770,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:37:06','admin'),(771,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:38:30','admin'),(772,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:40:45','admin'),(773,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:42:44','admin'),(774,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:43:03','admin'),(775,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:43:58','admin'),(776,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:44:20','admin'),(777,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:45:05','admin'),(778,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:45:34','admin'),(779,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:49:03','admin'),(780,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:49:24','admin'),(781,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:49:46','admin'),(782,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:50:14','admin'),(783,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:51:09','admin'),(784,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:53:46','admin'),(785,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:54:02','admin'),(786,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:54:32','admin'),(787,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:55:29','admin'),(788,'GLY-0000001','0:0:0:0:0:0:0:1','8888','登陆系统','2018-04-07 15:55:37','admin');

/*Table structure for table `mobilize` */

DROP TABLE IF EXISTS `mobilize`;

CREATE TABLE `mobilize` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '调动',
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `sectionameold` int(11) DEFAULT NULL COMMENT '原部门名称',
  `sectionamenew` int(11) DEFAULT NULL COMMENT '现部门名称',
  `educationold` int(5) DEFAULT NULL COMMENT '原学历',
  `educationnew` int(5) DEFAULT NULL COMMENT '现学历',
  `zhichengold` int(5) DEFAULT NULL COMMENT '原职称',
  `zhichengnew` int(5) DEFAULT NULL COMMENT '现职称',
  `employeesWorkold` int(5) DEFAULT NULL COMMENT '原工种',
  `employeesWorknew` int(5) DEFAULT NULL COMMENT '原工种',
  `jobsold` int(5) DEFAULT NULL COMMENT '原岗位',
  `jobsnew` int(5) DEFAULT NULL COMMENT '现岗位',
  `positionold` int(11) DEFAULT NULL COMMENT '原职务',
  `positionnew` int(11) DEFAULT NULL COMMENT '现职务',
  `qianold` varchar(100) DEFAULT NULL,
  `qiannew` varchar(100) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `time` date DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `archives` int(11) DEFAULT NULL COMMENT '员工',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `mobilize` */

insert  into `mobilize`(`id`,`title`,`sectionameold`,`sectionamenew`,`educationold`,`educationnew`,`zhichengold`,`zhichengnew`,`employeesWorkold`,`employeesWorknew`,`jobsold`,`jobsnew`,`positionold`,`positionnew`,`qianold`,`qiannew`,`type`,`time`,`remark`,`archives`) values (5,'晋升项目经理',1,1,63,63,39,39,34,34,102,102,2,2,'2000','3000',118,'2018-04-12','晋升1',17),(6,'降级',1,1,63,63,39,39,34,34,102,102,2,2,'3000','2000',119,'2018-04-23','',17),(7,'shengzhi',2,2,63,63,44,44,34,34,103,103,2,6,'333333','333333',118,'2018-04-17','asd',50),(8,'进度和忍耐和',2,2,57,57,36,36,35,35,103,103,2,2,'6000','3434534',120,'2018-03-27','尔特瑞特让他',22),(9,'是告诉对方',2,2,57,57,36,36,35,35,103,103,2,2,'3434534','444',120,'2018-04-10','微软微软',22),(10,'豆腐干地方',2,2,57,57,36,36,35,35,103,103,2,2,'444','45343',119,'2018-04-10','违法',22),(11,'123',2,2,58,58,37,37,34,34,101,101,2,2,'2000','123',118,'2018-04-24','1231',20),(12,'1',1,1,63,63,39,39,34,34,102,102,2,2,'2000','123',118,'2018-04-24','1',17),(13,'789',2,2,57,57,36,36,35,35,103,103,2,2,'45343','1111',118,'2018-04-24','111',22),(14,'123',1,1,63,63,39,39,34,34,102,102,2,2,'123','123',118,'2018-04-18','12',17),(15,'123123123123',1,1,63,63,39,39,34,34,102,102,2,2,'123','123',118,'2018-04-08','12313',17),(16,'啊实打实',1,1,58,58,43,43,34,35,104,104,2,2,'50000','50000',120,'2018-04-26','打打杀杀的',41),(17,'晋升',20,20,60,60,40,40,34,34,102,102,3,3,'5000','6000',118,'2018-04-19','阿斯达',57);

/*Table structure for table `operationlog` */

DROP TABLE IF EXISTS `operationlog`;

CREATE TABLE `operationlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user` int(11) DEFAULT NULL COMMENT '用户',
  `IP` varchar(50) DEFAULT NULL COMMENT 'IP',
  `name` varchar(50) DEFAULT NULL COMMENT '模块名称',
  `text` varchar(500) DEFAULT NULL COMMENT '日志内容',
  `time` varchar(50) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='操作日志';

/*Data for the table `operationlog` */

/*Table structure for table `position` */

DROP TABLE IF EXISTS `position`;

CREATE TABLE `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '职务',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '职位说明/备注',
  `describe` varchar(500) DEFAULT NULL COMMENT '职位描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `position` */

insert  into `position`(`id`,`name`,`remark`,`describe`) values (1,'总经理','公司负责人','负责公司的管理'),(2,'部门经理','部门负责人','负责管理各个部门'),(3,'财务总监','财务负责人','负责管理钱'),(4,'副总经理','公司副负责人','负责公司的管理'),(5,'人事总监','人事负责人','负责管理人事'),(6,'采购总监','采购负责人','负责买原料的人'),(7,'运营总监','运行负责人','负责产品运行'),(8,'系统管理员','系统负责人','负责系统的管理'),(9,'电脑员','计算机负责人','负责计算机'),(10,'董事长','公司法人','老大');

/*Table structure for table `positive` */

DROP TABLE IF EXISTS `positive`;

CREATE TABLE `positive` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '转正',
  `time` date DEFAULT NULL COMMENT '时间',
  `type` int(5) DEFAULT NULL COMMENT '类型',
  `positiveremark` varchar(500) DEFAULT NULL COMMENT '说明',
  `contractInfo` int(11) DEFAULT NULL COMMENT '合同信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `positive` */

insert  into `positive`(`id`,`time`,`type`,`positiveremark`,`contractInfo`) values (5,'2018-03-29',74,'123',2),(6,'2018-03-30',132,'',2),(7,'2018-03-20',132,'123',5),(8,'2018-04-04',131,'123',6),(9,'2018-04-26',132,'qweqe',7),(10,'2018-04-19',131,'asad',9);

/*Table structure for table `post` */

DROP TABLE IF EXISTS `post`;

CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '岗位编制',
  `name` int(5) DEFAULT NULL COMMENT '岗位',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `section_id` int(11) DEFAULT NULL COMMENT '部门',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='岗位编制';

/*Data for the table `post` */

/*Table structure for table `reinstatement` */

DROP TABLE IF EXISTS `reinstatement`;

CREATE TABLE `reinstatement` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '复职',
  `time` date DEFAULT NULL COMMENT '复职时间',
  `type` int(5) DEFAULT NULL COMMENT '复职类型',
  `remark` varchar(500) DEFAULT NULL COMMENT '复职说明',
  `whether` int(5) DEFAULT NULL COMMENT '是否恢复合同',
  `archives` int(11) DEFAULT NULL COMMENT '员工',
  `hftype` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `reinstatement` */

insert  into `reinstatement`(`id`,`time`,`type`,`remark`,`whether`,`archives`,`hftype`) values (10,'2018-04-01',127,'12345',1,17,128),(11,'2018-04-05',128,'a',1,17,128),(12,'2018-04-06',128,'\r\n123',2,17,NULL),(13,'2018-04-05',127,'11',2,17,NULL),(14,'2018-04-19',128,'委任为',1,57,128);

/*Table structure for table `removeinfo` */

DROP TABLE IF EXISTS `removeinfo`;

CREATE TABLE `removeinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '合同解除信息',
  `time` date DEFAULT NULL COMMENT '解除时间',
  `type` int(5) DEFAULT NULL COMMENT '解除类型',
  `text` text COMMENT '解除内容',
  `contractInfo` int(11) DEFAULT NULL COMMENT '合同信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='合同解除信息';

/*Data for the table `removeinfo` */

insert  into `removeinfo`(`id`,`time`,`type`,`text`,`contractInfo`) values (1,'2018-03-01',130,'123',2),(3,'2018-03-23',129,'123',2),(4,'2018-03-23',129,'解除合同',4),(5,'2018-04-11',129,'太懒',6),(6,'2018-04-01',129,'123',6),(7,'2018-04-05',129,'123',6),(8,'2018-04-01',129,'离职解除合同',6),(9,'2018-04-05',129,'离职解除合同',6),(10,'2018-04-12',129,'a',6),(11,'2018-04-05',130,'离职解除合同',6),(12,'2018-04-20',129,'12346',6),(13,'2018-04-19',129,'123',6),(14,'2018-04-26',130,'',8),(15,'2018-04-06',NULL,'离职解除合同',NULL),(16,'2018-04-05',130,'离职解除合同',NULL),(17,'2018-04-13',129,'离职解除合同',9),(18,'2018-04-04',130,'离职解除合同',7);

/*Table structure for table `renewinfo` */

DROP TABLE IF EXISTS `renewinfo`;

CREATE TABLE `renewinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '续签合同',
  `time` date DEFAULT NULL COMMENT '续签时间',
  `type` int(5) DEFAULT NULL COMMENT '续签类别',
  `operantTime` date DEFAULT NULL COMMENT '续签生效时间',
  `expireTime` date DEFAULT NULL COMMENT '续签到期时间',
  `contractInfo` int(11) DEFAULT NULL COMMENT '合同信息',
  `renewinforemark` varchar(500) DEFAULT NULL COMMENT '备注',
  `lasttime` varchar(50) DEFAULT NULL COMMENT '上次时间',
  `nexttime` varchar(50) DEFAULT NULL COMMENT '下次时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='合同续签信息';

/*Data for the table `renewinfo` */

insert  into `renewinfo`(`id`,`time`,`type`,`operantTime`,`expireTime`,`contractInfo`,`renewinforemark`,`lasttime`,`nexttime`) values (7,'2018-03-10',75,'2018-03-10','2018-03-10',2,'','2018-03-01 00:00:00','2018-03-01 00:00:00'),(8,'2018-03-02',75,'2018-03-16','2018-03-28',2,'123','2018-03-10 00:00:00','2018-03-10 00:00:00'),(9,'2018-05-17',75,'2018-05-18','2018-06-20',6,'','2018-05-01 00:00:00','2018-06-01 00:00:00'),(10,'2018-07-12',75,'2018-07-13','2018-08-23',6,'','2018-05-18 00:00:00','2018-06-20 00:00:00');

/*Table structure for table `restore` */

DROP TABLE IF EXISTS `restore`;

CREATE TABLE `restore` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '合同恢复',
  `time` date DEFAULT NULL COMMENT '时间',
  `type` int(5) DEFAULT NULL COMMENT '类型',
  `restoreremark` varchar(500) DEFAULT NULL COMMENT '说明',
  `contractInfo` int(11) DEFAULT NULL COMMENT '合同信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `restore` */

insert  into `restore`(`id`,`time`,`type`,`restoreremark`,`contractInfo`) values (2,'2018-03-01',127,'a',2),(3,'2018-03-22',127,'a',4),(4,'2018-03-01',128,'4152',4),(5,'2018-04-03',127,'',6),(6,'2018-03-31',127,'123',6),(7,'2018-04-01',127,'123',6),(8,'2018-04-18',127,'a',6),(9,'2018-04-19',127,'1',6),(10,'2018-04-19',128,'1',6),(11,'2018-04-25',127,'',6),(12,'2018-04-01',128,'复职恢复合同',6),(13,'2018-04-05',128,'复职恢复合同',6),(14,'2018-04-19',128,'123',6),(15,'2018-04-19',128,'复职恢复合同',9);

/*Table structure for table `rewardspunishments` */

DROP TABLE IF EXISTS `rewardspunishments`;

CREATE TABLE `rewardspunishments` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '奖惩项目',
  `name` varchar(50) DEFAULT NULL COMMENT '名字',
  `attribute` int(5) DEFAULT NULL COMMENT '属性',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `rewardspunishments` */

insert  into `rewardspunishments`(`id`,`name`,`attribute`) values (1,'最佳员工奖',108),(2,'全勤奖',108),(3,'最佳同事奖',108),(4,'会议迟到十分钟',109),(5,'连续迟到3天',109),(6,'部门业绩最优',108),(7,'部门业绩最差',109);

/*Table structure for table `rewardspunishmentsinfo` */

DROP TABLE IF EXISTS `rewardspunishmentsinfo`;

CREATE TABLE `rewardspunishmentsinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '奖惩信息',
  `time` varchar(50) DEFAULT NULL COMMENT '奖惩时间',
  `jcXiangMu` int(5) DEFAULT NULL COMMENT '奖惩项目',
  `attribute` varchar(50) DEFAULT NULL COMMENT '奖惩属性',
  `text` varchar(300) DEFAULT NULL COMMENT 'text',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `archives` int(11) DEFAULT NULL COMMENT '员工',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='奖惩项目';

/*Data for the table `rewardspunishmentsinfo` */

insert  into `rewardspunishmentsinfo`(`id`,`time`,`jcXiangMu`,`attribute`,`text`,`remark`,`archives`) values (1,'2018-04-07',2,'奖励','123',NULL,17),(2,'2018-04-03',4,'惩罚','asdad',NULL,23),(3,'2018-04-25',1,'奖励','套房源',NULL,39),(4,'2018-04-05',5,'惩罚','惩罚',NULL,57),(5,'2018-04-27',2,'奖励','奖励',NULL,57);

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色',
  `name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `available` int(5) DEFAULT NULL COMMENT '是否锁定',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='角色';

/*Data for the table `role` */

insert  into `role`(`id`,`name`,`remark`,`available`) values (1,'超级管理员','拥有全部模块权限',1),(2,'HR管理员','拥有HR系统的模块权限',1),(3,'CRM客户','拥有CRM系统的模块权限',1),(4,'经理','只拥有物业系统中某个分公司或者某个楼盘的权限',1),(5,'员工','没有一点点伤悲，没有一丝丝顾虑，就这样出现，在你世界里。',1);

/*Table structure for table `role_permission` */

DROP TABLE IF EXISTS `role_permission`;

CREATE TABLE `role_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `url` varchar(128) DEFAULT NULL COMMENT '访问url地址',
  `name` varchar(128) NOT NULL COMMENT '资源名称',
  `type` varchar(32) NOT NULL COMMENT '资源类型：menu,button,',
  `percode` varchar(128) DEFAULT NULL COMMENT '权限代码字符串',
  `parentid` bigint(20) DEFAULT NULL COMMENT '父结点id',
  `parentids` varchar(128) DEFAULT NULL COMMENT '父结点id列表串',
  `sortstring` varchar(128) DEFAULT NULL COMMENT '排序号',
  `available` char(1) DEFAULT NULL COMMENT '是否可用,1：可用，0不可用',
  `level` int(1) DEFAULT NULL COMMENT '级别',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;

/*Data for the table `role_permission` */

insert  into `role_permission`(`id`,`url`,`name`,`type`,`percode`,`parentid`,`parentids`,`sortstring`,`available`,`level`) values (1,'','我的人事','permission',NULL,0,NULL,NULL,'1',NULL),(2,'','系统管理','permission',NULL,0,NULL,NULL,'1',NULL),(3,'','人事档案','permission',NULL,0,NULL,NULL,'1',NULL),(4,'','人事调动','permission',NULL,0,NULL,NULL,'1',NULL),(5,'/user/admin','管理员信息','permission','user:admin',2,NULL,NULL,'1',NULL),(27,'/section/index','部门管理','permission','section:index',2,NULL,NULL,'1',NULL),(29,'/user/index','用户管理','permission','user:index',2,NULL,NULL,'1',NULL),(30,'/role/index','角色管理','permission','role:index',2,NULL,NULL,'1',2),(31,'/position/index','职务管理','permission','postition:index',2,NULL,NULL,'1',2),(32,'/rolepermission/index','权限管理','permission','permission:index',2,NULL,NULL,'1',2),(38,'/archives/index','人事管理','permission','archives:index',3,NULL,NULL,'1',NULL),(39,'/titleAssessInfo/index','职称评定','permission','titleAssessInfo:index',3,NULL,NULL,'1',NULL),(40,'/certificateInfo/index','证照资料','permission','certificateInfo:index',3,NULL,NULL,'1',NULL),(41,'/contractInfo/index','合同管理','permission','contractInfo:index',3,NULL,NULL,'1',NULL),(42,'/section/add','部门添加','menu','section:add',27,NULL,NULL,'1',NULL),(43,'/section/updata','部门修改','menu','section:updata',27,NULL,NULL,'1',NULL),(44,'/section/delSection','部门删除','menu','section:del',27,NULL,NULL,'1',NULL),(45,'/user/toadd','用户添加','menu','user:add',29,NULL,NULL,'1',NULL),(46,'/user/updata','用户修改','menu','user:edit',29,NULL,NULL,'1',NULL),(47,'/user/del','用户删除','menu','user:del',29,NULL,NULL,'1',NULL),(48,'/user/assign','分配角色','menu','user:assign',29,NULL,NULL,'1',NULL),(49,'/role/add','新增角色','menu','role:add',30,NULL,NULL,'1',NULL),(50,'/position/add','添加职务','menu','position:add',31,NULL,NULL,'1',NULL),(51,'/position/updata','更新职务','menu','position:updata',31,NULL,NULL,'1',NULL),(52,'/position/del','移除职务','menu','position:del',31,NULL,NULL,'1',NULL),(53,'/rolepermission/add','新增权限','menu','rolepermission:add',32,NULL,NULL,'1',NULL),(54,'/rolepermission/edit','权限维护','menu','rolepermission:edit',32,NULL,NULL,'1',NULL),(55,'/rolepermission/del','移除权限','menu','rolepermission:del',32,NULL,NULL,'1',NULL),(56,'/role/updata','维护角色','menu','role:updata',30,NULL,NULL,'1',NULL),(57,'/role/del','移除角色','menu','role:del',30,NULL,NULL,'1',NULL),(58,'/role/assign','分配权限','menu','role:assign',30,NULL,NULL,'1',NULL),(59,'/cultivate/index','培训计划','permission','cultivate:index',25,NULL,NULL,'1',NULL),(60,'/archive/add','新增档案','menu','archive:add',38,NULL,NULL,'1',NULL),(61,'/archive/edit','档案维护','menu','archive:edit',38,NULL,NULL,'1',NULL),(62,'/archive/dels','移除档案','menu','archive:dels',38,NULL,NULL,'1',NULL),(64,'/titleAssessInfo/add','新增职称','menu','titleAssessInfo:add',39,NULL,NULL,'1',NULL),(65,'/titleAssessInfo/edit','维护职称','menu','titleAssessInfo:edit',39,NULL,NULL,'1',NULL),(66,'/titleAssessInfo/dels','移除职称','menu','titleAssessInfo:dels',39,NULL,NULL,'1',NULL),(67,'/titleAssessInfo/add','新增证照资料','menu','certificateInfo:add',40,NULL,NULL,'1',NULL),(68,'/titleAssessInfo/edit','维护证照资料','menu','certificateInfo:edit',40,NULL,NULL,'1',NULL),(69,'/titleAssessInfo/dels','移除证照资料','menu','certificateInfo:dels',40,NULL,NULL,'1',NULL),(70,'/contractInfo/add','新增合同','menu','contractInfo:add',41,NULL,NULL,'1',NULL),(71,'/contractInfo/edit','维护合同','menu','contractInfo:edit',41,NULL,NULL,'1',NULL),(72,'/contractInfo/dels','移除合同','menu','contractInfo:dels',41,NULL,NULL,'1',NULL),(73,'/removeInfo/index','解除合同','menu','removeInfo:index',41,NULL,NULL,'1',NULL),(74,'/removeInfo/add','新增解除记录','menu','removeInfo:add',73,NULL,NULL,'1',NULL),(75,'/removeInfo/dels','移除解除记录','menu','removeInfo:dels',73,NULL,NULL,'1',NULL),(76,'/restore/dels','恢复合同','menu','restore:index',41,NULL,NULL,'1',NULL),(77,'/restore/add','新增合同恢复记录','menu','restore:add',76,NULL,NULL,'1',NULL),(78,'/restore/dels','移除合同恢复记录','menu','restore:dels',76,NULL,NULL,'1',NULL),(79,'/positive/index','转正记录','menu','positive:index',41,NULL,NULL,'1',NULL),(80,'/positive/add','新增转正记录','menu','positive:add',79,NULL,NULL,'1',NULL),(81,'/positive/edit','维护转正记录','menu','positive:edit',79,NULL,NULL,'1',NULL),(82,'/positive/dels','移除转正记录','menu','positive:dels',79,NULL,NULL,'1',NULL),(83,'/renewInfo/index','合同续签记录','menu','renewInfo:index',41,NULL,NULL,'1',NULL),(84,'/renewInfo/edit','新增续签记录','menu','renewInfo:add',83,NULL,NULL,'1',NULL),(85,'/renewInfo/add','维护续签记录','menu','renewInfo:edit',83,NULL,NULL,'1',NULL),(86,'/renewInfo/dels','移除续签记录','menu','renewInfo:dels',83,NULL,NULL,'1',NULL),(87,'/changeInfo/index','合同变更记录','menu','changeInfo:index',41,NULL,NULL,'1',NULL),(88,'/changeInfo/add','新增变更记录','menu','changeInfo:add',87,NULL,NULL,'1',NULL),(89,'/changeInfo/edit','维护变更记录','menu','changeInfo:edit',87,NULL,NULL,'1',NULL),(90,'/changeInfo/dels','移除变更记录','menu','changeInfo:dels',87,NULL,NULL,'1',NULL),(91,'/cultivate/add','新增计划','menu','cultivate:add',59,NULL,NULL,'1',NULL),(92,'/cultivate:edit','维护计划','menu','cultivate:edit',59,NULL,NULL,'1',NULL),(93,'/cultivate/dels','移除计划','menu','cultivate:dels',59,NULL,NULL,'1',NULL),(94,'/mobilize:index','人员调动','permission','mobilize:index',26,NULL,NULL,'1',NULL),(95,'/mobilize:add','新增人员调动','menu','mobilize:add',94,NULL,NULL,'1',NULL),(96,'/mobilize/edit','维护人员调动','menu','mobilize:edit',94,NULL,NULL,'1',NULL),(97,'/mobilize/dels','移除人员调动记录','menu','mobilize:dels',94,NULL,NULL,'1',NULL),(98,'/departure/index','员工离职','permission','departure:index',4,NULL,NULL,'1',NULL),(99,'/departure/add','新增员工离职记录','menu','departure:add',98,NULL,NULL,'1',NULL),(100,'/departure/edit','维护员工离职记录','menu','departure:edit',98,NULL,NULL,'1',NULL),(101,'/departure/dels','移除员工离职记录','menu','departure:dels',98,NULL,NULL,'1',NULL),(102,'/reinstatement/index','员工复职','permission','reinstatement:index',4,NULL,NULL,'1',NULL),(103,'/reinstatement/add','新增员工复职记录','menu','reinstatement:add',102,NULL,NULL,'1',NULL),(104,'/reinstatement/edit','维护员工复职记录','menu','reinstatement:edit',102,NULL,NULL,'1',NULL),(105,'/reinstatement/dels','移除员工复职记录','menu','reinstatement:dels',102,NULL,NULL,'1',NULL),(106,'/rewardspunishments/index','奖惩项目','permission','rewardspunishments:index',2,NULL,NULL,'1',NULL),(107,'/rewardspunishments/add','新增奖惩项目','menu','rewardspunishments:add',106,NULL,NULL,'1',NULL),(108,'/rewardspunishments/edit','维护奖惩项目','menu','rewardspunishments:edit',106,NULL,NULL,'1',NULL),(109,'/rewardspunishments/batchDelete','移除奖惩项目','menu','rewardspunishments:dels',106,NULL,NULL,'1',NULL),(110,'/rewardsPunishmentsInfo/index','奖惩管理','permission','rewardsPunishmentsInfo:index',4,NULL,NULL,'1',NULL),(111,'/rewardsPunishmentsInfo/add','新增奖惩记录','menu','rewardsPunishmentsInfo:add',110,NULL,NULL,'1',NULL),(112,'/rewardsPunishmentsInfo/edit','维护奖惩记录','menu','rewardsPunishmentsInfo:edit',110,NULL,NULL,'1',NULL),(113,'/rewardsPunishmentsInfo/dels','移除奖惩记录','menu','rewardsPunishmentsInfo:dels',110,NULL,NULL,'1',NULL),(115,'/log/index','日志管理','permission','log:index',2,NULL,NULL,'1',NULL),(116,'/user/addadmin','新增管理员','menu','user:addadmin',5,NULL,NULL,'1',NULL),(117,'/user/updataadmin','维护管理员信息','menu','user:editadmin',5,NULL,NULL,'1',NULL),(118,'/user/deladmin','移除管理员','menu','user:deladmin',5,NULL,NULL,'1',NULL),(119,'/user/assignadmin','管理员附加角色','menu','user:assignadmin',5,NULL,NULL,'1',NULL),(120,'/cultivate/index','培训计划','permission','cultivate:index',3,NULL,NULL,'1',NULL),(121,'/cultivate/add','新增培训计划','menu','cultivate:add',120,NULL,NULL,'1',NULL),(122,'/cultivate/edit','维护培训计划','menu','cultivate:edit',120,NULL,NULL,'1',NULL),(123,'/cultivate/dels','移除培训计划','menu','cultivate:dels',120,NULL,NULL,'1',NULL),(124,'/mobilize/index','员工调动','permission','mobilize:index',4,NULL,NULL,'1',NULL),(125,'/mobilize/add','新增调动','menu','mobilize:add',124,NULL,NULL,'1',NULL),(126,'/mobilize/edit','维护调动','menu','mobilize:edit',124,NULL,NULL,'1',NULL),(127,'/mobilize/dels','移除调动记录','menu','mobilize:dels',124,NULL,NULL,'1',NULL),(128,'','查看合同','permission','',41,NULL,NULL,'1',NULL),(129,'/user/my','个人档案','permission','user:my',1,NULL,NULL,'1',NULL);

/*Table structure for table `role_relationship` */

DROP TABLE IF EXISTS `role_relationship`;

CREATE TABLE `role_relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(250) NOT NULL,
  `role_permission_id` int(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=871 DEFAULT CHARSET=utf8;

/*Data for the table `role_relationship` */

insert  into `role_relationship`(`id`,`role_id`,`role_permission_id`) values (680,5,1),(681,5,129),(682,1,2),(683,1,5),(684,1,116),(685,1,117),(686,1,118),(687,1,119),(688,1,27),(689,1,42),(690,1,43),(691,1,44),(692,1,29),(693,1,45),(694,1,46),(695,1,47),(696,1,48),(697,1,30),(698,1,49),(699,1,56),(700,1,57),(701,1,58),(702,1,31),(703,1,50),(704,1,51),(705,1,52),(706,1,32),(707,1,53),(708,1,54),(709,1,55),(710,1,115),(711,1,3),(712,1,38),(713,1,60),(714,1,61),(715,1,62),(716,1,39),(717,1,64),(718,1,65),(719,1,66),(720,1,40),(721,1,67),(722,1,68),(723,1,69),(724,1,41),(725,1,70),(726,1,71),(727,1,72),(728,1,73),(729,1,74),(730,1,75),(731,1,76),(732,1,77),(733,1,78),(734,1,79),(735,1,80),(736,1,81),(737,1,82),(738,1,83),(739,1,84),(740,1,85),(741,1,86),(742,1,87),(743,1,88),(744,1,89),(745,1,90),(746,1,128),(747,1,120),(748,1,121),(749,1,122),(750,1,123),(751,1,4),(752,1,98),(753,1,99),(754,1,100),(755,1,101),(756,1,102),(757,1,103),(758,1,104),(759,1,105),(760,1,106),(761,1,107),(762,1,108),(763,1,109),(764,1,110),(765,1,111),(766,1,112),(767,1,113),(768,1,124),(769,1,125),(770,1,126),(771,1,127),(772,2,2),(773,2,5),(774,2,116),(775,2,117),(776,2,118),(777,2,119),(778,2,27),(779,2,42),(780,2,43),(781,2,44),(782,2,29),(783,2,45),(784,2,46),(785,2,47),(786,2,48),(787,2,30),(788,2,49),(789,2,56),(790,2,57),(791,2,58),(792,2,31),(793,2,50),(794,2,51),(795,2,52),(796,2,32),(797,2,53),(798,2,54),(799,2,55),(800,2,115),(801,2,3),(802,2,38),(803,2,60),(804,2,61),(805,2,62),(806,2,39),(807,2,64),(808,2,65),(809,2,66),(810,2,40),(811,2,67),(812,2,68),(813,2,69),(814,2,41),(815,2,70),(816,2,71),(817,2,72),(818,2,73),(819,2,74),(820,2,75),(821,2,76),(822,2,77),(823,2,78),(824,2,79),(825,2,80),(826,2,81),(827,2,82),(828,2,83),(829,2,84),(830,2,85),(831,2,86),(832,2,87),(833,2,88),(834,2,89),(835,2,90),(836,2,128),(837,2,120),(838,2,121),(839,2,122),(840,2,123),(841,2,4),(842,2,98),(843,2,99),(844,2,100),(845,2,101),(846,2,102),(847,2,103),(848,2,104),(849,2,105),(850,2,106),(851,2,107),(852,2,108),(853,2,109),(854,2,110),(855,2,111),(856,2,112),(857,2,113),(858,2,124),(859,2,125),(860,2,126),(861,2,127),(862,4,1),(863,4,129),(864,4,128),(865,4,3),(866,4,41),(867,3,118),(868,3,119),(869,3,2),(870,3,5);

/*Table structure for table `section` */

DROP TABLE IF EXISTS `section`;

CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门',
  `code` varchar(50) DEFAULT NULL COMMENT '部门编号',
  `name` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `parentids` int(11) DEFAULT NULL COMMENT '上级部门',
  `childids` int(11) DEFAULT NULL COMMENT '下级部门',
  `principal` varchar(50) DEFAULT NULL COMMENT '部门负责人',
  `tel` varchar(50) DEFAULT NULL COMMENT '部门电话',
  `fax` varchar(50) DEFAULT NULL COMMENT '部门传真',
  `type` int(5) DEFAULT NULL COMMENT '部门类型',
  `permissions` int(5) DEFAULT NULL COMMENT '部门管理权限',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `num` int(11) DEFAULT NULL COMMENT '单位编制数',
  `post` int(11) DEFAULT NULL COMMENT '岗位编制',
  `url` varchar(50) DEFAULT NULL COMMENT 'url',
  `percode` varchar(100) DEFAULT NULL COMMENT '权限代码字符串',
  `sortstring` varchar(100) DEFAULT NULL COMMENT '排序号',
  `level` int(5) DEFAULT NULL COMMENT '级别',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='部门';

/*Data for the table `section` */

insert  into `section`(`id`,`code`,`name`,`parentids`,`childids`,`principal`,`tel`,`fax`,`type`,`permissions`,`remark`,`num`,`post`,`url`,`percode`,`sortstring`,`level`) values (1,'jtzb','集团总部',0,NULL,'hr管理','0310-000001','000111-2221220',92,97,'测试1',100,5,NULL,NULL,NULL,NULL),(2,'lszx','零售事业部',1,NULL,'管理1','0310-000002','000111-2221221',92,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(3,'cwzx','财务中心',1,NULL,'管理2','0310-000003','000111-2221222',92,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(4,'rxzx','人事中心',1,NULL,'管理3','0310-000004','000111-2221223',92,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(5,'xxzx','信息中心',1,NULL,'管理4','0310-000005','000111-2221224',92,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(6,'wy','物业事业部',1,NULL,'管理5','0310-000006','000111-2221225',92,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(7,'yj','幼教事业部',1,NULL,'管理6','0310-000007','000111-2221488',92,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(8,'cgb','采购部',2,NULL,'管理7','0310-000008','000111-2221489',94,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(9,'yyb','营运部',2,NULL,'管理8','0310-000009','000111-22214810',94,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(10,'qhb','企划部',2,NULL,'管理9','0310-000010','000111-2221485',92,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(11,'kjb','会计室',3,NULL,'管理10','0310-000011','000111-2221486',92,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(12,'cns','出纳室',3,NULL,'管理11','0310-000012','000111-2221499',92,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(13,'zjb','总经办',4,NULL,'管理12','0310-000013','000111-2221487',92,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(14,'xzb','行政办',4,NULL,'管理13','0310-000014','000111-2221868',92,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(15,'rjxt','软件系统组',5,NULL,'管理14','0310-000015','000111-2841488',96,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(16,'yjwl','硬件网络组',5,NULL,'管理15','0310-000016','000111-2655488',96,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(18,'cg2','采购组二',8,NULL,'管理17','0310-000018','000111-22214865',94,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(19,'cg3','采购组三',8,NULL,'管理18','0310-000019','000111-ADS1488',94,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(20,'cg4','采购组四',8,NULL,'管理17','0310-000020','000111-22545488',94,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(21,'cg5','采购组五',8,NULL,'管理20','0310-000021','00011SDD1488',94,97,'测试1',NULL,NULL,NULL,NULL,NULL,NULL),(26,'564','采购组八',2,NULL,'123','156483141414','153453441414141',92,98,'1231312312',NULL,NULL,NULL,NULL,NULL,NULL),(27,'BM-84792152','又叫',6,NULL,'阿斯达','12141','qweq',93,NULL,'asdas',NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `titleassessinfo` */

DROP TABLE IF EXISTS `titleassessinfo`;

CREATE TABLE `titleassessinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '职称评定信息',
  `archives` int(11) DEFAULT NULL COMMENT '员工',
  `name` int(5) DEFAULT NULL COMMENT '获得职称',
  `way` int(5) DEFAULT NULL COMMENT '取得方式',
  `time` varchar(50) DEFAULT NULL COMMENT '取得时间',
  `nextName` varchar(50) DEFAULT NULL COMMENT '下次申报',
  `nextTime` varchar(50) DEFAULT NULL COMMENT '下次申报时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注信息',
  `accessory` varchar(200) DEFAULT NULL COMMENT '附件',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='职称评定信息';

/*Data for the table `titleassessinfo` */

insert  into `titleassessinfo`(`id`,`archives`,`name`,`way`,`time`,`nextName`,`nextTime`,`remark`,`accessory`) values (21,17,36,66,'2018-04-05','36','2018-04-12','123',NULL),(22,52,38,66,'2018-04-12','40','2018-04-09','会计',NULL),(23,57,36,65,'2018-04-12','37','2020-04-08','asd',NULL),(24,17,37,66,'2018-04-04','37','2018-04-04','0.12',NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户',
  `username` varchar(500) DEFAULT NULL COMMENT '账号',
  `password` varchar(500) DEFAULT NULL COMMENT '密码',
  `salt` varchar(50) DEFAULT NULL COMMENT '盐',
  `archives_id` int(11) DEFAULT NULL COMMENT '员工',
  `statu` int(5) DEFAULT NULL COMMENT '状态',
  `role_id` int(11) DEFAULT NULL COMMENT '角色',
  `userCode` varchar(50) DEFAULT NULL COMMENT '用户编码',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `judge` int(5) DEFAULT NULL COMMENT '管理员=0/用户=1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='用户';

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`salt`,`archives_id`,`statu`,`role_id`,`userCode`,`remark`,`judge`) values (17,'admin','4297f44b13955235245b2497399d7a93','123',NULL,1,NULL,'GLY-0000001',NULL,0),(18,'wang','4297f44b13955235245b2497399d7a93','123',17,1,NULL,'HD—003','123',1),(19,'root','b4b8daf4b8ea9d39568719e1e320076f','root',NULL,1,NULL,'GL-00000002',NULL,0),(20,'123','4297f44b13955235245b2497399d7a93','123',20,1,NULL,'YG-90955653','',1),(21,'asda','200820e3227815ed1756a6b531e7e0d2','qwe',NULL,1,NULL,'GL-47931436',NULL,0),(22,'qwe','4297f44b13955235245b2497399d7a93','123',57,1,NULL,'YG-12254374',NULL,1);

/*Table structure for table `userrole` */

DROP TABLE IF EXISTS `userrole`;

CREATE TABLE `userrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `userrole` */

insert  into `userrole`(`id`,`user_id`,`role_id`) values (1,13,1),(8,17,1),(15,20,4),(16,18,5),(17,19,1),(18,19,4),(19,19,5),(20,21,1),(21,21,3),(22,22,4);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
