package org.hr.shiro;

import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.hr.pojo.Rolepermission;
import org.hr.pojo.User;
import org.hr.service.RolepermissionService;
import org.hr.service.UserService;

public class CustomRealm extends AuthorizingRealm {
	
	@Resource
	private UserService userService;
	
	@Resource
	private RolepermissionService rolepermissionService;
	

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setRolepermissionService(RolepermissionService rolepermissionService) {
		this.rolepermissionService = rolepermissionService;
	}

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		System.out.println("用户授权验证！");
		User user = (User) principals.getPrimaryPrincipal();
		
		List<String> menus = rolepermissionService.getMenuCodeByUserId(user.getId());
		
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		info.addStringPermissions(menus);
		return info;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		System.out.println("用户登录认证！");
		String principal = (String) token.getPrincipal();
		System.out.println(11111);
		User user = userService.getByUsername(principal);
		
		if (user == null) {
			System.err.println("名字错误");
			return null;
		}
		List<Rolepermission> permission = rolepermissionService.getPermissionByUserId(user.getId());
		user.setPermission(permission);
		System.err.println("名字正确");
		
		return new SimpleAuthenticationInfo(user,user.getPassword(),ByteSource.Util.bytes(user.getSalt()),this.getName());
	}

}



