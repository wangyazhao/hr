package org.hr.shiro;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hr.pojo.LoginLog;
import org.hr.pojo.User;
import org.hr.service.LoginLogService;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LogInterceptor extends HandlerInterceptorAdapter {

	
	private static Properties properties = new Properties();
	
	@Resource
	private LoginLogService loginLogService;

	public void setLoginLogService(LoginLogService loginLogService) {
		this.loginLogService = loginLogService;
	}
	
	static {
		try {
			InputStream in = new FileInputStream(new File("src/main/resources/permission.properties"));
			properties.load(in);
		} catch (Exception e) {
			System.out.println("读取文件失败！");
			e.printStackTrace();
		}
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		
		//new String(name.getBytes(),"iso8859-1")
		String context = properties.getProperty(request.getRequestURI());
		if (context != null) {
			//模块名称
			
			User user = (User) request.getSession().getAttribute("login_user");
			LoginLog log = new LoginLog();
			log.setIp(LogInterceptor.getIP(request));
			log.setUsername(user.getUsername()); 
			log.setUsercode(user.getUsercode());
			log.setPort(request.getLocalPort()+"");
			log.setModule(new String(context.getBytes("iso8859-1")));
			loginLogService.add(log);
			System.out.println("添加日志成功");
		}
		
		
		System.out.println("访问了uri" + request.getRequestURI());
		String requestUrl = request.getRequestURL().toString();
		if (requestUrl.indexOf("user/index") > 0) {
			System.out.println("访问了user/index 页面");
		} 
		System.out.println("handler" + handler.toString());
        System.out.println("请求地址：" + requestUrl);
		System.out.println("*****"  + request.getContextPath());
		System.out.println("取得客户端的系统版本 = "+request.getHeader("User-Agent"));    //就是取得客户端的系统版本     
		System.out.println("取得客户端的IP="+request.getRemoteAddr());    //取得客户端的IP     
		System.out.println("取得客户端的主机名="+request.getRemoteHost());     //取得客户端的主机名     
		System.out.println("取得客户端的端口  = "+request.getRemotePort());    //取得客户端的端口     
		System.out.println("取得客户端的用户= "+request.getRemoteUser());    //取得客户端的用户     
		System.out.println("取得服务器IP= "+request.getLocalAddr());    //取得服务器IP     
		System.out.println("取得服务器端口="+request.getLocalPort());    //取得服务器端口
		System.out.println(request.getRemoteAddr());

		super.afterCompletion(request, response, handler, ex);
	}
	
	
	
	public static String getIP(HttpServletRequest request) {
		 String ip = request.getHeader("X-Forwarded-For");   
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("WL-Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getRemoteAddr();  
        }
        return ip;
	}

	
	
	
}
