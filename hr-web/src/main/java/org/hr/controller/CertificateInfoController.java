package org.hr.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.hr.pojo.CertificateInfo;
import org.hr.service.CertificateInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/certificateInfo")
public class CertificateInfoController {
	@Resource
	private CertificateInfoService certificateInfoService;

	public void setCertificateInfoService(CertificateInfoService certificateInfoService) {
		this.certificateInfoService = certificateInfoService;
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "certificateInfo/index";
	}
	@RequestMapping("/form")
	public String userForm() throws Exception {
		return "certificateInfo/form";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list(@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,CertificateInfo condition) throws Exception{
		return certificateInfoService.listMap(page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> add(CertificateInfo certificateInfo) throws Exception{
		return certificateInfoService.add(certificateInfo);
	}
	@RequestMapping("edit")
	@ResponseBody
	public Map<String, Object> edit(CertificateInfo certificateInfo) throws Exception{
		return certificateInfoService.edit(certificateInfo);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return certificateInfoService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public CertificateInfo view(Integer id) throws Exception{
		return certificateInfoService.getById(id);
	}
	
	@RequestMapping("/showform")
	public String showForm() throws Exception {
		return "certificateInfo/showform";
	}
}
