package org.hr.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.RenewInfo;
import org.hr.service.RenewInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/renewInfo")
public class RenewInfoController {
	@Resource
	private RenewInfoService renewInfoService;

	public void setRenewInfoService(RenewInfoService renewInfoService) {
		this.renewInfoService = renewInfoService;
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "contractInfo/renewInfo/index";
	}
	@RequestMapping("/form")
	public String userForm() throws Exception {
		return "contractInfo/renewInfo/form";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list( Integer byid,@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,RenewInfo condition) throws Exception{
		return renewInfoService.listMap(byid,page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("renewInfo:add")
	public Map<String, Object> add(RenewInfo renewInfo) throws Exception{
		return renewInfoService.add(renewInfo);
	}
	@RequestMapping("edit")
	@ResponseBody
	@RequiresPermissions("renewInfo:edit")
	public Map<String, Object> edit(RenewInfo renewInfo) throws Exception{
		return renewInfoService.edit(renewInfo);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("renewInfo:dels")
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return renewInfoService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public RenewInfo view(Integer id) throws Exception{
		return renewInfoService.getById(id);
	}
	
	@RequestMapping("/showform")
	public String showForm() throws Exception {
		return "contractInfo/renewInfo/showform";
	}
}
