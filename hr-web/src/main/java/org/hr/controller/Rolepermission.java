package org.hr.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.dao.RolepermissionDao;
import org.hr.service.RolepermissionService;
 import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/rolepermission")
public class Rolepermission {

	@Resource
	private RolepermissionService rolepermissionService;
	
	@Resource
	private RolepermissionDao rolepermissionDao;
	

	public void setRolepermissionDao(RolepermissionDao rolepermissionDao) {
		this.rolepermissionDao = rolepermissionDao;
	}
	public void setRolepermissionService(RolepermissionService rolepermissionService) {
		this.rolepermissionService = rolepermissionService;
	}
	@RequestMapping("/index")
	public String index() throws Exception{
		return "rolepermission/index";
	}
	
	@RequestMapping("/toadd")
	public String toadd() throws Exception{
		return "rolepermission/add_from";
	} 
	
	@RequestMapping("/assign")
	public String assign() throws Exception{
		return "role/assign";
	}
	 
	@RequestMapping("/all")
	@ResponseBody
	public List<org.hr.pojo.Rolepermission> list() throws Exception{ 
		return rolepermissionService.list();
	}
	
	@RequestMapping("/alls")
	@ResponseBody
	public List<org.hr.pojo.Rolepermission> lists() throws Exception{ 
		return rolepermissionService.lists();
	}
	
 	@RequestMapping("/del")
	@ResponseBody
	@RequiresPermissions("rolepermission:del")
	public Map<String,Object> del(Integer id) throws Exception{  
		Map<String,Object> map = new HashMap<>();

 		rolepermissionDao.deleteById(id);
		map.put("result", true);
 		return map;
	} 
	
	@RequestMapping("/level")
	@ResponseBody
	public List<org.hr.pojo.Rolepermission> level() throws Exception{
		return rolepermissionDao.getPermissionLevel();
	}
	 
	@RequestMapping("/type")
	@ResponseBody
	public List<org.hr.pojo.Rolepermission> type() throws Exception{
		return rolepermissionDao.getPermissionType();
	}
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("rolepermission:add")
	public Map<String,Object> del(org.hr.pojo.Rolepermission rolepermission) throws Exception{  
		Map<String,Object> map = new HashMap<>();
		rolepermissionDao.add(rolepermission);
		map.put("result", true);
		return map;
	}


	@RequestMapping("/view")
	@ResponseBody
	public org.hr.pojo.Rolepermission view(Integer id) throws Exception {
		return rolepermissionDao.getById(id);
	}
	
	
 	@RequestMapping(value="edit",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("rolepermission:edit")
	public Map<String, Object> edit(org.hr.pojo.Rolepermission rolepermission) throws Exception{
		Map<String, Object> map = new HashMap<>();
		rolepermissionDao.update(rolepermission);
		map.put("result", true);
		return map;
	} 
	
	
	
	
}
