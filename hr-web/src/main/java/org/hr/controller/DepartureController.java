package org.hr.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.Departure;
import org.hr.service.DepartureService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/departure")
public class DepartureController {
	@Resource
	private DepartureService departureService;

	public void setDepartureService(DepartureService departureService) {
		this.departureService = departureService;
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "departure/index";
	}
	@RequestMapping("/form")
	public String userForm() throws Exception {
		return "departure/form";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list(@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,Departure condition) throws Exception{
		return departureService.listMap(page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("departure:add")
	public Map<String, Object> add(Departure departure) throws Exception{
		return departureService.add(departure);
	}
	@RequestMapping("edit")
	@ResponseBody
	@RequiresPermissions("departure:edit")
	public Map<String, Object> edit(Departure departure) throws Exception{
		return departureService.edit(departure);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("departure:dels")
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return departureService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public Departure view(Integer id) throws Exception{
		return departureService.getById(id);
	}
	
	@RequestMapping("/showform")
	public String showForm() throws Exception {
		return "departure/showform";
	}
	@RequestMapping("/contractinfoid")
	@ResponseBody
	public Integer getcontractinfoid(Integer id) throws Exception{
		return departureService.getcontractinfoid(id);
	}
}
