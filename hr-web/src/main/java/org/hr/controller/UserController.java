package org.hr.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.dao.UserRoleDao;
import org.hr.pojo.User;
import org.hr.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Resource
	private UserService userSerice;
	
	@Resource
	private UserRoleDao userRoleDao;
	
	public void setUserRoleDao(UserRoleDao userRoleDao) {
		this.userRoleDao = userRoleDao;
	}

	public void setUserSerice(UserService userSerice) {
		this.userSerice = userSerice;
	}
	
	@RequestMapping("/index")
	public String index() throws Exception {
		return "user/index";
	}
	@RequestMapping("/my")
	public String my() throws Exception {
		return "my/index";
	}
	@RequestMapping("/my2")
	public String my2() throws Exception {
		return "my/index2";
	}
	

	@RequestMapping("/my3")
	public String my3() throws Exception {
		return "my/index3";
	}
	@RequestMapping("/my4")
	public String my4() throws Exception {
		return "my/index4";
	}

	@RequestMapping("/my5")
	public String my5() throws Exception {
		return "my/index5";
	}
	@RequestMapping("/my6")
	public String my6() throws Exception {
		return "my/index6";
	}

	@RequestMapping("/my7")
	public String my7() throws Exception {
		return "my/index7";
	}
	@RequestMapping("/my8")
	public String my8() throws Exception {
		return "my/index8";
	}
	@RequestMapping("/userForm")
	public String userForm() throws Exception {
		return "user/user_form";
	}
	@RequestMapping("/addForm")
	public String addForm() throws Exception {
		return "user/add_form";
	}
	
	@RequestMapping("/toassign")
	public String toassign(Integer rid,ModelMap modelMap) throws Exception {
		modelMap.put("userId",rid);
		return "user/assign";
	} 
	
	//admin
	@RequestMapping("/admin")
	public String admin() throws Exception {
		return "admin/index";
	}
	@RequestMapping("/adminadd")
	public String adminaddForm() throws Exception {
		return "admin/add_form";
	}
	
	@RequestMapping("/admintoassign")
	public String admintoassign() throws Exception {
		return "admin/assign";
	} 
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list(@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,User condition) throws Exception{
		return userSerice.listMap(page, rows, sort, order, condition);
	}
	@RequestMapping(value="adminlist",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> adminlist(@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,User condition) throws Exception{
		return userSerice.adminlistMap(page, rows, sort, order, condition);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public User view(Integer id) throws Exception {
		return userSerice.view(id);
	}
	
	
	@RequestMapping(value="/updata",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("user:edit")
	public Map<String, Object> updata(User user) throws Exception{
		return userSerice.updata(user);
	}
	
	@RequestMapping(value="/toadd",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("user:add")
	public Map<String, Object> toAdd(User user) throws Exception{
		return userSerice.addUser(user);
	}
	
	@RequestMapping(value="/del",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("user:del")
	public Map<String, Object> del(Integer id) throws Exception{
		return userSerice.delById(id);
	}
	@RequestMapping(value="/updataadmin",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("user:editadmin")
	public Map<String, Object> updataadmin(User user) throws Exception{
		return userSerice.updata(user);
	}
	
	@RequestMapping(value="/toaddadmin",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("user:addadmin")
	public Map<String, Object> toAddadmin(User user) throws Exception{
		return userSerice.addUser(user);
	}
	
	@RequestMapping(value="/deladmin",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("user:addadmin")
	public Map<String, Object> deladmin(Integer id) throws Exception{
		return userSerice.delById(id);
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	@ResponseBody
	public int login(String username, String password,HttpSession session){
		User user = userSerice.login(username, password);
		int result = user.getId();
		if(result>0) {
			session.setAttribute("login_user", user);
			return 0;
		}else if(result==-1){
			user = null;
			session.setAttribute("login_user", user);
			return -1;
		}else {
			user = null;
			session.setAttribute("login_user", user);
			return -2;
		}
	};
	
	@RequestMapping("/getPermissions")
	@ResponseBody
	public List<Integer> selectPermission(Integer userId) throws Exception{
		return userRoleDao.getPermissionIdsByUserId(userId);  
	}
	
	@RequestMapping("/assign")
	@ResponseBody
	@RequiresPermissions("user:assignadmin")
	public Map<String,Object> assign(Integer userId,Integer[] ids) throws Exception{
		Map<String, Object> map = new HashMap<>();
		userRoleDao.deletePermissionsByUserId(userId);  
		userRoleDao.addUserRoles(userId, Arrays.asList(ids));
		map.put("result", true);
		return map;
	}
	@RequestMapping("/assignadmin")
	@ResponseBody
	@RequiresPermissions("user:assignadmin")
	public Map<String,Object> assignadmin(Integer userId,Integer[] ids) throws Exception{
		Map<String, Object> map = new HashMap<>();
		userRoleDao.deletePermissionsByUserId(userId);  
		userRoleDao.addUserRoles(userId, Arrays.asList(ids));
		map.put("result", true);
		return map;
	}
	
	
	@RequestMapping(value="reviewer",method=RequestMethod.POST)
	@ResponseBody
	public List<User> getreviewer(Integer id) throws Exception{
		return userSerice.getreviewer(id);
	}
	
}
