package org.hr.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.RemoveInfo;
import org.hr.service.RemoveInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/removeInfo")
public class RemoveInfoController {

	@Resource
	private RemoveInfoService removeInfoService;

	public void setRemoveInfoService(RemoveInfoService removeInfoService) {
		this.removeInfoService = removeInfoService;
	}
	
	@RequestMapping("/index")
	public String index() throws Exception {
		return "contractInfo/removeInfo/index";
	}
	@RequestMapping("/form")
	public String userForm() throws Exception {
		return "contractInfo/removeInfo/form";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list( Integer byid,@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,RemoveInfo condition) throws Exception{
		return removeInfoService.listMap(byid,page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("removeInfo:add")
	public Map<String, Object> add(RemoveInfo removeInfo) throws Exception{
		return removeInfoService.add(removeInfo);
	}
	@RequestMapping("edit")
	@ResponseBody
	public Map<String, Object> edit(RemoveInfo removeInfo) throws Exception{
		return removeInfoService.edit(removeInfo);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("removeInfo:dels")
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return removeInfoService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public RemoveInfo view(Integer id) throws Exception{
		return removeInfoService.getById(id);
	}
	
	@RequestMapping("/showform")
	public String showForm() throws Exception {
		return "contractInfo/removeInfo/showform";
	}
}
