package org.hr.controller;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.hr.pojo.LoginLog;
import org.hr.pojo.User;
import org.hr.service.LoginLogService;
import org.hr.service.RewardspunishmentsService;
import org.hr.shiro.LogInterceptor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/hr")
public class HrController {

	@Resource
	private RewardspunishmentsService rewardspunishmentsService; 
	
	public void setRewardspunishmentsService(RewardspunishmentsService rewardspunishmentsService) {
		this.rewardspunishmentsService = rewardspunishmentsService;
	}
	@Resource
	private LoginLogService loginLogService;
	
	public void setLoginLogService(LoginLogService loginLogService) {
		this.loginLogService = loginLogService;
	}
	@RequestMapping("/main")
	public String main(HttpSession session,HttpServletRequest request) throws Exception {
		Subject subject = SecurityUtils.getSubject();
		User user = (User) subject.getPrincipal();
		session.setAttribute("login_user", user);
		LoginLog log = new LoginLog();
		log.setIp(LogInterceptor.getIP(request));
		log.setUsername(user.getUsername()); 
		log.setUsercode(user.getUsercode());
		log.setPort(request.getLocalPort()+"");
		log.setModule("登陆系统");
		loginLogService.add(log);
		System.out.println("添加日志成功");
		return "main";
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "index";
	}
	@RequestMapping("/no")
	public String no() throws Exception {
		return "no";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String login() throws Exception {
		return "Login";
	}
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String login(HttpSession session) throws Exception {
		System.err.println("认证失败！");
		return "redirect:/hr/login";
	}
	
	
	@RequestMapping("/logout")
	public String Logout(HttpSession session,HttpServletRequest request) throws Exception {
		User user = (User) session.getAttribute("login_user");
		session.setAttribute("login_user", user);
		LoginLog log = new LoginLog();
		log.setIp(LogInterceptor.getIP(request));
		log.setUsername(user.getUsername()); 
		log.setUsercode(user.getUsercode());
		log.setPort(request.getLocalPort()+"");
		log.setModule("安全退出系统");
		loginLogService.add(log);
		System.err.println("添加日志成功");
		
		
		session.removeAttribute("login_user");
		session.removeAttribute("permissions");
		return "redirect:/hr/login";
	}
	
	//上传文件
	@RequestMapping(value="/upload",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> upload(MultipartFile fileName,HttpServletRequest request) throws Exception{
		Map<String, Object> map = new HashMap<>();
		if (!fileName.isEmpty()) {
			String path = request.getServletContext().getRealPath("/file");
			File dest = new File(path + File.separator + fileName.getOriginalFilename());
			fileName.transferTo(dest);
			map.put("r", true);
			map.put("url", "file/"+fileName.getOriginalFilename());
			map.put("name", fileName.getOriginalFilename());
		}else {
			map.put("r", false);
		}
		return map;
	}
	/*@RequestMapping("/download/{name}")
	public ResponseEntity<byte[]> download(HttpServletRequest request,String name) throws IOException {
		String path = request.getServletContext().getRealPath("/file/add.png");
	    File file = new File(path);
	    byte[] body = null;
	    InputStream is = new FileInputStream(file);
	    body = new byte[is.available()];
	    is.read(body);
	    HttpHeaders headers = new HttpHeaders();
	    headers.add("Content-Disposition", "attchement;filename=" + file.getName());
	    HttpStatus statusCode = HttpStatus.OK;
	    ResponseEntity<byte[]> entity = new ResponseEntity<byte[]>(body, headers, statusCode);
	    return entity;
	}*/
	@RequestMapping("/download")    
    public ResponseEntity<byte[]> export(HttpServletRequest request,String name) throws IOException {    
          
    	String path = request.getServletContext().getRealPath("/file/"+name);
        HttpHeaders headers = new HttpHeaders();      
        File file = new File(path);  
        
        System.out.println(name);
          
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);      
        headers.setContentDispositionFormData("attachment", new String(name.getBytes(),"iso8859-1"));      
         
        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),      
                                              headers, HttpStatus.CREATED);      
    }   
 
}
