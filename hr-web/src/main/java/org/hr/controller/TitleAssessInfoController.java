package org.hr.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.TitleAssessInfo;
import org.hr.service.TitleAssessInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/titleAssessInfo")
public class TitleAssessInfoController {
	
	@Resource
	private TitleAssessInfoService titleAssessInfoService;

	public void setTitleAssessInfoService(TitleAssessInfoService titleAssessInfoService) {
		this.titleAssessInfoService = titleAssessInfoService;
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "titleAssessInfo/index";
	}
	@RequestMapping("/form")
	public String userForm() throws Exception {
		return "titleAssessInfo/form";
	}
	
	@RequestMapping(value="list")
	@ResponseBody
	public Map<String, Object> list(@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,TitleAssessInfo condition) throws Exception{
		return titleAssessInfoService.listMap(page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("titleAssessInfo:add")
	public Map<String, Object> add(TitleAssessInfo titleAssessInfo) throws Exception{
		return titleAssessInfoService.add(titleAssessInfo);
	}
	@RequestMapping("edit")
	@ResponseBody
	@RequiresPermissions("titleAssessInfo:edit")
	public Map<String, Object> edit(TitleAssessInfo titleAssessInfo) throws Exception{
		return titleAssessInfoService.edit(titleAssessInfo);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("titleAssessInfo:dels")
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return titleAssessInfoService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public TitleAssessInfo view(Integer id) throws Exception{
		return titleAssessInfoService.getById(id);
	}
	
	@RequestMapping("/showform")
	public String showForm() throws Exception {
		return "titleAssessInfo/showform";
	}
}
