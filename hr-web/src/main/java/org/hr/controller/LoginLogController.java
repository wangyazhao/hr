package org.hr.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.hr.pojo.LoginLog;
import org.hr.service.LoginLogService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/log")
public class LoginLogController {
	@Resource
	private LoginLogService loginLogService;

	public void setLoginLogService(LoginLogService loginLogService) {
		this.loginLogService = loginLogService;
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "log/index";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list( @RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="desc") String order,LoginLog condition) throws Exception{
		return loginLogService.listMap(page, rows, sort, order, condition);
	}
	
	
	
}
