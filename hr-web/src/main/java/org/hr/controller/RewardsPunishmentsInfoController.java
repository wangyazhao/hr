package org.hr.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.RewardsPunishmentsInfo;
import org.hr.service.RewardsPunishmentsInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/rewardsPunishmentsInfo")
public class RewardsPunishmentsInfoController {
	@Resource
	private RewardsPunishmentsInfoService rewardsPunishmentsInfoService;

	public void setRewardsPunishmentsInfoService(RewardsPunishmentsInfoService rewardsPunishmentsInfoService) {
		this.rewardsPunishmentsInfoService = rewardsPunishmentsInfoService;
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "rewardsPunishmentsInfo/index";
	}
	@RequestMapping("/form")
	public String userForm() throws Exception {
		return "rewardsPunishmentsInfo/form";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list(@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,RewardsPunishmentsInfo condition) throws Exception{
		return rewardsPunishmentsInfoService.listMap(page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("rewardsPunishmentsInfo:add")
	public Map<String, Object> add(RewardsPunishmentsInfo rewardsPunishmentsInfo) throws Exception{
		return rewardsPunishmentsInfoService.add(rewardsPunishmentsInfo);
	}
	@RequestMapping("edit")
	@ResponseBody
	@RequiresPermissions("rewardsPunishmentsInfo:edit")
	public Map<String, Object> edit(RewardsPunishmentsInfo rewardsPunishmentsInfo) throws Exception{
		return rewardsPunishmentsInfoService.edit(rewardsPunishmentsInfo);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("rewardsPunishmentsInfo:dels")
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return rewardsPunishmentsInfoService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public RewardsPunishmentsInfo view(Integer id) throws Exception{
		return rewardsPunishmentsInfoService.getById(id);
	}
	
	@RequestMapping("/showform")
	public String showForm() throws Exception {
		return "rewardsPunishmentsInfo/showform";
	}
}
