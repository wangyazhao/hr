package org.hr.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.ContractInfo;
import org.hr.service.ContractInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/contractInfo")
public class contractInfoConeroller {
	@Resource
	private ContractInfoService contractInfoService;

	public void setContractInfoService(ContractInfoService contractInfoService) {
		this.contractInfoService = contractInfoService;
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "contractInfo/index";
	}
	@RequestMapping("/form")
	public String userForm() throws Exception {
		return "contractInfo/form";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list(@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,ContractInfo condition) throws Exception{
		return contractInfoService.listMap(page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("contractInfo:add")
	public Map<String, Object> add(ContractInfo contractInfo) throws Exception{
		return contractInfoService.add(contractInfo);
	}
	@RequestMapping("edit")
	@ResponseBody
	@RequiresPermissions("contractInfo:edit")
	public Map<String, Object> edit(ContractInfo contractInfo) throws Exception{
		return contractInfoService.edit(contractInfo);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("contractInfo:dels")
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return contractInfoService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public ContractInfo view(Integer id) throws Exception{
		return contractInfoService.getById(id);
	}
	
	@RequestMapping(value="view2",method=RequestMethod.POST)
	@ResponseBody
	public ContractInfo getbyarchivesId(Integer id) throws Exception{
		return contractInfoService.getById(id);
	}
	
	@RequestMapping("/showform")
	public String showForm() throws Exception {
		return "contractInfo/showform";
	}
}
