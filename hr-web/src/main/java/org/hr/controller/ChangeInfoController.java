package org.hr.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.ChangeInfo;
import org.hr.service.ChangeInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/changeInfo")
public class ChangeInfoController {
	@Resource
	private ChangeInfoService changeInfoService;

	public void setChangeInfoService(ChangeInfoService changeInfoService) {
		this.changeInfoService = changeInfoService;
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "contractInfo/changeInfo/index";
	}
	@RequestMapping("/form")
	public String userForm() throws Exception {
		return "contractInfo/changeInfo/form";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list( Integer byid, @RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,ChangeInfo condition) throws Exception{
		return changeInfoService.listMap(byid,page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("changeInfo:add")
	public Map<String, Object> add(ChangeInfo changeInfo) throws Exception{
		return changeInfoService.add(changeInfo);
	}
	@RequestMapping("/edit")
	@ResponseBody
	@RequiresPermissions("changeInfo:edit")
	public Map<String, Object> edit(ChangeInfo changeInfo) throws Exception{
		return changeInfoService.edit(changeInfo);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("changeInfo:dels")
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return changeInfoService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public ChangeInfo view(Integer id) throws Exception{
		return changeInfoService.getById(id);
	}
	
	@RequestMapping("/showform")
	public String showForm() throws Exception {
		return "contractInfo/changeInfo/showform";
	}
}
