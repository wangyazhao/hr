package org.hr.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.Positive;
import org.hr.service.PositiveService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/positive")
public class PositiveController {
	@Resource
	private PositiveService positiveService;

	public void setPositiveService(PositiveService positiveService) {
		this.positiveService = positiveService;
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "contractInfo/positive/index";
	}
	@RequestMapping("/form")
	public String userForm() throws Exception {
		return "contractInfo/positive/form";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list( Integer byid, @RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,Positive condition) throws Exception{
		return positiveService.listMap(byid,page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("positive:add")
	public Map<String, Object> add(Positive positive) throws Exception{
		return positiveService.add(positive);
	}
	@RequestMapping("/edit")
	@ResponseBody
	@RequiresPermissions("positive:edit")
	public Map<String, Object> edit(Positive positive) throws Exception{
		return positiveService.edit(positive);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("positive:dels")
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return positiveService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public Positive view(Integer id) throws Exception{
		return positiveService.getById(id);
	}
	
	@RequestMapping("/showform")
	public String showForm() throws Exception {
		return "contractInfo/positive/showform";
	}
}
