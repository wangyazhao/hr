package org.hr.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.Mobilize;
import org.hr.service.MobilizeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/mobilize")
public class MobilizeController {
	@Resource
	private MobilizeService mobilizeService;

	public void setMobilizeService(MobilizeService mobilizeService) {
		this.mobilizeService = mobilizeService;
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "mobilize/index";
	}
	@RequestMapping("/form")
	public String userForm() throws Exception {
		return "mobilize/form";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list(@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,Mobilize condition) throws Exception{
		return mobilizeService.listMap(page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("mobilize:add")
	public Map<String, Object> add(Mobilize mobilize) throws Exception{
		return mobilizeService.add(mobilize);
	}
	@RequestMapping("edit")
	@ResponseBody
	@RequiresPermissions("mobilize:edit")
	public Map<String, Object> edit(Mobilize mobilize) throws Exception{
		return mobilizeService.edit(mobilize);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("mobilize:dels")
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return mobilizeService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public Mobilize view(Integer id) throws Exception{
		return mobilizeService.getById(id);
	}
	
	@RequestMapping("/showform")
	public String showForm() throws Exception {
		return "mobilize/showform";
	}
}
