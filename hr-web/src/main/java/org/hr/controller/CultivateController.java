package org.hr.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.Audit;
import org.hr.pojo.Cultivate;
import org.hr.service.CultivateService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/cultivate")
public class CultivateController {
	@Resource
	private CultivateService cultivateService;

	public void setCultivateService(CultivateService cultivateService) {
		this.cultivateService = cultivateService;
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "cultivate/index";
	}
	@RequestMapping("/form")
	public String Form() throws Exception {
		return "cultivate/form";
	}
	@RequestMapping("/form2")
	public String Form2() throws Exception {
		return "cultivate/form2";
	}
	@RequestMapping("/form3")
	public String Form3() throws Exception {
		return "cultivate/form3";
	}
	@RequestMapping("/form4")
	public String Form4() throws Exception {
		return "cultivate/form4";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list(@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,Cultivate condition) throws Exception{
		return cultivateService.listMap(page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("cultivate:add")
	public Map<String, Object> add(Cultivate cultivate,Integer [] aid) throws Exception{
		return cultivateService.add(cultivate,aid);
	}
	@RequestMapping("edit")
	@ResponseBody
	@RequiresPermissions("cultivate:edit")
	public Map<String, Object> edit(Cultivate cultivate,Integer [] aid) throws Exception{
		return cultivateService.edit(cultivate,aid);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("cultivate:dels")
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return cultivateService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public Cultivate view(Integer id) throws Exception{
		return cultivateService.getById(id);
	}
	
	@RequestMapping("/showform")
	public String showForm() throws Exception {
		return "cultivate/showform";
	}
	
	@RequestMapping(value="Archivesname",method=RequestMethod.POST)
	@ResponseBody
	public List<Cultivate> Archivesname(Integer id) throws Exception{
		return cultivateService.getArchivesname(id);
	}
	@RequestMapping("/addaudit")
	@ResponseBody
	public Map<String, Object> addAudit(Audit audit) throws Exception{
		return cultivateService.addAudit(audit);
	}
	
}
