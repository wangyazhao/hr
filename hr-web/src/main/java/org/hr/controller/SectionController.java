package org.hr.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.Section;
import org.hr.service.SectionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/section")
public class SectionController {

	@Resource
	private SectionService sectionService;

	public void setSectionService(SectionService sectionService) {
		this.sectionService = sectionService;
	}
	
	
	@RequestMapping("/index")
	public String index() throws Exception {
		return "section/index";
	}
	
	@RequestMapping("/form")
	public String form() throws Exception {
		return "section/form";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public List<Section> list() throws Exception{
		return sectionService.getAll();
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public Section view(Integer id) throws Exception {
		return sectionService.getById(id);
	}
	
	@RequestMapping(value="listdata",method=RequestMethod.POST)
	@ResponseBody
	public List<Section> listData() throws Exception{
		return sectionService.listData();
	}
	
	@RequestMapping(value="add",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("section:add")
	public Map<String, Object> add(Section section) throws Exception{
		return sectionService.add(section);
	}
	
	@RequestMapping(value="delSection",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("section:del")
	public Map<String, Object> delSection(Integer id) throws Exception{
		return sectionService.del(id);
	}
	
	@RequestMapping(value="updata",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("section:updata")
	public Map<String, Object> updata(Section section) throws Exception{
		
		return sectionService.update(section);
	}
}
