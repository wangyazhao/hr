package org.hr.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.Position;
import org.hr.service.PositionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/position")
public class PositionController {

	@Resource
	private PositionService positionService;

	public void setPositionService(PositionService positionService) {
		this.positionService = positionService;
	}
	
	@RequestMapping("/index")
	public String index() throws Exception{
		return "position/index";
	}
	@RequestMapping("/add_form")
	public String add_form() throws Exception{
		return "position/add_form";
	}
	
	@RequestMapping("/edit_form")
	public String edit_form() throws Exception{
		return "position/edit_form";
	}
	
	@RequestMapping(value="/all",method=RequestMethod.POST)
	@ResponseBody
	public List<Position> all() throws Exception{ 
		return positionService.getAll();
	}
	
	@RequestMapping(value="/list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> list(Integer page,Integer rows,Position condition) throws Exception{
		return positionService.listMap(page, rows, condition);
	}
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("position:add")
	public Map<String,Object> list(Position condition) throws Exception{
		return positionService.addposition(condition);
	}
	 
	@RequestMapping(value="/view",method=RequestMethod.POST)
	@ResponseBody
	public Position list(Integer id) throws Exception{
		return positionService.view(id);
	}	
	
	@RequestMapping(value="/updata",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("position:updata")
	public Map<String,Object> edit(Position condition) throws Exception{
		return positionService.update(condition);
	}
	
	@RequestMapping(value="/del",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("position:del")
	public Map<String,Object> edit(Integer id) throws Exception{
		return positionService.delById(id);
	}	
	
	
}
