package org.hr.controller;

import java.util.List;

import javax.annotation.Resource;

import org.hr.pojo.DataDictionary;
import org.hr.service.DataDictionaryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/data")
public class DataDictionaryController {

	@Resource
	private DataDictionaryService dataDictionaryService;

	public void setDataDictionaryService(DataDictionaryService dataDictionaryService) {
		this.dataDictionaryService = dataDictionaryService;
	}
	/**
	 * 培训计划状态
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cultivatezt")
	@ResponseBody
	public List<DataDictionary> cultivatezt() throws Exception{
		return dataDictionaryService.getTypeNameByType("cultivatezt");
	}
	/**
	 * 培训类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cultivatetype")
	@ResponseBody
	public List<DataDictionary> cultivatetype() throws Exception{
		return dataDictionaryService.getTypeNameByType("cultivatetype");
	}
	/**
	 * 培训方式
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/cultivateway")
	@ResponseBody
	public List<DataDictionary> cultivateway() throws Exception{
		return dataDictionaryService.getTypeNameByType("cultivateway");
	}
	/**
	 * 调动类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/percode")
	@ResponseBody
	public List<DataDictionary> percode() throws Exception{
		return dataDictionaryService.getTypeNameByType("percode");
	}
	
	/**
	 * 调动类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/ddtype")
	@ResponseBody
	public List<DataDictionary> ddtype() throws Exception{
		return dataDictionaryService.getTypeNameByType("ddtype");
	}
	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/ByTitleAssessInfoName")
	@ResponseBody
	public List<DataDictionary> ByTitleAssessInfoName() throws Exception{
		return dataDictionaryService.getByTitleAssessInfoName();
	}
	/**
	 * 复职类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/reinstatement")
	@ResponseBody
	public List<DataDictionary> reinstatement() throws Exception{
		return dataDictionaryService.getTypeNameByType("reinstatement");
	}
	/**
	 * 续签类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/renewType")
	@ResponseBody
	public List<DataDictionary> renewType() throws Exception{
		return dataDictionaryService.getTypeNameByType("renewType");
	}
	
	/**
	 * 转正类别
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/positivetype")
	@ResponseBody
	public List<DataDictionary> positivetype() throws Exception{
		return dataDictionaryService.getTypeNameByType("positivetype");
	}
	
	/**
	 * 解除类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/jctype")
	@ResponseBody
	public List<DataDictionary> jctype() throws Exception{
		return dataDictionaryService.getTypeNameByType("jctype");
	}
	/**
	 * 离职类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/departure")
	@ResponseBody
	public List<DataDictionary> departure() throws Exception{
		return dataDictionaryService.getTypeNameByType("departure");
	}

	/**
	 * 状态
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/zhuangtai")
	@ResponseBody
	public List<DataDictionary> zhuangtai() throws Exception{
		return dataDictionaryService.getTypeNameByType("zhuangtai");
	}
	/**
	 * 证照类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/zztype")
	@ResponseBody
	public List<DataDictionary> zztype() throws Exception{
		return dataDictionaryService.getTypeNameByType("ZZtype");
	}
	
	/**
	 * 是/否
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/whether")
	@ResponseBody
	public List<DataDictionary> whether() throws Exception{
		return dataDictionaryService.getTypeNameByType("whether");
	}
	
	/**
	 * 有/无
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/without")
	@ResponseBody
	public List<DataDictionary> without() throws Exception{
		return dataDictionaryService.getTypeNameByType("without");
	}
	
	/**
	 * 贯籍
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/nativePlace")
	@ResponseBody
	public List<DataDictionary> nativePlace() throws Exception{
		return dataDictionaryService.getTypeNameByType("nativePlace");
	}
	
	/**
	 * 政治面貌
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/politicalFace")
	@ResponseBody
	public List<DataDictionary> politicalFace() throws Exception{
		return dataDictionaryService.getTypeNameByType("politicalFace");
	}
	
	/**
	 * 民族
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/national")
	@ResponseBody
	public List<DataDictionary> national() throws Exception{
		return dataDictionaryService.getTypeNameByType("national");
	}
	
	/**
	 * 人才分级
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/rcFenJi")
	@ResponseBody
	public List<DataDictionary> rcFenJi() throws Exception{
		return dataDictionaryService.getTypeNameByType("rcFenJi");
	}
	
	/**
	 * 人才分类
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/rcFenLei")
	@ResponseBody
	public List<DataDictionary> rcFenLei() throws Exception{
		return dataDictionaryService.getTypeNameByType("rcFenLei");
	}
	
	/**
	 * 员工工中：合同/临时工
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/employeesWork")
	@ResponseBody
	public List<DataDictionary> employeesWork() throws Exception{
		return dataDictionaryService.getTypeNameByType("employeesWork");
	}
	
	/**
	 * 职称
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/zhiCheng")
	@ResponseBody
	public List<DataDictionary> zhiCheng() throws Exception{
		return dataDictionaryService.getTypeNameByType("zhiCheng");
	}
	
	/**
	 * 银行
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/bank")
	@ResponseBody
	public List<DataDictionary> bank() throws Exception{
		return dataDictionaryService.getTypeNameByType("bank");
	}
	
	/**
	 * 学历
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/education")
	@ResponseBody
	public List<DataDictionary> education() throws Exception{
		return dataDictionaryService.getTypeNameByType("education");
	}
	
	/**
	 * 取得方式
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/way")
	@ResponseBody
	public List<DataDictionary> way() throws Exception{
		return dataDictionaryService.getTypeNameByType("way");
	}
	
	/**
	 * 合同状态
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/contractState")
	@ResponseBody
	public List<DataDictionary> contractState() throws Exception{
		return dataDictionaryService.getTypeNameByType("contractState");
	}
	
	/**
	 * 合同类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/contractType")
	@ResponseBody
	public List<DataDictionary> contractType() throws Exception{
		return dataDictionaryService.getTypeNameByType("contractType");
	}
	
	/**
	 * 合同变更类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/changeType")
	@ResponseBody
	public List<DataDictionary> changeType() throws Exception{
		return dataDictionaryService.getTypeNameByType("changeType");
	}
	
	/**
	 * 发证机构
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/certificate")
	@ResponseBody
	public List<DataDictionary> certificate() throws Exception{
		return dataDictionaryService.getTypeNameByType("certificate");
	}
	
	/**
	 * 婚姻状况
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/marital")
	@ResponseBody
	public List<DataDictionary> marital() throws Exception{
		return dataDictionaryService.getTypeNameByType("marital");
	}
	
	/**
	 * 性别
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/sex")
	@ResponseBody
	public List<DataDictionary> sex() throws Exception{
		return dataDictionaryService.getTypeNameByType("sex");
	}
	
	/**
	 * 部门类型
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/sectionType")
	@ResponseBody
	public List<DataDictionary> sectionType() throws Exception{
		return dataDictionaryService.getTypeNameByType("sectionType");
	}
	
	/**
	 * 部门权限：允许/不允许管理子部门
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/sectionPermissions")
	@ResponseBody
	public List<DataDictionary> sectionPermissions() throws Exception{
		return dataDictionaryService.getTypeNameByType("sectionPermissions");
	}
	
	/**
	 * 岗位
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/gangwei")
	@ResponseBody
	public List<DataDictionary> gangwei() throws Exception{
		return dataDictionaryService.getTypeNameByType("gangwei");
	}
	/**
	 * 员工状态
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/ygzt")
	@ResponseBody
	public List<DataDictionary> ygzt() throws Exception{
		return dataDictionaryService.getTypeNameByType("ygzt");
	}
	/**
	 * 员工状态
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/jc")
	@ResponseBody
	public List<DataDictionary> jc() throws Exception{
		return dataDictionaryService.getTypeNameByType("jc");
	}
}
