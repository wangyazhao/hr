package org.hr.controller;

 
 import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.Rewardspunishments;
import org.hr.service.DataDictionaryService;
import org.hr.service.RewardspunishmentsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;  

@Controller
@RequestMapping("/rewardspunishments")
public class RewardspunishmentsController {
 
	@Resource
	private RewardspunishmentsService rewardspunishmentsService;
	 
	public void setRewardspunishmentsService(RewardspunishmentsService rewardspunishmentsService) {
		this.rewardspunishmentsService = rewardspunishmentsService;
	}
	
	@Resource
	private DataDictionaryService dataDictionaryService;

	public void setDataDictionaryService(DataDictionaryService dataDictionaryService) {
		this.dataDictionaryService = dataDictionaryService;
	}
	  
	@RequestMapping("/index")
	public String index() throws Exception{
		return "Rewardspunishments/index";
	}
	
	@RequestMapping("/toadd")
	public String toadd() throws Exception{
		return "Rewardspunishments/add_form";
	}
	 
	@RequestMapping("/las")
	@ResponseBody
	public List<Rewardspunishments> laogin() throws Exception {
		return rewardspunishmentsService.list();
	}
	
	@RequestMapping("/batchDelete")
	@ResponseBody
	@RequiresPermissions("rewardspunishments:dels")
	public Map<String,Object> batchdelete(Integer[] ids) throws Exception{
		return rewardspunishmentsService.delByIds(ids);
	}
 
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("rewardspunishments:add")
	public Map<String,Object> add(Rewardspunishments rewardspunishments) throws Exception{
		return rewardspunishmentsService.add(rewardspunishments);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public Rewardspunishments view(Integer id) throws Exception {
		return rewardspunishmentsService.view(id);
	}
	
	@RequestMapping(value="edit",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("rewardspunishments:edit")
	public Map<String,Object> edit(Rewardspunishments rewardspunishments) throws Exception {
		return rewardspunishmentsService.update(rewardspunishments);
	}
}
	














