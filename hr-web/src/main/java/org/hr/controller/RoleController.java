package org.hr.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.dao.RoleDao;
import org.hr.dao.RolepermissionDao;
import org.hr.pojo.Role;
import org.hr.service.RoleService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/role")
public class RoleController {
 
	@Resource
	private RoleService roleService;
	
	@Resource
	private RolepermissionDao rolepermissionDao;
	
	@Resource
	private RoleDao roleDao;

	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}

	public void setRolepermissionDao(RolepermissionDao rolepermissionDao) {
		this.rolepermissionDao = rolepermissionDao;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	@RequestMapping("/index")
	public String index() throws Exception{
		return "role/index";
	}
	 
	@RequestMapping("/toAssign")
	public String toAssign(Integer rid,ModelMap modelMap) throws Exception {
		modelMap.put("roleId",rid);
		return "role/assign";
	}	
	 
	
	
	@RequestMapping("/add_form")
	public String form() throws Exception{
		return "role/add_form";
	}
	
	@RequestMapping("/edit_form")
	public String Toedit() throws Exception{ 
		return "role/edit_form";
	}
	
	
	//查询全部
	@RequestMapping("/all")
	@ResponseBody
	public List<Role> all() throws Exception{
		return roleService.getAll();
	}
	
 	//查询名称
	@RequestMapping("/byname")
	@ResponseBody
	public List<Role> byname() throws Exception{
		return roleDao.getRoleName();
	}
	 
	
	//按条件查询
	@RequestMapping(value="/list",method=RequestMethod.POST)
	@ResponseBody 
	public Map<String, Object> list(Integer page,Integer rows,Role condition) throws Exception{ 
		return roleService.listMap(page, rows, condition);
	}
	
	//添加角色
	@RequestMapping(value="/add",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("role:add")
	public Map<String, Object> add(Role role) throws Exception{
		return roleService.addrole(role);
	}
	
	//修改查询
	@RequestMapping(value="/view",method=RequestMethod.POST)
	@ResponseBody
	public Role view(@RequestParam("id")Integer id) throws Exception{ 
		return roleService.view(id);
	}
	
	@RequestMapping(value="/updata",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("role:updata")
	public Map<String,Object> updata(Role role) throws Exception{
		return roleService.update(role);
	}
 
	//删除角色
	@RequestMapping(value="/del",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("role:del")
	public Map<String, Object> del(Integer id) throws Exception{
		return roleService.delById(id);
	}
	 
	@RequestMapping("/getPermissions")
	@ResponseBody
	public List<Integer> selectPermission(Integer roleId) throws Exception{
		return rolepermissionDao.getPermissionIdsByRoleId(roleId); 
	}
	
	@RequestMapping("/assign")
	@ResponseBody
	@RequiresPermissions("role:assign")
	public Map<String,Object> assign(Integer roleId,Integer[] ids) throws Exception{
		Map<String, Object> map = new HashMap<>();
		rolepermissionDao.deletePermissionsByRoleId(roleId);
		rolepermissionDao.addRolePermissions(roleId, Arrays.asList(ids));
		map.put("result", true);
		return map;
	}
	 
}









