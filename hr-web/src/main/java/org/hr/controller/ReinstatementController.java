package org.hr.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.Reinstatement;
import org.hr.service.ReinstatementService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/reinstatement")
public class ReinstatementController {
	
	@Resource
	private ReinstatementService reinstatementService;

	public void setReinstatementService(ReinstatementService reinstatementService) {
		this.reinstatementService = reinstatementService;
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "reinstatement/index";
	}
	@RequestMapping("/form")
	public String userForm() throws Exception {
		return "reinstatement/form";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list(@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,Reinstatement condition) throws Exception{
		return reinstatementService.listMap(page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("reinstatement:add")
	public Map<String, Object> add(Reinstatement reinstatement) throws Exception{
		return reinstatementService.add(reinstatement);
	}
	@RequestMapping("edit")
	@ResponseBody
	@RequiresPermissions("reinstatement:edit")
	public Map<String, Object> edit(Reinstatement reinstatement) throws Exception{
		return reinstatementService.edit(reinstatement);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("reinstatement:dels")
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return reinstatementService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public Reinstatement view(Integer id) throws Exception{
		return reinstatementService.getById(id);
	}
	
	@RequestMapping("/showform")
	public String showForm() throws Exception {
		return "reinstatement/showform";
	}
	
	@RequestMapping("/contractinfoid")
	@ResponseBody
	public Integer getcontractinfoid(Integer id) throws Exception{
		return reinstatementService.getcontractinfoid(id);
	}
}
