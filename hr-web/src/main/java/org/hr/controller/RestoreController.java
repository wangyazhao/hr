package org.hr.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.Restore;
import org.hr.service.RestoreService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/restore")
public class RestoreController {
	@Resource
	private RestoreService restoreService;

	public void setRestoreService(RestoreService restoreService) {
		this.restoreService = restoreService;
	}
	@RequestMapping("/index")
	public String index() throws Exception {
		return "contractInfo/restore/index";
	}
	@RequestMapping("/form")
	public String userForm() throws Exception {
		return "contractInfo/restore/form";
	}
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list( Integer byid,@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,Restore condition) throws Exception{
		return restoreService.listMap(byid,page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("restore:add")
	public Map<String, Object> add(Restore restore) throws Exception{
		return restoreService.add(restore);
	}
	@RequestMapping("edit")
	@ResponseBody
	public Map<String, Object> edit(Restore restore) throws Exception{
		return restoreService.edit(restore);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("restore:dels")
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return restoreService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public Restore view(Integer id) throws Exception{
		return restoreService.getById(id);
	}
	
	@RequestMapping("/showform")
	public String showForm() throws Exception {
		return "contractInfo/restore/showform";
	}
}
