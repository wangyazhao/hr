package org.hr.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hr.pojo.Archives;
import org.hr.pojo.Hr;
import org.hr.service.ArchivesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/archives")
public class ArchivesController {
	
	@Resource
	private ArchivesService archivesService;
	
	public void setArchivesService(ArchivesService archivesService) {
		this.archivesService = archivesService;
	}

	//查询所有员工
	@RequestMapping(value="/all",method=RequestMethod.POST)
	@ResponseBody
	public List<Archives> all() throws Exception{
		return archivesService.getAll();
	}
	
	@RequestMapping("/index")
	public String index() throws Exception {
		return "archive/index";
	}
	
	@RequestMapping("/form")
	public String form() throws Exception {
		return "archive/form";
	}
	@RequestMapping("/toview")
	public String toview() throws Exception {
		return "archive/toview";
	}
	
	
	@RequestMapping(value="toarchives",method=RequestMethod.POST)
	@ResponseBody
	public Archives toArchives(Integer id) throws Exception{
		return archivesService.getByID(id);
	}
	
	
	@RequestMapping(value="list",method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> list(@RequestParam(defaultValue="1")Integer page, @RequestParam(defaultValue="10")Integer rows,@RequestParam(defaultValue="id") String sort,@RequestParam(defaultValue="asc") String order,Archives condition) throws Exception{
		return archivesService.listMap(page, rows, sort, order, condition);
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	@RequiresPermissions("archive:add")
	public Map<String, Object> add(Archives archives) throws Exception{
		return archivesService.add(archives);
	}
	
	@RequestMapping(value="edit",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("archive:edit")
	public Map<String, Object> edit(Archives archives) throws Exception{
		return archivesService.edit(archives);
	}
	@RequestMapping(value="dels",method=RequestMethod.POST)
	@ResponseBody
	@RequiresPermissions("archive:dels")
	public Map<String, Object> batchDelete(Integer [] ids) throws Exception{
		return archivesService.batchDelete(ids);
	}
	
	@RequestMapping(value="view",method=RequestMethod.POST)
	@ResponseBody
	public Archives view(Integer id) throws Exception{
		return archivesService.getByID(id);
	}
	
	@RequestMapping(value="bysex",method=RequestMethod.POST)
	@ResponseBody
	public List<Hr> groupBysex(Integer sid) throws Exception{
		return archivesService.groupBysex(sid,"sex");
	}
	//婚姻
	@RequestMapping(value="bymarital",method=RequestMethod.POST)
	@ResponseBody
	public List<Hr> groupBymarital(Integer sid) throws Exception{
		return archivesService.groupBysex(sid,"marital");
	}//学历
	@RequestMapping(value="byeducation",method=RequestMethod.POST)
	@ResponseBody
	public List<Hr> groupByeducation(Integer sid) throws Exception{
		return archivesService.groupBysex(sid,"education");
	}
	//人才分类
	@RequestMapping(value="bytalentType",method=RequestMethod.POST)
	@ResponseBody
	public List<Hr> groupBytalentType(Integer sid) throws Exception{
		return archivesService.groupBysex(sid,"talentType");
	}
	//人才等级
	@RequestMapping(value="byTalentLevel",method=RequestMethod.POST)
	@ResponseBody
	public List<Hr> groupByTalentLevel(Integer sid) throws Exception{
		return archivesService.groupBysex(sid,"TalentLevel");
	}
	//工种分析
	@RequestMapping(value="byemployeesWork",method=RequestMethod.POST)
	@ResponseBody
	public List<Hr> groupByemployeesWork(Integer sid) throws Exception{
		return archivesService.groupBysex(sid,"employeesWork");
	}
	//员工状态
	@RequestMapping(value="bystatus",method=RequestMethod.POST)
	@ResponseBody
	public List<Hr> groupBystatus(Integer sid) throws Exception{
		return archivesService.groupBysex(sid,"status");
	}
	//职称分析
	@RequestMapping(value="byjobTitle",method=RequestMethod.POST)
	@ResponseBody
	public List<Hr> groupByjobTitle(Integer sid) throws Exception{
		return archivesService.groupBysex(sid,"jobTitle");
	}
	//职务分析
	@RequestMapping(value="byposition",method=RequestMethod.POST)
	@ResponseBody
	public List<Hr> groupByposition(Integer sid) throws Exception{
		return archivesService.groupByposition(sid);
	}
	
	
	
}
