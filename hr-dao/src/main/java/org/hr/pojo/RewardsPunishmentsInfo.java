package org.hr.pojo;

public class RewardsPunishmentsInfo {
    private Integer id;

    private String time;

    private Rewardspunishments jcxiangmu;

    private String attribute;

    private String text;

    private String remark;
    
    private Archives archives;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Rewardspunishments getJcxiangmu() {
		return jcxiangmu;
	}

	public void setJcxiangmu(Rewardspunishments jcxiangmu) {
		this.jcxiangmu = jcxiangmu;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Archives getArchives() {
		return archives;
	}

	public void setArchives(Archives archives) {
		this.archives = archives;
	}
    
    

}