package org.hr.pojo;

import java.sql.Date;

public class Archives {
    private Integer id;

    private String worknum;

    private DataDictionary sex;

    private String name;

    private Date birthday;

    private DataDictionary marital;

    private DataDictionary politicalface;

    private String health;

    private String idcard;

    private String salt;

    private DataDictionary nativeplace;

    private DataDictionary national;

    private Section section;
    
    private Date joinworktime;

    private String workyear;

    private DataDictionary education;

    private String finishschool;

    private String major;

    private Date graduatetime;

    private String courseslanguage;

    private String languagedcale;

    private String masterdegree;

    private String computerdcale;

    private String certificate;

    private String educationresume;

    private String specialty;

    private String hobbys;

    private String address;

    private String tel;

    private String height;

    private String weight;

    private String bloodtype;

    private String workrequire;

    private String zipcode;

    private String email;

    private DataDictionary talenttype;

    private DataDictionary talentlevel;
    
    private String socialrelations;

    private String workexperience;

    private String learningexperience;

    private String accessory;

    private String remark;
    
    private Date entrytime;

    private DataDictionary employeeswork;

    private Position position;

    private DataDictionary jobtitle;

    private String basicsalary;

    private DataDictionary status;

    private DataDictionary bank;

    private String account;

    private String photo;

    private DataDictionary jobs;
    
    private Integer data;

	public Integer getData() {
		return data;
	}

	public void setData(Integer data) {
		this.data = data;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getWorknum() {
		return worknum;
	}

	public void setWorknum(String worknum) {
		this.worknum = worknum;
	}

	public DataDictionary getSex() {
		return sex;
	}

	public void setSex(DataDictionary sex) {
		this.sex = sex;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public DataDictionary getMarital() {
		return marital;
	}

	public void setMarital(DataDictionary marital) {
		this.marital = marital;
	}

	public DataDictionary getPoliticalface() {
		return politicalface;
	}

	public void setPoliticalface(DataDictionary politicalface) {
		this.politicalface = politicalface;
	}

	public String getHealth() {
		return health;
	}

	public void setHealth(String health) {
		this.health = health;
	}

	public String getIdcard() {
		return idcard;
	}

	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public DataDictionary getNativeplace() {
		return nativeplace;
	}

	public void setNativeplace(DataDictionary nativeplace) {
		this.nativeplace = nativeplace;
	}

	public DataDictionary getNational() {
		return national;
	}

	public void setNational(DataDictionary national) {
		this.national = national;
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public Date getJoinworktime() {
		return joinworktime;
	}

	public void setJoinworktime(Date joinworktime) {
		this.joinworktime = joinworktime;
	}

	public String getWorkyear() {
		return workyear;
	}

	public void setWorkyear(String workyear) {
		this.workyear = workyear;
	}

	public DataDictionary getEducation() {
		return education;
	}

	public void setEducation(DataDictionary education) {
		this.education = education;
	}

	public String getFinishschool() {
		return finishschool;
	}

	public void setFinishschool(String finishschool) {
		this.finishschool = finishschool;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public Date getGraduatetime() {
		return graduatetime;
	}

	public void setGraduatetime(Date graduatetime) {
		this.graduatetime = graduatetime;
	}

	public String getCourseslanguage() {
		return courseslanguage;
	}

	public void setCourseslanguage(String courseslanguage) {
		this.courseslanguage = courseslanguage;
	}

	public String getLanguagedcale() {
		return languagedcale;
	}

	public void setLanguagedcale(String languagedcale) {
		this.languagedcale = languagedcale;
	}

	public String getMasterdegree() {
		return masterdegree;
	}

	public void setMasterdegree(String masterdegree) {
		this.masterdegree = masterdegree;
	}

	public String getComputerdcale() {
		return computerdcale;
	}

	public void setComputerdcale(String computerdcale) {
		this.computerdcale = computerdcale;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public String getEducationresume() {
		return educationresume;
	}

	public void setEducationresume(String educationresume) {
		this.educationresume = educationresume;
	}

	public String getSpecialty() {
		return specialty;
	}

	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	public String getHobbys() {
		return hobbys;
	}

	public void setHobbys(String hobbys) {
		this.hobbys = hobbys;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getBloodtype() {
		return bloodtype;
	}

	public void setBloodtype(String bloodtype) {
		this.bloodtype = bloodtype;
	}

	public String getWorkrequire() {
		return workrequire;
	}

	public void setWorkrequire(String workrequire) {
		this.workrequire = workrequire;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public DataDictionary getTalenttype() {
		return talenttype;
	}

	public void setTalenttype(DataDictionary talenttype) {
		this.talenttype = talenttype;
	}

	public DataDictionary getTalentlevel() {
		return talentlevel;
	}

	public void setTalentlevel(DataDictionary talentlevel) {
		this.talentlevel = talentlevel;
	}

	public String getSocialrelations() {
		return socialrelations;
	}

	public void setSocialrelations(String socialrelations) {
		this.socialrelations = socialrelations;
	}

	public String getWorkexperience() {
		return workexperience;
	}

	public void setWorkexperience(String workexperience) {
		this.workexperience = workexperience;
	}

	public String getLearningexperience() {
		return learningexperience;
	}

	public void setLearningexperience(String learningexperience) {
		this.learningexperience = learningexperience;
	}

	public String getAccessory() {
		return accessory;
	}

	public void setAccessory(String accessory) {
		this.accessory = accessory;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getEntrytime() {
		return entrytime;
	}

	public void setEntrytime(Date entrytime) {
		this.entrytime = entrytime;
	}

	public DataDictionary getEmployeeswork() {
		return employeeswork;
	}

	public void setEmployeeswork(DataDictionary employeeswork) {
		this.employeeswork = employeeswork;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public DataDictionary getJobtitle() {
		return jobtitle;
	}

	public void setJobtitle(DataDictionary jobtitle) {
		this.jobtitle = jobtitle;
	}

	public String getBasicsalary() {
		return basicsalary;
	}

	public void setBasicsalary(String basicsalary) {
		this.basicsalary = basicsalary;
	}

	public DataDictionary getStatus() {
		return status;
	}

	public void setStatus(DataDictionary status) {
		this.status = status;
	}

	public DataDictionary getBank() {
		return bank;
	}

	public void setBank(DataDictionary bank) {
		this.bank = bank;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public DataDictionary getJobs() {
		return jobs;
	}

	public void setJobs(DataDictionary jobs) {
		this.jobs = jobs;
	}
    
    
}