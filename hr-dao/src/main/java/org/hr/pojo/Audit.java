package org.hr.pojo;

import java.sql.Time;

public class Audit {
	
	private Integer id;
	  
	private DataDictionary whether;
	  
	private String opinion;
	  
	private Time time;
	  
	private Cultivate cultivate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public DataDictionary getWhether() {
		return whether;
	}

	public void setWhether(DataDictionary whether) {
		this.whether = whether;
	}

	public String getOpinion() {
		return opinion;
	}

	public void setOpinion(String opinion) {
		this.opinion = opinion;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public Cultivate getCultivate() {
		return cultivate;
	}

	public void setCultivate(Cultivate cultivate) {
		this.cultivate = cultivate;
	}
	  
}
