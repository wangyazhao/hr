package org.hr.pojo;

public class role_relationship {

	private Integer id;
	
	private Integer role_id;
	
	private Integer role_permission_id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRole_id() {
		return role_id;
	}

	public void setRole_id(Integer role_id) {
		this.role_id = role_id;
	}

	public Integer getRole_permission_id() {
		return role_permission_id;
	}

	public void setRole_permission_id(Integer role_permission_id) {
		this.role_permission_id = role_permission_id;
	}
	
}
