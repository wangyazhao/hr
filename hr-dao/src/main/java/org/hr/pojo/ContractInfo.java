package org.hr.pojo;

import java.sql.Date;

public class ContractInfo {
    private Integer id;

    private String contractno;

    private String name;

    private DataDictionary type;

    private DataDictionary deadline;

    private DataDictionary positive;

    private Date signing;

    private String contractyear;

    private DataDictionary contractstate;
    
    private String accessory;

    private String remark;

    private String textc;
    
    private String trialperiod;

    private String trialwage;

    private Date trialoperant;

    private Date trialexpire;
    
    private String contracperiod;

    private String positivewage;

    private Date positiveoperant;

    private Date positiveexpire;
    
    private Archives archives;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContractno() {
		return contractno;
	}

	public void setContractno(String contractno) {
		this.contractno = contractno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DataDictionary getType() {
		return type;
	}

	public void setType(DataDictionary type) {
		this.type = type;
	}

	public DataDictionary getDeadline() {
		return deadline;
	}

	public void setDeadline(DataDictionary deadline) {
		this.deadline = deadline;
	}

	public DataDictionary getPositive() {
		return positive;
	}

	public void setPositive(DataDictionary positive) {
		this.positive = positive;
	}

	public Date getSigning() {
		return signing;
	}

	public void setSigning(Date signing) {
		this.signing = signing;
	}

	public String getContractyear() {
		return contractyear;
	}

	public void setContractyear(String contractyear) {
		this.contractyear = contractyear;
	}

	public DataDictionary getContractstate() {
		return contractstate;
	}

	public void setContractstate(DataDictionary contractstate) {
		this.contractstate = contractstate;
	}

	public String getAccessory() {
		return accessory;
	}

	public void setAccessory(String accessory) {
		this.accessory = accessory;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTextc() {
		return textc;
	}

	public void setTextc(String textc) {
		this.textc = textc;
	}

	public String getTrialperiod() {
		return trialperiod;
	}

	public void setTrialperiod(String trialperiod) {
		this.trialperiod = trialperiod;
	}

	public String getTrialwage() {
		return trialwage;
	}

	public void setTrialwage(String trialwage) {
		this.trialwage = trialwage;
	}

	public Date getTrialoperant() {
		return trialoperant;
	}

	public void setTrialoperant(Date trialoperant) {
		this.trialoperant = trialoperant;
	}

	public Date getTrialexpire() {
		return trialexpire;
	}

	public void setTrialexpire(Date trialexpire) {
		this.trialexpire = trialexpire;
	}

	public String getContracperiod() {
		return contracperiod;
	}

	public void setContracperiod(String contracperiod) {
		this.contracperiod = contracperiod;
	}

	public String getPositivewage() {
		return positivewage;
	}

	public void setPositivewage(String positivewage) {
		this.positivewage = positivewage;
	}

	public Date getPositiveoperant() {
		return positiveoperant;
	}

	public void setPositiveoperant(Date positiveoperant) {
		this.positiveoperant = positiveoperant;
	}

	public Date getPositiveexpire() {
		return positiveexpire;
	}

	public void setPositiveexpire(Date positiveexpire) {
		this.positiveexpire = positiveexpire;
	}

	public Archives getArchives() {
		return archives;
	}

	public void setArchives(Archives archives) {
		this.archives = archives;
	}
}