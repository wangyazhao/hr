package org.hr.pojo;

public class TitleAssessInfo {
    private Integer id;

    private DataDictionary name;

    private DataDictionary way;

    private String time;

    private DataDictionary nextname;

    private String nexttime;

    private String remark;

    private String accessory;
    
    private Archives archives;
    
    public Archives getArchives() {
		return archives;
	}

	public void setArchives(Archives archives) {
		this.archives = archives;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public DataDictionary getName() {
		return name;
	}

	public void setName(DataDictionary name) {
		this.name = name;
	}

	public DataDictionary getNextname() {
		return nextname;
	}

	public void setNextname(DataDictionary nextname) {
		this.nextname = nextname;
	}

	public DataDictionary getWay() {
        return way;
    }

    public void setWay(DataDictionary way) {
        this.way = way;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public String getNexttime() {
        return nexttime;
    }

    public void setNexttime(String nexttime) {
        this.nexttime = nexttime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAccessory() {
        return accessory;
    }

    public void setAccessory(String accessory) {
        this.accessory = accessory;
    }
}