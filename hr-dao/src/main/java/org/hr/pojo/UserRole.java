package org.hr.pojo;

import java.util.List;

public class UserRole {
    private Integer id;

    private Integer userId;

    private Integer roleId;

    private List<UserRole> children;
    
    public List<UserRole> getChildren() {
		return children;
	}

	public void setChildren(List<UserRole> children) {
		this.children = children;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}