package org.hr.pojo;

public class ChangeInfo {
    private Integer id;

    private String time;

    private DataDictionary type;

    private ContractInfo contractInfo;

    private String updatetext;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public DataDictionary getType() {
        return type;
    }

    public void setType(DataDictionary type) {
        this.type = type;
    }

    public ContractInfo getContractInfo() {
		return contractInfo;
	}

	public void setContractInfo(ContractInfo contractInfo) {
		this.contractInfo = contractInfo;
	}

	public String getUpdatetext() {
        return updatetext;
    }

    public void setUpdatetext(String updatetext) {
        this.updatetext = updatetext;
    }
}