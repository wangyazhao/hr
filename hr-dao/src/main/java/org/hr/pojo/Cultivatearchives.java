package org.hr.pojo;

public class Cultivatearchives {
    private Integer id;

    private Integer cultivate;

    private Integer archives;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCultivate() {
        return cultivate;
    }

    public void setCultivate(Integer cultivate) {
        this.cultivate = cultivate;
    }

    public Integer getArchives() {
        return archives;
    }

    public void setArchives(Integer archives) {
        this.archives = archives;
    }
}