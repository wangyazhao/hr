package org.hr.pojo;

public class DataDictionary {
    private Integer id;

    private String typecode;

    private String typename;

    private Integer valueid;

    private String valuename;
    
    private Integer jctype;
    

    public Integer getJctype() {
		return jctype;
	}

	public void setJctype(Integer jctype) {
		this.jctype = jctype;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypecode() {
        return typecode;
    }

    public void setTypecode(String typecode) {
        this.typecode = typecode;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public Integer getValueid() {
        return valueid;
    }

    public void setValueid(Integer valueid) {
        this.valueid = valueid;
    }

    public String getValuename() {
        return valuename;
    }

    public void setValuename(String valuename) {
        this.valuename = valuename;
    }
}