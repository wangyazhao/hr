package org.hr.pojo;

import java.sql.Date;

public class RemoveInfo {
    private Integer id;

    private Date time;

    private DataDictionary type;

    private ContractInfo contractInfo;

    private String text;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public DataDictionary getType() {
        return type;
    }

    public void setType(DataDictionary type) {
        this.type = type;
    }

    public ContractInfo getContractInfo() {
		return contractInfo;
	}

	public void setContractInfo(ContractInfo contractInfo) {
		this.contractInfo = contractInfo;
	}

	public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}