package org.hr.pojo;

import java.util.List;

public class Section {
    private Integer id;

    private String text;

    private Integer parentids;

    private Section parent;
    
    private Integer childids;

    private String principal;

    private String tel;

    private String fax;

    private DataDictionary type;

    private DataDictionary permissions;

    private String remark;

    private Integer num;

    private Integer post;

    private String url;

    private String percode;

    private String sortstring;

    private Integer level;
    
    private String code;
    
    private List<Section> children;
    
    
    public Section getParent() {
		return parent;
	}

	public void setParent(Section parent) {
		this.parent = parent;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getParentids() {
		return parentids;
	}

	public void setParentids(Integer parentids) {
		this.parentids = parentids;
	}

	public Integer getChildids() {
		return childids;
	}

	public void setChildids(Integer childids) {
		this.childids = childids;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public DataDictionary getType() {
		return type;
	}

	public void setType(DataDictionary type) {
		this.type = type;
	}

	public DataDictionary getPermissions() {
		return permissions;
	}

	public void setPermissions(DataDictionary permissions) {
		this.permissions = permissions;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Integer getPost() {
		return post;
	}

	public void setPost(Integer post) {
		this.post = post;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPercode() {
		return percode;
	}

	public void setPercode(String percode) {
		this.percode = percode;
	}

	public String getSortstring() {
		return sortstring;
	}

	public void setSortstring(String sortstring) {
		this.sortstring = sortstring;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public List<Section> getChildren() {
		return children;
	}

	public void setChildren(List<Section> children) {
		this.children = children;
	}

   
}