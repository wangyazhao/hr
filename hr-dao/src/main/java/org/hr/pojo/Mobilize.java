package org.hr.pojo;

import java.sql.Date;

public class Mobilize {
    private Integer id;

    private String title;

    private Integer sectionameold;

    private Integer sectionamenew;

    private Integer educationold;

    private Integer educationnew;

    private Integer zhichengold;

    private Integer zhichengnew;

    private Integer employeesworkold;
    
    private Integer employeesworknew;

    private Integer jobsold;

    private Integer jobsnew;

    private Integer positionold;

    private Integer positionnew;

    private String qianold;

    private String qiannew;

    private DataDictionary type;

    private Date time;

    private String remark;
    
    private Archives archives;
    
    public Archives getArchives() {
		return archives;
	}

	public void setArchives(Archives archives) {
		this.archives = archives;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getSectionameold() {
        return sectionameold;
    }

    public void setSectionameold(Integer sectionameold) {
        this.sectionameold = sectionameold;
    }

    public Integer getSectionamenew() {
        return sectionamenew;
    }

    public void setSectionamenew(Integer sectionamenew) {
        this.sectionamenew = sectionamenew;
    }

    public Integer getEducationold() {
        return educationold;
    }

    public void setEducationold(Integer educationold) {
        this.educationold = educationold;
    }

    public Integer getEducationnew() {
        return educationnew;
    }

    public void setEducationnew(Integer educationnew) {
        this.educationnew = educationnew;
    }

    public Integer getZhichengold() {
        return zhichengold;
    }

    public void setZhichengold(Integer zhichengold) {
        this.zhichengold = zhichengold;
    }

    public Integer getZhichengnew() {
        return zhichengnew;
    }

    public void setZhichengnew(Integer zhichengnew) {
        this.zhichengnew = zhichengnew;
    }

    public Integer getEmployeesworkold() {
        return employeesworkold;
    }

    public void setEmployeesworkold(Integer employeesworkold) {
        this.employeesworkold = employeesworkold;
    }

    public Integer getEmployeesworknew() {
        return employeesworknew;
    }

    public void setEmployeesworknew(Integer employeesworknew) {
        this.employeesworknew = employeesworknew;
    }

    public Integer getJobsold() {
        return jobsold;
    }

    public void setJobsold(Integer jobsold) {
        this.jobsold = jobsold;
    }

    public Integer getJobsnew() {
        return jobsnew;
    }

    public void setJobsnew(Integer jobsnew) {
        this.jobsnew = jobsnew;
    }

    public Integer getPositionold() {
        return positionold;
    }

    public void setPositionold(Integer positionold) {
        this.positionold = positionold;
    }

    public Integer getPositionnew() {
        return positionnew;
    }

    public void setPositionnew(Integer positionnew) {
        this.positionnew = positionnew;
    }

    public String getQianold() {
        return qianold;
    }

    public void setQianold(String qianold) {
        this.qianold = qianold;
    }

    public String getQiannew() {
        return qiannew;
    }

    public void setQiannew(String qiannew) {
        this.qiannew = qiannew;
    }

    public DataDictionary getType() {
        return type;
    }

    public void setType(DataDictionary type) {
        this.type = type;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}