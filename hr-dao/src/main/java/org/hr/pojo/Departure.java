package org.hr.pojo;

import java.sql.Date;

public class Departure {
    private Integer id;

    private Date time;

    private Date moneytime;

    private String gowhere;

    private String remark;

    private Archives archives;

    private DataDictionary type;
    
    private DataDictionary whether;
    
    private Integer jctype;
    
    
    public Integer getJctype() {
		return jctype;
	}

	public void setJctype(Integer jctype) {
		this.jctype = jctype;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Date getMoneytime() {
        return moneytime;
    }

    public void setMoneytime(Date moneytime) {
        this.moneytime = moneytime;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Archives getArchives() {
        return archives;
    }

    public void setArchives(Archives archives) {
        this.archives = archives;
    }

	public DataDictionary getType() {
		return type;
	}

	public void setType(DataDictionary type) {
		this.type = type;
	}

	public DataDictionary getWhether() {
		return whether;
	}

	public void setWhether(DataDictionary whether) {
		this.whether = whether;
	}

	public String getGowhere() {
		return gowhere;
	}

	public void setGowhere(String gowhere) {
		this.gowhere = gowhere;
	}
    
}