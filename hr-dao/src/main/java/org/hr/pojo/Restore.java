package org.hr.pojo;

import java.sql.Date;

public class Restore {
    private Integer id;

    private Date time;

    private DataDictionary type;

    private String restoreremark;

    private ContractInfo contractInfo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }


    public String getRestoreremark() {
        return restoreremark;
    }

    public void setRestoreremark(String restoreremark) {
        this.restoreremark = restoreremark;
    }

	public DataDictionary getType() {
		return type;
	}

	public void setType(DataDictionary type) {
		this.type = type;
	}

	public ContractInfo getContractInfo() {
		return contractInfo;
	}

	public void setContractInfo(ContractInfo contractInfo) {
		this.contractInfo = contractInfo;
	}

}