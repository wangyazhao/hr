package org.hr.pojo;

import java.sql.Date;

public class Reinstatement {
    private Integer id;

    private Date time;

    private DataDictionary type;

    private String remark;

    private DataDictionary whether;

    private Archives archives;
    
    private Integer hftype;
    
    public Integer getHftype() {
		return hftype;
	}

	public void setHftype(Integer hftype) {
		this.hftype = hftype;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

	public DataDictionary getType() {
		return type;
	}

	public void setType(DataDictionary type) {
		this.type = type;
	}

	public DataDictionary getWhether() {
		return whether;
	}

	public void setWhether(DataDictionary whether) {
		this.whether = whether;
	}

	public Archives getArchives() {
		return archives;
	}

	public void setArchives(Archives archives) {
		this.archives = archives;
	}

}