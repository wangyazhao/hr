package org.hr.pojo;

import java.sql.Date;

public class Positive {

	private Integer id;
	
	private Date time;
	
	private DataDictionary type;
	
	private String positiveremark;
	
	private ContractInfo contractInfo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public DataDictionary getType() {
		return type;
	}

	public void setType(DataDictionary type) {
		this.type = type;
	}

	public String getPositiveremark() {
		return positiveremark;
	}

	public void setPositiveremark(String positiveremark) {
		this.positiveremark = positiveremark;
	}

	public ContractInfo getContractInfo() {
		return contractInfo;
	}

	public void setContractInfo(ContractInfo contractInfo) {
		this.contractInfo = contractInfo;
	}
	
	
}
