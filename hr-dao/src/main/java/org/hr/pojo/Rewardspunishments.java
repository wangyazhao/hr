package org.hr.pojo;

public class Rewardspunishments {
    private Integer id;

    private String name;

    private DataDictionary attribute;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public DataDictionary getAttribute() {
		return attribute;
	}

	public void setAttribute(DataDictionary attribute) {
		this.attribute = attribute;
	}

    
}