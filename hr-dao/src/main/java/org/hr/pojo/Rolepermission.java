package org.hr.pojo;

import java.util.List;

public class Rolepermission {

	private Integer id;
	 
	private String text;
	
	private String type;
	
	private String url;
	
	private String percode;
	
	private Integer parentid;
	
	private String parentids;
	
	private String sortstring;
	
	private Integer available;
	
    private Integer level;
    
    private List<Rolepermission> children;
	
	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
  
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPercode() {
		return percode;
	}

	public void setPercode(String percode) {
		this.percode = percode;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public String getParentids() {
		return parentids;
	}

	public void setParentids(String parentids) {
		this.parentids = parentids;
	}

	public String getSortstring() {
		return sortstring;
	}

	public void setSortstring(String sortstring) {
		this.sortstring = sortstring;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}

	public List<Rolepermission> getChildren() {
		return children;
	}

	public void setChildren(List<Rolepermission> children) {
		this.children = children;
	}
	
	
	
	
}
