package org.hr.pojo;

import java.sql.Date;

public class RenewInfo {
    private Integer id;

    private Date time;

    private DataDictionary type;

    private Date operanttime;

    private Date expiretime;

    private ContractInfo contractInfo;
    
    private String renewinforemark;
    
    private String lasttime;
    
    private String nexttime;

    public String getLasttime() {
		return lasttime;
	}

	public void setLasttime(String lasttime) {
		this.lasttime = lasttime;
	}

	public String getNexttime() {
		return nexttime;
	}

	public void setNexttime(String nexttime) {
		this.nexttime = nexttime;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public DataDictionary getType() {
        return type;
    }

    public void setType(DataDictionary type) {
        this.type = type;
    }


	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Date getOperanttime() {
		return operanttime;
	}

	public void setOperanttime(Date operanttime) {
		this.operanttime = operanttime;
	}

	public Date getExpiretime() {
		return expiretime;
	}

	public void setExpiretime(Date expiretime) {
		this.expiretime = expiretime;
	}

	public String getRenewinforemark() {
		return renewinforemark;
	}

	public void setRenewinforemark(String renewinforemark) {
		this.renewinforemark = renewinforemark;
	}

	public ContractInfo getContractInfo() {
		return contractInfo;
	}

	public ContractInfo getContractinfo() {
		return contractInfo;
	}

	public void setContractInfo(ContractInfo contractInfo) {
		this.contractInfo = contractInfo;
	}

}