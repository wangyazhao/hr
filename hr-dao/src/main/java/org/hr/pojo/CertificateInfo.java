package org.hr.pojo;

public class CertificateInfo {
    private Integer id;

    private DataDictionary type;

    private String number;

    private String name;

    private DataDictionary certificate;

    private String time;

    private String operant;

    private DataDictionary periodstate;

    private String expire;

    private DataDictionary state;

    private String remark;

    private String accessory;
    
    private Archives archives;

    public Archives getArchives() {
		return archives;
	}

	public void setArchives(Archives archives) {
		this.archives = archives;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DataDictionary getType() {
        return type;
    }

    public void setType(DataDictionary type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DataDictionary getCertificate() {
        return certificate;
    }

    public void setCertificate(DataDictionary certificate) {
        this.certificate = certificate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getOperant() {
        return operant;
    }

    public void setOperant(String operant) {
        this.operant = operant;
    }

    public DataDictionary getPeriodstate() {
        return periodstate;
    }

    public void setPeriodstate(DataDictionary periodstate) {
        this.periodstate = periodstate;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public DataDictionary getState() {
        return state;
    }

    public void setState(DataDictionary state) {
        this.state = state;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAccessory() {
        return accessory;
    }

    public void setAccessory(String accessory) {
        this.accessory = accessory;
    }
}