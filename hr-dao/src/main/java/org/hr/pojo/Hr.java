package org.hr.pojo;

public class Hr {

	private int id;
	
	private Integer num;
	
	private DataDictionary name;
	
	private Position position;
	
	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public DataDictionary getName() {
		return name;
	}

	public void setName(DataDictionary name) {
		this.name = name;
	}
	
	
}
