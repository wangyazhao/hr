package org.hr.pojo;

import java.sql.Date;
import java.util.List;

public class Cultivate {
    private Integer id;

    private String name;

    private Date time;
    
    private Date outtime;

    private Section section;

    private Integer num;

    private String budget;

    private String place;

    private String tel;

    private Archives approver;

    private String cultivatremark;

    private String attachment;

    private String info;
    
    private String no;
    
    private String head;
    
    private DataDictionary type;
    
    private DataDictionary way;
    
    private DataDictionary statu;
    
    private List<Archives> archives;
    
    private List<Audit> audit;
    
    
    public List<Audit> getAudit() {
		return audit;
	}

	public void setAudit(List<Audit> audit) {
		this.audit = audit;
	}

	public DataDictionary getType() {
		return type;
	}

	public void setType(DataDictionary type) {
		this.type = type;
	}

	public DataDictionary getWay() {
		return way;
	}

	public void setWay(DataDictionary way) {
		this.way = way;
	}

	public List<Archives> getArchives() {
		return archives;
	}

	public void setArchives(List<Archives> archives) {
		this.archives = archives;
	}

	public String getHead() {
		return head;
	}


	public void setHead(String head) {
		this.head = head;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }


    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCultivatremark() {
        return cultivatremark;
    }

    public void setCultivatremark(String cultivatremark) {
        this.cultivatremark = cultivatremark;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public Archives getApprover() {
		return approver;
	}

	public void setApprover(Archives approver) {
		this.approver = approver;
	}

	public DataDictionary getStatu() {
		return statu;
	}

	public void setStatu(DataDictionary statu) {
		this.statu = statu;
	}

	public Date getOuttime() {
		return outtime;
	}

	public void setOuttime(Date outtime) {
		this.outtime = outtime;
	}
    
}