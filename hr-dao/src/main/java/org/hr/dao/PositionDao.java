package org.hr.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.hr.pojo.Position;

public interface PositionDao extends CommonDao<Position, Integer>{

	/**
	 * 按照分页查询
	 */
	public List<Position> getByname(@Param("start") int start,@Param("limit")int limit,@Param("condition")Position condition);
	
}
