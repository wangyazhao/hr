package org.hr.dao;

import java.util.List;

import org.hr.pojo.Audit;

public interface AuditDao extends CommonDao<Audit, Integer>{

	public List<Audit> getByCultivateId(Integer id);
}
