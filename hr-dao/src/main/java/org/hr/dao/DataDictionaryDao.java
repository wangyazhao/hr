package org.hr.dao;

import java.util.List;

import org.hr.pojo.DataDictionary;

public interface DataDictionaryDao extends CommonDao<DataDictionary, Integer>{

	public List<DataDictionary> getTypeNameByType(String typeCode);
	
	public List<DataDictionary> getByTitleAssessinfoName();
	 
}
