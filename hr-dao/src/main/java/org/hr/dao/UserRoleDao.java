package org.hr.dao;
 
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.hr.pojo.UserRole;

public interface UserRoleDao extends CommonDao<UserRole, Integer>{
  
	public List<Integer> getPermissionIdsByUserId(Integer userId);
	
	public void deletePermissionsByUserId(Integer userId);
	
	public void addUserRoles(@Param("userId") Integer roleId,@Param("roleId") List<Integer> roleIds);
	
}
