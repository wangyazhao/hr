package org.hr.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.hr.pojo.Role;
import org.hr.pojo.User;

public interface UserDao extends CommonDao<User, Integer>{
	
	public String checkUser(@Param("username")String name);
	
	public User login(@Param("username")String username,@Param("password")String password);
	
	public User getByUsername(String username);
	
	public List<User> getListByAdmin(@Param("start") int start,@Param("limit") int limit,@Param("condition") User condition,@Param("column") String column,@Param("orderBy") String orderBy);
	
	public Integer getCountByAdmin(@Param("condition") User condition);
	
	public List<Role> getRols(Integer id);
	 
	public List<User> getreviewer(Integer id);
}
