package org.hr.dao;

import org.hr.pojo.ContractInfo;

public interface ContractInfoDao extends CommonDao<ContractInfo, Integer>{

 public ContractInfo getbyarchivesId(Integer id);
}
