package org.hr.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.hr.pojo.Archives;
import org.hr.pojo.Cultivate;

public interface CultivateDao extends CommonDao<Cultivate, Integer>{

	public List<Cultivate> getbuarchivesname(Integer id); 
	
	public void addCultivateArchives(@Param("cid") Integer cid,@Param("aid") Integer[] aid);
	
	public List<Archives> getCultivateArchives(Integer id);
	
	public void delCultivateArchives(Integer id);
}
