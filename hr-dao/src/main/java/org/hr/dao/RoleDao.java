package org.hr.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.hr.pojo.Role;

public interface RoleDao extends CommonDao<Role, Integer>{

	/**
	 * 按条件分页查询
	 */
	public List<Role> getListByName(@Param("start") int start,@Param("condition")Role Condition,@Param("limit") int limit);
					 
	public List<Role> getRoleName();
	
	
}
