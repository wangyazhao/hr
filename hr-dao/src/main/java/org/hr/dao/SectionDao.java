package org.hr.dao;

import java.util.List;

import org.hr.pojo.Section;

public interface SectionDao extends CommonDao<Section, Integer>{

	public List<Section> getSectionByParentId(Integer id);
	
	public Section getByParentId(Integer id);
	
	public void delSectionByParentId(Integer id);
}
