package org.hr.dao;

import org.hr.pojo.Reinstatement;

public interface ReinstatementDao extends CommonDao<Reinstatement, Integer>{
	public Integer getcontractinfoid(Integer id);
}
