package org.hr.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.hr.pojo.Rolepermission;
 
public interface RolepermissionDao extends CommonDao<Rolepermission,Integer>{

	public List<Integer> getPermissionIdsByRoleId(Integer roleId);
	
	public void deletePermissionsByRoleId(Integer roleId);
	
	public void addRolePermissions(@Param("roleId") Integer roleId,@Param("perIds") List<Integer> perIds);
   
	/**
	 * 获取数据库已有的Type
	 * @return
	 */
	public List<Rolepermission> getPermissionType();
	
	/**
	 * 获取数据库中已有的level
	 * @return
	 */
	public List<Rolepermission> getPermissionLevel();
	
	/**
	 * 根据登录用户的编号查询对应的权限信息
	 * @param userId
	 * @param type
	 * @return
	 */
	public List<Rolepermission> getPermissionsByUserId(@Param("userId") Integer userId,@Param("type") String type);
	
	public List<String> getPermissionCodeByUserId(Integer userId);
	
	public List<String> getMenuCodeByUserId(Integer userId);
	
	public List<Rolepermission> getPermissionByUserId(Integer userId);
	
	public Rolepermission getPermissionByParentId(Integer id);
	
	
	
}
