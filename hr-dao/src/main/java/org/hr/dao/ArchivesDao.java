package org.hr.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.hr.pojo.Archives;
import org.hr.pojo.Hr;

public interface ArchivesDao extends CommonDao<Archives, Integer>{

	public List<Archives> getListbyIds( Integer [] ids);
	
	public List<Archives> getAllBytitlAssessInfoArchivesId();
	
	public List<Hr> groupBysex(@Param("sid")Integer sid,@Param("column") String column);
	public List<Hr> groupByposition(Integer id);
}
