package org.hr.dao;

import org.hr.pojo.Departure;

public interface DepartureDao extends CommonDao<Departure, Integer>{

	public Integer getcontractinfoid(Integer id);
}
