package org.hr.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.hr.pojo.Rewardspunishments; 

public interface RewardspunishmentsDao extends CommonDao<Rewardspunishments, Integer>{
 
	public List<Rewardspunishments> getListByName(@Param("start") int start,@Param("condition")Rewardspunishments Condition,@Param("limit") int limit);
	  
}
